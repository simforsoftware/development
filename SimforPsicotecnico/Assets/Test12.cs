// GENERATED AUTOMATICALLY FROM 'Assets/Test12.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Test12 : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Test12()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Test12"",
    ""maps"": [
        {
            ""name"": ""signals"",
            ""id"": ""d4b022fb-0cec-4587-9532-40a3571c49b8"",
            ""actions"": [
                {
                    ""name"": ""manoIzquierda"",
                    ""type"": ""Button"",
                    ""id"": ""6a521a79-bec8-49fa-86bc-be60610ae25d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""manoDerecha"",
                    ""type"": ""Button"",
                    ""id"": ""b0d14cea-1dbe-493b-bc19-75d7ec209b02"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""pieDerecho"",
                    ""type"": ""Button"",
                    ""id"": ""73bd7c5d-ff25-4778-bf8f-62d834d5f74b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Acelerar"",
                    ""type"": ""Button"",
                    ""id"": ""e10fc528-735c-4217-9a98-cbfb9b109407"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""pieIzquierdo"",
                    ""type"": ""Button"",
                    ""id"": ""6123a120-7cbb-4253-aa15-300101f118fb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""coche1"",
                    ""type"": ""Button"",
                    ""id"": ""3fcfbe05-01dc-40a9-bb05-6a43b6c7d9af"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""coche2"",
                    ""type"": ""Button"",
                    ""id"": ""3282b717-f7ba-4083-9f26-6839c119e0e1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""mando"",
                    ""type"": ""Value"",
                    ""id"": ""bec9a85d-8220-4466-91e6-6be1c348d31b"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""baf09401-84a5-4538-a947-38004ed32f7f"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""manoIzquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""16c181d8-4424-42a2-a77c-b01304feefca"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""manoDerecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eee30719-915f-4cc0-aa23-5eaa31fe5b43"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""pieDerecho"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""828fedd0-ead8-46c9-bde6-5cde347ad771"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""pieIzquierdo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e0e7240-93f2-45bb-82c6-06883b8e15a5"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": ""Press(behavior=2)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""6211a29d-ff75-4a83-a076-a26d713e667e"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""coche1"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""63f4c278-3e9c-4de7-b9bc-0b1fda671433"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""396d2752-5c69-46bb-ad85-9edc6ceb2295"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""65df1f9d-d364-4cd7-86ff-b6551181059a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""941dfc36-4c69-4b81-8925-213bd45a130c"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""3221ce6f-97e5-4595-970c-9d5197c497ad"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""coche2"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""5ea098a2-480d-486a-872d-a4e3c31d5e67"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""010948a9-a249-4ace-ac5f-9edae4c18e86"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""3cd0b632-b129-4134-9334-9123adfb1fc9"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d170bd4b-f5f8-4a99-861d-db4e392d6646"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""coche2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""dd87e73f-f6b6-45ac-a781-b51e51851658"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""New control scheme"",
                    ""action"": ""mando"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""New control scheme"",
            ""bindingGroup"": ""New control scheme"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // signals
        m_signals = asset.FindActionMap("signals", throwIfNotFound: true);
        m_signals_manoIzquierda = m_signals.FindAction("manoIzquierda", throwIfNotFound: true);
        m_signals_manoDerecha = m_signals.FindAction("manoDerecha", throwIfNotFound: true);
        m_signals_pieDerecho = m_signals.FindAction("pieDerecho", throwIfNotFound: true);
        m_signals_Acelerar = m_signals.FindAction("Acelerar", throwIfNotFound: true);
        m_signals_pieIzquierdo = m_signals.FindAction("pieIzquierdo", throwIfNotFound: true);
        m_signals_coche1 = m_signals.FindAction("coche1", throwIfNotFound: true);
        m_signals_coche2 = m_signals.FindAction("coche2", throwIfNotFound: true);
        m_signals_mando = m_signals.FindAction("mando", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // signals
    private readonly InputActionMap m_signals;
    private ISignalsActions m_SignalsActionsCallbackInterface;
    private readonly InputAction m_signals_manoIzquierda;
    private readonly InputAction m_signals_manoDerecha;
    private readonly InputAction m_signals_pieDerecho;
    private readonly InputAction m_signals_Acelerar;
    private readonly InputAction m_signals_pieIzquierdo;
    private readonly InputAction m_signals_coche1;
    private readonly InputAction m_signals_coche2;
    private readonly InputAction m_signals_mando;
    public struct SignalsActions
    {
        private @Test12 m_Wrapper;
        public SignalsActions(@Test12 wrapper) { m_Wrapper = wrapper; }
        public InputAction @manoIzquierda => m_Wrapper.m_signals_manoIzquierda;
        public InputAction @manoDerecha => m_Wrapper.m_signals_manoDerecha;
        public InputAction @pieDerecho => m_Wrapper.m_signals_pieDerecho;
        public InputAction @Acelerar => m_Wrapper.m_signals_Acelerar;
        public InputAction @pieIzquierdo => m_Wrapper.m_signals_pieIzquierdo;
        public InputAction @coche1 => m_Wrapper.m_signals_coche1;
        public InputAction @coche2 => m_Wrapper.m_signals_coche2;
        public InputAction @mando => m_Wrapper.m_signals_mando;
        public InputActionMap Get() { return m_Wrapper.m_signals; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(SignalsActions set) { return set.Get(); }
        public void SetCallbacks(ISignalsActions instance)
        {
            if (m_Wrapper.m_SignalsActionsCallbackInterface != null)
            {
                @manoIzquierda.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnManoIzquierda;
                @manoIzquierda.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnManoIzquierda;
                @manoIzquierda.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnManoIzquierda;
                @manoDerecha.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnManoDerecha;
                @manoDerecha.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnManoDerecha;
                @manoDerecha.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnManoDerecha;
                @pieDerecho.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnPieDerecho;
                @pieDerecho.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnPieDerecho;
                @pieDerecho.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnPieDerecho;
                @Acelerar.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnAcelerar;
                @Acelerar.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnAcelerar;
                @Acelerar.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnAcelerar;
                @pieIzquierdo.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnPieIzquierdo;
                @pieIzquierdo.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnPieIzquierdo;
                @pieIzquierdo.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnPieIzquierdo;
                @coche1.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnCoche1;
                @coche1.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnCoche1;
                @coche1.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnCoche1;
                @coche2.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnCoche2;
                @coche2.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnCoche2;
                @coche2.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnCoche2;
                @mando.started -= m_Wrapper.m_SignalsActionsCallbackInterface.OnMando;
                @mando.performed -= m_Wrapper.m_SignalsActionsCallbackInterface.OnMando;
                @mando.canceled -= m_Wrapper.m_SignalsActionsCallbackInterface.OnMando;
            }
            m_Wrapper.m_SignalsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @manoIzquierda.started += instance.OnManoIzquierda;
                @manoIzquierda.performed += instance.OnManoIzquierda;
                @manoIzquierda.canceled += instance.OnManoIzquierda;
                @manoDerecha.started += instance.OnManoDerecha;
                @manoDerecha.performed += instance.OnManoDerecha;
                @manoDerecha.canceled += instance.OnManoDerecha;
                @pieDerecho.started += instance.OnPieDerecho;
                @pieDerecho.performed += instance.OnPieDerecho;
                @pieDerecho.canceled += instance.OnPieDerecho;
                @Acelerar.started += instance.OnAcelerar;
                @Acelerar.performed += instance.OnAcelerar;
                @Acelerar.canceled += instance.OnAcelerar;
                @pieIzquierdo.started += instance.OnPieIzquierdo;
                @pieIzquierdo.performed += instance.OnPieIzquierdo;
                @pieIzquierdo.canceled += instance.OnPieIzquierdo;
                @coche1.started += instance.OnCoche1;
                @coche1.performed += instance.OnCoche1;
                @coche1.canceled += instance.OnCoche1;
                @coche2.started += instance.OnCoche2;
                @coche2.performed += instance.OnCoche2;
                @coche2.canceled += instance.OnCoche2;
                @mando.started += instance.OnMando;
                @mando.performed += instance.OnMando;
                @mando.canceled += instance.OnMando;
            }
        }
    }
    public SignalsActions @signals => new SignalsActions(this);
    private int m_NewcontrolschemeSchemeIndex = -1;
    public InputControlScheme NewcontrolschemeScheme
    {
        get
        {
            if (m_NewcontrolschemeSchemeIndex == -1) m_NewcontrolschemeSchemeIndex = asset.FindControlSchemeIndex("New control scheme");
            return asset.controlSchemes[m_NewcontrolschemeSchemeIndex];
        }
    }
    public interface ISignalsActions
    {
        void OnManoIzquierda(InputAction.CallbackContext context);
        void OnManoDerecha(InputAction.CallbackContext context);
        void OnPieDerecho(InputAction.CallbackContext context);
        void OnAcelerar(InputAction.CallbackContext context);
        void OnPieIzquierdo(InputAction.CallbackContext context);
        void OnCoche1(InputAction.CallbackContext context);
        void OnCoche2(InputAction.CallbackContext context);
        void OnMando(InputAction.CallbackContext context);
    }
}
