// GENERATED AUTOMATICALLY FROM 'Assets/inputs port/port.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Port : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Port()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""port"",
    ""maps"": [
        {
            ""name"": ""volante"",
            ""id"": ""84fac466-220c-4e05-9b1d-ab3edbb29ad9"",
            ""actions"": [
                {
                    ""name"": ""marchas up"",
                    ""type"": ""Button"",
                    ""id"": ""6715bad7-d606-4153-a3ff-9c72b5aba3e9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""marchas down"",
                    ""type"": ""Button"",
                    ""id"": ""10667b45-9ef1-49ca-a79b-4d01f69fcd7c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""freno mano"",
                    ""type"": ""Button"",
                    ""id"": ""6ee1d789-8f22-42ee-86d9-269ae7110152"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""arranque"",
                    ""type"": ""Button"",
                    ""id"": ""6553d30e-3c0b-4897-9918-85b55cb32f33"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""acelerar"",
                    ""type"": ""Button"",
                    ""id"": ""1fc2ba45-fc4f-4584-8613-fb019d9994f4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""frenar"",
                    ""type"": ""Button"",
                    ""id"": ""7d73a040-5ba2-4be8-a5a7-410f05d66746"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""derecha"",
                    ""type"": ""Button"",
                    ""id"": ""eeecfd27-1f22-45cd-bac1-01a432a11a2b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""izquierda"",
                    ""type"": ""Button"",
                    ""id"": ""23880cd3-96d5-46f7-867d-6307075b3895"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""85f8d478-9cd3-4743-ba1e-ae27f82dc20d"",
                    ""path"": ""<HID::TW6 Wheel>/button6"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""marchas up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3272e2d2-4df0-4f74-bde6-80d23a2f41e4"",
                    ""path"": ""<HID::TW6 Wheel>/button5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""marchas down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""69e1fd05-26ce-4b30-bc95-8494b8eb1b8e"",
                    ""path"": ""<HID::TW6 Wheel>/button8"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""freno mano"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b08d6d64-5fc1-4552-b0c8-f6034d5e815d"",
                    ""path"": ""<HID::TW6 Wheel>/button7"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""arranque"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8a2947b4-8672-48f2-ad6e-1e6b5207a3fb"",
                    ""path"": ""<HID::TW6 Wheel>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dd764eac-17a3-4102-855f-50e853356f4d"",
                    ""path"": ""<HID::44F B66E>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""acelerar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eee7ae22-c1d4-40e9-9279-9574a00c7de7"",
                    ""path"": ""<HID::TW6 Wheel>/stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""frenar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""063dfff4-e9ef-481e-afa9-8951e3c4a3ce"",
                    ""path"": ""<HID::44F B66E>/stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""frenar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a9214191-c8de-40dc-aa3f-3fc93fc13cf1"",
                    ""path"": ""<HID::TW6 Wheel>/stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ec52f9ab-572d-4d68-8997-047b994f5d5d"",
                    ""path"": ""<HID::44F B66E>/stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4d65bfbf-6d82-4a5f-8c3c-97a4103114a5"",
                    ""path"": ""<HID::44F B66E>/button2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""77e93fdf-2864-4a33-9c2f-0eb3ad9ce0d7"",
                    ""path"": ""<HID::TW6 Wheel>/stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""687a24ba-521b-47cc-a5f5-c2de1c091a95"",
                    ""path"": ""<HID::44F B66E>/stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c7d3923c-de0b-4642-b898-854833271f57"",
                    ""path"": ""<HID::44F B66E>/trigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""MyControll"",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""MyControll"",
            ""bindingGroup"": ""MyControll"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<XRInputV1::EyeTrackingOpenXR>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<HID::TW6 Wheel>"",
                    ""isOptional"": true,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<HID::44F B66E>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // volante
        m_volante = asset.FindActionMap("volante", throwIfNotFound: true);
        m_volante_marchasup = m_volante.FindAction("marchas up", throwIfNotFound: true);
        m_volante_marchasdown = m_volante.FindAction("marchas down", throwIfNotFound: true);
        m_volante_frenomano = m_volante.FindAction("freno mano", throwIfNotFound: true);
        m_volante_arranque = m_volante.FindAction("arranque", throwIfNotFound: true);
        m_volante_acelerar = m_volante.FindAction("acelerar", throwIfNotFound: true);
        m_volante_frenar = m_volante.FindAction("frenar", throwIfNotFound: true);
        m_volante_derecha = m_volante.FindAction("derecha", throwIfNotFound: true);
        m_volante_izquierda = m_volante.FindAction("izquierda", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // volante
    private readonly InputActionMap m_volante;
    private IVolanteActions m_VolanteActionsCallbackInterface;
    private readonly InputAction m_volante_marchasup;
    private readonly InputAction m_volante_marchasdown;
    private readonly InputAction m_volante_frenomano;
    private readonly InputAction m_volante_arranque;
    private readonly InputAction m_volante_acelerar;
    private readonly InputAction m_volante_frenar;
    private readonly InputAction m_volante_derecha;
    private readonly InputAction m_volante_izquierda;
    public struct VolanteActions
    {
        private @Port m_Wrapper;
        public VolanteActions(@Port wrapper) { m_Wrapper = wrapper; }
        public InputAction @marchasup => m_Wrapper.m_volante_marchasup;
        public InputAction @marchasdown => m_Wrapper.m_volante_marchasdown;
        public InputAction @frenomano => m_Wrapper.m_volante_frenomano;
        public InputAction @arranque => m_Wrapper.m_volante_arranque;
        public InputAction @acelerar => m_Wrapper.m_volante_acelerar;
        public InputAction @frenar => m_Wrapper.m_volante_frenar;
        public InputAction @derecha => m_Wrapper.m_volante_derecha;
        public InputAction @izquierda => m_Wrapper.m_volante_izquierda;
        public InputActionMap Get() { return m_Wrapper.m_volante; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(VolanteActions set) { return set.Get(); }
        public void SetCallbacks(IVolanteActions instance)
        {
            if (m_Wrapper.m_VolanteActionsCallbackInterface != null)
            {
                @marchasup.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnMarchasup;
                @marchasup.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnMarchasup;
                @marchasup.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnMarchasup;
                @marchasdown.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnMarchasdown;
                @marchasdown.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnMarchasdown;
                @marchasdown.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnMarchasdown;
                @frenomano.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnFrenomano;
                @frenomano.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnFrenomano;
                @frenomano.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnFrenomano;
                @arranque.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnArranque;
                @arranque.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnArranque;
                @arranque.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnArranque;
                @acelerar.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnAcelerar;
                @acelerar.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnAcelerar;
                @acelerar.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnAcelerar;
                @frenar.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnFrenar;
                @frenar.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnFrenar;
                @frenar.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnFrenar;
                @derecha.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnDerecha;
                @derecha.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnDerecha;
                @derecha.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnDerecha;
                @izquierda.started -= m_Wrapper.m_VolanteActionsCallbackInterface.OnIzquierda;
                @izquierda.performed -= m_Wrapper.m_VolanteActionsCallbackInterface.OnIzquierda;
                @izquierda.canceled -= m_Wrapper.m_VolanteActionsCallbackInterface.OnIzquierda;
            }
            m_Wrapper.m_VolanteActionsCallbackInterface = instance;
            if (instance != null)
            {
                @marchasup.started += instance.OnMarchasup;
                @marchasup.performed += instance.OnMarchasup;
                @marchasup.canceled += instance.OnMarchasup;
                @marchasdown.started += instance.OnMarchasdown;
                @marchasdown.performed += instance.OnMarchasdown;
                @marchasdown.canceled += instance.OnMarchasdown;
                @frenomano.started += instance.OnFrenomano;
                @frenomano.performed += instance.OnFrenomano;
                @frenomano.canceled += instance.OnFrenomano;
                @arranque.started += instance.OnArranque;
                @arranque.performed += instance.OnArranque;
                @arranque.canceled += instance.OnArranque;
                @acelerar.started += instance.OnAcelerar;
                @acelerar.performed += instance.OnAcelerar;
                @acelerar.canceled += instance.OnAcelerar;
                @frenar.started += instance.OnFrenar;
                @frenar.performed += instance.OnFrenar;
                @frenar.canceled += instance.OnFrenar;
                @derecha.started += instance.OnDerecha;
                @derecha.performed += instance.OnDerecha;
                @derecha.canceled += instance.OnDerecha;
                @izquierda.started += instance.OnIzquierda;
                @izquierda.performed += instance.OnIzquierda;
                @izquierda.canceled += instance.OnIzquierda;
            }
        }
    }
    public VolanteActions @volante => new VolanteActions(this);
    private int m_MyControllSchemeIndex = -1;
    public InputControlScheme MyControllScheme
    {
        get
        {
            if (m_MyControllSchemeIndex == -1) m_MyControllSchemeIndex = asset.FindControlSchemeIndex("MyControll");
            return asset.controlSchemes[m_MyControllSchemeIndex];
        }
    }
    public interface IVolanteActions
    {
        void OnMarchasup(InputAction.CallbackContext context);
        void OnMarchasdown(InputAction.CallbackContext context);
        void OnFrenomano(InputAction.CallbackContext context);
        void OnArranque(InputAction.CallbackContext context);
        void OnAcelerar(InputAction.CallbackContext context);
        void OnFrenar(InputAction.CallbackContext context);
        void OnDerecha(InputAction.CallbackContext context);
        void OnIzquierda(InputAction.CallbackContext context);
    }
}
