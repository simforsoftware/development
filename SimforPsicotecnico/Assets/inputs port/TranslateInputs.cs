using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;
using UnityEngine.InputSystem;

public class TranslateInputs : MonoBehaviour
{
    InputSimulator IS;

    Port inputAction;

    int engine;

    private void Awake()
    {
        IS = new InputSimulator();

        inputAction = new Port();

        inputAction.volante.arranque.started += SimulateEngine;
        inputAction.volante.arranque.canceled += SimulateEngineUp;
        inputAction.volante.arranque.Enable();

        inputAction.volante.marchasup.started += SimulateNeutral;
        inputAction.volante.marchasup.Enable();

        inputAction.volante.marchasdown.started += SimulateReverse;
        inputAction.volante.marchasdown.Enable();

        inputAction.volante.frenomano.started += SimulateHandBrakeOn;
        inputAction.volante.frenomano.canceled += SimulateHandBrakeOf;
        inputAction.volante.frenomano.Enable();


        inputAction.volante.acelerar.started += SimulateUp;
        inputAction.volante.acelerar.canceled += SimulateUpEnd;
        inputAction.volante.acelerar.Enable();

        inputAction.volante.frenar.started += SimulateDown;
        inputAction.volante.frenar.canceled += SimulateDownEnd;
        inputAction.volante.frenar.Enable();

        inputAction.volante.derecha.started += SimulateRight;
        inputAction.volante.derecha.canceled += SimulateRightEnd;
        inputAction.volante.derecha.Enable();

        inputAction.volante.izquierda.started += SimulateLeft;
        inputAction.volante.izquierda.canceled += SimulateLeftEnd;
        inputAction.volante.izquierda.Enable();
    }
    public void SimulateEngine(InputAction.CallbackContext obj)
    {
        print("engine: " + engine);
        if (engine <= 0)
        {
            IS.Keyboard.KeyDown(VirtualKeyCode.VK_K);
            engine++;
        }
        else
        {
            IS.Keyboard.ModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_K);
            engine = -1;
        }
    }
    public void SimulateEngineUp(InputAction.CallbackContext obj)
    {
            IS.Keyboard.KeyUp(VirtualKeyCode.VK_K);
    }
    public void SimulateHandBrakeOn(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyDown(VirtualKeyCode.SPACE);
    }
    public void SimulateHandBrakeOf(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyUp(VirtualKeyCode.SPACE);
    }
    public void SimulateNeutral(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyPress(VirtualKeyCode.VK_N);
    }
    public void SimulateReverse(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyPress(VirtualKeyCode.VK_R);
    }


    //Simulacion Controles Basicos
    public void SimulateUp(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyDown(VirtualKeyCode.VK_W);
    }
    public void SimulateDown(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyDown(VirtualKeyCode.VK_S);
    }
    public void SimulateRight(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyDown(VirtualKeyCode.VK_D);
    }
    public void SimulateLeft(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyDown(VirtualKeyCode.VK_A);
    }

    public void SimulateUpEnd(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyUp(VirtualKeyCode.VK_W);
    }
    public void SimulateDownEnd(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyUp(VirtualKeyCode.VK_S);
    }
    public void SimulateRightEnd(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyUp(VirtualKeyCode.VK_D);
    }
    public void SimulateLeftEnd(InputAction.CallbackContext obj)
    {
        IS.Keyboard.KeyUp(VirtualKeyCode.VK_A);
    }
}
