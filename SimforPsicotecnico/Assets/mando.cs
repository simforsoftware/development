// GENERATED AUTOMATICALLY FROM 'Assets/mando.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Mando : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Mando()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""mando"",
    ""maps"": [
        {
            ""name"": ""coche"",
            ""id"": ""466b7ced-0de8-42a6-9c0d-bca858f378b6"",
            ""actions"": [
                {
                    ""name"": ""steering"",
                    ""type"": ""Button"",
                    ""id"": ""46843fdb-d859-4ab0-81d3-2a0a6b3b9e6a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""acelerador"",
                    ""type"": ""Button"",
                    ""id"": ""e152adf7-8b2d-4547-b7be-7644c451ebbe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""freno"",
                    ""type"": ""Button"",
                    ""id"": ""d3cffcfd-ae97-41f4-acfe-379ad724a3c1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""TrueAcelerador"",
                    ""type"": ""Button"",
                    ""id"": ""da5b7ee6-51df-4554-bc34-af1b41fc328c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""a"",
                    ""type"": ""Button"",
                    ""id"": ""7fa1ef58-03c4-4ce4-9399-070556579fa9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""b"",
                    ""type"": ""Button"",
                    ""id"": ""6d61db3b-6dc5-4199-8555-d66c290a2123"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""c"",
                    ""type"": ""Button"",
                    ""id"": ""d4d1537f-7445-4a56-890f-4f25506bce49"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(pressPoint=0.01)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""75661fcd-0acf-45be-ab83-1bfe56a92d88"",
                    ""path"": ""<HID::TW6 Wheel>/stick"",
                    ""interactions"": ""Press(pressPoint=0.001)"",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""steering"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8134c8e5-e34d-4ca9-8b66-78a377d5a782"",
                    ""path"": ""<HID::TW6 Wheel>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""acelerador"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d5ca0322-9b42-4cde-855d-6e5914afd860"",
                    ""path"": ""<Joystick>/stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""freno"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8808a6bd-f4a2-430a-b6e7-346c1a86d506"",
                    ""path"": ""<HID::TW6 Wheel>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""TrueAcelerador"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ffdf2e2-2c9d-4d7f-9e4c-c9ab4d9f0ef0"",
                    ""path"": ""<HID::TW6 Wheel>/button7"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""a"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8b0008ab-0dcb-4f34-9cfc-be14980ae78d"",
                    ""path"": ""<HID::TW6 Wheel>/button5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""a"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""691051eb-3a9e-4249-89c4-145223cb4b1d"",
                    ""path"": ""<HID::TW6 Wheel>/button8"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""b"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""87e9d23e-c7fd-4d6c-ba9e-5f75f9954ce2"",
                    ""path"": ""<HID::TW6 Wheel>/button6"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""b"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""0e3b824c-ba57-4fb6-9801-1652aee80be6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""c"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""0cf85019-42e0-4686-a5bf-7a01cb6dab95"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""c"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""14e477b1-8275-42fd-98d1-57d51b07e0d6"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""c"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""07a7dff1-aafe-46fe-ad3c-def89d3d126e"",
                    ""path"": ""<HID::TW6 Wheel>/stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""c"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""6b808165-0cc1-4369-8750-2152a5506372"",
                    ""path"": ""<HID::TW6 Wheel>/stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""mando"",
                    ""action"": ""c"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""mando"",
            ""bindingGroup"": ""mando"",
            ""devices"": [
                {
                    ""devicePath"": ""<HID::TW6 Wheel>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // coche
        m_coche = asset.FindActionMap("coche", throwIfNotFound: true);
        m_coche_steering = m_coche.FindAction("steering", throwIfNotFound: true);
        m_coche_acelerador = m_coche.FindAction("acelerador", throwIfNotFound: true);
        m_coche_freno = m_coche.FindAction("freno", throwIfNotFound: true);
        m_coche_TrueAcelerador = m_coche.FindAction("TrueAcelerador", throwIfNotFound: true);
        m_coche_a = m_coche.FindAction("a", throwIfNotFound: true);
        m_coche_b = m_coche.FindAction("b", throwIfNotFound: true);
        m_coche_c = m_coche.FindAction("c", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // coche
    private readonly InputActionMap m_coche;
    private ICocheActions m_CocheActionsCallbackInterface;
    private readonly InputAction m_coche_steering;
    private readonly InputAction m_coche_acelerador;
    private readonly InputAction m_coche_freno;
    private readonly InputAction m_coche_TrueAcelerador;
    private readonly InputAction m_coche_a;
    private readonly InputAction m_coche_b;
    private readonly InputAction m_coche_c;
    public struct CocheActions
    {
        private @Mando m_Wrapper;
        public CocheActions(@Mando wrapper) { m_Wrapper = wrapper; }
        public InputAction @steering => m_Wrapper.m_coche_steering;
        public InputAction @acelerador => m_Wrapper.m_coche_acelerador;
        public InputAction @freno => m_Wrapper.m_coche_freno;
        public InputAction @TrueAcelerador => m_Wrapper.m_coche_TrueAcelerador;
        public InputAction @a => m_Wrapper.m_coche_a;
        public InputAction @b => m_Wrapper.m_coche_b;
        public InputAction @c => m_Wrapper.m_coche_c;
        public InputActionMap Get() { return m_Wrapper.m_coche; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CocheActions set) { return set.Get(); }
        public void SetCallbacks(ICocheActions instance)
        {
            if (m_Wrapper.m_CocheActionsCallbackInterface != null)
            {
                @steering.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnSteering;
                @steering.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnSteering;
                @steering.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnSteering;
                @acelerador.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnAcelerador;
                @acelerador.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnAcelerador;
                @acelerador.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnAcelerador;
                @freno.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnFreno;
                @freno.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnFreno;
                @freno.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnFreno;
                @TrueAcelerador.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnTrueAcelerador;
                @TrueAcelerador.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnTrueAcelerador;
                @TrueAcelerador.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnTrueAcelerador;
                @a.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnA;
                @a.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnA;
                @a.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnA;
                @b.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnB;
                @b.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnB;
                @b.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnB;
                @c.started -= m_Wrapper.m_CocheActionsCallbackInterface.OnC;
                @c.performed -= m_Wrapper.m_CocheActionsCallbackInterface.OnC;
                @c.canceled -= m_Wrapper.m_CocheActionsCallbackInterface.OnC;
            }
            m_Wrapper.m_CocheActionsCallbackInterface = instance;
            if (instance != null)
            {
                @steering.started += instance.OnSteering;
                @steering.performed += instance.OnSteering;
                @steering.canceled += instance.OnSteering;
                @acelerador.started += instance.OnAcelerador;
                @acelerador.performed += instance.OnAcelerador;
                @acelerador.canceled += instance.OnAcelerador;
                @freno.started += instance.OnFreno;
                @freno.performed += instance.OnFreno;
                @freno.canceled += instance.OnFreno;
                @TrueAcelerador.started += instance.OnTrueAcelerador;
                @TrueAcelerador.performed += instance.OnTrueAcelerador;
                @TrueAcelerador.canceled += instance.OnTrueAcelerador;
                @a.started += instance.OnA;
                @a.performed += instance.OnA;
                @a.canceled += instance.OnA;
                @b.started += instance.OnB;
                @b.performed += instance.OnB;
                @b.canceled += instance.OnB;
                @c.started += instance.OnC;
                @c.performed += instance.OnC;
                @c.canceled += instance.OnC;
            }
        }
    }
    public CocheActions @coche => new CocheActions(this);
    private int m_mandoSchemeIndex = -1;
    public InputControlScheme mandoScheme
    {
        get
        {
            if (m_mandoSchemeIndex == -1) m_mandoSchemeIndex = asset.FindControlSchemeIndex("mando");
            return asset.controlSchemes[m_mandoSchemeIndex];
        }
    }
    public interface ICocheActions
    {
        void OnSteering(InputAction.CallbackContext context);
        void OnAcelerador(InputAction.CallbackContext context);
        void OnFreno(InputAction.CallbackContext context);
        void OnTrueAcelerador(InputAction.CallbackContext context);
        void OnA(InputAction.CallbackContext context);
        void OnB(InputAction.CallbackContext context);
        void OnC(InputAction.CallbackContext context);
    }
}
