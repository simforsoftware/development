using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNSimulacion3 : ManagerNSimulacion
{
    public static ManagerNSimulacion3 instance;
    ResultNuevaSimulacion1 resultados;
    private void Awake()
    {
        print(this.gameObject);
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }

    [SerializeField]  float timepoAmbar, tiempoRojo;
    public float[] rangoTiemposVerde = new float[2];

    public bool verde = true;
    bool readyFreno = false;
    float timerS = 0, timerReaccion = 0;
    List<float> reactiontimes = new List<float>();
    [SerializeField] float minTiempoReaccion, maxFallos;


    protected override void StartTest()
    {
        InfoNuevaSimulacion3 info2 = (InfoNuevaSimulacion3)info;
        resultados = new ResultNuevaSimulacion1();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        base.StartTest();
        if (info)
        {
            duracion_ejercicio = info2.duracion;
            timepoAmbar = info2.tiempoAmbar;
            tiempoRojo = info2.tiempoRojo;
            rangoTiemposVerde[0] = info2.rangoTiemposVerde[0];
            rangoTiemposVerde[1] = info2.rangoTiemposVerde[1];
            minTiempoReaccion = info2.minTiempoReaccion;
            maxFallos = info2.maxFallos;
        }
        timerS = Random.Range(rangoTiemposVerde[0], rangoTiemposVerde[1]);
        EventManager.StartListening("Freno", FrenoCoche);
    }
    protected override void Update()
    {
        timerS -= Time.deltaTime;
        timerReaccion += Time.deltaTime;
        if (verde)
        {
            if(timerS <= 0)
            {
                EventManager.TriggerEvent("SemaforosAmarillo");
                timerS = timepoAmbar;
                verde = !verde;
                readyFreno = true;
                recorder.SaveEvent();
                timerReaccion = 0;
            }
        }
        else if(timerS > 0)
        {
            if (timerS <= 0) EventManager.TriggerEvent("SemaforosRojo");
        }
        else 
        {
            if(coche.GetVelocidad() <= 0 && timerS < -tiempoRojo)
            {
                EventManager.TriggerEvent("SemaforosVerde");
                timerS = Random.Range(rangoTiemposVerde[0], rangoTiemposVerde[1]);
                verde = !verde;
            }
        }
        base.Update();
    }

    public void FrenoCoche()
    {
        if (readyFreno)
        {
            reactiontimes.Add(timerReaccion);
            readyFreno = false;
        }
        else
        {
            ++fallos;
        }
        print("tiempoReaccion: " + timerReaccion);
        print(reactiontimes.Count + " // " + fallos);
    }

    public override void End()
    {
        
        resultados.time = duracion_ejercicio;
        float reaccion_media = 0;
        for (int i = 0; i < reactiontimes.Count; ++i)
        {
            reaccion_media += reactiontimes[i];
        }
        reaccion_media = reaccion_media / reactiontimes.Count;
        resultados.reactionTime = reaccion_media;
        resultados.aciertos = aciertos;
        resultados.fallos = fallos;
        resultados.minutos = (int)(duracion_ejercicio / 60);
        if (duracion_ejercicio % 60 != 0) ++resultados.minutos;
        AuxiliarBuild.instance.tipo.Add(14);
        AuxiliarBuild.instance.resultados.Add(resultados);
        //Esto tendria que cambiar al upload tipico
        resultados.apto = true;
        if (reaccion_media > minTiempoReaccion) resultados.apto = false;
        if (fallos > maxFallos * fallos) resultados.apto = false;

        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        if (recorder.cognitiveLoadRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, coloresGraficas.cognitiveLoad));
        if (recorder.heartRateRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, coloresGraficas.hearthRate));
        if (recorder.pupilDilatationRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, coloresGraficas.pupilDilatation));
        if (recorder.timeEvents.Count > 0) resultados.eventos.Add(new Grafics.eventData(recorder.timeEvents, coloresGraficas.events));

        resultados.UploadAlumno();
        resultados.FSubirSession();

        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}
