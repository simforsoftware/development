using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaforo3 : MonoBehaviour
{
    [SerializeField] GameObject redLights, greenLights, yellowLights;
    [SerializeField] GameObject fallo, acierto;



    private void Start()
    {
        EventManager.StartListening("SemaforosVerde", SetVerde);
        EventManager.StartListening("SemaforosRojo", SetRojo);
        EventManager.StartListening("SemaforosAmarillo", SetYellow);

        if (ManagerNSimulacion3.instance.verde) SetVerde();
        else SetRojo();
    }
   

    public void SetYellow()
    {
        greenLights.SetActive(false);
        yellowLights.SetActive(true);
        Invoke("SetRojo", 1f);
    }
    public void SetRojo()
    {
        redLights.SetActive(true);
        yellowLights.SetActive(false);
        fallo.SetActive(true);
        acierto.SetActive(false);
    }
    public void SetVerde()
    {
        redLights.SetActive(false);
        greenLights.SetActive(true);
        fallo.SetActive(false);
        acierto.SetActive(true);
    }
}
