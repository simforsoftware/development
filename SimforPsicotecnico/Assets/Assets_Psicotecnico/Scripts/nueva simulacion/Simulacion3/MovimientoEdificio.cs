using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEdificio : MonoBehaviour
{
    Vector3 relativPostion;


    [SerializeField] GameObject ancla;




    private void Start()
    {
        if (ancla)
        {
            relativPostion = ancla.transform.position - this.transform.position;
        }
    }

    private void Update()
    {
        if (ancla)
        {
            this.transform.position = ancla.transform.position - relativPostion;
        }
    }
}
