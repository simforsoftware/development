using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreaObstaculos3 : MonoBehaviour
{
    public static CreaObstaculos3 instance;


    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);
    }


    [SerializeField] int[] nodos_por_obstaculo = new int[2];
    int distanciaProximoObdstaculo = 0;

    nodoCarretera nodoActual;


    [SerializeField] GameObject obstaculoParar;



    public void CrearObstaculos()
    {
        if (!nodoActual)
        {
            distanciaProximoObdstaculo = Random.Range(nodos_por_obstaculo[0], nodos_por_obstaculo[1]);
            nodoActual = CreaMapas.instance.currentTiles[0].primerNodo;
        }

        --distanciaProximoObdstaculo;
        nodoActual = nodoActual.nextNodo;


        if (distanciaProximoObdstaculo == 0)
        {
            GameObject obstaculo = Instantiate(obstaculoParar, nodoActual.transform);
            distanciaProximoObdstaculo = Random.Range(nodos_por_obstaculo[0], nodos_por_obstaculo[1]);
        }


        if (nodoActual.nextNodo)
        {
            CrearObstaculos();
        }
    }
}
