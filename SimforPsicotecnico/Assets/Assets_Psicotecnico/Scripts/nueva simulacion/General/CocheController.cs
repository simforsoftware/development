using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocheController : MonoBehaviour
{
    public static CocheController instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }

    nodoCarretera nodoActual;
    [SerializeField] float Velocidad_De_Giro;

    GameObject objetivo;

    [SerializeField] float velocidadMax_ms, aceleracion_ms;
    [SerializeField] bool falloAlFrenar;
    [SerializeField] GameObject modelo;
    [SerializeField] GameObject giroscopio;
    float velocidad;
    int carril;
    float desfase;
    bool unFallo;
    [SerializeField] bool autoAcelerar;
    [SerializeField] bool sinFreno;
    [SerializeField] AudioSource engineSound;
    [SerializeField] float maxPitch, minPitch;
    [SerializeField] Animator dummyAnimator, volanteAnimator;
    [SerializeField] GameObject coche1;

    public bool activo = true;

    bool tercera;


    public float GetVelocidad()
    {
        return velocidad;
    }
    public void SetVelocidadMaxima(float v)
    {
        velocidadMax_ms = v;
    }
    public float GetVelocidadMaxima()
    {
        return velocidadMax_ms;
    }

    


    bool acelerando;

    float distanciaCambioNodo;
    float tiempo_de_anticipacion;
    bool anticipado;

    TileInfo actualTile;

    private void Start()
    {
        distanciaCambioNodo = velocidadMax_ms / 3;
        unFallo = true;
    }

    private void Update()
    {
        if(!nodoActual || !activo) return;
        Quaternion rotationObjetivo = Quaternion.LookRotation(objetivo.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotationObjetivo, Velocidad_De_Giro);
        if (!sinFreno && (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)))
        {
            velocidad -= aceleracion_ms * 5 * Time.deltaTime;
            velocidad = Mathf.Max(velocidad, 0);
        }
        else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || autoAcelerar)
        {
            velocidad += aceleracion_ms * Time.deltaTime;
            velocidad = Mathf.Min(velocidad, velocidadMax_ms);
            unFallo = true;
        }
        else
        {
            velocidad -= aceleracion_ms * 0.5f * Time.deltaTime;
            velocidad = Mathf.Max(velocidad, 0);
        }
        transform.position += transform.forward * velocidad * Time.deltaTime;

        if (!acelerando && (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)))
        {
            acelerando = true;
        }
        if (unFallo && ((acelerando && (!autoAcelerar && (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W))) || (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S)))))
        {

            unFallo = false;
            EventManager.TriggerEvent("Freno");
            acelerando = false;
        }

        float lateralSpeed = 0;


        if (Mathf.Abs(modelo.transform.localPosition.x - desfase) > 0.2f)
        {
            lateralSpeed = velocidad / 3f;
            if (modelo.transform.localPosition.x > desfase) lateralSpeed *= -1;
            modelo.transform.localPosition += Vector3.right * lateralSpeed * Time.deltaTime;
            
        }
        else
        {
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                TurnRight();
            }

            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                TurnLeft();
            }
        }


        if (Mathf.Abs(giroscopio.transform.localPosition.x - desfase) > 0.2f)
        {
            lateralSpeed = velocidad / 2f;
            if (giroscopio.transform.localPosition.x > desfase) lateralSpeed *= -1;
            giroscopio.transform.localPosition += Vector3.right * lateralSpeed * Time.deltaTime;
        }

        dummyAnimator.SetFloat("turn", lateralSpeed);
        volanteAnimator.SetFloat("turn", lateralSpeed);

        modelo.transform.rotation = Quaternion.LookRotation(giroscopio.transform.position - modelo.transform.position);

        

        if (Mathf.Abs((objetivo.transform.position - transform.position).sqrMagnitude) <= distanciaCambioNodo * distanciaCambioNodo)
        {
            NextObjetive();
        }

        SetPitch();
    }

    void SetPitch()
    {
        engineSound.pitch = ((velocidad * (maxPitch - minPitch)) / velocidadMax_ms) + minPitch;
        if (tercera) engineSound.pitch += 0.1f;
    }

    public void ChangeEngineSound(bool tercera)
    {
        this.tercera = tercera;
        SetPitch();
        engineSound.Play();
    }

    void NextObjetive()
    {
        nodoActual = nodoActual.nextNodo;
        UpdateObjetive(nodoActual.gameObject);
    }

    public void SetNodo(nodoCarretera nodo)
    {
        nodoActual = nodo;
        actualTile = nodoActual.tile;
        objetivo = nodo.gameObject;
        carril = nodo.carriles.Count / 2;
        desfase = nodo.GetDesfase(carril);
        if (Tripode.instance) Tripode.instance.SetFirstNodo(nodo.gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10);
    }

    void TurnRight()
    {
        if (nodoActual.carriles.Count <= carril + 1) return;
        EventManager.TriggerEvent("TurnRight");
        carril++;
        desfase = nodoActual.GetDesfase(carril);
    }

    void TurnLeft()
    {
        if (carril == 0) return;
        EventManager.TriggerEvent("TurnLeft");
        carril--;
        desfase = nodoActual.GetDesfase(carril);
    }

    void UpdateObjetive(GameObject g)
    {
       
        objetivo = g;
        if(nodoActual.tile != actualTile)
        {
            actualTile = nodoActual.tile;
            CreaMapas.instance.CrearTile();
        }
    }
}
