using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class ControladorCamara : MonoBehaviour
{
    public bool terceraPersona;

    [SerializeField] GameObject coche1, coche3, posCam1, posCam3;

    [SerializeField] bool soloTercera;

    [SerializeField] CocheController car;

    Vector3 originPos1, originPos3;

    bool fixed1, fixed3;

    public bool bloqueada;

    public void StartTest()
    {
        if(!soloTercera) originPos1 = posCam1.transform.localPosition;
        originPos3 = posCam3.transform.localPosition;
        SetCamara(terceraPersona);
    }
    private void Update()
    {
        if (bloqueada) return;
        if (Input.GetKeyDown(KeyCode.C))
        {
            SwitchCamera();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (!soloTercera) posCam1.transform.localPosition = originPos1;
            posCam3.transform.localPosition = originPos3;
            fixed1 = fixed3 = false;
            SetCamara(terceraPersona);
        }
    }

    void SwitchCamera()
    {
        terceraPersona = !terceraPersona;
        if (soloTercera) terceraPersona = true;
        car.ChangeEngineSound(terceraPersona);
        SetCamara(terceraPersona);
    }

    void SetCamara(bool tercera)
    {
        if (!tercera)
        {
            coche3.SetActive(false);
            coche1.SetActive(true);
            transform.parent = posCam1.transform;
            transform.localPosition = Vector3.zero;
            transform.rotation = Quaternion.LookRotation(posCam1.transform.forward);
            if (!fixed1)
            {
                Vector3 desviacion = posCam1.transform.position - Camera.main.transform.position;
                posCam1.transform.position += desviacion;
                fixed1 = true;
                SetCamara(tercera);
            }
        }
        else
        {
            if(coche1) coche1.SetActive(false);
            coche3.SetActive(true);
            transform.parent = posCam3.transform;
            transform.localPosition = Vector3.zero;
            transform.rotation = Quaternion.LookRotation(posCam3.transform.forward);
            if (!fixed3)
            {
                Vector3 desviacion = posCam3.transform.position - Camera.main.transform.position;
                posCam3.transform.position += desviacion;
                fixed3 = true;
                SetCamara(tercera);
            }
        }
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevices(devices);
        if (devices.Count != 0) devices[0].subsystem.TryRecenter();

       if(car) car.ChangeEngineSound(terceraPersona);

    }


}
