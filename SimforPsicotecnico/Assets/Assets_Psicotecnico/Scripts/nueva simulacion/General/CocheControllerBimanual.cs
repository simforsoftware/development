using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocheControllerBimanual : MonoBehaviour
{
    [SerializeField] GameObject cocheDerecha, cocheIzquierda;

    ManagerNSimulacion5 manager;

    public TileBimodal tileActual;

    public nodoCarretera nodoDerecha, nodoIzquierda;

    [SerializeField] float speed, lateralSpeed, limiteLAteral;

    bool derechaFuera, izquierdaFuera, audioFuera;
    float timerFueraDerecha, timerFueraIzquierda;

    public List<float> errores = new List<float>();

    float zrefD, zrefI;

    AudioSource audio;

    public bool activo = true;

    private void Start()
    {
        zrefD = cocheDerecha.transform.position.z;
        zrefI = cocheIzquierda.transform.position.z;
        audio = GetComponent<AudioSource>();
    }

    public void SetManager(ManagerNSimulacion5 newManager)
    {
        manager = newManager;
    }

    void SetInitialNodo()
    {
        nodoDerecha = tileActual.primerNodo2;
        nodoIzquierda = tileActual.primerNodo;
    }

    private void Update()
    {
        if (!activo) return;
        if (!nodoDerecha)
        {
            SetInitialNodo();
        }

        transform.position += transform.forward * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.RightArrow) && cocheDerecha.transform.position.z < zrefD + limiteLAteral)
        {
            cocheDerecha.transform.position += transform.right * lateralSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && cocheDerecha.transform.position.z > zrefD - limiteLAteral)
        {
            cocheDerecha.transform.position -= transform.right * lateralSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D) && cocheIzquierda.transform.position.z < zrefI + limiteLAteral)
        {
            cocheIzquierda.transform.position += transform.right * lateralSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A) && cocheIzquierda.transform.position.z > zrefI - limiteLAteral)
        {
            cocheIzquierda.transform.position -= transform.right * lateralSpeed * Time.deltaTime;
        }
        if(transform.position.x - nodoDerecha.transform.position.x < 0.1)
        {
            if (tileActual != nodoDerecha.nextNodo.tile)
            {
                tileActual = tileActual.nextTile.gameObject.GetComponent<TileBimodal>();
                CreaMapas.instance.CrearTile();
            }

            nodoDerecha = nodoDerecha.nextNodo;
            nodoIzquierda = nodoIzquierda.nextNodo;
        }

        timerFueraDerecha += Time.deltaTime;
        timerFueraIzquierda += Time.deltaTime;

        RaycastHit derechaHit, izquierdaHit;
        Physics.Raycast(cocheDerecha.transform.position, Vector3.down, out derechaHit);
        Physics.Raycast(cocheIzquierda.transform.position, Vector3.down, out izquierdaHit);

        if(derechaHit.transform && izquierdaHit.transform)
        {
            if (!derechaFuera && derechaHit.transform.gameObject.name == "Plataforma")
            {
                derechaFuera = true;
                timerFueraDerecha = 0;
            }
            if (!izquierdaFuera && izquierdaHit.transform.gameObject.name == "Plataforma")
            {
                izquierdaFuera = true;
                timerFueraIzquierda = 0;
            }
            if (derechaFuera && derechaHit.transform.gameObject.name != "Plataforma")
            {
                derechaFuera = false;
                errores.Add(timerFueraDerecha);
                manager.Fallo();
            }
            if (izquierdaFuera && izquierdaHit.transform.gameObject.name != "Plataforma")
            {
                izquierdaFuera = false;
                errores.Add(timerFueraIzquierda);
                manager.Fallo();
            }
        }

        CheckAudio();
    }

    public void End()
    {
        if (derechaFuera)
        {
            errores.Add(timerFueraDerecha);
            manager.Fallo();
        }
        if (izquierdaFuera)
        {
            errores.Add(timerFueraIzquierda);
            manager.Fallo();
        }

        float timeMedia = 0;
        foreach(float f in errores)
        {
            timeMedia += f;
        }
        timeMedia /= (float)errores.Count;
        manager.tiempoReaccionMedia = timeMedia;
    }

    void CheckAudio()
    {
        if (audioFuera)
        {
            if(!derechaFuera && !izquierdaFuera)
            {
                audio.Stop();
                audioFuera = false;
            }
        }
        else
        {
            if(derechaFuera || izquierdaFuera)
            {
                audio.Play();
                audioFuera = true;
            }
        }
    }
}
