using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dibujante : MonoBehaviour
{
    static int  lastgiros1 = 2, lastgiros2 = 2;
    static bool inversion1 = false, inversion2 = false;

    GameObject pintura;
    [SerializeField] GameObject lienzo;
    [SerializeField] Dibujos dibujo;
    [SerializeField] float value;
    [SerializeField] TileInfo tile;

    enum Dibujos
    {
        none,
        cuartoCicunferenciaDerecha,
        cuartoCircunferenciaIzquierda,
        recta,
        bimanual
    }

    public void SetPintura(GameObject nodo)
    {
        pintura = nodo;
        Dibujar();
    }
    private void Dibujar()
    {
        switch (dibujo)
        {
            case Dibujos.cuartoCicunferenciaDerecha:
                CuartoCircunferenciaDerecha(value);
                break;
            case Dibujos.cuartoCircunferenciaIzquierda:
                CuartoCircunferenciaIzquierda(value);
                break;
            case Dibujos.recta:
                Recta(value);
                break;
            case Dibujos.bimanual:
                Bimanual(value);
                break;
            default:
                Debug.LogError("EL DIBUJANTE HA INTENTADO DIBUJAR UNA FIGURA QUE NO EXISTE");
                break;
        }

        tile.SetNodeTiles();
        CreaMapas.instance.RepasoConexiones();
        
        
    }

    void CuartoCircunferenciaDerecha(float radio)
    {
        nodoCarretera lastNodo =  null;
        for (int i = 0; i < 100; i++)
        {
            GameObject nodo = Instantiate(pintura, lienzo.transform);
            nodo.transform.position = transform.position + transform.forward * radio;
            nodo.transform.rotation = Quaternion.LookRotation(transform.right);
            if (lastNodo != null) lastNodo.nextNodo = nodo.GetComponent<nodoCarretera>();
            lastNodo = nodo.GetComponent<nodoCarretera>();
            transform.Rotate(transform.up, 0.9f);
            if (i == 0) tile.primerNodo = lastNodo;
            if (i == 99) tile.ultimoNodo = lastNodo;
        }        
    }
    void CuartoCircunferenciaIzquierda(float radio)
    {
        nodoCarretera lastNodo = null;
        for (int i = 0; i < 100; i++)
        {
            GameObject nodo = Instantiate(pintura, lienzo.transform);
            nodo.transform.position = transform.position + transform.forward * radio;
            nodo.transform.rotation = Quaternion.LookRotation(-transform.right);
            if (lastNodo != null) lastNodo.nextNodo = nodo.GetComponent<nodoCarretera>();
            lastNodo = nodo.GetComponent<nodoCarretera>();
            transform.Rotate(transform.up, -0.9f);
            if (i == 0) tile.primerNodo = lastNodo;
            if (i == 99) tile.ultimoNodo = lastNodo;
        }
    }

    void Recta(float distancia)
    {
        float intervalo = distancia / 100;
        nodoCarretera lastNodo = null;
        for (int i = 0; i < 100; i++)
        {
            GameObject nodo = Instantiate(pintura, lienzo.transform);
            nodo.transform.position = transform.position + transform.forward * intervalo * i;
            nodo.transform.rotation = transform.rotation;
            if (lastNodo != null) lastNodo.nextNodo = nodo.GetComponent<nodoCarretera>();
            lastNodo = nodo.GetComponent<nodoCarretera>();
            if (i == 0) tile.primerNodo = lastNodo;
            if (i == 99) tile.ultimoNodo = lastNodo;
        }
    }

    void Bimanual(float distancia)
    {
        int nodos = 800;
        float distNodos = distancia / nodos;

        float desviacionMaxima = 3;

        float referencia = 6;
       
        float pi = Mathf.PI;

        int giroPareja = 0;

        print(" ---------- ");
        for (int i = 0; i < 2; i++)
        {
            int lastGiros = 0;
            if (i == 0) lastGiros = lastgiros1;
            else lastGiros = lastgiros2;
            int giros = 1;
            int emergencia = 0;
            do
            {
                ++emergencia;
                giros = Random.Range(lastGiros - 2, lastGiros + 3);
                giros = Mathf.Clamp(giros, 1, 6);
            } while ((giros == giroPareja && i == 1 || giros == lastGiros) && emergencia < 100);
            if (emergencia >= 100) Debug.LogError("BucleRaro");
            
            giroPareja = giros;


            referencia *= -1;

            nodoCarretera lastNodo = null;

            bool inversion = false;

            if (i == 0) inversion = inversion1;
            else inversion = inversion2;
            print(lastGiros);
            float factor = 1;

            if (lastGiros % 2 == 0 && inversion || lastGiros % 2 != 0 && !inversion)
            {
                factor = -1;
                inversion = true;
            }
            else inversion = false;

            for (int j = 0; j < nodos; j++)
            {
                float x, z;
                GameObject nodo = Instantiate(pintura, lienzo.transform);

                z = distNodos * j;

                x = Mathf.Sin((z / distancia) * pi * giros) * desviacionMaxima;

                x *= factor;

                x += referencia;

                nodo.transform.position = transform.position;

                nodo.transform.position += transform.forward * z;
                nodo.transform.position += transform.right * x;

                nodo.transform.rotation = transform.rotation;

                nodoCarretera nodoLogic = nodo.GetComponent<nodoCarretera>();

                if (lastNodo != null) lastNodo.nextNodo = nodoLogic;
                lastNodo = nodoLogic;

                if(i == 0)
                {
                    if(j == 0)
                    {
                        tile.primerNodo = nodoLogic;
                    }
                    if(j == nodos - 1)
                    {
                        tile.ultimoNodo = nodoLogic;
                    }
                }
                else
                {
                    if (j == 0)
                    {
                        tile.gameObject.GetComponent<TileBimodal>().primerNodo2 = nodoLogic;
                    }
                    if (j == nodos - 1)
                    {
                        tile.gameObject.GetComponent<TileBimodal>().ultimoNodo2 = nodoLogic;
                    }
                }
            }

            lastGiros = giros;
            //if (inversion) lastGiros++;
            if(i == 0)
            {
                inversion1 = inversion;
                lastgiros1 = lastGiros;
            }
            else
            {
                inversion2 = inversion;
                lastgiros2 = lastGiros;
            }

        }
        print(inversion1 + " / " + lastgiros1 + " /// " + inversion2 + " / " + lastgiros2);
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10);
    }
}
