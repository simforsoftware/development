using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBimodal : TileInfo
{
    public nodoCarretera primerNodo2, ultimoNodo2;

    public override void SetNodeTiles()
    {
        base.SetNodeTiles();

        nodoCarretera current = primerNodo2;
        int contadorEmergencia = 0;
        while (current && contadorEmergencia < 1000)
        {
            ++contadorEmergencia;
            current.tile = this;
            if (current == ultimoNodo2) break;
            current = current.nextNodo;
        }
    }
}
