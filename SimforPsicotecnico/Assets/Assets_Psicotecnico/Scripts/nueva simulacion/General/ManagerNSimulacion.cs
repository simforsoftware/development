using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ManagerNSimulacion : MonoBehaviour
{

    [SerializeField] Text fallos_Text;
    [SerializeField] Text aciertos_Text;
    [SerializeField] protected CocheController coche;

    [SerializeField] protected float duracion_ejercicio;

    protected float timer;
    protected bool terminado;

    public List<int> lista_aciertos_fallos = new List<int>();


    protected float startTime = 0;

    protected int fallos;
    protected int aciertos;
    protected DataRecorder recorder;

    [SerializeField] protected bool falloAlFrenar;
    [SerializeField] protected Explicacion tuto;

    [SerializeField] protected InfoTest info;


    protected virtual void Start()
    {
        recorder = this.GetComponent<DataRecorder>();
        tuto.delegado += StartTest;
        coche.activo = false;
    }

    protected virtual void StartTest()
    {
        if(coche) coche.activo = true;
        recorder = GetComponent<DataRecorder>();
        terminado = false;
        timer = 0;
        fallos = 0;
        aciertos = 0;        
        if (falloAlFrenar) EventManager.StartListening("Freno", Fallo);
        startTime = Time.time;
        recorder.timeStart = Time.time;
    }

    protected virtual void Update()
    {
        timer += Time.deltaTime;
        if (timer >= duracion_ejercicio && !terminado)
        {
            terminado = true;
            End();
        }

    }

    public virtual void End()
    {
        Debug.Log("Soy un ga�an");
    }
    public void CambiarAciertoPorFallo()
    {
        --aciertos;
        ++fallos;
        lista_aciertos_fallos[lista_aciertos_fallos.Count - 1] = 0;
        if (fallos_Text) fallos_Text.text = "Fallos: " + fallos.ToString();
        if (aciertos_Text) aciertos_Text.text = "Aciertos: " + aciertos.ToString();
    }
    public void CambiarFalloPorAcierto()
    {
      
        recorder.timeEvents.RemoveAt(recorder.timeEvents.Count - 1);
        ++aciertos;
        --fallos;
        lista_aciertos_fallos[lista_aciertos_fallos.Count - 1] = 1;
        if (fallos_Text) fallos_Text.text = "Fallos: " + fallos.ToString();
        if (aciertos_Text) aciertos_Text.text = "Aciertos: " + aciertos.ToString();
    }
    public void Fallo()
    {
        recorder.SaveEvent();
        ++fallos;
        lista_aciertos_fallos.Add(0);
        if (fallos_Text) fallos_Text.text = "Fallos: " + fallos.ToString();
    }
    public void Acierto()
    {
        ++aciertos;
        lista_aciertos_fallos.Add(1);
        if (aciertos_Text) aciertos_Text.text = "Aciertos: " + aciertos.ToString();
    }  
}
