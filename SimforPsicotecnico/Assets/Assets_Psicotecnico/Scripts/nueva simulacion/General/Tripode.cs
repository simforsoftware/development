using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tripode : MonoBehaviour
{
    public static Tripode instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }

    public GameObject nodo;
    [SerializeField] float altura, rotacionInterpolada;
    [SerializeField] float aceptableError;

    public void SetFirstNodo(GameObject firstNodo)
    {
        nodo = firstNodo;
        transform.position = nodo.transform.position + Vector3.up * altura;
        transform.rotation = nodo.transform.rotation;
    }

    private void Update()
    {
        if (!nodo) return;
        if (((nodo.transform.position + Vector3.up) - transform.position).sqrMagnitude > 1)
        {     
            transform.position += ((nodo.transform.position + Vector3.up * altura) - transform.position).normalized * CocheController.instance.GetVelocidad() * Time.deltaTime;
        }
        /*transform.rotation = Quaternion.Lerp(transform.rotation, nodo.transform.rotation, 0.01f);*/
        /*Debug.Log((nodo.transform.position - (transform.forward * (nodo.transform.position - transform.position).sqrMagnitude)));
        Debug.Log((nodo.transform.position - (transform.forward * (nodo.transform.position - transform.position).sqrMagnitude)));
        if ((nodo.transform.position - (transform.forward * (nodo.transform.position - transform.position).sqrMagnitude)).sqrMagnitude > aceptableError* aceptableError) transform.Rotate(Vector3.up, rotacion * Time.deltaTime);
        else if((nodo.transform.position - (transform.forward * (nodo.transform.position - transform.position).sqrMagnitude)).sqrMagnitude < -aceptableError*aceptableError) transform.Rotate(Vector3.up, rotacion * Time.deltaTime);*/

        transform.rotation = Quaternion.Lerp(transform.rotation, nodo.transform.rotation, Mathf.Min(rotacionInterpolada, 1));
    }
    /*
    public bool Comparar(Quaternion value, float _error)
    {
        return Quaternion.Dot(transform.rotation, value) > 1f - _error;
    }*/


    
}
