using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileInfo : MonoBehaviour
{
    public enum Horientacion
    {
        norte,          //  +Z
        sur,            //  -Z
        este,           //  +X
        oeste           //  -X
    }

    public static Horientacion InvertHorientacion(Horientacion h)
    {
        switch (h)
        {
            case Horientacion.norte:
                return Horientacion.sur;
            case Horientacion.sur:
                return Horientacion.norte;
            case Horientacion.este:
                return Horientacion.oeste;
            case Horientacion.oeste:
                return Horientacion.este;
            default:
                return Horientacion.norte;
        }
    }

    public virtual void SetNodeTiles()
    {
        nodoCarretera current = primerNodo;
        while (current)
        {
            current.tile = this;
            if (current == ultimoNodo) break;
            current = current.nextNodo;            
        }
    }
    public float size;

    public Horientacion inicio, final;

    public nodoCarretera primerNodo, ultimoNodo;

    public TileInfo nextTile;

    public Vector3 GetNextPosition()
    {
        switch (final)
        {
            case Horientacion.norte:
                return transform.position + new Vector3(0, 0, size);
            case Horientacion.sur:
                return transform.position + new Vector3(0, 0, -size);
            case Horientacion.este:
                return transform.position + new Vector3(size, 0, 0);
            case Horientacion.oeste:
                return transform.position + new Vector3(-size, 0, 0);
            default:
                return Vector3.zero;
        }
    }
}
