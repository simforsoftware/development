using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nodoCarretera : MonoBehaviour
{
    public List<GameObject> carriles;
    public nodoCarretera nextNodo, prevNodo;
    List<float> desfases = new List<float>();

    public TileInfo tile;

    private void Start()
    {
        if (nextNodo) nextNodo.prevNodo = this;
        foreach(GameObject carril in carriles)
        {
            desfases.Add(carril.transform.localPosition.x);
        }
    }
    public float GetDesfase(int carril)
    {
        return desfases[carril];
    }
    private void OnDrawGizmos()
    {
        foreach(GameObject g in carriles)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(g.transform.position, 0.5f);
            Gizmos.color = Color.red;
            if (nextNodo)
            {
                for (int i = 0; i < carriles.Count; i++)
                {
                    Gizmos.DrawLine(carriles[i].transform.position, nextNodo.carriles[i].transform.position);
                }
            }
        }
    }
}
