using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CreaMapas : MonoBehaviour
{
    public static CreaMapas instance;

    public Action<GameObject> NewTile;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }
    [SerializeField] GameObject tipoCarril;
    [SerializeField] protected List<GameObject> tiles = new List<GameObject>();

    public List<TileInfo> currentTiles = new List<TileInfo>();

    TileInfo.Horientacion[] ultimaHorientacion = new TileInfo.Horientacion[2];

    public bool bimanualTile, infiniteRecta;

    public int maxTiles, initialTiles;

    protected virtual void Start()
    {
        currentTiles[0].GetComponentInChildren<Dibujante>().SetPintura(tipoCarril);
        ultimaHorientacion[0] = currentTiles[0].GetComponent<TileInfo>().inicio;
        ultimaHorientacion[1] = currentTiles[0].GetComponent<TileInfo>().final;
        if (bimanualTile)
        {
            maxTiles += 2;
            initialTiles += 2;
        }
        while (currentTiles.Count < initialTiles) CrearTile();
        Invoke("SetCar", 1f);
    }

    public virtual void CrearTile()
    {
        if (currentTiles.Count >= maxTiles) DeleteTile();
        TileInfo lastTile = currentTiles[currentTiles.Count - 1];
        TileInfo.Horientacion nextInicio = TileInfo.InvertHorientacion(lastTile.final);
        List<GameObject> posiblesTiles = new List<GameObject>();
        foreach(GameObject g in tiles)
        {
            if (g.GetComponent<TileInfo>().inicio == nextInicio) posiblesTiles.Add(g);
        }
        GameObject newTile = null;
        TileInfo.Horientacion[] nuevaHorientacion = new TileInfo.Horientacion[2];
        do
        {
            newTile = posiblesTiles[UnityEngine.Random.Range(0, posiblesTiles.Count)];
            nuevaHorientacion[0] = newTile.GetComponent<TileInfo>().inicio;
            nuevaHorientacion[1] = newTile.GetComponent<TileInfo>().final;

        } while (!bimanualTile && !infiniteRecta && nuevaHorientacion[0] == ultimaHorientacion[0] && nuevaHorientacion[1] == ultimaHorientacion[1]);
        newTile = Instantiate(newTile, transform);
        newTile.GetComponentInChildren<Dibujante>().SetPintura(tipoCarril);
        TileInfo newTileInfo = newTile.GetComponent<TileInfo>();
        newTile.transform.position = lastTile.GetNextPosition();
        lastTile.nextTile = newTileInfo;        
        ultimaHorientacion = nuevaHorientacion;
        currentTiles.Add(newTileInfo);
        NewTile?.Invoke(newTile);
        if (CreaObstaculos.instance) CreaObstaculos.instance.CrearObstaculos();
        else if (CreaObstaculos3.instance) CreaObstaculos3.instance.CrearObstaculos();
    }

    public void RepasoConexiones()
    {
        
        for (int i = 0; i < currentTiles.Count - 1; i++)
        {
            if (!currentTiles[i].ultimoNodo.nextNodo) ConectTile(currentTiles[i]);
        }
    }

    public void ConectTile(TileInfo lastTile)
    {
        if (bimanualTile)
        {
            TileBimodal lastTileB = lastTile.GetComponent<TileBimodal>();

            if (lastTileB.nextTile)
            {
                TileBimodal newTileInfoB = lastTileB.nextTile.GetComponent<TileBimodal>();
                lastTileB.ultimoNodo.nextNodo = newTileInfoB.primerNodo;
                newTileInfoB.primerNodo.prevNodo = lastTileB.ultimoNodo;

                lastTileB.ultimoNodo2.nextNodo = newTileInfoB.primerNodo2;
                newTileInfoB.primerNodo2.prevNodo = lastTileB.ultimoNodo2;
            }
        }
        else
        {
            if (lastTile.nextTile)
            {
                TileInfo newTileInfo = lastTile.nextTile;
                lastTile.ultimoNodo.nextNodo = newTileInfo.primerNodo;
                newTileInfo.primerNodo.prevNodo = lastTile.ultimoNodo;            
            }
        }
    }

    void SetCar()
    {
        if(!bimanualTile) CocheController.instance.SetNodo(currentTiles[0].primerNodo);
    }

    public void DeleteTile()
    {        
        Destroy(currentTiles[0].gameObject);
        currentTiles.RemoveAt(0);
    }
}
