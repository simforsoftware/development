using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multimaterial : MonoBehaviour
{
    [SerializeField] List<ListaMateriales> materiales;

    private void Start()
    {
        foreach(ListaMateriales lista in materiales)
        {
            if(lista.materiales.Count == 0)
            {
                Debug.LogError("Multimaterial no puede seleccionar un material si la lista esta vacia");
                return;
            }
        }

        MeshRenderer mesh = GetComponent<MeshRenderer>();

        if (mesh.materials.Length > materiales.Count)
        {
            //Debug.LogError("Multimaterial requiere mas materiales para funcionar");
            return;
        }

        if(mesh.materials.Length > 1)
        {
            List<Material> materialesAleatorios = new List<Material>();
            for (int i = 0; i < materiales.Count; i++)
            {
                materialesAleatorios.Add(materiales[i].materiales[Random.Range(0, materiales[i].materiales.Count)]);
            }
            mesh.materials = materialesAleatorios.ToArray();
        }
        else
        {
            mesh.material = materiales[0].materiales[Random.Range(0, materiales[0].materiales.Count)];
        }
    }
}

[System.Serializable]
public class ListaMateriales
{
    public List<Material> materiales = new List<Material>();
}
