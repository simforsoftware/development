using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaCamara : MonoBehaviour
{

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("nodo"))
        {
            Tripode.instance.nodo = other.gameObject;
        }
    }
}
