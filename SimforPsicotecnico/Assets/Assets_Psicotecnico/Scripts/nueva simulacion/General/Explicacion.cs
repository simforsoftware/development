using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public  delegate void VoidNoInput();
public class Explicacion : MonoBehaviour
{
    [SerializeField] GameObject positionCamera, canvasSeleccion;
    public VoidNoInput delegado;
    public Image icono;
    public Sprite sprite;

    GameObject g;
    VRSelector vrs;

    Vector3 positionInicial;
    Quaternion rotacionInicial;

    private void Start()
    {
        icono.sprite = sprite;

        Camera.main.clearFlags = CameraClearFlags.SolidColor;
        Camera.main.backgroundColor = Color.white;
        positionInicial = Camera.main.transform.parent.parent.transform.position;
        rotacionInicial = Camera.main.transform.parent.parent.transform.rotation;
        Camera.main.transform.parent.parent.transform.position = positionCamera.transform.position;
        Camera.main.transform.parent.parent.transform.rotation = positionCamera.transform.rotation;

        Camera.main.GetComponentInParent<ControladorCamara>().bloqueada = true;

        g = Instantiate(canvasSeleccion);
        g.GetComponent<Canvas>().worldCamera = Camera.main;
        vrs = Camera.main.gameObject.AddComponent<VRSelector>();
        vrs.ui = g.transform.GetChild(1).gameObject;
        vrs.segundos_para_seleccionar = 1;

    }

    public void EndTutorial(bool terceraPersona = false)
    {
        Camera.main.GetComponentInParent<ControladorCamara>().bloqueada = false;

        Destroy(g);
        Destroy(vrs);
        Camera.main.GetComponentInParent<ControladorCamara>().terceraPersona = terceraPersona;
        Camera.main.clearFlags = CameraClearFlags.Skybox;
        Camera.main.transform.parent.parent.transform.position = positionInicial;
        Camera.main.transform.parent.parent.transform.rotation = rotacionInicial;
        Camera.main.GetComponentInParent<ControladorCamara>().StartTest();
        delegado();
        Destroy(this.gameObject);
    }
}
