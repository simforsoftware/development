using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CarBreakZoneBH : MonoBehaviour
{
    public Action CarBreak;

    GameObject car = null;

    Vector3 carLastPosition = Vector3.zero;

    float timerChecker = 0.2f;


    private void Start()
    {
        StartCoroutine(CheckBreak());
    }    

    IEnumerator CheckBreak()
    {
        if(car != null)
        {
            print(gameObject.name);
            if (carLastPosition == car.transform.position)
            {
                print(gameObject.name + ": break");
                CarBreak?.Invoke();
                StopAllCoroutines();
            }
            carLastPosition = car.transform.position;
        }
        yield return new WaitForSeconds(timerChecker);
        StartCoroutine(CheckBreak());
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("car")) car = other.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("car")) car = null;
    }

    /*
    private void OnTriggerStay(Collider other)
    {
        print(gameObject.name);
        if (other.CompareTag("car"))
        {
            print(other.gameObject.name);
            print(other.transform.position);
            if (carLastPosition == other.transform.position)
            {
                print(gameObject.name + ": break");
                CarBreak?.Invoke();
            }
            carLastPosition = other.transform.position;
        }
    }
    */
}
