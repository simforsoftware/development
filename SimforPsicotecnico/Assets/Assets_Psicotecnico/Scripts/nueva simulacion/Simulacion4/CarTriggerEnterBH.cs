using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CarTriggerEnterBH : MonoBehaviour
{
    public Action CarTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("car"))
        {
            CarTrigger?.Invoke();
        }
    }
}
