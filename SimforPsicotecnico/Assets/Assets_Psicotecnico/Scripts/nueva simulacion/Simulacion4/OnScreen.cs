using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreen : MonoBehaviour
{
    [SerializeField] float distanciaVision;
    bool visto;
    public bool parar;
    void Start()
    {
        visto = false;
    }

    private void Update()
    {
        if (!visto)
        {
            SoyVisible();
        }

    }

    void SoyVisible()
    {
        if((CocheController.instance.transform.position - this.transform.position).sqrMagnitude < distanciaVision * distanciaVision)
        {
            RaycastHit hitInfo;
            Physics.Raycast(this.transform.position, (CocheController.instance.transform.position + Vector3.up - this.transform.position), out hitInfo, distanciaVision);
            if (hitInfo.collider && hitInfo.collider.GetComponentInParent<CocheController>())
            {
                visto = true;
                if(parar)ManagerNSimulacion1.instnace.Anticiparse();
            }
        }

    }
}
