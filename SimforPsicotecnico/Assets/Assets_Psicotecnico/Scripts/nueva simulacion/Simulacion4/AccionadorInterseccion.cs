using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccionadorInterseccion : MonoBehaviour
{
    [SerializeField] CocheInterseccion padre;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("car"))
        {
            padre.listo = true;
        }
    }
}
