using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNSimulacion4 : ManagerNSimulacion
{
    [SerializeField] int tipoTest;

    [SerializeField] float aciertos_por_fallos_para_apto;
    [SerializeField] float minTiempoAnticipacion;

    int[] resultadosTest = new int[3];

    List<float> riskFactorList = new List<float>();

    [SerializeField] CreaMapas creaMapas;   

    [SerializeField] int maxRefecenceRiskFactor, minReferenceRiskFactor;


    ResultNuevaSimulacion1 resultados = new ResultNuevaSimulacion1();

    protected override void StartTest()
    {
        resultados = new ResultNuevaSimulacion1();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        InfoNuevaSimulacion12 info2 = (InfoNuevaSimulacion12)info;
        if (info2)
        {
            duracion_ejercicio = info2.duracionEjercicio;
            aciertos_por_fallos_para_apto = info2.aciertosPorFalloAprovado;
            minTiempoAnticipacion = info2.minTiempoAnticipacion;
        }
        for (int i = 0; i < creaMapas.transform.childCount; i++)
        {
            SetFunction(creaMapas.transform.GetChild(i).gameObject);
        }
        creaMapas.NewTile += SetFunction;
        base.StartTest();        
    }   

    public override void End()
    {
        int respuestas = aciertos + fallos;
        float porcentajeAcierto = (aciertos / respuestas) * 100;
        float riskFactorListSum = 0;
        foreach (float f in riskFactorList) riskFactorListSum += f;
        float riskFactor = riskFactorListSum / riskFactorList.Count;
        riskFactor -= minReferenceRiskFactor;
        riskFactor *= 133;
        riskFactor /= maxRefecenceRiskFactor;
        print(riskFactor + " // " + resultadosTest[0] + " / " + resultadosTest[1] + " / " + resultadosTest[2]);


        DataRecorder recorder = this.GetComponent<DataRecorder>();
        if (recorder.cognitiveLoadRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, coloresGraficas.cognitiveLoad));
        if (recorder.heartRateRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, coloresGraficas.hearthRate));
        if (recorder.pupilDilatationRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, coloresGraficas.pupilDilatation));
        if (recorder.timeEvents.Count > 0) resultados.eventos.Add(new Grafics.eventData(recorder.timeEvents, coloresGraficas.events));
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.time = porcentajeAcierto;
        resultados.aciertos = aciertos;
        resultados.reactionTime = riskFactor;
        resultados.fallos = respuestas;
        resultados.minutos = Mathf.Max((int)(duracion_ejercicio / 60));
        AuxiliarBuild.instance.tipo.Add(16);
        AuxiliarBuild.instance.resultados.Add(resultados);
        resultados.UploadAlumno();
        resultados.FSubirSession();
        /*resultados.time = duracion_ejercicio;
        float reaccion_media = 0;
        
        resultados.reactionTime = reaccion_media;
        resultados.aciertos = aciertos;
        resultados.fallos = fallos;
        resultados.eventos.Add(new Grafics.eventData(eventos, Color.yellow));
        AuxiliarBuild.instance.tipo.Add(14);
        AuxiliarBuild.instance.resultados.Add(resultados);
        //Esto tendria que cambiar al upload tipico
        resultados.apto = true;
        if (reaccion_media < minTiempoAnticipacion) resultados.apto = false;
        if (aciertos < aciertos_por_fallos_para_apto * fallos) resultados.apto = false;
        resultados.minutos = Mathf.Max((int)(duracion_ejercicio / 60));
        if (duracion_ejercicio % 60 != 0) ++resultados.minutos;
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();

        DataRecorder recorder = this.GetComponent<DataRecorder>();
        if (recorder.cognitiveLoadRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, Color.green));
        if (recorder.heartRateRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, Color.red));
        if (recorder.pupilDilatationRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, Color.blue));
        if (recorder.timeEvents.Count > 0) resultados.eventos.Add(new Grafics.eventData(recorder.timeEvents, Color.yellow));

        resultados.UploadAlumno();
        resultados.FSubirSession();*/
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }

    public void SetFunction(GameObject tile)
    {
        ObstaculoInterseccionController controller = tile.GetComponentInChildren<ObstaculoInterseccionController>();
        if (controller)
        {
            controller.resultReady += AddResult;
            controller.SetCar(coche);
        }
    }

    public void AddResult(decisionResult result, float variation)
    {
        recorder.SaveEvent();

        //Debug.Log();
        Debug.Log("HEY");
        Debug.Log((int)result-1);
        Debug.Log(resultadosTest.Length);
        resultadosTest[(int)result - 1]++;
        if (result != decisionResult.undecided) riskFactorList.Add(variation);
        if (result == decisionResult.correct) ++aciertos;
        if (result == decisionResult.incorrect) ++fallos;
        print(variation + " // " +resultadosTest[0] + " / " + resultadosTest[1] + " / " + resultadosTest[2]);
    }

}
