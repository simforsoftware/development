using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreaMapasCruces : CreaMapas
{
    [SerializeField] List<GameObject> tilesCruce = new List<GameObject>();
    List<GameObject> tilesBaseOriginales = new List<GameObject>();

    bool tocaCruce = true;

    protected override void Start()
    {
        foreach(GameObject g in tiles)
        {
            tilesBaseOriginales.Add(g);
        }

        base.Start();
    }

    public override void CrearTile()
    {
        if (tocaCruce) tiles = tilesCruce;
        else tiles = tilesBaseOriginales;
        //tocaCruce = !tocaCruce;

        base.CrearTile();
    }
}
