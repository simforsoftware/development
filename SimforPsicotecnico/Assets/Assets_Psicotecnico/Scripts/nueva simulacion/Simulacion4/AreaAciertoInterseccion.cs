using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaAciertoInterseccion : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            ManagerNSimulacion1.instnace.Acierto();
        }
    }
}
