using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObstaculoInterseccionController : MonoBehaviour
{
    [SerializeField] CocheInterseccion carDummy;
    [SerializeField] CarTriggerEnterBH activeTrigger, crossRoadTrigger, finalTrigger;
    [SerializeField] CarBreakZoneBH farBreakZone, nearBreakZone;
    CocheController car;

    decisionResult result = decisionResult.none;

    public Action<decisionResult, float> resultReady;

    float maxRisk = 5;
    float initialSpeed = 13;

    float risk;

    private void Start()
    {
        //Random direction
        if (UnityEngine.Random.Range(0, 2) == 0) ChangeDirection();

        //Random setup
        risk = UnityEngine.Random.Range(0, maxRisk);
        activeTrigger.CarTrigger += StartCar;
        finalTrigger.CarTrigger += SenddResult;

        crossRoadTrigger.CarTrigger += IncorrectAnswer;
        crossRoadTrigger.CarTrigger += carDummy.Playfrenazo;
        nearBreakZone.CarBreak += CorrectAnswer;
        farBreakZone.CarBreak += UndecidedtAnswer;
    }

    public void CorrectAnswer()
    {
        Answer(decisionResult.correct);
    }

    public void IncorrectAnswer()
    {
        Answer(decisionResult.incorrect);
    }

    public void UndecidedtAnswer()
    {
        Answer(decisionResult.undecided);
    }

    void Answer(decisionResult answer)
    {
        print(answer);
        if (result == decisionResult.none)
        {
            result = answer;
            farBreakZone.gameObject.SetActive(false);
            nearBreakZone.gameObject.SetActive(false);
            crossRoadTrigger.gameObject.SetActive(false);
        }
    }

    void ChangeDirection()
    {
        carDummy.transform.localPosition *= -1;
        carDummy.transform.Rotate(Vector3.up,180f);        
    }

    public void StartCar()
    {
        carDummy.SetSpeed(initialSpeed + risk);
        carDummy.PlayClaxon();
        car.SetVelocidadMaxima(initialSpeed + risk);
        carDummy.listo = true;
    }

    public void SenddResult()
    {
        resultReady?.Invoke(result, risk);
    }

    public void SetCar(CocheController managerCar)
    {
        car = managerCar;
    }
}


public enum decisionResult
{
    none,
    correct,
    incorrect,
    undecided
}
