using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocheInterseccion : MonoBehaviour
{
    public bool listo;

    [SerializeField] float velocidad, pitch;
    [SerializeField] AudioSource frenazo, engineSound, claxon;
    bool engineOn = false;

    private void Start()
    {
        listo = false;
    }
    private void Update()
    {
        if (!listo) return;
        if (transform.localPosition.x < -90 || transform.localPosition.x > 90) return;
        transform.position += transform.forward * velocidad * Time.deltaTime;
        SetPitch();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10);
    }

    public void SetSpeed(float speed)
    {
         velocidad = speed;
    }

    public void Playfrenazo()
    {
        frenazo.time = 0.5f;
        frenazo.Play();
        claxon.volume = 1f;
        claxon.pitch = 1f;
        claxon.Play();
        engineSound.Stop();
    }
    public void PlayClaxon()
    {
        claxon.volume = 0.3f;
        claxon.pitch = 1.2f;
        claxon.Play();
    }
    
    void SetPitch()
    {
        if (listo && !engineOn)
        {
            engineOn = true;
            engineSound.pitch = pitch;
            engineSound.Play();
        }
        if(!listo && engineOn)
        {
            engineOn = false;
            engineSound.Stop();
        }
    }
}
