using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo : MonoBehaviour
{
    public bool destruible;
    public GameObject triggerCorrect, obstaculoPadre;

    ManagerNSimulacion manager;

    private void Start()
    {
        if (ManagerNSimulacion1.instnace) manager = ManagerNSimulacion1.instnace;
        else if (ManagerNSimulacion3.instance) manager = ManagerNSimulacion3.instance;
    }

    public void Set(GameObject padre, GameObject correct)
    {
        obstaculoPadre = padre;
        triggerCorrect = correct;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("car"))
        {
            manager.Fallo();

            if (triggerCorrect) triggerCorrect.SetActive(false);
            if (destruible && obstaculoPadre) obstaculoPadre.SetActive(false);
        }
    }   
}
