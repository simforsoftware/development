using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNSimulacion1 : ManagerNSimulacion
{
    public static ManagerNSimulacion1 instnace;
    [SerializeField] int tipoTest;

    [SerializeField] float aciertos_por_fallos_para_apto;
    [SerializeField] float minTiempoAnticipacion;


    public List<float> lista_tiempos_anticipacion = new List<float>();
    private void Awake()
    {
        if (!instnace) instnace = this;
        else Destroy(this.gameObject);
    }
   
    public bool anticipado;
    float tiempo_anticipacion;

    ResultNuevaSimulacion1 resultados = new ResultNuevaSimulacion1();
    protected override void StartTest()
    {
        resultados = new ResultNuevaSimulacion1();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        InfoNuevaSimulacion12 info2 = (InfoNuevaSimulacion12)info;
        if (info2)
        {
            duracion_ejercicio = info2.duracionEjercicio;
            aciertos_por_fallos_para_apto = info2.aciertosPorFalloAprovado;
            minTiempoAnticipacion = info2.minTiempoAnticipacion;
        }

        base.StartTest();
        anticipado = false;
        tiempo_anticipacion = 0;

        if(tipoTest <= 2)
        {
            EventManager.StartListening("TurnRight", Anticiparse);
            EventManager.StartListening("TurnLeft", Anticiparse);
        }
        else if(tipoTest == 4)
        {
            EventManager.StartListening("Freno", Obstaculo_Encontrado);
        }
    }

    protected override void Update()
    {
        if (anticipado)
        {
            tiempo_anticipacion += Time.deltaTime;
           
        }
        base.Update();
    }
    
    public override void End()
    {
        
        resultados.time = duracion_ejercicio;
        float reaccion_media = 0;
        for (int i = 0; i < lista_tiempos_anticipacion.Count; ++i)
        {
            reaccion_media += lista_tiempos_anticipacion[i];
        }
        reaccion_media = reaccion_media / lista_tiempos_anticipacion.Count;
        resultados.reactionTime = reaccion_media;
        resultados.aciertos = aciertos;
        resultados.fallos = fallos;

        AuxiliarBuild.instance.tipo.Add(14);
        AuxiliarBuild.instance.resultados.Add(resultados);
        //Esto tendria que cambiar al upload tipico
        resultados.apto = true;
        if (reaccion_media < minTiempoAnticipacion) resultados.apto = false;
        if (aciertos < aciertos_por_fallos_para_apto * fallos) resultados.apto = false;
        resultados.minutos = Mathf.Max((int)(duracion_ejercicio / 60) );
        if (duracion_ejercicio % 60 != 0) ++resultados.minutos;
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();


        if (recorder.cognitiveLoadRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, coloresGraficas.cognitiveLoad));
        if (recorder.heartRateRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, coloresGraficas.hearthRate));
        if (recorder.pupilDilatationRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, coloresGraficas.pupilDilatation));
        if (recorder.timeEvents.Count > 0) resultados.eventos.Add(new Grafics.eventData(recorder.timeEvents, coloresGraficas.events));

        resultados.UploadAlumno();
        resultados.FSubirSession();
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }

    public void Obstaculo_Encontrado()
    {
        if (anticipado)
        {
            lista_tiempos_anticipacion.Add(tiempo_anticipacion);
            anticipado = false;
            tiempo_anticipacion = 0;
        }
    }

    public void Anticiparse()
    {
        anticipado = true;
        tiempo_anticipacion = 0;
    }
}
