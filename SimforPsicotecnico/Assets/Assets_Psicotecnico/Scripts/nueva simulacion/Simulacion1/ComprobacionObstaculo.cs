using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComprobacionObstaculo : MonoBehaviour
{
    ManagerNSimulacion manager;
    private void Start()
    {
        if (ManagerNSimulacion1.instnace) manager = ManagerNSimulacion1.instnace;
        else if (ManagerNSimulacion3.instance) manager = ManagerNSimulacion3.instance;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("car"))
        {
            manager.Acierto();
            if (ManagerNSimulacion1.instnace) ManagerNSimulacion1.instnace.Obstaculo_Encontrado();            
        }
    }
}
