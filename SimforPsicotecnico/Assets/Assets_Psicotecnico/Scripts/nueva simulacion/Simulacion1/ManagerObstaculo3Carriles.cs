using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerObstaculo3Carriles : MonoBehaviour
{    
    [SerializeField] GameObject areaAcierto;
    [SerializeField] List<GameObject> obstaculosIndividualesCentrales, obstaculosIndividualesLaterales, obstaculosDobles;

    nodoCarretera thisNodo;

    public int[] SetDisposicion(nodoCarretera nodo, int[] disposicionPrevia = null)
    {
        thisNodo = nodo;

        int numObstaculos = 0;
        int carrilEsp = 0;

        if (disposicionPrevia == null)
        {
            numObstaculos = Random.Range(1, 3);
            carrilEsp = Random.Range(0, 3);
        }
        else
        {
            if(disposicionPrevia[0] == 1)
            {
                numObstaculos = 2;
                carrilEsp = disposicionPrevia[1];
            }
            else
            {
                numObstaculos = Random.Range(1, 3);
                switch (numObstaculos)
                {
                    case 1:
                        carrilEsp = disposicionPrevia[1];
                        break;
                    case 2:
                        do
                        {
                            carrilEsp = Random.Range(0, 3);
                        } while (carrilEsp == disposicionPrevia[1] || Mathf.Abs(carrilEsp - disposicionPrevia[1]) >= 2);
                        break;
                }
            }
        }

        GameObject obstaculo = null;


        if (numObstaculos == 1)
        {
            switch (carrilEsp)
            {
                case 0:
                    obstaculo = Instantiate(obstaculosIndividualesLaterales[Random.Range(0, obstaculosIndividualesLaterales.Count)], transform);
                    obstaculo.transform.position = thisNodo.carriles[carrilEsp].transform.position;
                    Set(obstaculo);
                    obstaculo.transform.Rotate(Vector3.up, 180);
                    break;
                case 1:
                    obstaculo = Instantiate(obstaculosIndividualesCentrales[Random.Range(0, obstaculosIndividualesCentrales.Count)], transform);
                    obstaculo.transform.position = thisNodo.carriles[carrilEsp].transform.position;
                    Set(obstaculo);
                    break;
                case 2:
                    obstaculo = Instantiate(obstaculosIndividualesLaterales[Random.Range(0, obstaculosIndividualesLaterales.Count)], transform);
                    obstaculo.transform.position = thisNodo.carriles[carrilEsp].transform.position;
                    Set(obstaculo);
                    break;
            }
           
        }
        else
        {
            switch (carrilEsp)
            {
                case 0:
                    obstaculo = Instantiate(obstaculosDobles[Random.Range(0, obstaculosDobles.Count)], transform);
                    obstaculo.transform.position = (thisNodo.carriles[1].transform.position + thisNodo.carriles[2].transform.position) / 2;
                    Set(obstaculo);
                    break;
                case 1:
                    obstaculo = Instantiate(obstaculosIndividualesLaterales[Random.Range(0, obstaculosIndividualesLaterales.Count)], transform);
                    obstaculo.transform.position = thisNodo.carriles[0].transform.position;
                    Set(obstaculo);
                    obstaculo.transform.Rotate(Vector3.up, 180);

                    obstaculo = Instantiate(obstaculosIndividualesLaterales[Random.Range(0, obstaculosIndividualesLaterales.Count)], transform);
                    obstaculo.transform.position = thisNodo.carriles[2].transform.position;
                    Set(obstaculo);
                    break;
                case 2:
                    obstaculo = Instantiate(obstaculosDobles[Random.Range(0, obstaculosDobles.Count)], transform);
                    obstaculo.transform.position = (thisNodo.carriles[0].transform.position + thisNodo.carriles[1].transform.position) / 2;
                    Set(obstaculo);
                    obstaculo.transform.Rotate(Vector3.up, 180);
                    break;
            }
        }

        int[] disposicion = { numObstaculos, carrilEsp };
        return disposicion;
    }

    void Set(GameObject obstaculo)
    {
        foreach(Obstaculo obst in obstaculo.GetComponentsInChildren<Obstaculo>())
        {
            obst.Set(this.gameObject, areaAcierto);
        }
    }
}
