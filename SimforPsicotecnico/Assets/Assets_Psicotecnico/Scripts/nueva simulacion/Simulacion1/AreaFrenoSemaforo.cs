using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaFrenoSemaforo : MonoBehaviour
{
    Semaforo1Controller manager;

    public void SetManager(Semaforo1Controller s1c)
    {
        manager = s1c;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("car"))
        {
            if (other.GetComponentInParent<CocheController>().GetVelocidad() == 0)
            {
                manager.PonerEnVerde();
            }
        }
    }
}
