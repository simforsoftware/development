using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreaObstaculos : MonoBehaviour
{
    public static CreaObstaculos instance;

    enum tipoObstaculo
    {
        esquivar,
        frenar
    }

    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);
    }


    int carril_Objetivo = 0;
    [SerializeField] int[] nodos_por_obstaculo = new int[2];
    int distanciaProximoObdstaculo = 0;

    nodoCarretera nodoActual;


    [SerializeField] GameObject obstaculoEsquivar;
    [SerializeField] GameObject obstaculoParar;

    [SerializeField] bool RepetirCarril;

    tipoObstaculo ultimoObstaculo;

    int[] ultimaDisposicion = null;
    
    public void CrearObstaculos()
    {
        if (!nodoActual)
        {
            distanciaProximoObdstaculo = Random.Range(nodos_por_obstaculo[0], nodos_por_obstaculo[1]);
            nodoActual = CreaMapas.instance.currentTiles[0].primerNodo;            
        }

        --distanciaProximoObdstaculo;
        nodoActual = nodoActual.nextNodo;


        if (distanciaProximoObdstaculo == 0)
        {
            //Obstaculo Esquivar
            if(Random.Range(0,2) == 0 || ultimoObstaculo == tipoObstaculo.frenar)
            {                
                GameObject obstaculo = Instantiate(obstaculoEsquivar, nodoActual.transform);
                ManagerObstaculo3Carriles managerObstaculo = obstaculo.GetComponent<ManagerObstaculo3Carriles>();
                if (!RepetirCarril) ultimaDisposicion = managerObstaculo.SetDisposicion(nodoActual, ultimaDisposicion);
                else managerObstaculo.SetDisposicion(nodoActual);
                distanciaProximoObdstaculo = Random.Range(nodos_por_obstaculo[0], nodos_por_obstaculo[1]);
                ultimoObstaculo = tipoObstaculo.esquivar;
            }
            //ObstaculoParar
            else
            {
                GameObject obstaculo = Instantiate(obstaculoParar, nodoActual.transform);
                distanciaProximoObdstaculo = Random.Range(nodos_por_obstaculo[0], nodos_por_obstaculo[1]);
                ultimoObstaculo = tipoObstaculo.frenar;
                if (RepetirCarril && Random.Range(0, 2) == 0) obstaculo.GetComponent<Semaforo1Controller>().SetVerde();
            }
        }

        
        if (nodoActual.nextNodo)
        {
            CrearObstaculos();
        }
    }
    
}
