using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaforo1Controller : MonoBehaviour
{
    [SerializeField] GameObject redLights, greenLights;
    [SerializeField] GameObject areaFreno, fallo, acierto;

    private void Start()
    {
        areaFreno.GetComponent<AreaFrenoSemaforo>().SetManager(this);
    }

    public void PonerEnVerde()
    {
        redLights.SetActive(false);
        greenLights.SetActive(true);
        areaFreno.SetActive(false);
        fallo.SetActive(false);
        ManagerNSimulacion1.instnace.CambiarFalloPorAcierto();
        ManagerNSimulacion1.instnace.Obstaculo_Encontrado();
    }

    public void SetVerde()
    {
        redLights.SetActive(false);
        greenLights.SetActive(true);
        areaFreno.SetActive(false);
        fallo.SetActive(false);
        acierto.SetActive(true);
    }
}
