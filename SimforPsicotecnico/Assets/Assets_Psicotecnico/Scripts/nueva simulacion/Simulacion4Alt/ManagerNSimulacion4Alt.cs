using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNSimulacion4Alt : ManagerNSimulacion
{
    [SerializeField] CreaMapas creaMapas;

    ResultNuevaSimulacion1 resultados = new ResultNuevaSimulacion1();

    List<float> diferences = new List<float>();

    protected override void StartTest()
    {
        resultados = new ResultNuevaSimulacion1();
       
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        for (int i = 0; i < creaMapas.transform.childCount; i++)
        {
            SetFunction(creaMapas.transform.GetChild(i).gameObject);
        }
        creaMapas.NewTile += SetFunction;
        base.StartTest();
    }

    public void SetFunction(GameObject tile)
    {
        CrossRoad4Controller controller = tile.GetComponentInChildren<CrossRoad4Controller>();
        if (controller)
        {
            controller.ResultReady += AddResult;
            controller.SetCar(coche);
        }
    }

    public void AddResult(bool fallo, float diference)
    {

        recorder.SaveEvent();
        if (fallo) ++fallos;
        else
        {
            ++aciertos;
            diferences.Add(diference);
        }
    }

    public override void End()
    {
        float mediaDiff = 0;
        if(diferences.Count> 0)
        {
            float sumaDiff = 0;
            foreach (float f in diferences) sumaDiff += f;
            mediaDiff = sumaDiff / diferences.Count;
        }

        print("Aciertos: " + aciertos + " // Fallos: " + fallos + " // Media Diferencias: " + mediaDiff.ToString("F2"));

        resultados.aciertos = aciertos;
        resultados.fallos = fallos;
        resultados.reactionTime = mediaDiff;
        resultados.time = duracion_ejercicio;
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.minutos = Mathf.Max((int)(duracion_ejercicio / 60));

        resultados.apto = true;

        if (recorder.cognitiveLoadRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, coloresGraficas.cognitiveLoad));
        if (recorder.heartRateRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, coloresGraficas.hearthRate));
        if (recorder.pupilDilatationRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, coloresGraficas.pupilDilatation));

        if (recorder.timeEvents.Count > 0)
        {
            resultados.eventos.Add(new Grafics.eventData(recorder.timeEvents, coloresGraficas.events));
        }


        AuxiliarBuild.instance.tipo.Add(14);
        AuxiliarBuild.instance.resultados.Add(resultados);
        resultados.UploadAlumno();
        resultados.FSubirSession();
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}
