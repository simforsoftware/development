using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyVisibilityBH : MonoBehaviour
{
    public void SetVisible()
    {
        gameObject.SetActive(true);
    }
    public void SetInvisible()
    {
        gameObject.SetActive(false);
    }
}
