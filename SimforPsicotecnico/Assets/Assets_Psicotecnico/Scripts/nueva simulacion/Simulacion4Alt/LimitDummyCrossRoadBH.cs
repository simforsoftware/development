using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LimitDummyCrossRoadBH : MonoBehaviour
{
    public Action LimitDummy;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("dummy"))
        {
            LimitDummy?.Invoke();
        }
    }
}
