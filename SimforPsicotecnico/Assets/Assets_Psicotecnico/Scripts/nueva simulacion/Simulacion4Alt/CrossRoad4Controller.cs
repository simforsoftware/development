using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CrossRoad4Controller : MonoBehaviour
{
    public Action<bool, float> ResultReady;

    [SerializeField] CarTriggerEnterBH start, finish;
    [SerializeField] CocheInterseccion dummyCar;
    [SerializeField] DummyBreakBH dummyBreak;
    [SerializeField] ReferencePositionBH reference;
    [SerializeField] LimitDummyCrossRoadBH limit;
    [SerializeField] float speed;
    [SerializeField] GameObject visualObstacle;
    [SerializeField] DummyTriggerExitBH visibilityTrigger;
    [SerializeField] DummyVisibilityBH visibilityBH;
    [SerializeField] Animator rueda1, rueda2, rueda3, rueda4;

    CocheController car;

    float distance = 0;
    bool fallo;
    float speedRef;

    private void Start()
    {
        if (UnityEngine.Random.Range(0, 2) == 0) SwapDirection();
        start.CarTrigger += StartTest;
        dummyBreak.dummy = dummyCar;
        reference.target = dummyCar.gameObject;
        dummyBreak.DummyBreak += reference.CalculateDistance;
        reference.DistanceReady += SetDistance;
        limit.LimitDummy += dummyBreak.Break;
        limit.LimitDummy += SetFallo;
        finish.CarTrigger += End;
        visibilityTrigger.DummyExit += visibilityBH.SetInvisible;
        dummyBreak.DummyBreak += visibilityBH.SetVisible;
        limit.LimitDummy += dummyCar.Playfrenazo;
        dummyBreak.DummyBreak += DestroyAnimators;
        start.CarTrigger += dummyCar.PlayClaxon;
    }

    void SwapDirection()
    {
        dummyCar.transform.localPosition *= -1;
        dummyCar.transform.Rotate(Vector3.up, 180);
        limit.transform.localPosition *= -1;
        reference.transform.localPosition *= -1;
        visibilityTrigger.transform.localPosition *= -1;
        for (int i = 0; i < visualObstacle.transform.childCount; i++)
        {
            Transform child = visualObstacle.transform.GetChild(i);
            child.localPosition = new Vector3(child.localPosition.x,child.localPosition.y, -child.localPosition.z);
            child.Rotate(Vector3.up, 180);
        }
    }

    public void SetCar(CocheController c)
    {
        car = c;
    }

    public void StartTest()
    {
        dummyCar.listo = true;
        dummyCar.SetSpeed(speed);
        speedRef = car.GetVelocidadMaxima();
        car.SetVelocidadMaxima(speed);
    }

    public void SetDistance(float f)
    {
        distance = f;
    }

    public void SetFallo()
    {
        fallo = true;
    }

    public void End()
    {
        print(fallo + " // " + distance);
        car.SetVelocidadMaxima(speedRef);
        ResultReady?.Invoke(fallo, distance);
    }

    public void DestroyAnimators()
    {
        Destroy(rueda1);
        Destroy(rueda2);
        Destroy(rueda3);
        Destroy(rueda4);
    }
}
