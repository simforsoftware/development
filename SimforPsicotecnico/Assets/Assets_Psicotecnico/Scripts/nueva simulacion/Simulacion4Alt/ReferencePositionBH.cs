using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ReferencePositionBH : MonoBehaviour
{
    public Action<float> DistanceReady;

    public GameObject target;

    float distance = 0;

    public void CalculateDistance()
    {
        distance = (transform.position - target.transform.position).magnitude;
        DistanceReady?.Invoke(distance);
    }
}
