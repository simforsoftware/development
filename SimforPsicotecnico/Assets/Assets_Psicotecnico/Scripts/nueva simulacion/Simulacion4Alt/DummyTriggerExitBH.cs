using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DummyTriggerExitBH : MonoBehaviour
{
    public Action DummyExit;

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("dummy"))
        {
            DummyExit?.Invoke();
        }
    }
}
