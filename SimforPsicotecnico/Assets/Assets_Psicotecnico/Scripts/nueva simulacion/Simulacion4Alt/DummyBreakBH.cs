using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DummyBreakBH : MonoBehaviour
{
    public Action DummyBreak;

    public CocheInterseccion dummy;


    private void Update()
    {
        if (!dummy) return;

        if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            Break();
        }
    }

    public void Break()
    {
        if (!dummy.listo) return;
        dummy.listo = false;
        dummy.SetSpeed(0);
        DummyBreak?.Invoke();
    }
}
