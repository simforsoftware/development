using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaHelicoptero : MonoBehaviour
{
    public bool vr;
    [SerializeField] GameObject coche;

    private void Update()
    {
        if (vr)
        {
            transform.rotation = Quaternion.identity;
        }
        if(!vr)
        {
            transform.rotation = Quaternion.LookRotation(coche.transform.position - transform.position);
        }
    }
}
