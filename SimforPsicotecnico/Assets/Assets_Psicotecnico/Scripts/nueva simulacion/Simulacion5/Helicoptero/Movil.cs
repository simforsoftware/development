using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movil : MonoBehaviour
{
    float lifeTime;
    bool particle;
    private void Start()
    {
        if (GetComponent<ParticleSystem>()) particle = true;
    }
    void Update()
    {
        if (particle && !GetComponent<ParticleSystem>().isPlaying) Destroy(this.gameObject);
        transform.position -= transform.forward * MovimientoPlano.Speed * Time.deltaTime;
    }
}
