using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopteroManager : ManagerNSimulacion
{
    [SerializeField] GameObject objetivo;
    ControladorHelicoptero cocheController;
    float puntos;

    [SerializeField] float[] rangoAparicionObjetivos = new float[2];
    [SerializeField] float maxDistanciaObjetivos;
    [SerializeField] int objetivosSimultaneos;
    [SerializeField] Vector3 centroPantalla;
    [SerializeField] float areaSegura;
    [SerializeField] int aciertos_por_fallos_para_apto;

    [SerializeField] InfoNuevaSimulacion5 info;
    GameObject lastObjetivo;


    float marcaEspacial;

    ResultNuevaSimulacion1 resultados;
    protected override void StartTest()
    {
        resultados = new ResultNuevaSimulacion1();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        Debug.Log(resultados.fecha);
        Debug.Log(resultados.dateInit);
        if (info)
        {
            duracion_ejercicio = info.duracion;
            maxDistanciaObjetivos = info.maxDistanciaEntreObjetivos;
            objetivosSimultaneos = info.objetivosSimultaneos;
        }
        base.StartTest();
        cocheController = coche.GetComponent<ControladorHelicoptero>();
        
        StartCoroutine(CrearObstaculo());
    }


    public IEnumerator CrearObstaculo()
    {        
        Vector3 nuevaPosicion;
        do
        {
            if (!lastObjetivo) nuevaPosicion = new Vector3(Random.Range(cocheController.margenesx[0], cocheController.margenesx[1]), Random.Range(cocheController.margenesy[0], cocheController.margenesy[1]), transform.position.z);
            else nuevaPosicion = new Vector3(Mathf.Clamp(lastObjetivo.transform.position.x + Random.Range(-maxDistanciaObjetivos, maxDistanciaObjetivos), cocheController.margenesx[0], cocheController.margenesx[1]),
                                                Mathf.Clamp(lastObjetivo.transform.position.y + Random.Range(-maxDistanciaObjetivos, maxDistanciaObjetivos), cocheController.margenesy[0], cocheController.margenesy[1]), transform.position.z);
        } while ((new Vector2(nuevaPosicion.x,nuevaPosicion.y) - new Vector2(centroPantalla.x,centroPantalla.y)).sqrMagnitude < areaSegura * areaSegura);

        GameObject obstaculo = Instantiate(objetivo);
        obstaculo.GetComponent<ObjetivoController>().manager = this;
        obstaculo.transform.position = nuevaPosicion;

        yield return new WaitForSeconds(Random.Range(rangoAparicionObjetivos[0], rangoAparicionObjetivos[1]));

        StartCoroutine(CrearObstaculo());
    }

    public void Puntuar()
    {
        Acierto();
    }

    public override void End()
    {
        /*if (puntos >= puntosMinimos) print("Win");
        else print("Lose");*/

        resultados.time = duracion_ejercicio;
        float reaccion_media = 0;
        /*for (int i = 0; i < lista_tiempos_anticipacion.Count; ++i)
        {
            reaccion_media += lista_tiempos_anticipacion[i];
        }*/
        //reaccion_media = reaccion_media / lista_tiempos_anticipacion.Count;
        resultados.reactionTime = reaccion_media;
        resultados.aciertos = aciertos;
        resultados.fallos = fallos;

        AuxiliarBuild.instance.tipo.Add(14);
        AuxiliarBuild.instance.resultados.Add(resultados);
        //Esto tendria que cambiar al upload tipico
        resultados.apto = true;
        //if (reaccion_media < minTiempoAnticipacion) resultados.apto = false;
        if (aciertos < aciertos_por_fallos_para_apto * fallos) resultados.apto = false;
        resultados.minutos = Mathf.Max((int)(duracion_ejercicio / 60));
        if (duracion_ejercicio % 60 != 0) ++resultados.minutos;
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();

        DataRecorder recorder = this.GetComponent<DataRecorder>();
        if (recorder.cognitiveLoadRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, coloresGraficas.cognitiveLoad));
        if (recorder.heartRateRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, coloresGraficas.hearthRate));
        if (recorder.pupilDilatationRegister.Count > 0) resultados.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, coloresGraficas.pupilDilatation));
        if (recorder.timeEvents.Count > 0) resultados.eventos.Add(new Grafics.eventData(recorder.timeEvents, coloresGraficas.events));

        Debug.Log(resultados.fecha);
        Debug.Log(resultados.dateInit);
        resultados.UploadAlumno();
        resultados.FSubirSession();

        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}
