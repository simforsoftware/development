using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapaController : MonoBehaviour
{
    private void Update()
    {
        transform.position -= transform.forward * MovimientoPlano.Speed * Time.deltaTime;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("limite"))
        {
            transform.position += transform.forward * 100 * 5;
        }
    }
}
