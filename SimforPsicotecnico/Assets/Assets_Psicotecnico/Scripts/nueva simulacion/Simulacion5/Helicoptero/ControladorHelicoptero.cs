using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorHelicoptero : MonoBehaviour
{
    [SerializeField] float margenx, margeny, speed, rotationSpeed;
    [SerializeField] GameObject rotacionDelantera, rotacionSuperior, modelo;

    public float[] margenesx = new float[2];
    public float[] margenesy = new float[2];

    [SerializeField] bool hardMode;

    private void Start()
    {
        margenx /= 2;
        margeny /= 2;


        margenesx[0] = -margenx + 1;
        margenesx[1] = margenx - 1;
        margenesy[0] = -margeny + 3;
        margenesy[1] = margeny + 3 - 2;

    }

    private void Update()
    {
        float x = transform.position.x, y = transform.position.y;

        /// Los valores harcode significal
        /// -1 mitad del tama�o alteral del coche
        /// -2 tama�o vertical del coche
        /// +3 elevacion del area de aparicion de objetos

        if (Input.GetKey(KeyCode.RightArrow))
        {
            x += speed * Time.deltaTime;
            if (x > margenesx[1]) x = margenesx[1];
            if (rotacionSuperior.transform.localPosition.x < 10)
                rotacionSuperior.transform.localPosition += transform.right * rotationSpeed * Time.deltaTime;            
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            x -= speed * Time.deltaTime;
            if (x < margenesx[0]) x = margenesx[0];
            if (rotacionSuperior.transform.localPosition.x > -10)
                rotacionSuperior.transform.localPosition -= transform.right * rotationSpeed * Time.deltaTime;
        }
        if(!Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftArrow))
        {
            if(Mathf.Abs(rotacionSuperior.transform.localPosition.x) > 0.1)
            {
                if (rotacionSuperior.transform.localPosition.x > 0)
                    rotacionSuperior.transform.localPosition -= transform.right * rotationSpeed * Time.deltaTime;
                if (rotacionSuperior.transform.localPosition.x < 0)
                    rotacionSuperior.transform.localPosition += transform.right * rotationSpeed * Time.deltaTime;
            }
        }
        if (Input.GetKey(KeyCode.A))
        {
            y += speed * Time.deltaTime;
            if (y > margenesy[1]) y = margenesy[1];
            if (rotacionDelantera.transform.localPosition.y < 10)
                rotacionDelantera.transform.localPosition += transform.up * rotationSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            y -= speed * Time.deltaTime;
            if (y < margenesy[0]) y = margenesy[0];
            if (rotacionDelantera.transform.localPosition.y > -10 && rotacionDelantera.transform.position.y > 0)
                rotacionDelantera.transform.localPosition -= transform.up * rotationSpeed * Time.deltaTime;
        }
        if (rotacionDelantera.transform.position.y < 0 || !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D))
        {
            if (Mathf.Abs(rotacionDelantera.transform.localPosition.y) > 0.1)
            {
                if (rotacionDelantera.transform.localPosition.y > 0)
                    rotacionDelantera.transform.localPosition -= transform.up * rotationSpeed * Time.deltaTime;
                if (rotacionDelantera.transform.localPosition.y < 0)
                    rotacionDelantera.transform.localPosition += transform.up * rotationSpeed * Time.deltaTime;
            }
        }

        if(!hardMode || transform.position.x != x && transform.position.y != y) transform.position = new Vector3(x, y, transform.position.z);
        modelo.transform.rotation = Quaternion.LookRotation(rotacionDelantera.transform.position - transform.position, rotacionSuperior.transform.position - transform.position);        
    }
}
