using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetivoController : MonoBehaviour
{
    public HelicopteroManager manager;
    [SerializeField] GameObject particulas;

    private void OnTriggerEnter(Collider other)
    {
        print(other.gameObject.name);
        if (other.CompareTag("car"))
        {
            manager.Puntuar();
            Destroy(this.gameObject);
            if (particulas)
            {
                GameObject g = Instantiate(particulas);
                g.transform.position = transform.position;
                g.transform.rotation = transform.rotation;
                g.AddComponent<Movil>();
            }
        }
        if (other.CompareTag("limite"))
        {
            manager.Fallo();
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        transform.position -= transform.forward * MovimientoPlano.Speed * Time.deltaTime;
    }
}
