using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNSimulacion5 : ManagerNSimulacion
{
    [SerializeField] CocheControllerBimanual cocheBimanual;

    [SerializeField] float erroresMaximos, tiempoReaccionMaximo;

    public float tiempoReaccionMedia = 0;
    ResultNuevaSimulacion1 resultado;

    protected override void Start()
    {
        tuto.delegado += StartTest;
        cocheBimanual.activo = false;
    }
    protected override void StartTest()
    {
        cocheBimanual.activo = true;
        resultado = new ResultNuevaSimulacion1();
        resultado.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultado.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        cocheBimanual.SetManager(this);

        terminado = false;
        timer = 0;
        fallos = 0;
        aciertos = 0;
        base.StartTest();
        if (falloAlFrenar) EventManager.StartListening("Freno", Fallo);
        startTime = Time.time;
    }

    public override void End()
    {
        cocheBimanual.End();

        resultado.fallos = cocheBimanual.errores.Count;
        resultado.reactionTime = tiempoReaccionMedia;
        resultado.time = duracion_ejercicio;


        

        AuxiliarBuild.instance.tipo.Add(17);
        AuxiliarBuild.instance.resultados.Add(resultado);
        //Esto tendria que cambiar al upload tipico
        resultado.apto = true;
        if (fallos > erroresMaximos) resultado.apto = false;
        if (resultado.reactionTime > tiempoReaccionMaximo) resultado.apto = false;
       
        resultado.minutos = Mathf.Max((int)(duracion_ejercicio / 60));
        if (duracion_ejercicio % 60 != 0) ++resultado.minutos;
        resultado.nombreEjrcicio = ScenesSelector.GetCurrentScene();


        if (recorder.cognitiveLoadRegister.Count > 0) resultado.graficas.Add(new Grafics.graficasData(recorder.cognitiveLoadRegister, recorder.timesCL, coloresGraficas.cognitiveLoad));
        if (recorder.heartRateRegister.Count > 0) resultado.graficas.Add(new Grafics.graficasData(recorder.heartRateRegister, recorder.timesHR, coloresGraficas.hearthRate));
        if (recorder.pupilDilatationRegister.Count > 0) resultado.graficas.Add(new Grafics.graficasData(recorder.pupilDilatationRegister, recorder.timesPD, coloresGraficas.pupilDilatation));
        if (recorder.timeEvents.Count > 0) resultado.eventos.Add(new Grafics.eventData(recorder.timeEvents, coloresGraficas.events));

        resultado.UploadAlumno();
        resultado.FSubirSession();

        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}
