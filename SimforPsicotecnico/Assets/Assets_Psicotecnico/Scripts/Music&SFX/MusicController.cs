using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    [SerializeField] AudioClip clip;
    private void Start()
    {
        if (!clip)
        {
            if (MusicInstance.instance) Destroy(MusicInstance.instance.gameObject);
        }
        else
        {
            if (!MusicInstance.instance) Instantiate(Resources.Load<GameObject>("Music"));

            AudioSource audio = MusicInstance.instance.GetComponent<AudioSource>();
            if (audio.clip != clip)
            {
                audio.clip = clip;
                audio.time = 0;
            }
        }
    }
}
