using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracker : MonoBehaviour
{

    public static Tracker instance;

    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);
    }
}
