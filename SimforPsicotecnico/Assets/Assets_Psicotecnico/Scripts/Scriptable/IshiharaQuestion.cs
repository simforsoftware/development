using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ishahara", menuName = "ScriptableObjects/Ishahara", order = 1)]

public class IshiharaQuestion : ScriptableObject
{
    [SerializeField] Sprite imagen;
    [SerializeField] string[] respuestas;
    [SerializeField] bool sinRespuesta;

    public Sprite Imagen()
    {
        return imagen;
    }

    public string[] Respuestas()
    {
        int cifras = respuestas[0].Length;
        List<string> thisRespuestas = new List<string>();
        for (int i = 0; i < respuestas.Length; i++)
        {
            if (respuestas[i] == "rnd")
            {                
                int rndInt = 0;
                do
                {
                    switch (cifras)
                    {
                        case 1:
                            rndInt = Random.Range(0, 10);
                            break;
                        case 2:
                            rndInt = Random.Range(10, 100);
                            break;
                        default:
                            rndInt = Random.Range(0, 100);
                            break;
                    }
                } while (thisRespuestas.Contains(rndInt.ToString()));
                thisRespuestas.Add(rndInt.ToString());
            }
            else
            {
                thisRespuestas.Add(respuestas[i]);
            }
        }

        return thisRespuestas.ToArray();
    }

    public bool SinRespuesta()
    {
        return sinRespuesta;
    }
}
