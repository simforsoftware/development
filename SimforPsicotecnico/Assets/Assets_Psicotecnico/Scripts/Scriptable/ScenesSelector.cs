using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Script Encargado de gestionar el flujo entre escenas
public static class ScenesSelector
{
    public static bool VR_actived = false;

    public static Scenes lastScene, sceneBefore;

    //Funcion que te lleva a la escena especificada por enumerador
    public static void GoToScene(Scenes escena)
    {
        string sceneName = "";
        switch (escena)
        {
            case Scenes.MainMenu:
                sceneName = "MainMenu";
                break;
            case Scenes.SeleccionModulo:
                sceneName = "SeleccionModulo";
                break;
            case Scenes.SeleccionEjercicio:
                sceneName = "SeleccionEjercicio";
                break;
            case Scenes.Test1:
                sceneName = "Test1";
                break;
            case Scenes.Test2:
                sceneName = "Test2";
                break;
            case Scenes.Test3:
                sceneName = "Test3";
                break;
            case Scenes.Test4:
                sceneName = "Test4";
                break;
            case Scenes.Test5:
                sceneName = "Test5";
                break;
            case Scenes.Test6:
                sceneName = "Test6";
                break;
            case Scenes.Test7:
                sceneName = "Test7";
                break;
            case Scenes.Login:
                sceneName = "Login";
                break;
            case Scenes.Registro:
                sceneName = "Register";
                break;
            case Scenes.Resultado:
                sceneName = "Resultado";
                break;
            case Scenes.Historial:
                sceneName = "Historial";
                break;
            case Scenes.InfoUsuario:
                sceneName = "infoUsuario";
                break;
            case Scenes.CambiarContrasena:
                sceneName = "CambiarContraseņa";
                break;
            case Scenes.Simulacion1:
                sceneName = "Simulacion1";
                break;
            case Scenes.Simulacion2:
                sceneName = "Simulacion2";
                break;
            case Scenes.Simulacion3:
                sceneName = "Simulacion3";
                break;
            case Scenes.Simulacion4:
                sceneName = "Simulacion4";
                break;
            case Scenes.Simulacion5:
                sceneName = "Simulacion5";
                break;
            case Scenes.Test8:
                sceneName = "Test8";
                break;
            case Scenes.NuevaSimulacion1:
                sceneName = "new simulacion 1";
                break;
            case Scenes.NuevaSimulacion2:
                sceneName = "new simulacion 2";
                break;
            case Scenes.NuevaSimulacion3:
                sceneName = "new simulacion 3";
                break;
            case Scenes.NuevaSimulacion4:
                sceneName = "new simulacion 4";
                break;
            case Scenes.NuevaSimulacion5:
                sceneName = "new simulacion 5";
                break;
            case Scenes.NuevaSimulacion5Alt:
                sceneName = "new simulacion 5 alt";
                break;
            case Scenes.SeleccionMultisesion:
                sceneName = "SeleccionMultisesion";
                break;
            case Scenes.InstructorMultisesion:
                sceneName = "InstructorMultisesion";
                break;
            case Scenes.EdicionMultisesion:
                sceneName = "EdicionMultisesion";
                break;
            case Scenes.Ishihara:
                sceneName = "TestIshihara";
                break;
            case Scenes.Daltonismo:
                sceneName = "TestDaltonismo";
                break;
            case Scenes.SeleccionIdioma:
                sceneName = "SeleccionIdioma";
                break;
            case Scenes.TestESenllen:
                sceneName = "TestE_Snellen";
                break;
            case Scenes.SeleccionModuloCompleto:
                sceneName = "SeleccionModulosCompletos";
                break;
            case Scenes.NuevaSimulacion3Alt:
                sceneName = "new simulacion 3 alt";
                break;            
            case Scenes.Test3Alt:
                sceneName = "Test3Alt";
                break;
            case Scenes.Creditos:
                sceneName = "Creditos";
                break;
        }

        if (sceneName != "")
        {
            SceneManager.LoadScene(sceneName);
            sceneBefore = lastScene;
            lastScene = escena;
            RevisarEscena();
        }
    }

    public static bool IsCurrentScene(Scenes escena)
    {
        return lastScene == escena;
    }

    public static Scenes CurrentScene()
    {
        return lastScene;
    }
    public static string GetCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }
    static void RevisarEscena()
    {
        switch (lastScene)
        {
            case Scenes.SeleccionMultisesion:
                BotonAtras.inMultisesion = true;
                break;
            case Scenes.SeleccionModuloCompleto:
                BotonAtras.inMultisesion = true;
                break;
            case Scenes.MainMenu:
                BotonAtras.inMultisesion = false;
                break;
        }
    }
}


//Lista de todas las escenas del juego
public enum Scenes
{
    MainMenu = 0,
    SeleccionModulo = 1,
    SeleccionEjercicio = 2,
    Test1 = 3,
    Test2 = 4,
    Test3 = 5,
    Login = 6,
    Registro = 7,
    Resultado = 8,
    Historial = 9,
    InfoUsuario = 10,
    CambiarContrasena = 11,
    Test5  = 12,
    Test4 = 13,
    Test6 = 14,
    Test7 = 15,
    Simulacion1 = 16,
    Simulacion2 = 17,
    Simulacion3 = 18,
    Simulacion4 = 19,
    Simulacion5 = 20,
    Test8 = 21,
    NuevaSimulacion1 = 22,
    NuevaSimulacion2 = 23,
    NuevaSimulacion3 = 24,
    NuevaSimulacion4 = 25,
    NuevaSimulacion5 = 26,
    NuevaSimulacion5Alt = 27,
    SeleccionMultisesion = 28,
    InstructorMultisesion = 29,
    EdicionMultisesion = 30,
    Ishihara = 31,
    Daltonismo = 32,
    SeleccionIdioma = 33,
    TestESenllen = 34,
    SeleccionModuloCompleto = 35,
    NuevaSimulacion3Alt = 36,
    Test3Alt = 37,
    NuevaSimulacion4Alt = 38,
    Creditos = 39,
}
