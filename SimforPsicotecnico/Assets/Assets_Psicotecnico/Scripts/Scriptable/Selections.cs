using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Informacion del modulo y ejercicio seleccionados
[CreateAssetMenu(fileName = "Selections", menuName = "ScriptableObjects/Selections", order = 1)]
public class Selections : ScriptableObject
{
    public Modulo activeModulo;
    //public InfoTest activeTest;  -> Por ahora no es necesaria esta informacion
}
