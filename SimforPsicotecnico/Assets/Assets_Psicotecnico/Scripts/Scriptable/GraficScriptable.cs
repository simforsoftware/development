using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Grafics
{
    public class graficasData
    {
        public List<float> values, tiempos;
        public coloresGraficas color;

        public graficasData(List<float> v, List<float> t, coloresGraficas c)
        {
            values = v;
            tiempos = t;
            color = c;
        }
    }

    public class eventData
    {
        public List<float> tiempos;
        public coloresGraficas color;

        public eventData(List<float> t, coloresGraficas c)
        {
            tiempos = t;
            color = c;
        }
    }
    public static List<graficasData> graficas;
    public static List<eventData> eventos;
    public static int minutos;
}
