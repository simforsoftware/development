using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultNuevaSimulacion1 : Result
{

    public string nombreEjrcicio;
    public float time;
    public bool apto;
    public float reactionTime;
    public int aciertos;
    public int fallos;



    public List<Grafics.graficasData> graficas = new List<Grafics.graficasData>();
    public List<Grafics.eventData> eventos = new List<Grafics.eventData>();
    public int minutos;

    bool green = false;
    bool blue = false;
    bool red = false;
    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }


    public void FSubirSession()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        ResultNewSimulacionDB resultDB = new ResultNewSimulacionDB();

        resultDB.addData(sessions.GetLastSessionID(), time, reactionTime, aciertos, fallos, minutos, graficas, eventos);
        

    }

}
