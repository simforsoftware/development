 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Data;

//Informacion sobre los resultados de los ejercicios 1, 2 y 3
public class Result123 : Result
{
    public string nombreEjrcicio;
    public float time;
    public bool apto;
    public float reactionTime;
    public int aciertos;
    public int fallos;

    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }

    
    public void FSubirSession()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        Result123DB resultados123 = new Result123DB();
        resultados123.addData(sessions.GetLastSessionID(), time.ToString("F2"), reactionTime.ToString("F2"), aciertos, fallos);

        IDataReader reader = sessions.GetAllData();
        /*while (reader.Read())
        {
            for(int i = 0; i < reader.FieldCount; ++i)
            {
                Debug.Log(reader.GetName(i) + " : " + reader[i]);
            }
        }*/

        /*reader = resultados123.GetAllData();
        while (reader.Read())
        {
            for (int i = 0; i < reader.FieldCount; ++i)
            {
                Debug.Log(reader.GetName(i) + " : " + reader[i]);
            }
        }*/
        reader.Close();
        sessions.Close();
        resultados123.Close();
    }

    //DEPRECATED
    /*
    public IEnumerator SubirSesion()
    {
        WWWForm form = new WWWForm();
        form.AddField("tipo", 123);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());
        form.AddField("tiempo", time.ToString("F2"));
        form.AddField("tiempoReaccion", reactionTime.ToString("F2"));
        form.AddField("aciertos", aciertos);
        form.AddField("fallos", fallos);

        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/
}
