using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Data;
public class Result4 : Result
{
    public string nombreEjrcicio;

    public bool apto;

    public int aciertos;
    public int fallos;
    public float tiempoReaccion;
    public float tiempoTotal;


    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }
    public void FSubirSesion()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        Result4DB resultados4 = new Result4DB();
        resultados4.addData(sessions.GetLastSessionID(), tiempoTotal.ToString("F2"), tiempoReaccion.ToString("F2"), aciertos, fallos);

        IDataReader reader = sessions.GetAllData();
       

        reader = resultados4.GetAllData();
        
        reader.Close();
        sessions.Close();
        resultados4.Close();
    }
    //DEPRECATED
    /*
    public IEnumerator SubirSesion()
    {
        Debug.Log("me cabreo");
        Debug.Log(dateInit);
        Debug.Log(dateEnd);
        Debug.Log(nombreEjrcicio);
        Debug.Log(apto.ToString());
        WWWForm form = new WWWForm();
        form.AddField("tipo", 4);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());
        form.AddField("aciertos", aciertos);


        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/
}
