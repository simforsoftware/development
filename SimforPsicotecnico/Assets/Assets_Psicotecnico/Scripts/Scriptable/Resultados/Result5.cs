using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Data;

public class Result5 : Result
{
    public string nombreEjrcicio;
    public float time;
    public bool apto;
    public int fallos;
    public float reactionTime;

    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }

    public void FSubirSesion()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        Result5DB resultados5 = new Result5DB();
        resultados5.addData(sessions.GetLastSessionID(), time.ToString("F2"), reactionTime.ToString("F2"), fallos);

        IDataReader reader = sessions.GetAllData();


 
        reader.Close();
        sessions.Close();
        resultados5.Close();
    }
    //DEPRECATED
    /*
    public IEnumerator SubirSesion()
    {
        Debug.Log("me cabreo");
        Debug.Log(dateInit);
        Debug.Log(dateEnd);
        Debug.Log(nombreEjrcicio);
        Debug.Log(apto.ToString());
        WWWForm form = new WWWForm();
        form.AddField("tipo", 5);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());
        form.AddField("tiempo", time.ToString("F2"));
        form.AddField("tiempoReaccion", reactionTime.ToString("F2"));
        form.AddField("fallos", fallos);

        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/
}
