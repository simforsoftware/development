using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ReadOnly", menuName = "ScriptableObjects/ReadOnly", order = 1)]
public class ReadOnlyResultados : ScriptableObject
{

    public Result resultPadre;
}
