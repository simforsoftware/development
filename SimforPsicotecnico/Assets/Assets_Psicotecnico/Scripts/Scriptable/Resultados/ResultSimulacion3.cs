using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ResultSimulacion3 : Result
{
    //duracion
    //tipos de fallos
    //semaforo, choque salida, velocidad superior, velocidad inferior
    public float duracion;
    public float semaforoFallos;
    public float choqueFallos;
    public float salidaFallos;
    public float velocidadSuperiorFallos;
    public float veloidadInferiorFallos;
    public string nombreEjrcicio;
    public bool apto;
    public float MaxHeartRate;
    public float MinHeartRate;


    public  List<Grafics.graficasData> graficas = new List<Grafics.graficasData>();
    public List<Grafics.eventData> eventos = new List<Grafics.eventData>();
    public  int minutos;

    bool green = false;
    bool blue = false;
    bool red = false;


    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }

    public IEnumerator SubirSesion()
    {
        Debug.Log(veloidadInferiorFallos);
        Debug.Log(velocidadSuperiorFallos);
        Debug.Log(nombreEjrcicio);
        Debug.Log(apto.ToString());
        Debug.Log(MaxHeartRate);
        Debug.Log(MinHeartRate);

        WWWForm form = new WWWForm();
        form.AddField("tipo", 7);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());
        form.AddField("duracion", duracion.ToString("F2"));
        form.AddField("choqueFallos", choqueFallos.ToString());
        form.AddField("velocidadInferior", veloidadInferiorFallos.ToString());
        form.AddField("velocidadSuperior", velocidadSuperiorFallos.ToString());
        form.AddField("semaforoFallos", semaforoFallos.ToString());
        form.AddField("fueraDePista", salidaFallos.ToString());
        form.AddField("MaxHeart", MaxHeartRate.ToString());
        form.AddField("MinHeart", MinHeartRate.ToString());
        form.AddField("AverageHeart", (MaxHeartRate - MinHeartRate).ToString());

        foreach (Grafics.graficasData g in graficas)
        {
            string v = "";
            string t = "";

            for (int i = 0; i < g.values.Count; ++i)
            {
                v += g.values[i].ToString("F0");
                t += g.tiempos[i].ToString("F0");
                if(i < g.values.Count - 1)
                {
                    v += ":";
                    t += ":";
                }
            }

            if (g.color == coloresGraficas.hearthRate)
            {
                form.AddField("HRV", v);
                form.AddField("HRT", t);
                
                red = true;

            }
            else if(g.color == coloresGraficas.pupilDilatation)
            {
                form.AddField("PV", v);
                form.AddField("PT", t);

                blue = true;
            }
            else if(g.color == coloresGraficas.cognitiveLoad)
            {
                form.AddField("CV", v);
                form.AddField("CT", t);

                green = true;
            }

        }

        if (!red)
        {
            form.AddField("HRV", "VACIO");
            form.AddField("HRT", "VACIO");

        }
        if (!blue)
        {
            form.AddField("PV", "VACIO");
            form.AddField("PT", "VACIO");

        }
        if (!green)
        {
            form.AddField("CV", "VACIO");
            form.AddField("CT", "VACIO");

        }
        form.AddField("HRM", minutos);
        Debug.Log(eventos.Count);
        Debug.Log(eventos[0].tiempos.Count);

        if(eventos.Count == 0)
        {
            form.AddField("ET", "VACIO");
        }
        else
        {
            string et = "";
            for (int i = 0; i < eventos[0].tiempos.Count; ++i)
            {

                et += eventos[0].tiempos[i].ToString("F0");
                if (i < eventos[0].tiempos.Count - 1)
                {
                    et += ":";
                }
            }
            form.AddField("ET", et);
        }

        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }

}
