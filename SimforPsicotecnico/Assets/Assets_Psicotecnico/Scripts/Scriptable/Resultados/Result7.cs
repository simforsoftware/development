using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Result7 : Result
{

    public float time;
    public bool apto;
    public int linea;
    public string nombreEjrcicio;

    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }

    public void FSubirSession()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        Result7DB resultados7 = new Result7DB();

        resultados7.addData(sessions.GetLastSessionID(), linea);

        sessions.Close();
        resultados7.Close();
    }

    public void IshiharaFSubir()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());


        IshiharaDB ishDB = new IshiharaDB();

        ishDB.addData(sessions.GetLastSessionID(), linea);

        sessions.Close();
        ishDB.Close();
    }
    //DEPRECATED
    /*
    public IEnumerator SubirSesion()
    {
        Debug.Log("me cabreo");


        WWWForm form = new WWWForm();
        form.AddField("tipo", 6);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());
        form.AddField("linea", linea.ToString());

        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/
}
