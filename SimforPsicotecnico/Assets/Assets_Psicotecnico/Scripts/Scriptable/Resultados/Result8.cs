using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Result8 : Result
{
    public bool apto;
    public string nombreEjrcicio;
    public float iz250, iz500, iz1000, iz2000, iz4000, iz8000;
    public float de250, de500, de1000, de2000, de4000, de8000;



    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }

    public void FSubirSesion()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        Result8DB resultados8 = new Result8DB();

        resultados8.addData(sessions.GetLastSessionID(), iz250, iz500, iz1000, iz2000, iz4000, iz8000, de250, de500, de1000, de2000, de4000, de8000);

        sessions.Close();
        resultados8.Close();
    }

    //DEPRECATED
    /*
    public IEnumerator SubirSesion()
    {
        Debug.Log("me cabreo");


        WWWForm form = new WWWForm();
        form.AddField("tipo", 8);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());

        form.AddField("iz250", iz250.ToString());
        form.AddField("iz500", iz500.ToString());
        form.AddField("iz1000", iz1000.ToString());
        form.AddField("iz2000", iz2000.ToString());
        form.AddField("iz4000", iz4000.ToString());
        form.AddField("iz8000", iz8000.ToString());

        form.AddField("de250", de250.ToString());
        form.AddField("de500", de500.ToString());
        form.AddField("de1000", de1000.ToString());
        form.AddField("de2000", de2000.ToString());
        form.AddField("de4000", de4000.ToString());
        form.AddField("de8000", de8000.ToString());



        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/


}
