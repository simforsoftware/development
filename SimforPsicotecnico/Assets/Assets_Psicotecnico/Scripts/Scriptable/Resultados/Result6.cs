using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Data;


public class Result6 : Result
{

    public float time;
    public bool apto;
    public string nombreEjrcicio;
    public float neuroticismo, paranoia, agitacion, inestabilidad, psicastenia, hipocondria, suicidio;
    
    public void UploadAlumno()
    {
        //Result alumnoData = Resources.Load<Result>("Result");
        IDAlumno = AuxiliarBuild.instance.IDAlumno;
        IDGrupo = AuxiliarBuild.instance.IDGrupo;
    }

    public void FSubirSesion()
    {
        SessionsDB sessions = new SessionsDB();

        sessions.addData(IDAlumno, IDGrupo, fecha, dateInit, nombreEjrcicio, apto.ToString());

        Result6DB resultados6 = new Result6DB();
        
        resultados6.addData(sessions.GetLastSessionID(), neuroticismo, paranoia, agitacion, inestabilidad, psicastenia, hipocondria, suicidio);

        sessions.Close();
        resultados6.Close();
    }

    //DEPRECATED
    /*
    public IEnumerator SubirSesion()
    {
        Debug.Log("me cabreo");


        WWWForm form = new WWWForm();
        form.AddField("tipo", 0);
        form.AddField("idAlumno", IDAlumno);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("grupo", IDGrupo);
        form.AddField("fechaInit", dateInit);
        form.AddField("fechaEnd", dateEnd);
        form.AddField("fecha", fecha);
        form.AddField("ejercicio", nombreEjrcicio);
        form.AddField("estado", apto.ToString());


        //CAMBIAR ESTO AL NUEVO PHP
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionUpload.php", form))
        {

            yield return www.Send();
            Debug.Log("yay");
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());

            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/

}
