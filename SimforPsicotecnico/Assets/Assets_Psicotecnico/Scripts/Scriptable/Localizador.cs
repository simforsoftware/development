using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public static class Localizador
{
    public enum Idioma
    {
        espa�ol = 1,
        ingles = 2,
        italiano = 3,
        frances = 4,
        chino = 5,
    }

    static List<string[]> textos = new List<string[]>();
    public static Idioma idioma = Idioma.espa�ol;
    static bool actualizado;
    public static void ActualizarTextos(string textoBase = "")
    {
        if(textoBase == "")
        {
            string pathName = Path.Combine(Application.streamingAssetsPath, "textos");
            FileStream loc = File.Open(pathName, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(loc);
            textoBase = sr.ReadToEnd();
            sr.Close();
            loc.Close();
        }
        string[] compartimentos = textoBase.Split('[');
        for (int i = 0; i < compartimentos.Length; i++)
        {
            textos.Add(compartimentos[i].Split(','));
        }
        foreach(string[] sarr in textos)
        {
            if (sarr.Length > 0 && sarr[0].Length > 1) sarr[0] = sarr[0].Replace("\n", "f");
        }
        foreach(string[] texto in textos)
        {
            texto[0].Trim();            
        }
        actualizado = true;
    }

    public static string Texto(string ID)
    {
        if (ID == null) return "):";
        if (!actualizado) ActualizarTextos();
        foreach(string[] texto in textos)
        {            
            if(texto[0] != "" &&  texto[0].Contains(ID))
            {
                string s = texto[(int)idioma];
                s = s.Replace('"', ' ');
                s = s.Replace('*', ',');
                return s;
            }
        }
        return ID;
    }

    
}
