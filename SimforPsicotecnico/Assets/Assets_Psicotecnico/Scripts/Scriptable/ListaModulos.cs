using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Lista de modulos habilitados para realizarse
[CreateAssetMenu(fileName = "ListaModulos", menuName = "ScriptableObjects/ListaModulos", order = 1)]
public class ListaModulos : ScriptableObject
{
    public List<Modulo> tests;    
}
