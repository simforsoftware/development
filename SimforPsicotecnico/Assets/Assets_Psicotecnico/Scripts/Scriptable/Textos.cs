using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Sistema de locacizacion de textos
[CreateAssetMenu(fileName = "Textos", menuName = "ScriptableObjects/Textos", order = 1)]
public class Textos : ScriptableObject
{
    //Individuales
    public string

        //Main Menu
        menuTitulo,
        menuBotonModulos,
        menuBotonHistorial,
        menuBotonUsuarion,

        //Botones Generales
        salirApp,
        botonAtras,
        logIn,
        registroarse,

        //Menu Modulos
        menuModuloTitulo,

        //Menu Ejercicios
        menuEjercicioTitulo,

        //Info Usuario
        infoUsuarioTitulo,

        //Resultado
        resultadoTitulo,
        tiempoTotal,
        tiempoReaccion,
        aciertos,
        fallos,
        apto,

        //Historial
        historialTitulo,
        id,
        fechaInicio,
        ejercicio,
        grupo,
        fechaFinal,
        fecha,
        horaInicio,
        horaFinal,

        //Contraseņa
        nueva,
        antigua,
        cambioContraseņaTitulo,


        //Input fields
        escribaAqui,
        nombre,
        apellido,
        instructor,
        passwordIntructor,
        email,
        password,

        //Inputs
        pieDerecho,
        pieIzquierdo,
        manoDerecha,
        manoIzquierda,

        //datos Simulacion
        duracion,
        falloSalida,
        fallosChoque,
        fallosVelocidadMin,
        fallosVelocidadMax,
        semaforoFallos,
        minHeartRate,
        maxHeartRate,
        averageHeartRate;



    //Arrays
    public string[]

        //Modulos
        modulosNombre,
        ejerciciosNombre,

        //Ejercicios
        descripcionEjercicio,
        descripcionBreveEjercicio;


    public string GetText(Texto t)
    {
        switch (t)
        {
            //Main Menu
            case Texto.MainMenuTitulo:
                return menuTitulo;
            case Texto.MainMenuBotonModulo:
                return menuBotonModulos;
            case Texto.MainMenuBotonHistorial:
                return menuBotonHistorial;
            case Texto.MainMenuBotonUsuario:
                return menuBotonUsuarion;

            //Botones Generales
            case Texto.BotonSalir:
                return salirApp;
            case Texto.BotonAtras:
                return botonAtras;
            case Texto.LogIn:
                return logIn;
            case Texto.Registrarse:
                return registroarse;

            //Info Usuario
            case Texto.InfoUsuarioTitulo:
                return infoUsuarioTitulo;

            //Resultado
            case Texto.ResultadoTitulo:
                return resultadoTitulo;
            case Texto.TiempoTotal:
                return tiempoTotal;
            case Texto.TiempoReaccion:
                return tiempoReaccion;
            case Texto.Aciertos:
                return aciertos;
            case Texto.Fallos:
                return fallos;
            case Texto.Apto:
                return apto;

            //Historial
            case Texto.HistorialTitulo:
                return historialTitulo;
            case Texto.ID:
                return id;
            case Texto.FechaInicio:
                return fechaInicio;
            case Texto.FechaFinal:
                return fechaFinal;
            case Texto.Ejercicio:
                return ejercicio;
            case Texto.Grupo:
                return grupo;
            case Texto.Fecha:
                return fecha;
            case Texto.HoraInicio:
                return horaInicio;
            case Texto.HoraFinal:
                return horaFinal;

            //Input Fields
            case Texto.EscribaAqui:
                return escribaAqui;
            case Texto.Nombre:
                return nombre;
            case Texto.Apellido:
                return apellido;
            case Texto.Instructor:
                return instructor;
            case Texto.Password:
                return password;
            case Texto.PasswordInstructor:
                return passwordIntructor;
            case Texto.Email:
                return email;

            //Menu Modulos
            case Texto.MenuModuloTitulo:
                return menuModuloTitulo;

            //Menu Ejercicios
            case Texto.MenuEjercicioTitulo:
                return menuEjercicioTitulo;

            //Modulos
            case Texto.Modulo1Nombre:
                return modulosNombre[0];

            //ejercicios
            case Texto.Ejercicio1Nombre:
                return ejerciciosNombre[0];
            case Texto.Ejercicio1Descripcion:
                return descripcionEjercicio[0];
            case Texto.Ejercicio1Breve:
                return descripcionBreveEjercicio[0];
            case Texto.Ejercicio2Nombre:
                return ejerciciosNombre[1];
            case Texto.Ejercicio2Descripcion:
                return descripcionEjercicio[1];
            case Texto.Ejercicio2Breve:
                return descripcionBreveEjercicio[1];
            case Texto.Ejercicio3Nombre:
                return ejerciciosNombre[2];
            case Texto.Ejercicio3Descripcion:
                return descripcionEjercicio[2];
            case Texto.Ejercicio3Breve:
                return descripcionBreveEjercicio[2];
            case Texto.Ejercicio4Nombre:
                return ejerciciosNombre[3];
            case Texto.Ejercicio4Descripcion:
                return descripcionEjercicio[3];
            case Texto.Ejercicio4Breve:
                return descripcionBreveEjercicio[3];
            case Texto.Ejercicio5Nombre:
                return ejerciciosNombre[4];
            case Texto.Ejercicio5Descripcion:
                return descripcionEjercicio[4];
            case Texto.Ejercicio5Breve:
                return descripcionBreveEjercicio[4];

            //Inputs
            case Texto.PieDerecho:
                return pieDerecho;
            case Texto.PieIzquierdo:
                return pieIzquierdo;
            case Texto.ManoDerecha:
                return manoDerecha;
            case Texto.ManoIzquierda:
                return manoIzquierda;

            case Texto.Nueva:
                return nueva;
            case Texto.Antigua:
                return antigua;
            case Texto.CambioContraseņaTitulo:
                return cambioContraseņaTitulo;

            //Simulaciones
            case Texto.Simulacion1Nombre:
                return ejerciciosNombre[5];

            //Simulaciones fallos
            case Texto.Duracion:
                return duracion;
            case Texto.FallosChoque:
                return fallosChoque;
            case Texto.FallosSalida:
                return falloSalida;
            case Texto.FalloSemaforo:
                return semaforoFallos;
            case Texto.FalloVelocidadMin:
                return fallosVelocidadMin;
            case Texto.FalloVelocidadMax:
                return fallosVelocidadMax;
            case Texto.MinHearRate:
                return minHeartRate;
            case Texto.MaxHeartRate:
                return maxHeartRate;
            case Texto.AverageHearRate:
                return averageHeartRate;
            case Texto.Ejercicio8Nombre:
                return ejerciciosNombre[6];
        }

        return "";
    }    
}

public enum Texto
{
    //MainMenu
    MainMenuTitulo,
    MainMenuBotonModulo,
    MainMenuBotonHistorial,
    MainMenuBotonUsuario,

    //BotonesGenerales
    BotonSalir,
    BotonAtras,
    LogIn,
    Registrarse,

    //MenuModulos
    MenuModuloTitulo,

    //MenuEjercicios
    MenuEjercicioTitulo,

    //Info Usuario
    InfoUsuarioTitulo,

    //Resultado
    ResultadoTitulo,
    TiempoTotal,
    TiempoReaccion,
    Aciertos,
    Fallos,
    Apto,

    //Historial
    HistorialTitulo,
    ID,
    FechaInicio,
    Ejercicio,
    Grupo,
    FechaFinal,

    //Input Fields
    EscribaAqui,
    Nombre,
    Apellido,
    Instructor,
    PasswordInstructor,
    Email,
    Password,

    //Modulos
    Modulo1Nombre,

    //Ejercicios
    Ejercicio1Nombre,
    Ejercicio1Descripcion,
    Ejercicio2Nombre,
    Ejercicio2Descripcion,
    Ejercicio3Nombre,
    Ejercicio3Descripcion,

    //Inputs
    PieDerecho,
    PieIzquierdo,
    ManoDerecha,
    ManoIzquierda,

    //Adicionales
    Fecha,

    //Descripciones Breves
    Ejercicio1Breve,
    Ejercicio2Breve,
    Ejercicio3Breve,

    //Adicionales 2 (este sistema empiezo a verlo con otros ojos)
    HoraInicio,
    HoraFinal,

    //CambioContraseņa
    Nueva,
    Antigua,
    CambioContraseņaTitulo,

    //Test5
    Ejercicio5Nombre,
    Ejercicio5Descripcion,
    Ejercicio5Breve,

    //Test4
    Ejercicio4Nombre,
    Ejercicio4Descripcion,
    Ejercicio4Breve,

    //Simulacion1
    Simulacion1Nombre,

    //Simulacion
    Duracion,
    FallosSalida,
    FallosChoque,
    FalloVelocidadMin,
    FalloVelocidadMax,
    FalloSemaforo,
    MinHearRate,
    MaxHeartRate,
    AverageHearRate,

    //Test8
    Ejercicio8Nombre

        
}
