using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Version para los test 1 y 2
[CreateAssetMenu(fileName = "Test12", menuName = "ScriptableObjects/InfoTest12", order = 1)]
public class InfoTest12 : InfoTest
{
    public int repeticiones;
    public float timePerSignal;
    public int maxFallos;
    public float minTimepoReaccion;
}
