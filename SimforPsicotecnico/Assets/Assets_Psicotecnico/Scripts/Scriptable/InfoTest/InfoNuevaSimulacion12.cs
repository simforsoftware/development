using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NuevaSimulacion1", menuName = "ScriptableObjects/NuevaSimulacion1", order = 1)]
public class InfoNuevaSimulacion12 : InfoTest
{
     public float duracionEjercicio;
    public int aciertosPorFalloAprovado;
    public float minTiempoAnticipacion;
}
