using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NuevaSimulacion5", menuName = "ScriptableObjects/NuevaSimulacion5", order = 3)]
public class InfoNuevaSimulacion5 : InfoTest
{
    public float duracion;
    public float maxDistanciaEntreObjetivos;
    public int objetivosSimultaneos;

}
