using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NuevaSimulacion3", menuName = "ScriptableObjects/NuevaSimulacion3", order = 2)]
public class InfoNuevaSimulacion3 : InfoTest
{

    public float duracion;
    public float tiempoAmbar;
    public float tiempoRojo;
    public float[] rangoTiemposVerde = new float[2];
    public float minTiempoReaccion;
    public float maxFallos;

}
