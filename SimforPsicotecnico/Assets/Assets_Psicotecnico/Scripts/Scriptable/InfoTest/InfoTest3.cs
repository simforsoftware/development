using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Version para el test 3
[CreateAssetMenu(fileName = "Test3", menuName = "ScriptableObjects/InfoTest3", order = 1)]
public class InfoTest3 : InfoTest
{
    public int repeticiones;
    public float timePerSignal1, timePerSignal2;
    public int maxFallos;
    public float minTimepoReaccion;
}

