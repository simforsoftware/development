using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Test5", menuName = "ScriptableObjects/InfoTest5", order = 1)]
public class InfoTest5 : InfoTest
{
    public float tiempo;
    public int fallos;
    public float tiempoReaccion;
}
