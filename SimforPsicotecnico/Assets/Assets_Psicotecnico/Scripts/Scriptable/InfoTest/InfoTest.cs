using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Script del que heredan los scripts con la informacion de cada test
[CreateAssetMenu(fileName = "InfoTest", menuName = "ScriptableObjects/InfoTest", order = 1)]
public class InfoTest : ScriptableObject
{
    public string nombre;
    public Scenes escena;
    public Sprite icono;
}
