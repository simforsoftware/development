using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Test4", menuName = "ScriptableObjects/InfoTest4", order = 1)]
public class InfoTest4 : InfoTest
{
    public int repeticiones, aciertosNecesarios;
    public float tiempo1, tiempo2, margenTemporal;
}
