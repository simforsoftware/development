using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ListIshihara", menuName = "ScriptableObjects/ListaIshihara", order = 1)]
public class ListaIsihara : ScriptableObject
{
    public List<IshiharaGroup> grupos = new List<IshiharaGroup>();
}

[System.Serializable]
public class IshiharaGroup
{
    public List<IshiharaQuestion> preguntas = new List<IshiharaQuestion>();
}
