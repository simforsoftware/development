using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Lista de ejercicios que componen un modulo
[CreateAssetMenu(fileName = "Modulo", menuName = "ScriptableObjects/Modulo", order = 1)]
public class Modulo : ScriptableObject
{
    public string nombre;
    public List<InfoTest> tests;
    public Sprite icono;
}
