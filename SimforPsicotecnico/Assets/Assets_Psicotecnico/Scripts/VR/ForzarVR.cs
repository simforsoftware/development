using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForzarVR : MonoBehaviour
{

    [SerializeField] bool VR;
    [SerializeField] AudioSource aviso;
    private void Start()
    {
        if (VR && !ScenesSelector.VR_actived)
        {
            ScenesSelector.VR_actived = true;
            if(aviso)aviso.Play();
        }
        else if (!VR && ScenesSelector.VR_actived)
        {
            ScenesSelector.VR_actived = false;
            aviso.Play();
        }
    }
}
