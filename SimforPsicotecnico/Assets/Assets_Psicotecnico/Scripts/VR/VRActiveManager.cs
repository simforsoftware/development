using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VRActiveManager : MonoBehaviour
{
    [SerializeField] GameObject VRrig;
    [SerializeField] GameObject camera;
    [SerializeField] GameObject canvasSelector;
    [SerializeField] GameObject mainCanvas;


    // Start is called before the first frame update
    void Awake()
    {
        UnityEngine.XR.XRSettings.enabled = false;

        if (!VRState.allGood) ScenesSelector.VR_actived = false;

        if (ScenesSelector.VR_actived)
        {
            camera.SetActive(false);
            UnityEngine.XR.XRSettings.enabled = true;
            VRrig.SetActive(true);
            mainCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
            canvasSelector.SetActive(true);
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            ScenesSelector.VR_actived = (!ScenesSelector.VR_actived);
            Cambio();
        }
    }

    private void Cambio()
    {        
        Scene scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);
    }
}
