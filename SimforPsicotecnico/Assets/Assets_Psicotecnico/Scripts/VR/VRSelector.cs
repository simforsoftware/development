using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VRSelector : MonoBehaviour
{
    public GameObject ui;
    public float segundos_para_seleccionar;
    GameObject lastSelected;
    Sprite lastSprite;
    float segundos_selecionando = 0;

    [SerializeField] bool conCooldown = false;
    [SerializeField] float maxCooldown;
    float timer = 0;
    private void Update()
    {
        if (conCooldown)
        {
            timer += Time.deltaTime;
        }

        RaycastHit hit;
        Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity);
        if(hit.collider )Debug.Log(hit.collider.name);
        
       
        if((!conCooldown && Physics.Raycast(transform.position, transform.forward, out hit,Mathf.Infinity) )|| (conCooldown && timer > maxCooldown && Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity)))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            EventManager.TriggerEvent("afknt");
            if (hit.transform.gameObject.GetComponent<Button>() && hit.transform.gameObject.GetComponent<Button>().interactable)
            {
                ui.SetActive(true);
                if (lastSelected == hit.transform.gameObject)
                {
                    /*lastSprite = hit.transform.gameObject.GetComponent<Image>().sprite;
                    Debug.Log(lastSprite);
                    hit.transform.gameObject.GetComponent<Image>().sprite=  hit.transform.gameObject.GetComponent<Button>().spriteState.highlightedSprite;*/
                    hit.transform.gameObject.GetComponent<Button>().Select();
                    
                    segundos_selecionando += Time.deltaTime;
                    ui.GetComponent<Image>().fillAmount = segundos_selecionando / segundos_para_seleccionar;
                    if (segundos_selecionando > segundos_para_seleccionar)
                    {
                        hit.transform.gameObject.GetComponent<Button>().onClick.Invoke();
                        segundos_selecionando = 0;
                        if (conCooldown) timer = 0;
                    }
                }
                else
                {
                   // if(lastSelected) lastSelected.GetComponent<Image>().sprite = lastSprite;
                    lastSelected = hit.transform.gameObject;
                    segundos_selecionando = 0;
                }
            }
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
            ui.SetActive(false);
            //Debug.DrawRay(transform.position - new Vector3(0,-5,0) , transform.forward * 600, Color.green);
            segundos_selecionando = 0;
        }
    }
}
