using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DataRecorder : MonoBehaviour
{
    HPManager device;


    //Heart Rate
    int minHeartRate = int.MaxValue, maxHeartRate = int.MinValue;
    public List<float> heartRateRegister = new List<float>();
    public List<float> timesHR = new List<float>();
    float averageHeartRage;

    //Cognitive Load
    float minCognitiveLoad = float.MaxValue, maxCognitiveLoad = float.MinValue;
    public List<float> cognitiveLoadRegister = new List<float>();
    public List<float> timesCL = new List<float>();
    float averageCognitiveLoad;

    //Pupil Dilatation
    float minPupilDilatation = float.MaxValue, maxPupilDilatation = float.MinValue;
    public List<float> pupilDilatationRegister = new List<float>();
    public List<float> pupilDilaltationBrute = new List<float>();
    public List<float> timesPD = new List<float>();
    float averagePupilDilatation;
    public float timeStart;
    //Eventos
    public List<float> timeEvents = new List<float>();



    void Start()
    {
        device = HPManager.instance;
        timeStart = Time.time;
        if (!device)
        {
            Debug.LogError("EL HP MANAGER NO SE HA SETEADO BIEN EN EL RECORDER");
            VRState.allGood = false;
            return;
        }

        InvokeRepeating("AccumulateHeartRate", 0, 5);
        InvokeRepeating("AccumulateCognitiveLoad", 0, 5);
        InvokeRepeating("BruteAccumulatePupilDilatation", 0, 0.1f);
        InvokeRepeating("AccumulatePupilDilatation", 0, 5);
    }

    public void SaveEvent()
    {

        timeEvents.Add((float)Math.Truncate( Time.time - timeStart));
;
    }




    #region Heart Rate
    // Your code using heart rate messages
    private void AccumulateHeartRate()
    {
        if (!device) return;
        int hr = device.heartRate;
        if (hr < 40 || hr > 350) return;

        // Min Max
        maxHeartRate = maxHeartRate > hr ? maxHeartRate : hr;
        minHeartRate = minHeartRate < hr ? minHeartRate : hr;

        //Average
        heartRateRegister.Add(hr);
        timesHR.Add((float) Math.Truncate (Time.time - timeStart));
        averageHeartRage = 0f;
        foreach (int i in heartRateRegister) averageHeartRage += i;
        averageHeartRage /= (float)heartRateRegister.Count;
    }
    #endregion

    #region Cognitive Load
    // Your code using heart rate messages
    private void AccumulateCognitiveLoad()
    {
        if (!device) return;
        float cl = device.cognitiveLoad;
        if (cl <= 0 || cl > 1) return;

        // Min Max
        maxCognitiveLoad = maxCognitiveLoad > cl ? maxCognitiveLoad : cl;
        minCognitiveLoad = minCognitiveLoad < cl ? minCognitiveLoad : cl;

        //Average
        cognitiveLoadRegister.Add(cl * 100);
        timesCL.Add((float ) Math.Truncate( Time.time - timeStart));
        averageCognitiveLoad = 0f;
        foreach (float f in cognitiveLoadRegister) averageCognitiveLoad += f;
        averageCognitiveLoad /= (float)cognitiveLoadRegister.Count;
    }
    #endregion

    #region Pupil Dilatation
    private void BruteAccumulatePupilDilatation()
    {
        if (!device) return;
        float pd = device.pupilDilatation;

        if (pd < 2 || pd > 8) return;

        //Average
        pupilDilaltationBrute.Add(pd);
    }
    private void AccumulatePupilDilatation()
    {
        if (pupilDilaltationBrute.Count <= 0) return;
        float pd = 0;

        foreach (float f in pupilDilaltationBrute)
        {
            pd += f;
        }

        pd /= pupilDilaltationBrute.Count;

        pupilDilaltationBrute = new List<float>();

        print("Dilatation: " + pd);

        // Min Max
        maxPupilDilatation = maxPupilDilatation > pd ? maxCognitiveLoad : pd;
        minPupilDilatation = minPupilDilatation < pd ? minCognitiveLoad : pd;

        pupilDilatationRegister.Add(pd);
        timesPD.Add((float) Math.Truncate( Time.time - timeStart));
        averagePupilDilatation = 0f;
        foreach (float f in pupilDilatationRegister) averagePupilDilatation += f;
        averagePupilDilatation /= (float)pupilDilatationRegister.Count;
    }
    #endregion

}
