using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRShutdown : MonoBehaviour
{
    private void Awake()
    {
        UnityEngine.XR.XRSettings.enabled = false;
    }
}
