using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class Result8DB : SQLiteHelper
{
    private const string TABLE_NAME = "resultados8";
    private const string KEY_ID = "id";
    private const string KEY_IDSESSION = "idSession";
    private const string KEY_IZ250 = "iz250";
    private const string KEY_IZ500 = "iz500";
    private const string KEY_IZ1000= "iz1000";
    private const string KEY_IZ2000 = "iz2000";
    private const string KEY_IZ4000 = "iz4000";
    private const string KEY_IZ8000 = "iz8000";
    private const string KEY_DE250 = "de250";
    private const string KEY_DE500 = "de500";
    private const string KEY_DE1000 = "de1000";
    private const string KEY_DE2000 = "de2000";
    private const string KEY_DE4000 = "de4000";
    private const string KEY_DE8000 = "de8000";


    public Result8DB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " +
            KEY_IDSESSION + " INT(10) UNIQUE, " +
            KEY_IZ250 + " TEXT, " +
            KEY_IZ500 + " TEXT, " +
            KEY_IZ1000 + " TEXT, " +
            KEY_IZ2000 + " TEXT, " +
            KEY_IZ4000 + " TEXT, " +
            KEY_IZ8000 + " TEXT, " +
            KEY_DE250 + " TEXT, " +
            KEY_DE500 + " TEXT, " +
            KEY_DE1000 + " TEXT, " +
            KEY_DE2000 + " TEXT, " +
            KEY_DE4000 + " TEXT, " +
            KEY_DE8000 + " TEXT )";
        dbcmd.ExecuteNonQuery();
    }

    public void addData(int idSession, float iz250, float iz500, float iz1000, float iz2000, float iz4000, float iz8000, float de250, float de500, float de1000, float de2000, float de4000, float de8000)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);



        Debug.Log("EL ultimo id es" + reader[0]);
        Debug.Log("EL depth de la busqueda de id es " + reader.Depth);
        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_IDSESSION + ", "
            + KEY_IZ250 + ", "
            + KEY_IZ500 + ", "
            + KEY_IZ1000 + ", "
            + KEY_IZ2000 + ", "
            + KEY_IZ4000 + ", "
            + KEY_IZ8000 + ", "
            + KEY_DE250 + ", "
            + KEY_DE500 + ", "
            + KEY_DE1000 + ", "
            + KEY_DE2000 + ", "
            + KEY_DE4000 + ", "
            + KEY_DE8000 + " ) "

            + "VALUES ( "
            + id + ", "
            + idSession + ", '"
            + iz250 + "', '"
            + iz500 + "', '"
            + iz1000 + "', '"
            + iz2000 + "', '"
            + iz4000 + "', '"
            + iz8000 + "', '"
            + de250 + "', '"
            + de500 + "', '"
            + de1000 + "', '"
            + de2000 + "', '"
            + de4000 + "', '"
            + de8000 + "' );";

        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public IDataReader GetAllData()
    {
        return GetAllData(TABLE_NAME);
    }

    public IDataReader GetDataUsingID(int id)
    {

        return GetDataFromIntTypeByInt(id, TABLE_NAME, KEY_IDSESSION);
    }
}
