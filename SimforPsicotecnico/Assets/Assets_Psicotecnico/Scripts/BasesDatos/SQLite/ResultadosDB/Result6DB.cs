using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class Result6DB : SQLiteHelper
{
    private const string TABLE_NAME = "resultados6P";
    private const string KEY_ID = "id"; //0
    private const string KEY_IDSESSION = "idSession"; //1
    private const string KEY_NEUROTICISMO = "neuroticismo"; //2
    private const string KEY_PARANOIA = "paranoia"; //3
    private const string KEY_AGITACION = "agitacion"; //4
    private const string KEY_INESTABILIDAD = "inestabilidad"; //5
    private const string KEY_PSICASTENIA = "psicastenia"; //6
    private const string KEY_HIPOCONDRIA = "hipocondria"; //7
    private const string KEY_SUICIDIO = "suicidio"; //8

    public Result6DB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " +
            KEY_IDSESSION + " INT(10) UNIQUE, " +
            KEY_NEUROTICISMO + " TEXT(10), " +
            KEY_PARANOIA + " TEXT(10), " +
            KEY_AGITACION + " TEXT(10), " +
            KEY_INESTABILIDAD + " TEXT(10), " +
            KEY_PSICASTENIA + " TEXT(10), " +
            KEY_HIPOCONDRIA + " TEXT(10), " +
            KEY_SUICIDIO + " TEXT(10))";
        dbcmd.ExecuteNonQuery();
    }

    public void addData(float idSession, float neuroticismo, float paranoia, float agitacion, float inestabilidad, float psicastenia, float hipocondria, float suicidio)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);



        Debug.Log("EL ultimo id es" + reader[0]);
        Debug.Log("EL depth de la busqueda de id es " + reader.Depth);
        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_IDSESSION + ", "
            + KEY_NEUROTICISMO + ", "
            + KEY_PARANOIA + ", "
            + KEY_AGITACION + ", "
            + KEY_INESTABILIDAD + ", "
            + KEY_PSICASTENIA + ", "
            + KEY_HIPOCONDRIA + ", "
            + KEY_SUICIDIO + " ) "

            + "VALUES ( "
            + id + ", "
            + idSession + ", '"
            + neuroticismo + "', '"
            + paranoia + "', '"
            + agitacion + "', '"
            + inestabilidad + "', '"
            + psicastenia + "', '"
            + hipocondria + "', '"
            + suicidio + "' );";

        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public IDataReader GetAllData()
    {
        return GetAllData(TABLE_NAME);
    }

    public IDataReader GetDataUsingID(int id)
    {

        return GetDataFromIntTypeByInt(id, TABLE_NAME, KEY_IDSESSION);
    }
}
