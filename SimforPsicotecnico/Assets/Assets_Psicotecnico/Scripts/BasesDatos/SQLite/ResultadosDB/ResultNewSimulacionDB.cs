using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class ResultNewSimulacionDB : SQLiteHelper
{

    private const string TABLE_NAME = "resultadosNuevaSimulacion";
    private const string KEY_ID = "id";
    private const string KEY_IDSESSION = "idSession";
    private const string KEY_TIEMPOTOTAL = "tiempoTotal";
    private const string KEY_TIEMPOANTCIPACION = "tiempoAnticipacion";
    private const string KEY_ACIERTOS = "aciertos";
    private const string KEY_FALLOS = "fallos";
    private const string KEY_HRV = "HRV";
    private const string KEY_HRT = "HRT";
    private const string KEY_HRM = "HRM";
    private const string KEY_PV = "PV";
    private const string KEY_PT = "PT";
    private const string KEY_CV = "CV";
    private const string KEY_CT = "CT";
    private const string KEY_ET = "ET";

    public ResultNewSimulacionDB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " + //1
            KEY_IDSESSION + " INT(10) UNIQUE, " + //2
            KEY_TIEMPOTOTAL + " TEXT, " + //3
            KEY_TIEMPOANTCIPACION + " TEXT, " + //4
            KEY_ACIERTOS + " INT(10), " + //5
            KEY_FALLOS + " INT(10), " + //6
            KEY_HRV + " TEXT, " + //7
            KEY_HRT + " TEXT, " + //8
            KEY_HRM + " INT(10), " + //9
            KEY_PV + " TEXT, " + //10
            KEY_PT + " TEXT, " + //11
            KEY_CV + " TEXT, " + //12
            KEY_CT + " TEXT, " + //13
            KEY_ET + " TEXT)"; //14
        dbcmd.ExecuteNonQuery();
    }


    public void addData(int idSession, float tiempoTotal, float tiempoAnticipacion, int aciertos, int fallos, int minutos, List<Grafics.graficasData> graficas, List<Grafics.eventData> eventos)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);


        Debug.Log("EL depth de la busqueda de id es " + reader.Depth);
        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        bool green = false;
        string vg = "";
        string tg = "";
        bool blue = false;
        string vb = "";
        string tb = "";
        bool red = false;
        string vr = "";
        string tr = "";
        string et = "";
        foreach (Grafics.graficasData g in graficas)
        {
            if (g.color == coloresGraficas.hearthRate)
            {

                red = true;
                for (int i = 0; i < g.values.Count; ++i)
                {
                    vg += g.values[i].ToString("F0");
                    tg += g.tiempos[i].ToString("F0");
                    if (i < g.values.Count - 1)
                    {
                        vg += ":";
                        tg += ":";
                    }
                }

            }
            else if (g.color == coloresGraficas.pupilDilatation)
            {
                for (int i = 0; i < g.values.Count; ++i)
                {
                    vb += g.values[i].ToString("F0");
                    tb += g.tiempos[i].ToString("F0");
                    if (i < g.values.Count - 1)
                    {
                        vb += ":";
                        tb += ":";
                    }
                }

                blue = true;
            }
            else if (g.color == coloresGraficas.cognitiveLoad)
            {
                for (int i = 0; i < g.values.Count; ++i)
                {
                    vg += g.values[i].ToString("F0");
                    tg += g.tiempos[i].ToString("F0");
                    if (i < g.values.Count - 1)
                    {
                        vg += ":";
                        tg += ":";
                    }
                }

                green = true;
            }

        }

        if (!red)
        {
            vr = "VACIO";
            tr = "VACIO";

        }
        if (!blue)
        {
            vb =  "VACIO";
            tb =  "VACIO";

        }
        if (!green)
        {
            vg = "VACIO";
            tg =  "VACIO";

        }

        if (eventos.Count == 0 || eventos[0].tiempos.Count == 0)
        {
            Debug.Log("AAAAAAAA");
            et =  "VACIO";
        }
        else
        {

            for (int i = 0; i < eventos[0].tiempos.Count; ++i)
            {
                Debug.Log(eventos[0].tiempos[i] + "`p");
                et += eventos[0].tiempos[i].ToString("F0");
                if (i < eventos[0].tiempos.Count - 1)
                {
                    et += ":";
                }
            }
        }

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", " //1
            + KEY_IDSESSION + ", " //2
            + KEY_TIEMPOTOTAL + ", " //3
            + KEY_TIEMPOANTCIPACION + ", " //4
            + KEY_ACIERTOS + ", " //5
            + KEY_FALLOS + ", " //6
            + KEY_HRV + ", " //7
            + KEY_HRT + ", " //8
            + KEY_HRM + ", " //9
            + KEY_PV + ", " //10
            + KEY_PT + ", " //11
            + KEY_CV + ", " //12
            + KEY_CT + ", " //13
            + KEY_ET + " ) " //14

            + "VALUES ( "
            + id + ", " //1
            + idSession + ", '" //2
            + tiempoTotal + "', '" //3
            + tiempoAnticipacion + "', " //4
            + aciertos + ", " //5
            + fallos + ", '" //6
            + vr + "', '" //7
            + tr + "', " //8
            + minutos + ", '" //9
            + vb + "', '" //10
            + tb + "', '" //11
            + vg + "', '" //12
            + tg + "', '" //13
            + et + "' );"; //14




        reader.Close();
        dbcmd.ExecuteNonQuery();
        Debug.Log(dbcmd.CommandText);
        //ESTO ES SOLO DE DEBUG
         IDataReader r = GetAllData();
    }

    public IDataReader GetAllData()
    {
        return GetAllData(TABLE_NAME);
    }

    public IDataReader GetDataUsingID(int id)
    {

        return GetDataFromIntTypeByInt(id, TABLE_NAME, KEY_IDSESSION);
    }
}
