using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class Result7DB : SQLiteHelper
{
    private const string TABLE_NAME = "resultados7";
    private const string KEY_ID = "id";
    private const string KEY_IDSESSION = "idSession";
    private const string KEY_LINEA = "linea";

    public Result7DB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " +
            KEY_IDSESSION + " INT(10) UNIQUE, " +
            KEY_LINEA + " INT(2))";
        dbcmd.ExecuteNonQuery();
    }

    public void addData(int idSession, int linea)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);



        Debug.Log("EL ultimo id es" + reader[0]);
        Debug.Log("EL depth de la busqueda de id es " + reader.Depth);
        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_IDSESSION + ", "
            + KEY_LINEA + " ) "

            + "VALUES ( "
            + id + ", "
            + idSession + ", "
            + linea + " );";

        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public IDataReader GetAllData()
    {
        return GetAllData(TABLE_NAME);
    }

    public IDataReader GetDataUsingID(int id)
    {

        return GetDataFromIntTypeByInt(id, TABLE_NAME, KEY_IDSESSION);
    }
}
