using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class Result123DB : SQLiteHelper
{
    private const string TABLE_NAME = "resultados123";
    private const string KEY_ID = "id";
    private const string KEY_IDSESSION = "idSession";
    private const string KEY_TIEMPOTOTAL = "tiempoTotal";
    private const string KEY_REACCION = "tiempoReaccion";
    private const string KEY_ACIERTOS = "aciertos";
    private const string KEY_FALLOS = "fallos";

    public Result123DB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " +
            KEY_IDSESSION + " INT(10) UNIQUE, " +
            KEY_TIEMPOTOTAL + " TEXT, " +
            KEY_REACCION + " TEXT, " +
            KEY_ACIERTOS + " INT(10), " +
            KEY_FALLOS + " INT(10))";
        dbcmd.ExecuteNonQuery();
    }
    public void addData(int idSession, string tiempoTotal, string reaccion, int aciertos, int fallos)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);



        Debug.Log("EL ultimo id es" + reader[0]);
        Debug.Log("EL depth de la busqueda de id es " + reader.Depth);
        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_IDSESSION + ", "
            + KEY_TIEMPOTOTAL + ", "
            + KEY_REACCION + ", "
            + KEY_ACIERTOS + ", "
            + KEY_FALLOS + " ) "

            + "VALUES ( "
            + id + ", "
            + idSession + ", '"
            + tiempoTotal + "', '"
            + reaccion + "', "
            + aciertos + ", "
            + fallos + " );";

        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public IDataReader GetAllData()
    {
       return GetAllData(TABLE_NAME);
    }

    public IDataReader GetDataUsingID(int id)
    {

        return GetDataFromIntTypeByInt(id, TABLE_NAME, KEY_IDSESSION);
    }
}
