using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class SessionsDB : SQLiteHelper
{
    private const string TABLE_NAME = "sessions";
    private const string KEY_ID = "id";
    private const string KEY_IDALUMNO = "idAlumno";
    private const string KEY_GRUPO = "grupo";
    private const string KEY_FECHA = "fecha";
    private const string KEY_HORA = "hora";
    private const string KEY_EJERCICIO = "ejercicio";
    private const string KEY_ESTADO = "estado";


    private string[] COLUMNS = new string[] { KEY_ID, KEY_IDALUMNO, KEY_GRUPO, KEY_FECHA, KEY_HORA, KEY_EJERCICIO, KEY_ESTADO };


    public SessionsDB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " +
            KEY_IDALUMNO + " INT(10), " +
            KEY_GRUPO + " INT(10), " +
            KEY_FECHA + " TEXT, " +
            KEY_HORA + " TEXT, " +
            KEY_EJERCICIO + " TEXT, " +
            KEY_ESTADO + " TEXT)";
        dbcmd.ExecuteNonQuery();
    }

    public void addData(int idAlumno, int grupo, string fecha, string hora, string ejercicio, string estado)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);



        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_IDALUMNO + ", "
            + KEY_GRUPO + ", "
            + KEY_FECHA + ", "
            + KEY_HORA + ", "
            + KEY_EJERCICIO + ", "
            + KEY_ESTADO + " ) "

            + "VALUES ( "
            + id + ", "
            + idAlumno + ", "
            + grupo + ", '"
            + fecha + "', '"
            + hora + "', '"
            + ejercicio + "', '"
            + estado + "' );";

        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public int GetLastSessionID()
    {
        IDataReader reader = base.GetLastRowOfAKey(KEY_ID, TABLE_NAME);
        int respuesta = (int)reader[0];
        reader.Close();
        return respuesta;
    }
    public IDataReader GetAllData()
    {
        return GetAllData(TABLE_NAME);
    }

    public IDataReader GetAllFechasOfUser(int id)
    {
        return base.GetAllKeyDataUsingKeyIntType(KEY_FECHA, id, KEY_IDALUMNO, TABLE_NAME);
    }

    public IDataReader GetHistorialBetweenFechasXY(int id, string fechaInit, string fechaEnd )
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;

        string query = "";
        if(fechaInit == "" && fechaEnd == "")
        {
             query = "SELECT " +  KEY_ID + ", " + KEY_FECHA + ", " + KEY_HORA + ", " + KEY_EJERCICIO + ", " + KEY_ESTADO +  " FROM " + TABLE_NAME + " WHERE " + KEY_IDALUMNO +  " = " + id + ";";

        }
        else 
        {
            int sessionFirstId;
            int sessionSecondId;
            if (fechaInit == "")
            {
                IDataReader r = GetFirstRowOfAKey(KEY_ID, TABLE_NAME);
                sessionFirstId = (int)r[0];
                r.Close();

                IDbCommand pre = dbConnection.CreateCommand();
                IDataReader r2;
                string s = "SELECT " + KEY_ID + " FROM " + TABLE_NAME + " WHERE " + KEY_FECHA + " ='" + fechaEnd + "'  ORDER BY " + KEY_ID + " DESC LIMIT 1;";
                pre.CommandText = s;
                r2 = pre.ExecuteReader();
                sessionSecondId = (int)r2[0];
                r2.Close();


            }
            else if(fechaEnd == "")
            {
                IDbCommand pre = dbConnection.CreateCommand();
                IDataReader r;
                string s = "SELECT " + KEY_ID + " FROM " + TABLE_NAME + " WHERE " + KEY_FECHA + " ='" + fechaInit + "'  ORDER BY " + KEY_ID + " ASC LIMIT 1;";
                pre.CommandText = s;
                r = pre.ExecuteReader();
                sessionFirstId = (int)r[0];
                r.Close();

                IDataReader r2 = GetLastRowOfAKey(KEY_ID, TABLE_NAME);
                sessionSecondId = (int)r2[0];
                r2.Close();
            }
            else
            {
                IDbCommand pre = dbConnection.CreateCommand();
                IDataReader r;
                string s = "SELECT " + KEY_ID + " FROM " + TABLE_NAME + " WHERE " + KEY_FECHA + " ='" + fechaInit + "'  ORDER BY " + KEY_ID + " ASC LIMIT 1;";
                pre.CommandText = s;
                r = pre.ExecuteReader();
                sessionFirstId = (int)r[0];
                r.Close();

                IDbCommand pre2 = dbConnection.CreateCommand();
                IDataReader r2;
                string s2 = "SELECT " + KEY_ID + " FROM " + TABLE_NAME + " WHERE " + KEY_FECHA + " ='" + fechaEnd + "'  ORDER BY " + KEY_ID + " DESC LIMIT 1;";
                pre2.CommandText = s2;
                r2 = pre2.ExecuteReader();
                sessionSecondId = (int)r2[0];
                r2.Close();

            }
            query = "SELECT " +
                KEY_ID + ", " +
                KEY_FECHA + ", " +
                KEY_HORA + ", " +
                KEY_EJERCICIO + ", " +
                KEY_ESTADO +
                " FROM " + TABLE_NAME + " WHERE " + KEY_IDALUMNO + " = " + id + " AND " + KEY_ID + " BETWEEN '" + sessionFirstId + "' AND '" + sessionSecondId + "';";
        }



        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }

}
