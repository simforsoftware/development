using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class GruposDB : SQLiteHelper
{


    private const string TABLE_NAME = "grupos";
    private const string KEY_GRUPO = "grupo";
    private const string KEY_IDNSTRUCTOR = "idInstructor";
    private const string KEY_MULTISESIONS = "multiSessions";


    public GruposDB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_GRUPO + " INT(10) PRIMARY KEY, " +
            KEY_IDNSTRUCTOR + " INT(10) UNIQUE, " +
            KEY_MULTISESIONS + " TEXT)";
        dbcmd.ExecuteNonQuery();
    }

    public void addData(int idInstructor, string multisesions)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_GRUPO, TABLE_NAME);



        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.

        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL id de la query de insert nuevo usuario a dado nullo");
        }

        IDbCommand dbcmd = GetDbCommand();
        
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_GRUPO + ", "
            + KEY_IDNSTRUCTOR + ", "
            + KEY_MULTISESIONS + " ) "

            + "VALUES ( "
            + id + ", "
            + idInstructor + ", '"
            + multisesions + "' );";
        Debug.Log(dbcmd.CommandText);
        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public int GetLastGrupo()
    {
        IDataReader reader = base.GetLastRowOfAKey(KEY_GRUPO, TABLE_NAME);
        int respuesta = (int)reader[0];
        reader.Close();
        return respuesta;
    }
    public IDataReader GetAllGrupos()
    {
        return base.GetAllRowsOfAKey(KEY_GRUPO, TABLE_NAME);
    }
    public string GetMultisesionesFromGrupo(int i)
    {
        IDataReader reader =  base.GetAllKeyDataUsingKeyIntType(KEY_MULTISESIONS, i, KEY_GRUPO, TABLE_NAME);
        string respuesta = reader[0].ToString();
        reader.Close();
        return respuesta;
    }

    public void UpdatearMultisesiones(string s, int grupo)
    {
        base.UpdateKeyStringTypeWithStringUsingKeyIntType(s, TABLE_NAME, KEY_MULTISESIONS, KEY_GRUPO, grupo);
    }

    public void DeletearUltimoGrupo()
    {
        IDataReader reader = base.GetLastRowOfAKey(KEY_GRUPO, TABLE_NAME);

        base.DestroyRowWhereKeyIntByInt((int)reader[0], KEY_GRUPO, TABLE_NAME);
        reader.Close();
    }
}
