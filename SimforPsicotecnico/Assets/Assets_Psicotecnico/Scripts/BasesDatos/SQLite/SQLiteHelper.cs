using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

public  class SQLiteHelper 
{
    private const string dbName = "Simforpsi";
    //Clanta: DEPRECATED public string ruta = "C:\\Users\\SIMFOR\\Documents\\SharedFoler";

    public string dbConnectionString;
    public IDbConnection dbConnection;

    public bool error;

    public SQLiteHelper()
    {
        error = false;
        try
        {


            string path = string.Format("{0}/{1}", Application.streamingAssetsPath, "path");
            StreamReader sr = new StreamReader(path);
            string recuperado = sr.ReadToEnd();

            dbConnectionString = "URI=file:" + recuperado + "/" + dbName;
            dbConnection = new SqliteConnection(dbConnectionString);
            dbConnection.Open();
        }
        catch
        {
            error = true;
            Debug.LogError("Ha habido un error con la conexion con la DB en el servidor. Pasando a una DB local");
            dbConnectionString = "URI=file:" + Application.persistentDataPath + "/" + dbName;
            dbConnection = new SqliteConnection(dbConnectionString);
            dbConnection.Open();
        }
        
    }

    ~SQLiteHelper()
    {
        dbConnection.Close();
    }
    public void Close()
    {
        dbConnection.Close();
    }

    public IDbCommand GetDbCommand()
    {
        return dbConnection.CreateCommand();
    }

    public virtual IDataReader GetDataFromIntTypeByInt(int id, string tableName, string KeyName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT * FROM " + tableName + " WHERE " +  KeyName + " = " + id + ";";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
    protected IDataReader GetKeyDataWithIntUsingKeyIntType(string keyName, string tableName, string KeyNameIndexer, int index)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT " + keyName + " FROM " + tableName + " WHERE " + KeyNameIndexer + " = " + index + ";";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
    protected IDataReader GetAllKeyDataUsingKeyIntType(string keySearched, int id, string keyIndexer, string tableName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT " +keySearched  +  " FROM " + tableName + " WHERE " + keyIndexer + " = " + id + ";";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
    public virtual IDataReader GetDataFromKeyStringTypeByString(string s, string tableName, string KeyName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT * FROM " + tableName + " WHERE " + KeyName + " = '" + s + "';";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
    protected virtual void DestroyRowWhereKeyIntByInt(int i, string keyName, string tableName)
    {
        IDbCommand dbcmd = dbConnection.CreateCommand();
        dbcmd.CommandText = "DELETE FROM " + tableName + " WHERE " + keyName + " = " + i;
        dbcmd.ExecuteNonQuery();
    }

    protected virtual void UpdateKeyStringTypeWithStringUsingKeyIntType(string sChange, string tableName, string KeyNameChange, string KeyNameIndexer, int index)
    {
        IDbCommand dbcmd = dbConnection.CreateCommand();
        string query = "UPDATE " + tableName + " SET " + KeyNameChange + " = @schange WHERE " + KeyNameIndexer + " = " + index + ";";
        IDataParameter s = dbcmd.CreateParameter();
        s.Value = sChange;
        s.ParameterName = "@schange";
        dbcmd.Parameters.Add(s);
        Debug.Log(query);
        dbcmd.CommandText = query;
        dbcmd.ExecuteNonQuery();

       

    }
    protected IDataReader GetLastRowOfAKey(string keyname, string tableName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT " + keyname + " FROM " + tableName + " ORDER BY " + keyname + " DESC LIMIT 1;";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }

    protected IDataReader GetFirstRowOfAKey(string keyName, string tableName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT " + keyName + " FROM " + tableName + " ORDER BY " + keyName + " ASC LIMIT 1;";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
    protected  IDataReader GetAllData(string tableName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT * FROM " + tableName + ";";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
    protected IDataReader GetAllRowsOfAKey(string keyname, string tableName)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;
        string query = "SELECT " + keyname + " FROM " + tableName + " ;";
        cmnd_read.CommandText = query;
        
        reader = cmnd_read.ExecuteReader();
        return reader;
    }
}
