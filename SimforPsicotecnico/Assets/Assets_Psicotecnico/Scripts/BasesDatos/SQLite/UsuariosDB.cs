using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;

public class UsuariosDB : SQLiteHelper
{
    private const string TABLE_NAME = "users";
    private const string KEY_ID = "id";
    private const string KEY_NOMBRE = "nombre";
    private const string KEY_APELLIDO = "apellido";
    private const string KEY_PASSWORD = "password";
    private const string KEY_EMAIL = "email";
    private const string KEY_INSTRUCTOR = "instructor";
    private const string KEY_GRUPO = "grupo";

    private string[] COLUMNS = new string[] { KEY_ID, KEY_NOMBRE, KEY_APELLIDO, KEY_PASSWORD, KEY_EMAIL, KEY_INSTRUCTOR, KEY_GRUPO};


    public UsuariosDB() : base()
    {
        //ESTO DEBERIA CREAR LA BASE DE DATOS EN CASO DE QUE NO EXISTA
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
            KEY_ID + " INT(10) PRIMARY KEY, " +
            KEY_NOMBRE + " TEXT, " +
            KEY_APELLIDO + " TEXT, " +
            KEY_PASSWORD + " TEXT, " +
            KEY_EMAIL + " TEXT UNIQUE, " +
            KEY_INSTRUCTOR + " TEXT, " +
            KEY_GRUPO + " INT(10))";
        dbcmd.ExecuteNonQuery();
    }

    //SE PUEDE UTILIZAR TAMBIEN PARA CUALQUIER TIPO DE INT // CLanta: A lo mejor seria mejor hacer una especifica para cada campo
    public override IDataReader GetDataFromIntTypeByInt(int id, string KeyName = KEY_ID , string tableName = "")
    {
        return base.GetDataFromIntTypeByInt(id, TABLE_NAME, KeyName);
    }
    public void DestroyUserByID(int id)
    {
        base.DestroyRowWhereKeyIntByInt(id, KEY_ID, TABLE_NAME);
    }
    public  string GetPasswordUsingId(int id)
    {
        IDataReader reader = GetKeyDataWithIntUsingKeyIntType(KEY_PASSWORD, TABLE_NAME, KEY_ID, id);
        string respuesta = reader[0].ToString();
        reader.Close();
        return respuesta;
    }
    public string GetNombreUsingId(int id)
    {
        IDataReader reader = GetKeyDataWithIntUsingKeyIntType(KEY_NOMBRE, TABLE_NAME, KEY_ID, id);
        string respuesta = reader[0].ToString();
        reader.Close();
        return respuesta;
    }
    public string GetApellidoUsingId(int id)
    {
        IDataReader reader = GetKeyDataWithIntUsingKeyIntType(KEY_NOMBRE, TABLE_NAME, KEY_ID, id);
        string respuesta = reader[0].ToString();
        reader.Close();
        return respuesta;
    }
    public void UpdatePasswordUsingId(string password, int id)
    {
        base.UpdateKeyStringTypeWithStringUsingKeyIntType(password, TABLE_NAME, KEY_PASSWORD, KEY_ID, id);
    }

    public override IDataReader GetDataFromKeyStringTypeByString(string s, string KeyName, string tableName = "")
    {
        return base.GetDataFromKeyStringTypeByString(s, TABLE_NAME, KeyName);
    }
    //FUNCION QUE A�ADE UN USUARIO A LA BASE DE DATOS // CLanta: Hay que hacer sobrecargas 
    public void addData( string nombre, string apellido, string password, string email, string instructor ,int grupo)
    {

        IDataReader reader = GetLastRowOfAKey(KEY_ID, TABLE_NAME);



        Debug.Log("EL ultimo id es" + reader[0]);
        Debug.Log("EL depth de la busqueda de id es " + reader.Depth);
        int id = 0;
        //Encontrar la manera de hacer esto con un if 
        //Va a dar error siempre en la primera vez que intente crear un usuario.
        try
        {
            id = (int)reader[0] + 1;
        }
        catch
        {
            Debug.LogError(" EL ide de la query de insert nuevo usuario a dado nullo");
        }   

        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_NOMBRE + ", "
            + KEY_APELLIDO + ", "
            + KEY_PASSWORD + ", "
            + KEY_EMAIL + ", "
            + KEY_INSTRUCTOR + ", "
            + KEY_GRUPO + " ) "

            + "VALUES ( "
            + id +
            ", @nombre , " +
            "@apellido, " +
            "@password, " +
            "@email, '"
            + instructor + "', "
            + grupo + " );";

        //Bindeacion del nombre
        IDataParameter n =  dbcmd.CreateParameter();
        n.Value = nombre;
        n.ParameterName = "@nombre";
        dbcmd.Parameters.Add(n);
        //bindeado del apellido
        IDataParameter a = dbcmd.CreateParameter();
        a.Value = apellido;
        a.ParameterName = "@apellido";
        dbcmd.Parameters.Add(a);
        //bindeado del password
        IDataParameter p = dbcmd.CreateParameter();
        p.Value = password;
        p.ParameterName = "@password";
        dbcmd.Parameters.Add(p);
        
        //bindeado del email
        IDataParameter e = dbcmd.CreateParameter();
        e.Value = email;
        e.ParameterName = "@email";
        dbcmd.Parameters.Add(e);



        reader.Close();
        dbcmd.ExecuteNonQuery();
    }
    public void addData(int id, string nombre, string apellido, string password, string email, string instructor, int grupo)
    {
        IDbCommand dbcmd = GetDbCommand();
        dbcmd.CommandText =
            "INSERT INTO " + TABLE_NAME
            + " ( "
            + KEY_ID + ", "
            + KEY_NOMBRE + ", "
            + KEY_APELLIDO + ", "
            + KEY_PASSWORD + ", "
            + KEY_EMAIL + ", "
            + KEY_INSTRUCTOR + ", "
            + KEY_GRUPO + " ) "

            + "VALUES ( "
             + id + ", '"
            + nombre + "', '"
            + apellido + "', '"
            + password + "', '"
            + email + "', '"
            + instructor + "', "
            + grupo + " );";


        dbcmd.ExecuteNonQuery();
    }
    public string GetInstructorUsingId(int id)
    {
        IDataReader reader = base.GetAllKeyDataUsingKeyIntType(KEY_INSTRUCTOR, id, KEY_ID, TABLE_NAME);
        string respuesta = reader[0].ToString();
        reader.Close();
        return respuesta;
    }
    public IDataReader GetAllGrupos()
    {
        return base.GetAllRowsOfAKey(KEY_GRUPO, TABLE_NAME);   
    }
    public IDataReader Get_AllNombres()
    {
        return base.GetAllRowsOfAKey(KEY_NOMBRE, TABLE_NAME);
    }
    public IDataReader Get_AllIds()
    {
        return base.GetAllRowsOfAKey(KEY_ID, TABLE_NAME);
    }
    public IDataReader LogInQuery(string email)
    {
        IDbCommand cmnd_read = dbConnection.CreateCommand();
        IDataReader reader;

        string query = "SELECT " + KEY_ID + ", " + KEY_GRUPO + ", " + KEY_PASSWORD + " FROM " + TABLE_NAME + " WHERE " + KEY_EMAIL + " = @email;";
        //query = "SELECT * FROM " + TABLE_NAME + ";";
        IDataParameter e = cmnd_read.CreateParameter();
        e.Value = email;
        e.ParameterName = "@email";
        cmnd_read.Parameters.Add(e);
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();

        /*while (reader.Read())
        {
            for(int i = 0; i < reader.FieldCount; ++i)
            {
                Debug.Log( i + " " + reader.GetName(i) + " : " + reader[i]);
                if (i == 4) Debug.Log(reader[i].ToString() == email);
            }
        }*/
        return reader;
    }

    public int GetLastID()
    {
        IDataReader reader = base.GetLastRowOfAKey(KEY_ID, TABLE_NAME);
        int respuesta = (int)reader[0];
        reader.Close();
        return respuesta;
    }


}
