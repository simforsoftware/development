using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Data;
using Mono.Data.Sqlite;

public class Register : MonoBehaviour
{
    public InputField name;
    public InputField password;
    public InputField apellido;
    public InputField email;
    public InputField inputPassInstruc;
    UsuariosDB usuarios;
    GruposDB gruposDB;
    [SerializeField]  ErrorText error;

    public Dropdown drop;

    public Button register;
    bool validInputs = false;

    string passIntructor = "SIMFOR";
    public void CallRegister()
    {
        
        if (validInputs)
            FRegister();
        else
        {
            if (name.text.Length > 0 && password.text.Length > 0 && apellido.text.Length > 0 && email.text.Length > 0) error.PrintError(9);
            else error.PrintError(8);
        }
    }

    private void Start()
    {
        gruposDB = new GruposDB();
        usuarios = new UsuariosDB();
        FGetGrupos();

    }
    //DEPRECATED FUCTION
    /*IEnumerator CGetGrupos()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getGrupos.php"))
        {

            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
                //error.text = "Fallo de conexion o formulario php";
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                int errorID = int.Parse(www.downloadHandler.text[0].ToString());
                if (errorID != 0) error.PrintError(errorID);
                string[] s = www.downloadHandler.text.Split(',');     
                List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
                for(int i = 1; i < s.Length; ++i)
                {
                    Dropdown.OptionData a = new Dropdown.OptionData();
                    a.text = s[i];
                    lista.Add(a);
                }
                drop.AddOptions(lista);
            }
        }
    }*/

    public void FGetGrupos()
    {
        IDataReader reader = gruposDB.GetAllGrupos();

        List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
        //List<int> auxiliar = new List<int>();

        while (reader.Read())
        {
            Dropdown.OptionData a = new Dropdown.OptionData();
            a.text = reader[0].ToString();
            lista.Add(a);
            /*if (!auxiliar.Contains((int)reader[0]))
            {
                auxiliar.Add((int)reader[0]);
                lista.Add(a);
            }*/
        }
        if(lista.Count == 0)
        {
            Dropdown.OptionData a = new Dropdown.OptionData();
            a.text = "0";
            lista.Add(a);

        }
        drop.AddOptions(lista);
        reader.Close();
    }
    //DEPRECATED FUNCTION
    /*IEnumerator CRegister()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name.text);
        form.AddField("password", password.text);
        form.AddField("apellido", apellido.text);
        form.AddField("email", email.text);
        Debug.Log(int.Parse(drop.options[drop.value].text));
        form.AddField("grupo", int.Parse( drop.options[drop.value].text));

        string instruc = "no";
        bool espera = false;

        if(inputPassInstruc.text == passIntructor)
        {
            instruc = "yes";
        }
        else if (inputPassInstruc.gameObject.activeSelf) 
        {
            espera = true;
        }

        form.AddField("instructor", instruc);
        if (!espera)
        {

            using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/register.php", form))
            {

                yield return www.Send();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.result.ToString());
                    error.PrintError(0);
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                    if(www.downloadHandler.text[0] == '3')
                    {
                        error.PrintError(3);
                    }
                    else ScenesSelector.GoToScene(Scenes.Login);
                }
            }
        }
    }*/
    //Clanta: Falta poner la logica de los grupos 
    public void FRegister()
    {
        bool errorGrupoDB = false;
        string instruc = "no";
        bool espera = false;
        int grupo = 0;
        if (drop.options.Count > 0)
        {
            grupo = int.Parse(drop.options[drop.value].text);
        }

        if (inputPassInstruc.text == passIntructor)
        {
            
            instruc = "yes";
            /*int auxiliar = int.MinValue;
            for(int i = 0; i < drop.options.Count; ++i)
            {
                if (auxiliar < int.Parse(drop.options[i].text)) auxiliar = int.Parse(drop.options[i].text);
            }*/

            //grupo = auxiliar + 1;
            int idInstruc;
            try
            {


                idInstruc = usuarios.GetLastID() + 1;
            }
            catch
            {
                idInstruc = 0;

            }
            try
            {
                gruposDB.addData(idInstruc, "VACIO:VACIO:VACIO");
                error.PrintError(12);
            }
            catch
            {
                errorGrupoDB = true;
            }
            grupo = gruposDB.GetLastGrupo();

        }
        else if (inputPassInstruc.gameObject.activeSelf)
        {
            espera = true;
        }


        if (!espera && !errorGrupoDB)
        {
            bool errorDb = false;
            try
            {
                usuarios.addData(name.text, apellido.text, Encriptador.Encode(password.text), email.text, instruc, grupo);
            }
            catch (SqliteException e)
            {
                if (!errorGrupoDB && inputPassInstruc.text == passIntructor) gruposDB.DeletearUltimoGrupo();
                errorDb = true;
                Debug.Log(e.Message);
                string s = e.Message;

                Debug.Log(s.Split('\n')[1]);

                if (s.Split('\n')[1] == "column email is not unique") error.PrintError(3);
                throw;
            }
            if (!errorDb)
            {
                // ESTO ES SOLO PARA DEBUGAR LA AQUELLOS QUE ESTAN DENTRO
                /*IDataReader reader = usuarios.GetDataFromIntTypeByInt(grupo, "grupo");
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; ++i)
                    {
                        Debug.Log(reader.GetName(i) + " :" + reader[i]);
                    }
                }

                reader.Close();*/
                ScenesSelector.GoToScene(Scenes.Login);
            }
        }
    }
    public void ActivatePassIns()
    {
        inputPassInstruc.gameObject.SetActive(!inputPassInstruc.gameObject.activeSelf);
        drop.gameObject.SetActive(!drop.gameObject.activeSelf);
    }
    public void CheckInputs()
    {
        validInputs = (name.text.Length > 0 && password.text.Length >= 6 && apellido.text.Length > 0 && email.text.Length > 0);
    }
}
