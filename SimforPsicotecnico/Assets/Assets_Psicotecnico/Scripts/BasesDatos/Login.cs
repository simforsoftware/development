using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Data;

public class Login : MonoBehaviour
{
    public InputField email;
    public InputField password;
    [SerializeField] ErrorText error;
    //[SerializeField] Result myResult;

    public Button login;
    UsuariosDB usuarios;

    public void CallLogIN()
    {
        FLogIn();
    }
    private void Start()
    {

            usuarios = new UsuariosDB();

        if (usuarios.error)
        {
            error.PrintError(1);
        }
    }

    //DEPRECATED
    /*IEnumerator LogIN()
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email.text);
        form.AddField("password", password.text);

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/login.php", form) )
        {
            yield return www.Send();

            if(www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                int errorID = int.Parse(www.downloadHandler.text[0].ToString());
                if (errorID != 0) error.PrintError(errorID);
                //Result alumnoData = Resources.Load<Result>("Result");
                string[] s = www.downloadHandler.text.Split(',');
                //alumnoData.IDAlumno = int.Parse(s[1]);
                //alumnoData.IDGrupo = int.Parse(s[2]);
                AuxiliarBuild.instance.IDAlumno = int.Parse(s[1]);
                AuxiliarBuild.instance.IDGrupo = int.Parse(s[2]);
                //Debug.Log(alumnoData.IDAlumno);
                //Debug.Log(alumnoData.IDGrupo);
                ScenesSelector.GoToScene(Scenes.MainMenu);
            }
        }
    }*/
    public void FLogIn()
    {
        IDataReader reader =  usuarios.LogInQuery(email.text);
        IDataReader auxReader = usuarios.LogInQuery(email.text);
        int rows = 0;
        while (auxReader.Read())
        {
            ++rows;
        }
        if(rows!= 1)
        {
            Debug.LogError("O existe mas de un usuario con el mismo mail o este mail no existe "  + rows);
            error.PrintError(4);
        }
        else
        {
            if(Encriptador.Decode((string)reader[2]) == password.text)
            {
                AuxiliarBuild.instance.IDAlumno = (int)reader[0];
                AuxiliarBuild.instance.IDGrupo = (int)reader[1];
                ScenesSelector.GoToScene(Scenes.MainMenu);
            }
            else
            {
                Debug.LogError("La contraseņa es incorrrecta");
                error.PrintError(7);
            }
        }
        reader.Close();
    }
    public void CheckInputs()
    {
        login.interactable = ( password.text.Length > 5 && email.text.Length > 0);
    }   
}
