using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Data;
public class Historial : MonoBehaviour
{

    [SerializeField] Text nombre;
    [SerializeField] Text grupo;
    [SerializeField] Text Id;

    [SerializeField] GameObject myScrollView;
    [SerializeField] GameObject etiqueta;

     int parametrosEtiqueta = 7;

    [SerializeField] Dropdown dropIni;
    [SerializeField] Dropdown dropFin;
    bool primera_vez = true;
    [SerializeField] Dropdown nombreDrop;
    [SerializeField] Dropdown idDrop;

    List<Dropdown.OptionData> opcionesInit;
    List<Dropdown.OptionData> optionesEnd;

    bool esperando_Respuesta = false;
    bool recolocando = false;

    bool intructor = false;

    List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();

    UsuariosDB usuarios;
    SessionsDB sessions;
    private void Start()
    {
        usuarios = new UsuariosDB();
        sessions = new SessionsDB();
        Primera_Vez();
    }
    public void Primera_Vez()
    {
        FGetInfo();
        FGetAllAlumnos();
        FGetAllIds();
        Recolocar_Drops(true);
        FGetHistory();
        FGetDates();

    }
    public void Search_history()
    {
        FGetHistory();
    }
    public void Get_Fechas()
    {
        FGetDates();
        Debug.Log("Busqueda fechas");
    }
    public void FGetAllAlumnos()
    {
        IDataReader reader = usuarios.Get_AllNombres();

        List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();

        while (reader.Read())
        {
            Dropdown.OptionData a = new Dropdown.OptionData();
            a.text = reader[0].ToString();
            lista.Add(a);
        }
        nombreDrop.AddOptions(lista);
        reader.Close();
    }
    //DEPRECATED
    /*
    IEnumerator Get_All_Alumnos()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getAllAlumnos.php"))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');

                esperando_Respuesta = true;

                List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
                for (int i = 0; i < s.Length; ++i)
                {
                    Dropdown.OptionData a = new Dropdown.OptionData();
                    a.text = s[i];
                    lista.Add(a);
                }
                nombreDrop.AddOptions(lista);

                esperando_Respuesta = false;
            }
        }
    }*/
    public void Recolocar_Drops(bool byId)
    {
        if (primera_vez)
        {
            //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre;
            for (int i = 0; i < idDrop.options.Count; ++i)
            {
                if(idDrop.options[i].text == AuxiliarBuild.instance.IDAlumno.ToString())
                {
                    idDrop.value = i;
                    nombreDrop.value = i;
                }
            }
        }
        else if (byId)
        {
            nombreDrop.value = idDrop.value;
        }
        else
        {
            idDrop.value = nombreDrop.value;
        }
        primera_vez = true;
    }
    public void FGetAllIds()
    {
        IDataReader reader = usuarios.Get_AllIds();


        List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
        while (reader.Read())
        {
            Dropdown.OptionData a = new Dropdown.OptionData();
            a.text = reader[0].ToString();
            lista.Add(a);
        }
        idDrop.AddOptions(lista);
        reader.Close();
    }
    //DEPRECATED
    /*
    IEnumerator Get_All_Ids()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/GetAllid.php"))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');

                esperando_Respuesta = true;

                List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
                for (int i = 0; i < s.Length; ++i)
                {
                    Dropdown.OptionData a = new Dropdown.OptionData();
                    a.text = s[i];
                    lista.Add(a);
                }
                idDrop.AddOptions(lista);

                esperando_Respuesta = false;
            }
        }
    }*/
    
    public void FGetInfo()
    {
        IDataReader reader = usuarios.GetDataFromIntTypeByInt(AuxiliarBuild.instance.IDAlumno);
        if (reader[5].ToString() == "yes")
        {
            intructor = true;
            idDrop.gameObject.SetActive(true);
            nombreDrop.gameObject.SetActive(true);
        }

        reader.Close();
    }
    //DEPRECATED
    /*
    IEnumerator Get_Info()
    {
        //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre;
        WWWForm form = new WWWForm();

        form.AddField("id", AuxiliarBuild.instance.IDAlumno);


        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/getInfoUsuario.php", form))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');
                if (s.Length > 1)
                {
                    if(s[3] == "yes") 
                    {
                        intructor = true;
                        idDrop.gameObject.SetActive(true);
                        nombreDrop.gameObject.SetActive(true);
                    }

                }
            }
   
        }
    }*/
    public void FGetDates()
    {
        dropFin.options.Clear();
        dropIni.options.Clear();
        dropIni.RefreshShownValue();
        dropFin.RefreshShownValue();
        lista.Clear();

        int idBusqueda;
        if (!intructor) idBusqueda = AuxiliarBuild.instance.IDAlumno;
        else idBusqueda =  int.Parse(idDrop.options[idDrop.value].text);

        IDataReader reader = sessions.GetAllFechasOfUser(idBusqueda);
        IDataReader aux = sessions.GetAllFechasOfUser(idBusqueda);
        List<string> datos = new List<string>();

        while (reader.Read())
        {
           
            if(!datos.Contains(reader[0].ToString()))
            {
                Dropdown.OptionData a = new Dropdown.OptionData();
                a.text = reader[0].ToString();
                lista.Add(a);
                datos.Add(reader[0].ToString());
            }
        }
        dropIni.AddOptions(lista);
        List<Dropdown.OptionData> listaTemporal = new List<Dropdown.OptionData>();
        for (int i = 1; i < lista.Count; ++i)
        {
            listaTemporal.Add(lista[i]);
        }
        dropFin.AddOptions(listaTemporal);
        dropFin.value = listaTemporal.Count - 1;

    }
    //DEPRECATED
    /*
    IEnumerator Get_dates()
    {
        Debug.Log("Limpio");
        dropFin.options.Clear();
        dropIni.options.Clear();
        dropIni.RefreshShownValue();
        dropFin.RefreshShownValue();
        lista.Clear();

        //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre; ;
        WWWForm form = new WWWForm();
        if (!intructor) form.AddField("id", AuxiliarBuild.instance.IDAlumno);
        else form.AddField("id", int.Parse(idDrop.options[idDrop.value].text));

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/getSessionDates.php", form))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);

                string lastDato = "";
                //Debug.Log("Fechas: " + www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');
                if (s.Length < 2) yield break;
                
                for (int i = 0; i < s.Length; ++i)
                {
                    if (s[i].Split(':')[0] != lastDato)
                    {
                        Dropdown.OptionData a = new Dropdown.OptionData();
                        a.text = s[i].Split(':')[0];
                        lista.Add(a);
                        lastDato = a.text;
                        Debug.Log("FF: A�ADO");
                    }
                }
                dropIni.AddOptions(lista);
                List<Dropdown.OptionData> listaTemporal = new List<Dropdown.OptionData>();
                for (int i = 1; i < lista.Count; ++i)
                {
                    listaTemporal.Add(lista[i]);
                }
                dropFin.AddOptions(listaTemporal);
                dropFin.value = listaTemporal.Count - 1;
            }
        }
        Debug.Log("termino");
    }*/
    
    public void Recolocar_DropDownFinal()
    {
        int i = dropIni.value + 1;
        List<Dropdown.OptionData> listaTemporal = new List<Dropdown.OptionData>();
        for (; i < lista.Count; ++i)
        {
            listaTemporal.Add(lista[i]);
        }
        dropFin.ClearOptions();
        dropFin.AddOptions(listaTemporal);
        if(listaTemporal.Count == 0)
        {
            dropFin.value = 0;
        }
        else
        {
            dropFin.value = listaTemporal.Count - 1;
        }
        FGetHistory();
    }
    public void FGetHistory()
    {
        int z = myScrollView.transform.childCount;

        for (int i = 0; i < z; ++i)
        {
            Destroy(myScrollView.transform.GetChild(i).gameObject);
        }

        int id;

        if (intructor) id = int.Parse(idDrop.options[idDrop.value].text);
        else id = AuxiliarBuild.instance.IDAlumno;


        string fechaInit = "";
        string fechaEnd = "";
        if (primera_vez)
        {
            fechaInit = "";
            fechaEnd = "";
            primera_vez = false;
        }
        else
        {
            Debug.Log(dropIni.options[dropIni.value].text);
            fechaInit =  dropIni.options[dropIni.value].text;
            if (dropFin.options.Count > 0) fechaEnd =  dropFin.options[dropFin.value].text;
            //COLOCAR AQUI TODA LA LOGICA DE BUSQUEDA SI SE ES INTRUCTOR
        }
        Id.text = AuxiliarBuild.instance.IDAlumno.ToString();
        grupo.text = AuxiliarBuild.instance.IDGrupo.ToString();


        Debug.Log(id);
        string nombreS = usuarios.GetNombreUsingId(id) + " " + usuarios.GetApellidoUsingId(id);
        
        Debug.Log(nombre);
        IDataReader reader = sessions.GetHistorialBetweenFechasXY(id, fechaInit, fechaEnd);
        IDataReader auxReader = sessions.GetHistorialBetweenFechasXY(id, fechaInit, fechaEnd);
        nombre.text = nombreS;
        int y = 0;
        while (auxReader.Read())
        {
            Debug.Log(y);
            ++y;
            for(int i = 0; i < auxReader.FieldCount; ++i)
            {
                Debug.Log(i + " " + auxReader.GetName(i) + " " + auxReader[i]);

            }
        }
        /*IDataReader r = sessions.GetAllData();
        while (r.Read())
        {

            for (int i = 0; i < r.FieldCount; ++i)
            {
                Debug.Log(i + " " + r.GetName(i) + " " + r[i]);

            }
        }
        r.Close();*/
        auxReader.Close();

        myScrollView.GetComponent<RectTransform>().sizeDelta = new Vector2(myScrollView.GetComponent<RectTransform>().sizeDelta.x,
                                                                                        (myScrollView.GetComponent<VerticalLayoutGroup>().padding.top + myScrollView.GetComponent<VerticalLayoutGroup>().padding.bottom) +
                                                                                      (etiqueta.GetComponent<RectTransform>().sizeDelta.y * y));


        while (reader.Read())
        {
            GameObject e = Instantiate(etiqueta, myScrollView.transform);
            e.GetComponent<EtiquetaHistorial>().SetDatos( reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), (int)reader[0], id, reader[1].ToString(), nombreS );
        }
        reader.Close();
    }

    //DEPRECATED
    /*
    IEnumerator Get_History()
    {
        if (esperando_Respuesta) yield break;
        esperando_Respuesta = true;

        int z = myScrollView.transform.childCount;

        for (int i = 0; i < z; ++i)
        {
            Destroy(myScrollView.transform.GetChild(i).gameObject);
        }

        //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre;
        WWWForm form = new WWWForm();

        if (intructor) form.AddField("id", int.Parse(idDrop.options[idDrop.value].text));
        else form.AddField("id", AuxiliarBuild.instance.IDAlumno);

        if (primera_vez)
        {
            form.AddField("fechaInit", "");
            form.AddField("fechaEnd", "");
            primera_vez = false;
        }
        else
        {
            form.AddField("fechaInit", dropIni.options[dropIni.value].text);
            if(dropFin.options.Count >0) form.AddField("fechaEnd", dropFin.options[dropFin.value].text);
            else form.AddField("fechaInit", "");

            //COLOCAR AQUI TODA LA LOGICA DE BUSQUEDA SI SE ES INTRUCTOR
        }
        Id.text = AuxiliarBuild.instance.IDAlumno.ToString();
        grupo.text = AuxiliarBuild.instance.IDGrupo.ToString();
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/historial.php", form))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');
                if (s.Length > 3)
                {
                    nombre.text = s[0] + " " + s[1];
                    myScrollView.GetComponent<RectTransform>().sizeDelta = new Vector2( myScrollView.GetComponent<RectTransform>().sizeDelta.x, 
                                                                                        (myScrollView.GetComponent<VerticalLayoutGroup>().padding.top + myScrollView.GetComponent<VerticalLayoutGroup>().padding.bottom) + 
                                                                                        (etiqueta.GetComponent<RectTransform>().sizeDelta.y * ((s.Length - 2) / parametrosEtiqueta)));
                    for (int i = 0; i < ((s.Length - 2) / parametrosEtiqueta); ++i)
                    {
                        GameObject e = Instantiate(etiqueta, myScrollView.transform);
                        e.GetComponent<EtiquetaHistorial>().SetDatos(s[(i * parametrosEtiqueta) + 2], s[(i* parametrosEtiqueta) + 1 + 2], s[(i * parametrosEtiqueta) + 2 + 2], s[(i * parametrosEtiqueta) + 3 + 2], int.Parse(s[(i* parametrosEtiqueta) +4+2]), int.Parse(s[(i * parametrosEtiqueta) + 5 + 2]), s[(i * parametrosEtiqueta) + 6 + 2], nombre.text);
                    }
                    for(int o = ((s.Length - 2) / parametrosEtiqueta); o < 5; ++o)
                    {
                        GameObject e = Instantiate(etiqueta, myScrollView.transform);
                        e.GetComponent<EtiquetaHistorial>().SetDatosVacio();
                        myScrollView.GetComponent<RectTransform>().sizeDelta += new Vector2(0, etiqueta.GetComponent<RectTransform>().sizeDelta.y);
                    }
                }
                else
                {
                    nombre.text = s[0] + " " + s[1];
                    for (int o = 0; o < 5; ++o)
                    {
                        GameObject e = Instantiate(etiqueta, myScrollView.transform);
                        e.GetComponent<EtiquetaHistorial>().SetDatosVacio();
                        myScrollView.GetComponent<RectTransform>().sizeDelta += new Vector2(0, etiqueta.GetComponent<RectTransform>().sizeDelta.y);
                    }

                }
            }
            esperando_Respuesta = false;
        }
    }*/
}
