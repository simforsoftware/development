using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Data;

public class infoUsuario : MonoBehaviour
{

    [SerializeField] Text nombre;
    [SerializeField] Text apellido;
    [SerializeField] Text email;

    [SerializeField] Text id;
    [SerializeField] Text grupo;

    [SerializeField] Dropdown idDrop;
    [SerializeField] Dropdown nombreDrop;

    bool busquedaPorNombre = false;
    bool busquedaPorId = false;
    bool instructor = false;
    bool esperandoRespuesta = false;

    UsuariosDB usuarios;

    private void Start()
    {
        usuarios = new UsuariosDB();
        Primera_Vez();
    }
    
    public void Busqueda_Por_Nombre()
    {
        busquedaPorNombre = true;
        FGet_Info();
    }
    public void Busqueda_por_Id()
    {
        busquedaPorId = true;
        FGet_Info();
    }
    public void Primera_Vez()
    {
        FGetAllAlumnos();
        FGetAllIds();
        FGet_Info();
    }
    //DEPRECATED
    /*IEnumerator Get_All_Alumnos()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getAllAlumnos.php"))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');

                esperandoRespuesta = true;

                List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
                for (int i = 0; i < s.Length; ++i)
                {
                    Dropdown.OptionData a = new Dropdown.OptionData();
                    a.text = s[i];
                    lista.Add(a);
                }
                nombreDrop.AddOptions(lista);

                esperandoRespuesta = false;
            }
        }
    }*/

    public void FGetAllAlumnos()
    {
        IDataReader reader = usuarios.Get_AllNombres();

        List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
        while (reader.Read())
        {
            Dropdown.OptionData a = new Dropdown.OptionData();
            a.text = reader[0].ToString() ;
            lista.Add(a);
        }
        nombreDrop.AddOptions(lista);
        reader.Close();
    }

    public void FGetAllIds()
    {
        IDataReader reader = usuarios.Get_AllIds();
        List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
        while (reader.Read())
        {
            Dropdown.OptionData a = new Dropdown.OptionData();
            a.text = reader[0].ToString();
            lista.Add(a);
        }
        idDrop.AddOptions(lista);
    }
    //DEPRECATED
    /*IEnumerator Get_All_Ids()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/GetAllid.php"))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');

                esperandoRespuesta = true;

                List<Dropdown.OptionData> lista = new List<Dropdown.OptionData>();
                for (int i = 0; i < s.Length; ++i)
                {
                    Dropdown.OptionData a = new Dropdown.OptionData();
                    a.text = s[i];
                    lista.Add(a);
                }
                idDrop.AddOptions(lista);

                esperandoRespuesta = false;
            }
        }
    }*/
    public void Cambio_Contraseņa()
    {
        //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre;

        if (instructor)
        {
            AuxiliarBuild.instance.IDcambio = int.Parse( idDrop.options[idDrop.value].text);
        }
    }
    //DEPRECATED
    /*
    IEnumerator Get_Info()
    {
        if (esperandoRespuesta) yield break;
        esperandoRespuesta = true;
        //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre;
        id.text = AuxiliarBuild.instance.IDAlumno.ToString();
        grupo.text = AuxiliarBuild.instance.IDGrupo.ToString();

        WWWForm form = new WWWForm();
        if (busquedaPorNombre)
        {
            //form.AddField("nombre", nombreDrop.options[nombreDrop.value].text);
            Debug.Log( idDrop.options[nombreDrop.value].text);
            form.AddField("id", idDrop.options[nombreDrop.value].text);
        }
        else if (busquedaPorId)
        {
            form.AddField("id", idDrop.options[idDrop.value].text);
        }
        else
        {
            form.AddField("id", AuxiliarBuild.instance.IDAlumno);
        }

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/getInfoUsuario.php", form))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] s = www.downloadHandler.text.Split(',');
                if(s.Length > 1)
                {
                    if (!instructor)
                    {
                        nombre.text = s[0];
                        apellido.text = s[1];
                        email.text = s[2];
                        if (s[3] == "yes")
                        {
                            idDrop.gameObject.SetActive(true);
                            for (int i = 0; i < idDrop.options.Count; ++i)
                            {
                                if (idDrop.options[i].text == s[4])
                                {
                                    idDrop.value = i;
                                }
                            }

                            nombreDrop.gameObject.SetActive(true);
                            for (int i = 0; i < nombreDrop.options.Count; ++i)
                            {
                                if (nombreDrop.options[i].text == s[0])
                                {
                                    nombreDrop.value = i;
                                }
                            }

                            AuxiliarBuild.instance.esInstructor = true;
                           


                            instructor = true;
                        }
                        else
                        {
                            AuxiliarBuild.instance.esInstructor = false;
                        }
                    }
                    else
                    {
                        for(int i = 0; i < nombreDrop.options.Count; ++i)
                        {
                            if(nombreDrop.options[i].text == s[0])
                            {
                                nombreDrop.value = i;
                            }
                        }
                        apellido.text = s[1];
                        email.text = s[2];
                        grupo.text = s[5];
                        for(int i = 0; i < idDrop.options.Count; ++i)
                        {
                            if(idDrop.options[i].text == s[4])
                            {
                                idDrop.value = i;
                            }
                        }
                    }
                }
            }
            busquedaPorNombre = false;
            busquedaPorId = false;
            esperandoRespuesta = false;
        }
    }*/

    public void FGet_Info()
    {


        id.text = AuxiliarBuild.instance.IDAlumno.ToString();
        grupo.text = AuxiliarBuild.instance.IDGrupo.ToString();

        IDataReader reader;
        if (busquedaPorNombre)
        {
            //form.AddField("nombre", nombreDrop.options[nombreDrop.value].text);

            reader = usuarios.GetDataFromIntTypeByInt(int.Parse(idDrop.options[nombreDrop.value].text));
        }
        else if (busquedaPorId)
        {
            reader = usuarios.GetDataFromIntTypeByInt(int.Parse(idDrop.options[idDrop.value].text));
        }
        else
        {
            reader = usuarios.GetDataFromIntTypeByInt(AuxiliarBuild.instance.IDAlumno);
        }

        if (!instructor)
        {
            nombre.text = reader[1].ToString();
            apellido.text = reader[2].ToString();
            email.text = reader[4].ToString();
            if (reader[5].ToString() == "yes")
            {
                idDrop.gameObject.SetActive(true);
                for (int i = 0; i < idDrop.options.Count; ++i)
                {
                    if (idDrop.options[i].text == reader[0].ToString())
                    {
                        idDrop.value = i;
                    }
                }

                nombreDrop.gameObject.SetActive(true);
                for (int i = 0; i < nombreDrop.options.Count; ++i)
                {
                    if (nombreDrop.options[i].text == reader[1].ToString())
                    {
                        nombreDrop.value = i;
                    }
                }

                AuxiliarBuild.instance.esInstructor = true;
                instructor = true;
            }
            else
            {
                AuxiliarBuild.instance.esInstructor = false;
            }
        }
        else
        {
            for (int i = 0; i < nombreDrop.options.Count; ++i)
            {
                if (nombreDrop.options[i].text == reader[1].ToString())
                {
                    nombreDrop.value = i;
                }
            }
            apellido.text = reader[2].ToString();
            email.text = reader[4].ToString();
            grupo.text = reader[6].ToString();
            for (int i = 0; i < idDrop.options.Count; ++i)
            {
                if (idDrop.options[i].text == reader[0].ToString())
                {
                    idDrop.value = i;
                }
            }
        }

        reader.Close();
        busquedaPorNombre = false;
        busquedaPorId = false;


    }
}
