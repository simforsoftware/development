using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Data;

public class CambiarContrasena : MonoBehaviour
{

    [SerializeField] InputField nuevaContraseña1;
    [SerializeField] InputField nuevaContraseña2;
    [SerializeField] InputField antiguaContraseña;
    [SerializeField] Text text;
    [SerializeField] Button cambiar;

    bool instructor = false;
    int idCambio;

    UsuariosDB usuarios;
    // Start is called before the first frame update
    void Start()
    {
        usuarios = new UsuariosDB();
        //Result alumnoData = Resources.Load<ReadOnlyResultados>("ReadOnly").resultPadre;

        if (AuxiliarBuild.instance.esInstructor)
        {
            instructor = true;
            antiguaContraseña.gameObject.SetActive(false);
            text.gameObject.SetActive(false);
            idCambio = AuxiliarBuild.instance.IDcambio;

        }
        else
        {
            idCambio = AuxiliarBuild.instance.IDAlumno;
        }
    }
    public void CheckInputs()
    {
        if (nuevaContraseña1.text == nuevaContraseña2.text) cambiar.interactable = true;
        else cambiar.interactable = false;
    
    }
    public void Cambiar_Contraseña()
    {
        if (!instructor) FComprobacion();
        else FCambiarContraseña();
    }

    //DEPRECATED
    /*
    IEnumerator Comprobacion()
    {
        WWWForm form = new WWWForm();
        form.AddField("id", idCambio);
        form.AddField("contraseña", antiguaContraseña.text);


        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/CheckContraseñaByID.php", form))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                if (www.downloadHandler.text[0] == '0')
                {
                    StartCoroutine(CCambiar_Contraseña());
                }
            }
        }
    }*/

    public void FComprobacion()
    {
        if(antiguaContraseña.text == Encriptador.Decode(usuarios.GetPasswordUsingId(idCambio)))
        {
            FCambiarContraseña();
        }
        else
        {
            Debug.LogError("La contraseña es incorrecta");
        }
    }
    public void FCambiarContraseña()
    {
        bool error = false;
        try
        {
            usuarios.UpdatePasswordUsingId(Encriptador.Encode(nuevaContraseña1.text), idCambio);
        }
        catch
        {
            error = true;
            Debug.LogError("PROBLEMA A LA HORA DE ACTUALIZAR LA CONSTRASEÑA");
            throw;
        }
        finally
        {
            if (!error)
            {
                ScenesSelector.GoToScene(Scenes.Login);
            }
        }
    }
    //DEPRECATED
    /*
    IEnumerator CCambiar_Contraseña()
    {
        WWWForm form = new WWWForm();
        form.AddField("id", idCambio);
        form.AddField("contraseña", nuevaContraseña1.text);


        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/cambioContraseña.php", form))
        {
            yield return www.Send();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }*/
}
