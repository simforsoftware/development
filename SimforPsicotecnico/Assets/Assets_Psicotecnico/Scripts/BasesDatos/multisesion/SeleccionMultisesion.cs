using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SeleccionMultisesion : MonoBehaviour
{

    [SerializeField] GameObject boton;
    [SerializeField] GameObject botonInstruc;

    private void Start()
    {
        Multisesion.DownLoadMultisesiones();
        RellenarBotones();
        ComprobarInstrucctor();
    }

    public void RellenarBotones()
    {


        for(int i = 0; i < 3; ++i)
        {
            Debug.Log("DAFAUQ");
            GameObject nuevoBoton = Instantiate(boton, transform);

            if (Multisesion.multisesiones[i].Count == 0)
            {
                nuevoBoton.GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto("loc_0190");
                nuevoBoton.GetComponent<Button>().interactable = false;

            }
            else
            {
                BotonMultisesion m = nuevoBoton.GetComponent<BotonMultisesion>();
                
                nuevoBoton.GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto("loc_0177");

                for (int j = 0; j < Multisesion.multisesiones[i].Count; ++j)
                {
                    m.ejercicios.Add((Scenes)Multisesion.multisesiones[i][j]);
                }
            }
        }
    }

    public void ComprobarInstrucctor()
    {
        UsuariosDB usuarios = new UsuariosDB();

        string instruc = usuarios.GetInstructorUsingId(AuxiliarBuild.instance.IDAlumno);
        if (instruc == "yes")
        {
            botonInstruc.SetActive(true);
        }
    }
}

