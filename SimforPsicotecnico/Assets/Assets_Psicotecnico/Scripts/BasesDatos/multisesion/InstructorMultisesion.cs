using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructorMultisesion : MonoBehaviour
{
    [SerializeField] GameObject boton;


    private void Start()
    {
        RellenarBotones();
    }
    public void RellenarBotones()
    {


        for (int i = 0; i < 3; ++i)
        {
            GameObject nuevoBoton = Instantiate(boton, transform);

            nuevoBoton.GetComponent<botonControladorMultisesion>().indexMultisesion = i;
            if (Multisesion.multisesiones[i].Count == 0)
            {
                nuevoBoton.GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto("loc_0113");
            }
            else
            {
                BotonMultisesion m = nuevoBoton.GetComponent<BotonMultisesion>();

                nuevoBoton.GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto("loc_0107") ;
                nuevoBoton.GetComponent<botonControladorMultisesion>().deleteButton.gameObject.SetActive(true);

            }
        }
    }
}
