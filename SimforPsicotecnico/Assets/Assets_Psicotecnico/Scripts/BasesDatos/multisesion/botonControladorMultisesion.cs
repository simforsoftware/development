using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class botonControladorMultisesion : MonoBehaviour
{
    // Start is called before the first frame update
    public int indexMultisesion;
    public Button deleteButton;
    public Button editButton;

    private void Awake()
    {
        deleteButton.gameObject.SetActive(false);
    }

    public void Borrar()
    {
        Multisesion.BorrarUnaMultisesion(indexMultisesion);
        deleteButton.gameObject.SetActive(false);
        GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto("loc_0113");
        Debug.Log("borro");
    }

    public void SelecionarMultisesionAEditar()
    {
        Multisesion.multisesionAEditar = indexMultisesion;
    }
}
