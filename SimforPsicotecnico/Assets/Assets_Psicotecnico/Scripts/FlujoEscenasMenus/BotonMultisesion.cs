using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonMultisesion : MonoBehaviour
{
    public List<Scenes> ejercicios = new List<Scenes>();

    public void ChangeScene()
    {
        Multisesion.ejercicios = new List<Scenes>();
        foreach(Scenes s in ejercicios)
        {
            Multisesion.ejercicios.Add(s);
        }
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}
