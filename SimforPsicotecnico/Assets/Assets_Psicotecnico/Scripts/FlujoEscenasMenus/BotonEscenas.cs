using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script Auxiliar para gestionar la funcionalidad de los botones encargados de cambiar de escena
public class BotonEscenas : MonoBehaviour
{
    public Scenes escenaObjetivo;

    public void ChangeScene()
    {
        ScenesSelector.GoToScene(escenaObjetivo);
    }
}
