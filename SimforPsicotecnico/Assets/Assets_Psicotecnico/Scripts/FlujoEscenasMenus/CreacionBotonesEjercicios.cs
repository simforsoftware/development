using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//A partir de un modulo crea una interfaz desde la que acceder a los correspondientes ejercicios
public class CreacionBotonesEjercicios : MonoBehaviour
{
    Modulo ejercicios;
    [SerializeField] GameObject botonEjercicio;
    [SerializeField] Text titulo;

    private void Awake()
    {
        ejercicios = Resources.Load<Selections>("Selections").activeModulo;

        titulo.text = Localizador.Texto(ejercicios.nombre);

        foreach (InfoTest ejercicio in ejercicios.tests)
        {
      
            GameObject nuevoBoton = Instantiate(botonEjercicio, transform);
            nuevoBoton.GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto(ejercicio.nombre);
            nuevoBoton.GetComponent<BotonEjercicios>().icono.sprite = ejercicio.icono;
            nuevoBoton.GetComponent<BotonEscenas>().escenaObjetivo = ejercicio.escena;
        }
    }
}
