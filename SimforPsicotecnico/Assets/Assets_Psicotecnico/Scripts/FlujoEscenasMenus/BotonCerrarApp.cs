using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Boton encargado de cerrar la aplicacion
public class BotonCerrarApp : MonoBehaviour
{
    public void Salir()
    {
        Application.Quit();
    }
}
