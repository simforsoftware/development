using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class Intro : MonoBehaviour
{
    [SerializeField] Scenes primeraEscena;
    [SerializeField] Localizador.Idioma idioma;

    string textoBase;
    private IEnumerator Start()
    {
        Application.targetFrameRate = 60;
        yield return StartCoroutine(CargaTextos());
        Localizador.ActualizarTextos(textoBase);
        Localizador.idioma = idioma;
        ScenesSelector.GoToScene(primeraEscena);
    }

    private IEnumerator CargaTextos()
    {
        WWW csv = new WWW("https://docs.google.com/spreadsheets/d/e/2PACX-1vRRHKkjSDfaicWkCSZTNiBvqTJMxySkmm81TN8a_w55JBp5bXiIVUEHqrBDBlF5zbq_dQBqS1lImLOo/pub?gid=0&single=true&output=csv");
        
        yield return csv;

        if (csv.error != null)
        {
            Debug.LogError("Sin conexion, usando archivo local");
            textoBase = "";
        }
        else
        {
            Debug.Log("CSV Descargado:");
            Debug.Log(csv.text);
            string pathName = Path.Combine(Application.streamingAssetsPath, "textos");
            textoBase = csv.text;
            FileStream loc = File.Open(pathName, FileMode.Create, FileAccess.ReadWrite);
            StreamWriter sw = new StreamWriter(loc);
            sw.Write(textoBase);
            sw.Close();
            loc.Close();
        }
    }
}
