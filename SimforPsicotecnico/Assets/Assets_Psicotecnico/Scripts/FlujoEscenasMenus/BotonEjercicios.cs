using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonEjercicios : MonoBehaviour
{
    public Text nombre;
    public Image icono;
    [SerializeField] private bool vrMode = true;


    private void Start()
    {
        if (vrMode)
            ReajustarColliderF();
    }

    public void ReajustarColliderF()
    {
        StartCoroutine(ReajustarCollider());
    }
    IEnumerator ReajustarCollider()
    {
        yield return new WaitForSeconds(1);
        float width = this.GetComponent<RectTransform>().rect.width;
        Vector3 newSize = new Vector3(width, this.GetComponent<BoxCollider>().size.y, this.GetComponent<BoxCollider>().size.z);
        this.GetComponent<BoxCollider>().size = newSize;
    }
}
