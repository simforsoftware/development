using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//A partir de una lista de modulos crea una interfaz desde la que acceder a los correspondientes modulos
public class CreacionBotonesModulos : MonoBehaviour
{
    [SerializeField] ListaModulos modulos;
    [SerializeField] GameObject botonModulo;
    [SerializeField] bool multi;

    private void Awake()
    {
        if (multi)
        {
            foreach (Modulo modulo in modulos.tests)
            {
                GameObject nuevoBoton = Instantiate(botonModulo, transform);
                BotonMultisesion bm = nuevoBoton.GetComponent<BotonMultisesion>();
                foreach (InfoTest test in modulo.tests)
                {
                    bm.ejercicios.Add(test.escena);
                }                
                nuevoBoton.GetComponent<BotonEjercicios>().nombre.text = Localizador.Texto(modulo.nombre);
                nuevoBoton.GetComponent<BotonEjercicios>().icono.sprite = modulo.icono;
            }
        }
        else
        {
            foreach (Modulo modulo in modulos.tests)
            {
                GameObject nuevoBoton = Instantiate(botonModulo, transform);
                nuevoBoton.GetComponent<BotonModulo>().nombre.text = Localizador.Texto(modulo.nombre);
                nuevoBoton.GetComponent<BotonModulo>().modulo = modulo;
                nuevoBoton.GetComponent<BotonModulo>().icono.sprite = modulo.icono;
                if (transform.childCount == 4)
                {
                    Instantiate(Resources.Load<GameObject>("empty"), transform);
                }
            }
        }
    }   
}