using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Informacion del modulo al que esta asociado este boton
public class BotonModulo : BotonEjercicios
{
    [HideInInspector] public Modulo modulo;

    public void SetSelectedModule()
    {
        Resources.Load<Selections>("Selections").activeModulo = modulo;
        Resources.Load<Selections>("Selections").SetDirty();
    }

    private void Start()
    {
        base.ReajustarColliderF();
    }
}
