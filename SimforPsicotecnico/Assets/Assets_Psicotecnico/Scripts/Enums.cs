﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Initialization, 
    Instructions, 
    Countdown, 
    StartGameplay, 
    EndGameplay
}
