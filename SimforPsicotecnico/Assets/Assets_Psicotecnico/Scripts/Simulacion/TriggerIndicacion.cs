using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerIndicacion : MonoBehaviour
{
    public string indicacion;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            Simulacion1Manager.instance.Indicacion(indicacion);
        }
    }
}
