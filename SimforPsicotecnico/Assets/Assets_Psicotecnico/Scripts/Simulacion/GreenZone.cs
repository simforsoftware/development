using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenZone : MonoBehaviour
{
    [SerializeField] float speed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
            Simulacion1Manager.instance.GreenZone(speed);
    }
}
