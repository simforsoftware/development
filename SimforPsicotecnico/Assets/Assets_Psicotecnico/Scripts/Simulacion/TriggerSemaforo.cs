using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSemaforo : MonoBehaviour
{
    [SerializeField] Semaforo semaforo;
    public float timeToGreen;
    float timer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            semaforo.Red();
            timer = timeToGreen;
        }
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0) semaforo.Green();
    }
}
