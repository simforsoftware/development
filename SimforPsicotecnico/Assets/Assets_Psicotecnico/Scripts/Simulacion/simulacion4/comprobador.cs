using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class comprobador : MonoBehaviour
{
    [SerializeField] GameObject p;
    [SerializeField] string etiqueta;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        if (other.gameObject.GetComponent<policia>())
        {
            if (!p.GetComponent<areaTrigger>().dentro)
            {
                Simulacion1Manager.instance.Fallo(etiqueta);
            }
        }
    }

}
