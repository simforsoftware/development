using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerPolicia : MonoBehaviour
{
    [SerializeField] policia policia;



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            policia.listo = true;
            policia.gameObject.GetComponent<AudioSource>().Play();
        }
    }
}
