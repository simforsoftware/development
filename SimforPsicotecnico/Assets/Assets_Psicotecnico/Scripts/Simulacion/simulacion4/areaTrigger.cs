using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class areaTrigger : MonoBehaviour
{

    public bool dentro = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car")) dentro = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("car"))  dentro = false;
    }
}
