using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class policia : MonoBehaviour
{
    public static policia instance;

    [SerializeField] float velocidad;
    [SerializeField] List<GameObject> ruedas;
    [SerializeField] float velocidad_Rotacion_Ruedas;

    Vector3 posicion_Original;
    public bool listo = false;

    [SerializeField] List<GameObject> posiciones;
    int index;

    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);
    }
    private void Start()
    {
        posicion_Original = transform.position;
        Rotar();
    }

    private void Update()
    {
        if (index >= posiciones.Count)
        {
            return;
        }
        foreach (GameObject r in ruedas)
        {
            r.transform.Rotate(new Vector3(0, 1, 0), -velocidad_Rotacion_Ruedas * Time.deltaTime);
        }
        if (!listo) return;
        transform.position += transform.forward * Time.deltaTime * velocidad;
        
        if(transform.position.z >= posiciones[index].transform.position.z)
        {
            ++index;
            Rotar();
        }

    }
    public void Rotar()
    {
        if(index >= posiciones.Count)
        {
           // this.gameObject.SetActive(false);
            return;
        }
        transform.LookAt(posiciones[index].transform);
    }

    public void Respawn()
    {
        //this.gameObject.SetActive(true);
        listo = false;
        transform.position = posicion_Original;
        index = 0;
        Rotar();
    }

    
}
