using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTracker : MonoBehaviour
{
    float timer = 1f;
    bool active = true;
    private void OnTriggerEnter(Collider other)
    {
        if (active && other.CompareTag("car"))
        {
            print("Event");
            Simulacion1Manager.instance.SaveEvent();
            active = false;
            Invoke("Reactivate", timer);
        }
    }

    void Reactivate()
    {
        active = true;
    }
}
