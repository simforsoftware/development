using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocimetro : MonoBehaviour
{
    public static Velocimetro instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }

    public float Velocidad()
    {
        float speed = 0;

        speed = 135 - transform.rotation.eulerAngles.z;        

        return speed;
    }
}
