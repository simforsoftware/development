using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparcamientoTrigger : MonoBehaviour
{
    [SerializeField] GameObject handbreak;

    bool fuera;

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            fuera = true;            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            fuera = false;
        }
    }

    private void Update()
    {
        if (!fuera) return;

        if (VehiclePhysics.UI.IgnitionKey.key == -1)
            Simulacion1Manager.instance.EndTest();
    }
}
