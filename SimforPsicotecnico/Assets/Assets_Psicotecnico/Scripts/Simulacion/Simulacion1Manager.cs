using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VehiclePhysics;
using UnityEngine.Networking;
using UnityEngine.InputSystem;
using WindowsInput;
using WindowsInput.Native;



public class Simulacion1Manager : MonoBehaviour
{
    public static Simulacion1Manager instance;

    [SerializeField] GameObject coche;
    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }
    int freezes = 0;
    [SerializeField] GameObject mensaje;
    [SerializeField] MensajeFallo mensajeFallo;
    [SerializeField] MensajeFallo mensajeIndicacion;
     ResultSimulacion3 resultados;

    [SerializeField] List<GameObject> obstaculosOpcionales;

    [SerializeField] float probabilidadObstaculo;
    [SerializeField] int tipo;

    public float delayFallos, margenAceleracion;
    float minSpeed, maxSpeed;
    float timer, timerAceleracion;
    List<int> fallos = new List<int>();
    List<string> etiquetaFallos = new List<string>();
    bool tramoAcelerado;
    List<string> fallosCooldown = new List<string>();

    Vector3 respawnPosition;
    Quaternion respawnRotation;

    bool finalizado = false;

    float timeStart;
    //HP

    //Heart Rate
    int minHeartRate = int.MaxValue, maxHeartRate = int.MinValue;
    List<float> heartRateRegister = new List<float>();
    List<float> timesHR = new List<float>();
    float averageHeartRage;

    //Cognitive Load
    float minCognitiveLoad = float.MaxValue, maxCognitiveLoad = float.MinValue;
    List<float> cognitiveLoadRegister = new List<float>();
    List<float> timesCL = new List<float>();
    float averageCognitiveLoad;

    //Pupil Dilatation
    float minPupilDilatation = float.MaxValue, maxPupilDilatation = float.MinValue;
    List<float> pupilDilatationRegister = new List<float>();
    List<float> pupilDilaltationBrute = new List<float>();
    List<float> timesPD = new List<float>();
    float averagePupilDilatation;

    //Eventos
    List<float> timeEvents = new List<float>();

    InputSimulator IS;

    private void Start()
    {
        if (BuildDebugger.instance) BuildDebugger.instance.Print_Message("HOLA QUE ASE");
        resultados = new ResultSimulacion3();
        respawnPosition = coche.transform.position + new Vector3(0, 0.15f, 0);
        respawnRotation = coche.transform.rotation;
        //RandomizarObstaculos();
        StartCoroutine(start());

        IS = new InputSimulator();

        IS.Keyboard.KeyDown(VirtualKeyCode.VK_K);
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (tramoAcelerado) ComprobacionAceleracion();
    }

    void RandomizarObstaculos()
    {
        foreach(GameObject g in obstaculosOpcionales)
        {
            if (Random.Range(0, 100) < probabilidadObstaculo * 100f) g.SetActive(true);
            else g.SetActive(false);
        }
    }

    void ComprobacionAceleracion()
    {
        float velocidad = Velocimetro.instance.Velocidad();
        if (velocidad < minSpeed)
        {
            timerAceleracion -= Time.deltaTime;
            if (timerAceleracion > 0) return;
            Fallo("Velocidad Minima No Alcanzada");
        }
        else if (velocidad > maxSpeed)
        {
            timerAceleracion -= Time.deltaTime;
            if (timerAceleracion > 0) return;
            Fallo("Superado Limite De Velocidad");
        }
    }

    void Arrancar()
    {
        IS.Keyboard.KeyDown(VirtualKeyCode.VK_K);
        IS.Keyboard.KeyUp(VirtualKeyCode.VK_K);
    }
    IEnumerator Estabilizar()
    {
        yield return new WaitForSeconds(0.01f);
        IS.Keyboard.KeyDown(VirtualKeyCode.RETURN);
        IS.Keyboard.KeyUp(VirtualKeyCode.RETURN);
        coche.transform.rotation = respawnRotation;
    }

    public void StartTest()
    {
        timer = 0;
        print("Start Test");

        timeStart = Time.time;
        InvokeRepeating("AccumulateHeartRate", 0, 5);
        InvokeRepeating("AccumulateCognitiveLoad", 0, 5);
        InvokeRepeating("BruteAccumulatePupilDilatation", 0, 0.1f);
        InvokeRepeating("AccumulatePupilDilatation", 0, 5);
    }

    public void Fallo(string etiqueta)
    {
        if (fallosCooldown.Contains(etiqueta)) return;


        if (etiquetaFallos.Contains(etiqueta))
        {
            for (int i = 0; i < etiquetaFallos.Count; i++)
            {
                if (etiquetaFallos[i] == etiqueta)
                {
                    ++fallos[i];
                }
            }
        }
        else
        {
            etiquetaFallos.Add(etiqueta);
            fallos.Add(1);
        }
        StartCoroutine(ColdownFallos(etiqueta));
        mensajeFallo.MostrarFallo(etiqueta);

        if (etiqueta == "Fuera Circuito" || etiqueta == "Colision Obstaculo" || etiqueta == "Semaforo En Rojo") Respawn();
    }

    IEnumerator ColdownFallos(string etiqueta)
    {
        fallosCooldown.Add(etiqueta);
        yield return new WaitForSeconds(delayFallos);
        fallosCooldown.Remove(etiqueta);
    }

    public void EndTest()
    {
        if (finalizado) return;
        finalizado = true;

        CancelInvoke();


        ///Las listas a guardar para las graficas son seis:
        ///HR
        ///Datos    -> heartRateRegister
        ///Tiempos  -> timesHR
        ///CL
        ///Datos    -> cognitiveLoadRegister
        ///Timepos  -> timesCL
        ///PL
        ///Datos    -> pupilDilatationRegister
        ///Tiempos  -> timesPD
        ///

        Grafics.graficas = new List<Grafics.graficasData>();
        Grafics.eventos = new List<Grafics.eventData>();

        if (cognitiveLoadRegister.Count > 0) Grafics.graficas.Add(new Grafics.graficasData(cognitiveLoadRegister, timesCL, coloresGraficas.cognitiveLoad));
        if (heartRateRegister.Count > 0) Grafics.graficas.Add(new Grafics.graficasData(heartRateRegister, timesHR, coloresGraficas.hearthRate));
        if (pupilDilatationRegister.Count > 0) Grafics.graficas.Add(new Grafics.graficasData(pupilDilatationRegister, timesPD, coloresGraficas.pupilDilatation));
        if (timeEvents.Count > 0) Grafics.eventos.Add(new Grafics.eventData(timeEvents, coloresGraficas.events));

        float minutos = Time.time - timeStart;
        Grafics.minutos = (int)(minutos / 60) + 1;

        print(finalFallosString());
        resultados.duracion = timer ;
        resultados.veloidadInferiorFallos = 0;
        resultados.velocidadSuperiorFallos = 0;
        resultados.salidaFallos = 0;
        resultados.semaforoFallos = 0;
        resultados.choqueFallos = 0;

        //fix exponenciales
        if (minHeartRate < 40 || minHeartRate > 350) minHeartRate = 0;
        if (maxHeartRate > 350 || maxHeartRate < 40) maxHeartRate = 0;


        resultados.MaxHeartRate = maxHeartRate;
        resultados.MinHeartRate = minHeartRate;

        for (int i = 0; i < etiquetaFallos.Count; ++i)
        {
            switch (etiquetaFallos[i])
            {
                case "Velocidad Minima No Alcanzada":
                    resultados.veloidadInferiorFallos = fallos[i];
                        break;
                case "Superado Limite De Velocidad":
                    resultados.velocidadSuperiorFallos = fallos[i];
                    break;
                case "Fuera Circuito":
                    resultados.salidaFallos = fallos[i];
                    break;
                case "Semaforo En Rojo":
                    resultados.semaforoFallos = fallos[i];
                    break;
                case "Colision Obstaculo":
                    resultados.choqueFallos = fallos[i];
                    break;
            }
        }
        AuxiliarBuild.instance.tipo.Add( tipo);
        resultados.minutos = Grafics.minutos;
        resultados.graficas = Grafics.graficas;
        resultados.eventos = Grafics.eventos;
        resultados.enUso = true;
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        AuxiliarBuild.instance.resultados.Add(resultados);
        resultados.UploadAlumno();
        StartCoroutine(PrepararSubida());
    }
    string finalFallosString()
    {
        string s = "Final // ";
        for (int i = 0; i < fallos.Count; i++)
        {
            s += etiquetaFallos[i] + ": " + fallos[i] + " ";
        }
        s += "// Tiempo: " + timer;
        return s;
    }

    public void GreenZone(float referenceSpeed)
    {
        minSpeed = referenceSpeed + 10;
        maxSpeed = referenceSpeed + 10;
        mensaje.GetComponentInChildren<Text>().text = "Mantengase a " + referenceSpeed + "km/h";
        mensaje.SetActive(true);
        tramoAcelerado = true;
        timerAceleracion = margenAceleracion;
    }

    public void RedZone()
    {
        mensaje.SetActive(false);
        tramoAcelerado = false;
    }

    public void Respawn()
    {
        print("respawn");


        EventManager.TriggerEvent("Respawn");
        RedZone();

        /*Debug.Log(coche.GetComponent<VehicleBase>());
        //coche.GetComponent<VehicleBase>().enabled = false;
        coche.GetComponent<Rigidbody>().velocity = Vector3.zero;
        coche.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;*/


        coche.transform.position = respawnPosition;
        freezes = 0;

        if (policia.instance) policia.instance.Respawn();
        if (Respawneador.instance) Respawneador.instance.Resetear();

        StartCoroutine (Estabilizar());
        coche.transform.rotation = respawnRotation;
        //StartCoroutine(Estabilizador());
        //coche.transform.position = respawnPosition;
        //coche.transform.rotation = respawnRotation;
        Arrancar();
    }

    public void Indicacion(string indicacion)
    {
        mensajeIndicacion.MostrarFallo(indicacion);
    }

    public void SetRespawn(Transform newRespawn)
    {
        respawnPosition = newRespawn.position;
        respawnRotation = newRespawn.rotation;
    }

    IEnumerator Estabilizador()
    {
        //ESTO NO FUNUCIONA Y NO LLEGO A COMPRENDER PORQUE
        //coche.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        coche.transform.rotation = respawnRotation;
        coche.transform.position = respawnPosition;
        yield return new WaitForSeconds(0.01f);
        ++freezes;
        if (freezes < 30) StartCoroutine(Estabilizador());
        //coche.GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePosition;
        //coche.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        //coche.GetComponent<Rigidbody>().freezeRotation = false;
        StartCoroutine(Reactivar());
    }
    IEnumerator Reactivar()
    {
        yield return new WaitForSeconds(1f);
        coche.GetComponent<VehicleBase>().enabled = true;
    }

    IEnumerator PrepararSubida()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                string s = "";
                bool cambio = false;
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio && www.downloadHandler.text[i] == ':')
                    {
                        cambio = true;
                    }
                    else if (cambio)
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateEnd = s;
                yield return resultados.SubirSesion();
            }
        }
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
    IEnumerator start()
    {

        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                bool cambio = false;
                string s = "";
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio)
                    {
                        if (www.downloadHandler.text[i] == ':')
                        {
                            cambio = true;
                            resultados.fecha = s;
                            s = "";
                        }
                        else
                        {
                            s += www.downloadHandler.text[i];
                        }
                    }
                    else
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateInit = s;
            }
        }
    }

    public void SaveEvent()
    {
        timeEvents.Add(Time.time - timeStart);
    }




    #region Heart Rate
    // Your code using heart rate messages
    private void AccumulateHeartRate()
    {
        if (!HPManager.instance) return;
        int hr = HPManager.instance.heartRate;
        if (hr < 40 || hr > 350) return;

        // Min Max
        maxHeartRate = maxHeartRate > hr ? maxHeartRate : hr;
        minHeartRate = minHeartRate < hr ? minHeartRate : hr;

        //Average
        heartRateRegister.Add(hr);
        timesHR.Add(Time.time - timeStart);
        averageHeartRage = 0f;
        foreach (int i in heartRateRegister) averageHeartRage += i;
        averageHeartRage /= (float)heartRateRegister.Count;
    }
    #endregion

    #region Cognitive Load
    // Your code using heart rate messages
    private void AccumulateCognitiveLoad()
    {
        if (!HPManager.instance) return;
        float cl = HPManager.instance.cognitiveLoad;
        print("Cognitive: " + cl);
        if (cl <= 0 || cl > 1) return;

        // Min Max
        maxCognitiveLoad = maxCognitiveLoad > cl ? maxCognitiveLoad : cl;
        minCognitiveLoad = minCognitiveLoad < cl ? minCognitiveLoad : cl;

        //Average
        cognitiveLoadRegister.Add(cl);
        timesCL.Add(Time.time - timeStart);
        averageCognitiveLoad = 0f;
        foreach (float f in cognitiveLoadRegister) averageCognitiveLoad += f;
        averageCognitiveLoad /= (float)cognitiveLoadRegister.Count;
    }
    #endregion

    #region Pupil Dilatation
    private void BruteAccumulatePupilDilatation()
    {
        if (!HPManager.instance) return;
        float pd = HPManager.instance.pupilDilatation;

        if (pd < 2 || pd > 8) return;

        //Average
        pupilDilaltationBrute.Add(pd);        
    }
    private void AccumulatePupilDilatation()
    {
        if (pupilDilaltationBrute.Count <= 0) return;
        float pd = 0;

        foreach(float f in pupilDilaltationBrute)
        {
            pd += f;
        }

        pd /= pupilDilaltationBrute.Count;

        pupilDilaltationBrute = new List<float>();

        print("Dilatation: " + pd);

        // Min Max
        maxPupilDilatation = maxPupilDilatation > pd ? maxCognitiveLoad : pd;
        minPupilDilatation = minPupilDilatation < pd ? minCognitiveLoad : pd;

        pupilDilatationRegister.Add(pd);
        timesPD.Add(Time.time - timeStart);
        averagePupilDilatation = 0f;
        foreach (float f in pupilDilatationRegister) averagePupilDilatation += f;
        averagePupilDilatation /= (float)pupilDilatationRegister.Count;
    }
    #endregion
}
