using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaculoActivado : MonoBehaviour
{
    [SerializeField] GameObject destino;
    [SerializeField] float speed;
    public bool active;
    Vector3 initialPosition;

    private void Start()
    {
        initialPosition = transform.position;
        EventManager.StartListening("Respawn", Reset);
    }

    private void Update()
    {
        if (!active) return;

        Vector3 direccion = destino.transform.position - transform.position;
        direccion = Vector3.Normalize(direccion);

        transform.position += direccion * speed * Time.deltaTime;
    }

    public void Reset()
    {
        active = false;
        transform.position = initialPosition;
    }
}
