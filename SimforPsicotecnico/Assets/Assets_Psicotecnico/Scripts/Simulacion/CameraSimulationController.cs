using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSimulationController : MonoBehaviour
{
    [SerializeField] Transform head, vehicle;
    [SerializeField] Transform camera;

    private void Update()
    {
        camera.position = vehicle.position + head.localPosition;
        //camera.rotation = vehicle.rotation;
    }
}
