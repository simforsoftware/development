using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MensajeFallo : MonoBehaviour
{
    [SerializeField] float timeScreen;
    float timer;
    public void MostrarFallo(string etiqueta)
    {
        GetComponentInChildren<Text>().text = etiqueta;
        gameObject.SetActive(true);
        timer = timeScreen;
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0) gameObject.SetActive(false);
    }
}
