using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CierraDescripciones : MonoBehaviour
{
    Vector3 posicionOrigninal;
    [SerializeField] GameObject objetoADesactivar;

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("DESACTIVO DESCRIPCION");
        if (other.gameObject.CompareTag("car"))
        {
            objetoADesactivar.SetActive(false);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("DESACTIVO DESCRIPCION");
        if (collision.gameObject.CompareTag("car"))
        {
            objetoADesactivar.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("DESACTIVO DESCRIPCION");
        if (other.gameObject.CompareTag("car"))
        {
            objetoADesactivar.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("DESACTIVO DESCRIPCION");
        if (collision.gameObject.CompareTag("car"))
        {
            objetoADesactivar.SetActive(false);
        }
    }
    /*private void Start()
    {
        posicionOrigninal = transform.position;
    }
    private void Update()
    {
        if(posicionOrigninal != transform.position)
        {
            objetoADesactivar.SetActive(false);
            Destroy(this.gameObject);
        }
    }*/
}
