using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaforo : MonoBehaviour
{
    [SerializeField] GameObject areaAccion;

    [SerializeField] Material red, green;

    bool open;

    private void Start()
    {
        Green();

    }

    public void Red()
    {
        open = false;
        areaAccion.gameObject.SetActive(true);
        GetComponent<MeshRenderer>().material = red;
    }

    public void Green()
    {
        open = true;
        areaAccion.gameObject.SetActive(false);
        GetComponent<MeshRenderer>().material = green;
    }

    public void Swap()
    {
        if (open) Red();
        else Green();
    }
}
