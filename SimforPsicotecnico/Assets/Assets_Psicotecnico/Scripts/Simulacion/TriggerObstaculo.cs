using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObstaculo : MonoBehaviour
{
    [SerializeField] ObstaculoActivado obstaculo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
            obstaculo.active = true;
    }
}
