using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simulacionSemaforos : MonoBehaviour
{
    [SerializeField] Result123 resultados;

    List<Semaforo> listaSemaforo = new List<Semaforo>();

    [SerializeField] float minTiempoEntreCambios;
    [SerializeField] float maxTiempoEntreCambios;

    bool verde;
    private void Start()
    {
        verde = true;
        for(int i = 0; i < GetComponentsInChildren<Semaforo>().Length; ++i)
        {
            listaSemaforo.Add(GetComponentsInChildren<Semaforo>()[i]);
        }
        StartCoroutine(CambioColor());
    }

    IEnumerator CambioColor()
    {
        float f = Random.Range(minTiempoEntreCambios, maxTiempoEntreCambios);
        if (!verde) f = f / 3;
        yield return new WaitForSeconds(f);
        verde = !verde;
        Debug.Log("cambio de color");
        foreach(Semaforo s in listaSemaforo)
        {
            s.Swap();
        }
        StartCoroutine(CambioColor());
    }
}
