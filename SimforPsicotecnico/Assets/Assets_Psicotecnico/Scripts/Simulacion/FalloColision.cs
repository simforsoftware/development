using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FalloColision : MonoBehaviour
{
    [SerializeField] string etiqueta = "Velocidad Minima No Alcanzada";
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("car"))
            Simulacion1Manager.instance.Fallo(etiqueta);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
            Simulacion1Manager.instance.Fallo(etiqueta);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
            Simulacion1Manager.instance.Fallo(etiqueta);
    }
}
