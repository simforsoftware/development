using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTest : MonoBehaviour
{
    [SerializeField] GameObject end;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
        {
            Simulacion1Manager.instance.StartTest();
            end.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
