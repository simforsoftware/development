using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTest : MonoBehaviour
{   
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("car"))
            Simulacion1Manager.instance.EndTest();
    }
}
