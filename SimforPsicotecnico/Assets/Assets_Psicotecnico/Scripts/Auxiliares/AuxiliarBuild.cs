using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuxiliarBuild : MonoBehaviour
{

    public static AuxiliarBuild instance;
    public bool enUso, esInstructor;
    public int IDAlumno, IDGrupo, IDcambio;
    public List<int> tipo;
    public List<Result> resultados;
    public string ruta;
    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);
        resultados = new List<Result>();
        tipo = new List<int>();
        DontDestroyOnLoad(this.gameObject);
    }
}
