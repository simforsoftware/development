using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//Gestion de los inputs
public  class GodOfInputs:MonoBehaviour
{
    public static GodOfInputs instance;
    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);
    }
        /*
        private Test12 test12Input;
        private Mando mandoInputs;
        //Tengo que eliminar esto cuando tenga mas tiempo

        public bool acelerardor;
        public bool test5;
        public bool simulacion;
         bool acelerando = false;

        public InputAction movimiento1;
        public InputAction movimiento1b;
        public InputAction movimiento2;
        public InputAction movimiento2b;
        public InputAction mandoSinSimulacion;

        List<TypeImput> lista = new List<TypeImput>();
        List<TypeImput> continuedLista = new List<TypeImput>();
        private void Awake()
        {
            if (!instance) instance = this;
            else Destroy(this.gameObject);
            test12Input = new Test12();
            mandoInputs = new Mando();
            if (!simulacion && !test5)
            {
                mandoInputs.coche.acelerador.performed += PieDerecho;
                mandoInputs.coche.acelerador.Enable();
                mandoInputs.coche.freno.performed += PieIzquierdo;
                mandoInputs.coche.freno.Enable();
                /*mandoInputs.coche.steering.performed += Volante;
                mandoSinSimulacion = mandoInputs.coche.steering;*//*
                mandoInputs.coche.steering.Enable();
                mandoInputs.coche.a.performed += ManoIzquierda;
                mandoInputs.coche.a.Enable();
                mandoInputs.coche.b.performed += ManoDerecha;
                mandoInputs.coche.b.Enable();



            }

            if (!test5)
            {
                test12Input.signals.manoDerecha.performed += ManoDerecha;
                test12Input.signals.manoDerecha.Enable();
                test12Input.signals.manoIzquierda.performed += ManoIzquierda;
                test12Input.signals.manoIzquierda.Enable();
                if (!acelerardor)
                {
                    test12Input.signals.pieDerecho.performed += PieDerecho;
                    test12Input.signals.pieDerecho.Enable();
                }
                else
                {

                    test12Input.signals.Acelerar.started += StartedPieDerecho;
                    test12Input.signals.Acelerar.canceled += StoppedPieDerecho;
                    test12Input.signals.Enable();
                    mandoInputs.coche.acelerador.performed -= PieDerecho;
                    mandoInputs.coche.TrueAcelerador.started += StartedPieDerecho;
                    mandoInputs.coche.TrueAcelerador.canceled += StoppedPieDerecho;
                    mandoInputs.coche.Enable();
                }
                test12Input.signals.pieIzquierdo.performed += PieIzquierdo;
                test12Input.signals.pieIzquierdo.Enable();
            }
            else
            {
                //movimiento1 = test12Input.signals.coche1;
                movimiento1 = mandoInputs.coche.c;
                movimiento1.Enable();
                movimiento1b = test12Input.signals.coche1;
                movimiento1b.Enable();
                movimiento2 = test12Input.signals.coche2;
                movimiento2.Enable();
            }

        }
        private void Update()
        {
            if (acelerando)
            {
                if (!continuedLista.Contains(TypeImput.PieDerecho)) continuedLista.Add(TypeImput.PieDerecho);
            }
            else if (test5)
            {
            }
        }

        public void MDisable()
        {
            mandoInputs.coche.a.Disable();
            mandoInputs.coche.b.Disable();
        }
        public Vector2 Get_Eje_Coche(bool derecha)
        {
            if (derecha) return movimiento2.ReadValue<Vector2>();
            else
            {
                if (movimiento1.ReadValue<Vector2>().x == 0) return movimiento1b.ReadValue<Vector2>();
                else return movimiento1.ReadValue<Vector2>();
            }
        }

        static bool pieDerechoPressed, pieIzquierdoPresed;

    */
        public  bool GetInput(TypeImput input)
    {
        switch (input)
        {            
            case TypeImput.ManoDerecha:
                return Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
            case TypeImput.ManoIzquierda:
                return Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
            case TypeImput.PieDerecho:
                return Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
            case TypeImput.PieIzquierdo:
                return Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
            default:
                return false;
        }

        //restos
        //bool respuesta = lista.Contains(input);
        //lista.Clear();
        /*if (respuesta)
        {
            lista.Clear();
        }*/

        //return respuesta;
    }
    /*
    public void Volante(InputAction.CallbackContext obj)
    {
        if (mandoSinSimulacion.ReadValue<Vector2>().x > 0)
        {
            if (!lista.Contains(TypeImput.ManoDerecha)) lista.Add(TypeImput.ManoDerecha);
        }
        else if (mandoSinSimulacion.ReadValue<Vector2>().x < 0)
        {
            if (!lista.Contains(TypeImput.ManoIzquierda)) lista.Add(TypeImput.ManoIzquierda);
        }
    }*/
    public TypeImput GetAnyInput()
    {
        if (Input.anyKey)
        {
            for (int i = 0; i < 600; i++)
            {
                if (Input.GetKey((KeyCode)i)) return TransformKeyCodeToTypeImput((KeyCode)i);
            }
        }

        return TypeImput.none;

        //restos
        //if (lista.Count == 0) return TypeImput.none;
        //else return lista[0];
    }

    TypeImput TransformKeyCodeToTypeImput(KeyCode kc)
    {
        switch (kc)
        {
            case KeyCode.D:
                return TypeImput.ManoDerecha;
            case KeyCode.A:
                return TypeImput.ManoIzquierda;
            case KeyCode.W:
                return TypeImput.PieDerecho;
            case KeyCode.S:
                return TypeImput.PieIzquierdo;
            case KeyCode.RightArrow:
                return TypeImput.ManoDerecha;
            case KeyCode.LeftArrow:
                return TypeImput.ManoIzquierda;
            case KeyCode.UpArrow:
                return TypeImput.PieDerecho;
            case KeyCode.DownArrow:
                return TypeImput.PieIzquierdo;
            default:
                return TypeImput.none;
        }
    }
    /*
    public bool GetPressedInput(TypeImput input)
    {
        bool respuesta = continuedLista.Contains(input);
        return respuesta;
    }
    public void StartedPieDerecho(InputAction.CallbackContext obj)
    {
        Debug.Log("Acelerar");
        acelerando = true;
    }
    public void StoppedPieDerecho(InputAction.CallbackContext obj)
    {
        Debug.Log("terminoAcelerar");
        acelerando = false;
        continuedLista.Remove(TypeImput.PieDerecho);
    }
    public void PieDerecho(InputAction.CallbackContext obj)
    {
        if (!lista.Contains(TypeImput.PieDerecho)) lista.Add(TypeImput.PieDerecho);
    }
    public void PieIzquierdo(InputAction.CallbackContext obj)
    {
        if (!lista.Contains(TypeImput.PieIzquierdo)) lista.Add(TypeImput.PieIzquierdo);
        /*
        if (!prelista.Contains(TypeImput.PieDerecho)) prelista.Add(TypeImput.PieIzquierdo);
        else
        {
             if (!lista.Contains(TypeImput.PieIzquierdo)) lista.Add(TypeImput.PieIzquierdo);
        }
    }
    public void ManoDerecha(InputAction.CallbackContext obj)
    {
        if (!lista.Contains(TypeImput.ManoDerecha)) lista.Add(TypeImput.ManoDerecha);
        /*
        if (!prelista.Contains(TypeImput.PieDerecho)) prelista.Add(TypeImput.ManoDerecha);
        else
        {
            if (!lista.Contains(TypeImput.ManoDerecha)) lista.Add(TypeImput.ManoDerecha);
        }
    }
    public void ManoIzquierda(InputAction.CallbackContext obj)
    {
        if (!lista.Contains(TypeImput.ManoIzquierda)) lista.Add(TypeImput.ManoIzquierda);
        /*
        if (!prelista.Contains(TypeImput.PieDerecho)) prelista.Add(TypeImput.ManoDerecha);
        else
        {
            if (!lista.Contains(TypeImput.ManoIzquierda)) lista.Add(TypeImput.ManoIzquierda);
        }
    }*/

}



public enum TypeImput
{
    none,
    ManoDerecha,
    ManoIzquierda,
    PieDerecho,
    PieIzquierdo
}
