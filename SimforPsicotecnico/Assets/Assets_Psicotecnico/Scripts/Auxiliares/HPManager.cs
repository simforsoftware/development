using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HP.Omnicept.Unity;
using HP.Omnicept.Messaging.Messages;

public class HPManager : MonoBehaviour
{
    public static HPManager instance;

                                                                                // 0.2hz
    public int heartRate;                                                       // Rango de deteccion (40, 350)

                                                                                // 120hz
    public Vector3 eyeTraking;                                                  // Direccion hacia donde se mira normalizada
    public Vector2 rightPupilPosition, leftPupilPosition, pupilPosition;        // Posicion en pantalla de la pupila (Centro = {0.5, 0.5})
    public float rightPupilDilatation, leftPupilDilatation, pupilDilatation;    // Diametro de la pupila en milimetros (Rango -> 2 / 8 mm)
    public bool rightOpen, leftOpen;                                            // Ojo abierto o cerrado

                                                                                // 1hz
    public float cognitiveLoad;                                                 // Capacidad de concentracion utilizada (Rango -> 0 nada concentrado / 1 maximo de concentración)


    private void Start()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        if (GliaBehaviour.instance)
        {
            GliaBehaviour.instance.OnHeartRate.AddListener(TrakeHR);
            GliaBehaviour.instance.OnEyeTracking.AddListener(TrakeEyeTraking);
            GliaBehaviour.instance.OnCognitiveLoad.AddListener(TrakeCognitiveLoad);
        }
    }  

    void TrakeHR(HeartRate hr)
    {
        heartRate = (int)hr.Rate;
    }

    void TrakeEyeTraking(EyeTracking et)
    {
        if (et.CombinedGaze.Confidence >= 0.5f) eyeTraking = new Vector3(et.CombinedGaze.X, et.CombinedGaze.Y, et.CombinedGaze.Z);
        if (et.RightEye.PupilPosition.Confidence >= 0.5f) rightPupilPosition = new Vector2(et.RightEye.PupilPosition.X, et.RightEye.PupilPosition.X);
        if (et.LeftEye.PupilPosition.Confidence >= 0.5f) leftPupilPosition = new Vector2(et.LeftEye.PupilPosition.X, et.LeftEye.PupilPosition.X);
        pupilPosition = (rightPupilPosition + leftPupilPosition) / 2;
        if (et.RightEye.PupilDilationConfidence >= 0.5f) rightPupilDilatation = et.RightEye.PupilDilation;
        if (et.LeftEye.PupilDilationConfidence >= 0.5f) leftPupilDilatation = et.LeftEye.PupilDilation;
        pupilDilatation = (rightPupilDilatation + leftPupilDilatation) / 2;
        if (et.RightEye.OpennessConfidence >= 0.5f) rightOpen = et.RightEye.Openness == 1;
        if (et.LeftEye.OpennessConfidence >= 0.5f) leftOpen = et.LeftEye.Openness == 1;
    }
    void TrakeCognitiveLoad(CognitiveLoad cl)
    {
        cognitiveLoad = cl.CognitiveLoadValue;
    }
}
