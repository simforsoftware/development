using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grafiquero : MonoBehaviour
{
    public float cognitiveLoadMedio = 0;
    public float cognitiveLoadMedioEventos = 0;
    public int fallosDesconcentrado = 0;

    public float mediaVariacionHRPostEvento = 0;
    public float maximaVariacionHRPostEvento = 0;
    public float mediaVariacionHRDuranteEvento = 0;

    List<float> clEvents = new List<float>();
    List<HRDataEvent> hrEvents = new List<HRDataEvent>();


    [SerializeField] Text min, max, time0, time05, time1;
    [SerializeField] GameObject dot, eventMarks, leyenda, etiquetasLeyenda;

    [SerializeField] float margen;

    public int samples;

    [SerializeField] bool relativizar;
    public float minutosPrintear;

    List<List<GameObject>> graficas = new List<List<GameObject>>();
    List<Color> coloresGraficas = new List<Color>();
    List<minMaxValues> leyendas = new List<minMaxValues>();
    [SerializeField] Color colorHearthRate, colorPupilDilatation, colorCognitiveLoad, colorEvents;

    int distanciaUsoValores;

    int showleyenda = 0;

    List<float> clvalueData = null;
    List<float> cltimesData = null;
    List<float> eventsData = null;
    List<float> hrvalueData = null;
    List<float> hrtimesData = null;

    private void Start()
    {
        time0.text = "0min";
    }

    class minMaxValues
    {
        public float min, max;
        public Color color;
        public float Average()
        {
            return (min + max) / 2f;
        }
    }
    public string UnitsByColor(Color color)
    {
        if (color == colorHearthRate)
        {
            return "hr";
        }
        else if(color == colorPupilDilatation)
        {
            return "mm";
        }
        else if(color == colorCognitiveLoad)
        {
            return "%";
        }

        return "";
    }

    void ChangeLeyenda()
    {
        List<minMaxValues> trueLeyenda = new List<minMaxValues>();
        foreach(minMaxValues mmv in leyendas)
        {
            if (mmv.max != 0) trueLeyenda.Add(mmv);
        }

        if (trueLeyenda.Count == 0)
        {
            min.text = max.text  = "";
            return;
        }

        string s = "";
        for (int i = 0; i < trueLeyenda.Count; i++)
        {
            s += trueLeyenda[i].min.ToString("F2");
            s += UnitsByColor(trueLeyenda[i].color);
            if (i < trueLeyenda.Count - 1) s += " / ";
        }
        min.text = s;

        s = "";
        for (int i = 0; i < trueLeyenda.Count; i++)
        {
           
            s += trueLeyenda[i].max.ToString("F2");
            s += UnitsByColor(trueLeyenda[i].color);
            if (i < trueLeyenda.Count - 1) s += " / ";
        }
        max.text = s;

        s = "";
        for (int i = 0; i < trueLeyenda.Count; i++)
        {
            s += trueLeyenda[i].Average().ToString("F2");
            s += UnitsByColor(trueLeyenda[i].color);
            if (i < trueLeyenda.Count - 1) s += " / ";
        }
        ClearLeyenda();
        foreach(Color color in coloresGraficas)
        {
            GameObject leyendita = Instantiate(etiquetasLeyenda, leyenda.transform);
            ActualizarLeyenda(leyendita, color);
        }

    }

    void ClearLeyenda()
    {
        List<GameObject> leyendas = new List<GameObject>();
        for (int i = 0; i < leyenda.transform.childCount; i++)
        {
            leyendas.Add(leyenda.transform.GetChild(i).gameObject);
        }

        for (int i = leyendas.Count - 1; i >= 0; i--)
        {
            Destroy(leyendas[i]);
        }
    }
    void ActualizarLeyenda(GameObject leyenda, Color color)
    {
        leyenda.GetComponentInChildren<Text>().color = color;
        string s = "";
        if(color == colorHearthRate)
        {
            s = Localizador.Texto("loc_0089");
        }
        else if(color == colorPupilDilatation)
        {
            s = Localizador.Texto("loc_0090");
        }
        else if(color == colorCognitiveLoad)
        {
            s = Localizador.Texto("loc_0091");
        }
        else if(color == colorEvents)
        {
            s = Localizador.Texto("loc_0092");
        }
        else
        {
            Destroy(leyenda);
        }
        leyenda.GetComponentInChildren<Text>().text = s;
    }

    public void Draw(List<float> values, List<float> times, coloresGraficas tipo)
    {
        if(tipo == global::coloresGraficas.cognitiveLoad)
        {
            DrawCL(values, times, tipo);
            return;
        }

        Color color = Color.black;
        switch (tipo)
        {
            case global::coloresGraficas.hearthRate:
                color = colorHearthRate;
                break;
            case global::coloresGraficas.pupilDilatation:
                color = colorPupilDilatation;
                break;
            case global::coloresGraficas.cognitiveLoad:
                color = colorCognitiveLoad;
                break;
            case global::coloresGraficas.events:
                color = colorEvents;
                break;
        }

        time05.text = (minutosPrintear / 2f).ToString() + "min";
        time1.text = minutosPrintear.ToString() + "min";
        distanciaUsoValores = 1;
        int numLista = 0;
        
        //Gestionar Graficas Repetidas por colores
        if (coloresGraficas.Contains(color))
        {
            for (int i = 0; i < coloresGraficas.Count; i++)
            {
                if (coloresGraficas[i] == color)
                {
                    numLista = i;
                    break;
                }
            }
            ClearList(graficas[numLista]);

        }
        else
        {
            coloresGraficas.Add(color);
            numLista = coloresGraficas.Count - 1;
            graficas.Add(new List<GameObject>());
            leyendas.Add(new minMaxValues());
            leyendas[leyendas.Count - 1].color = color;
        }
        if(values.Count == 0 || times.Count != values.Count)
        {
            print("No Hay suficientes Datos");
            return;
        }


        //Eliminar datos inservibles
        //CribaDatos(values);

        //Eliminar exceso de muestras
        while(values.Count/distanciaUsoValores > samples)
        {
            ++distanciaUsoValores;
        }

        

        float height = GetComponent<RectTransform>().rect.height - margen;
        float width = GetComponent<RectTransform>().rect.width - margen;

        float minValue = float.MaxValue;
        foreach (float f in values) minValue = f < minValue ? f : minValue;

        if (relativizar) leyendas[numLista].min = minValue;
        else leyendas[numLista].min = 0;

        List<float> relativeValues = new List<float>();

        float maxValue = float.MinValue;
        foreach (float f in values) maxValue = f > maxValue ? f : maxValue;

        leyendas[numLista].max = maxValue;

        float relativeMaxValue = maxValue;
        if (relativizar) relativeMaxValue -= minValue;

        for (int i = 0; i < values.Count; i++) relativeValues.Add(values[i]);
        if (relativizar) for (int i = 0; i < relativeValues.Count; i++) relativeValues[i] = values[i] - minValue;

        float heightScaler = height / relativeMaxValue;
        float widhtStep = width / samples;

        List<float> valuesSimple = Simplify(relativeValues, distanciaUsoValores);
        List<float> timesSimple = Simplify(times, distanciaUsoValores);

        float duracion = minutosPrintear * 60;
        float distanciaTemporal = (float)duracion / (float)samples;

        List<float> interpolateTimes = new List<float>();
        float time = 0;
        for (int i = 0; i < samples; i++)
        {
            interpolateTimes.Add(time);
            time += distanciaTemporal;
        }

        List<float> interpolateValues = new List<float>();
        for (int i = 0; i < samples; i++)
        {
            interpolateValues.Add(-1f);
        }

        int valueActual = 0;
        for (int i = 0; i < valuesSimple.Count; i++)
        {
            for (int j = 0; j < interpolateTimes.Count; j++)
            {
                if(timesSimple[i] <= interpolateTimes[j])
                {
                    interpolateValues[j] = valuesSimple[i];
                    break;
                }
            }
        }

        valueActual = -1;
        float valorInterpolacion = 0;
        bool fin = false;
        for (int i = 0; i < interpolateValues.Count; i++)
        {
            if (interpolateValues[i] != -1)
            {
                ++valueActual;
                if (valueActual + 1 < valuesSimple.Count)
                {
                    float distanciaEntreValores = (timesSimple[valueActual + 1] - timesSimple[valueActual]) / distanciaTemporal;
                    valorInterpolacion = (float)((float)valuesSimple[valueActual + 1] - (float)valuesSimple[valueActual]) / (float)distanciaEntreValores;
                }
                else
                {
                    fin = true;
                }
            }
            else if (fin)
            {
                interpolateValues[i] = -1;
            }
            else
            {
                if (i - 1 >= 0) interpolateValues[i] = interpolateValues[i - 1] + valorInterpolacion;
            }
        }


        for (int i = 0; i < interpolateValues.Count; i++)
        {
            if (interpolateValues[i] == -1) continue;
            GameObject newDot = Instantiate(dot, GetComponent<RectTransform>());
            newDot.GetComponent<Image>().color = color;
            newDot.GetComponent<RectTransform>().localPosition = new Vector2((i * widhtStep) + (margen / 2), (interpolateValues[i] * heightScaler) + (margen / 2)) - new Vector2((width + margen) / 2, (height + margen) / 2);
            graficas[numLista].Add(newDot);
        }

        ChangeLeyenda();

        if(tipo == global::coloresGraficas.hearthRate)
        {
            hrvalueData = new List<float>(values);
            hrtimesData = new List<float>(times);

            CruceDatosHREvents();
        }
    }

    void DrawCL(List<float> values, List<float> times, coloresGraficas tipo)
    {
        Color color = Color.black;
        switch (tipo)
        {
            case global::coloresGraficas.hearthRate:
                color = colorHearthRate;
                break;
            case global::coloresGraficas.pupilDilatation:
                color = colorPupilDilatation;
                break;
            case global::coloresGraficas.cognitiveLoad:
                color = colorCognitiveLoad;
                break;
            case global::coloresGraficas.events:
                color = colorEvents;
                break;
        }

        time05.text = (minutosPrintear / 2f).ToString() + "min";
        time1.text = minutosPrintear.ToString() + "min";
        distanciaUsoValores = 1;
        int numLista = 0;

        //Gestionar Graficas Repetidas por colores
        if (coloresGraficas.Contains(color))
        {
            for (int i = 0; i < coloresGraficas.Count; i++)
            {
                if (coloresGraficas[i] == color)
                {
                    numLista = i;
                    break;
                }
            }
            ClearList(graficas[numLista]);

        }
        else
        {
            coloresGraficas.Add(color);
            numLista = coloresGraficas.Count - 1;
            graficas.Add(new List<GameObject>());
            leyendas.Add(new minMaxValues());
            leyendas[leyendas.Count - 1].color = color;
        }
        if (values.Count == 0 || times.Count != values.Count)
        {
            print("No Hay suficientes Datos");
            return;
        }

        clvalueData = new List<float>(values);
        cltimesData = new List<float>(times);

        float valorMedio = 0;
        foreach(float f in values)
        {
            valorMedio += f;
        }
        valorMedio /= (float)values.Count;
        cognitiveLoadMedio = valorMedio;

        //Eliminar datos inservibles
        //CribaDatos(values);

        //Eliminar exceso de muestras
        while (values.Count / distanciaUsoValores > samples)
        {
            ++distanciaUsoValores;
        }



        float height = GetComponent<RectTransform>().rect.height - margen;
        float width = GetComponent<RectTransform>().rect.width - margen;

        float minValue = float.MaxValue;
        foreach (float f in values) minValue = f < minValue ? f : minValue;

        leyendas[numLista].min = 0;

        List<float> relativeValues = new List<float>();

        

        leyendas[numLista].max = 100;

        float relativeMaxValue = 100;
        

        for (int i = 0; i < values.Count; i++) relativeValues.Add(values[i]);

        float heightScaler = height / relativeMaxValue;
        float widhtStep = width / samples;

        List<float> valuesSimple = Simplify(relativeValues, distanciaUsoValores);
        List<float> timesSimple = Simplify(times, distanciaUsoValores);

        float duracion = minutosPrintear * 60;
        float distanciaTemporal = (float)duracion / (float)samples;

        List<float> interpolateTimes = new List<float>();
        float time = 0;
        for (int i = 0; i < samples; i++)
        {
            interpolateTimes.Add(time);
            time += distanciaTemporal;
        }

        List<float> interpolateValues = new List<float>();
        for (int i = 0; i < samples; i++)
        {
            interpolateValues.Add(-1f);
        }

        int valueActual = 0;
        for (int i = 0; i < valuesSimple.Count; i++)
        {
            for (int j = 0; j < interpolateTimes.Count; j++)
            {
                if (timesSimple[i] <= interpolateTimes[j])
                {
                    interpolateValues[j] = valuesSimple[i];
                    break;
                }
            }
        }

        valueActual = -1;
        float valorInterpolacion = 0;
        bool fin = false;
        for (int i = 0; i < interpolateValues.Count; i++)
        {
            if (interpolateValues[i] != -1)
            {
                ++valueActual;
                if (valueActual + 1 < valuesSimple.Count)
                {
                    float distanciaEntreValores = (timesSimple[valueActual + 1] - timesSimple[valueActual]) / distanciaTemporal;
                    valorInterpolacion = (float)((float)valuesSimple[valueActual + 1] - (float)valuesSimple[valueActual]) / (float)distanciaEntreValores;
                }
                else
                {
                    fin = true;
                }
            }
            else if (fin)
            {
                interpolateValues[i] = -1;
            }
            else
            {
                if (i - 1 >= 0) interpolateValues[i] = interpolateValues[i - 1] + valorInterpolacion;
            }
        }


        for (int i = 0; i < interpolateValues.Count; i++)
        {
            if (interpolateValues[i] == -1) continue;
            GameObject newDot = Instantiate(dot, GetComponent<RectTransform>());
            newDot.GetComponent<Image>().color = color;
            newDot.GetComponent<RectTransform>().localPosition = new Vector2((i * widhtStep) + (margen / 2), (interpolateValues[i] * heightScaler) + (margen / 2)) - new Vector2((width + margen) / 2, (height + margen) / 2);
            graficas[numLista].Add(newDot);

            GameObject newDotMedia = Instantiate(dot, GetComponent<RectTransform>());
            newDotMedia.GetComponent<Image>().color = color;
            newDotMedia.GetComponent<RectTransform>().localPosition = new Vector2((i * widhtStep) + (margen / 2), (valorMedio * heightScaler) + (margen / 2)) - new Vector2((width + margen) / 2, (height + margen) / 2);
            newDotMedia.GetComponent<RectTransform>().localScale *= 0.5f;
            graficas[numLista].Add(newDotMedia);
        }

        ChangeLeyenda();

        CruceDatosCLEvents();
    }

    public void Draw(List<float> timeEvents, coloresGraficas tipo)
    {
        Color color = Color.black;
        switch (tipo)
        {
            case global::coloresGraficas.hearthRate:
                color = colorHearthRate;
                break;
            case global::coloresGraficas.pupilDilatation:
                color = colorPupilDilatation;
                break;
            case global::coloresGraficas.cognitiveLoad:
                color = colorCognitiveLoad;
                break;
            case global::coloresGraficas.events:
                color = colorEvents;
                break;
        }

        time05.text = (minutosPrintear / 2f).ToString();
        time1.text = minutosPrintear.ToString();
        distanciaUsoValores = 1;
        int numLista = 0;

        //Gestionar Graficas Repetidas por colores
        if (coloresGraficas.Contains(color))
        {
            for (int i = 0; i < coloresGraficas.Count; i++)
            {
                if (coloresGraficas[i] == color)
                {
                    numLista = i;
                    break;
                }
            }
            ClearList(graficas[numLista]);

        }
        else
        {
            coloresGraficas.Add(color);
            numLista = coloresGraficas.Count - 1;
            graficas.Add(new List<GameObject>());
            leyendas.Add(new minMaxValues());
            leyendas[leyendas.Count - 1].color = color;
        }
        if (timeEvents.Count == 0)
        {
            print("No Hay suficientes Datos");
            return;
        }

        eventsData = new List<float>(timeEvents);

        float height = GetComponent<RectTransform>().rect.height;
        float width = GetComponent<RectTransform>().rect.width;

        float escalaTemporal = width / (minutosPrintear * 60);

        for (int i = 0; i < timeEvents.Count; i++)
        {
            GameObject newDot = Instantiate(eventMarks, GetComponent<RectTransform>());
            newDot.GetComponent<Image>().color = color;
            newDot.GetComponent<RectTransform>().localPosition = new Vector2(timeEvents[i] * escalaTemporal, height / 2) - new Vector2(width / 2, height / 2);
            graficas[numLista].Add(newDot);
        }

        CruceDatosCLEvents();
        CruceDatosHREvents();
    }

    void CruceDatosCLEvents()
    {
        if (cltimesData == null || clvalueData == null || eventsData == null) return;


        foreach(float f in eventsData)
        {
            for (int i = 0; i < cltimesData.Count; i++)
            {
                if (f <= cltimesData[i])
                {
                    clEvents.Add(clvalueData[i]);
                    break;
                }
            }
        }

        float media = 0;
        foreach (float f in clEvents) media += f;
        media /= (float)clEvents.Count;
        cognitiveLoadMedioEventos = media;

        float margen = 40;
        margen = margen < cognitiveLoadMedio ? margen : cognitiveLoadMedio;

        foreach(float f in clEvents)
        {
            if (f < margen) fallosDesconcentrado++;
        }

        print("CL Medio: " + cognitiveLoadMedio + " /// CL Medio Eventos: " + cognitiveLoadMedioEventos + " /// Fallos Desconcentrado: " + fallosDesconcentrado);
    }

    void CruceDatosHREvents()
    {
        if (hrvalueData == null || hrtimesData == null || eventsData == null) return;

        float margen = 2f;

        foreach(float f in eventsData)
        {
            float prev = 0, actual = 0, post = 0;

            for (int i = 0; i < hrtimesData.Count; i++)
            {
                if (f - margen <= hrtimesData[i] && prev == 0) prev = hrvalueData[i];
                if (f  <= hrtimesData[i] && actual == 0) actual = hrvalueData[i];
                if (f + margen <= hrtimesData[i] && post == 0) post = hrvalueData[i];
            }

            hrEvents.Add(new HRDataEvent(prev, actual, post));
        }

        List<float> hrVariationPostEvent = new List<float>();
        List<float> hrVariationEvent = new List<float>();

        foreach(HRDataEvent hrd in hrEvents)
        {
            hrVariationPostEvent.Add(hrd.hrPost - hrd.hrEvent);
            hrVariationEvent.Add(hrd.hrPost - hrd.hrPrev);
        }

        float mediaPost = 0;
        float maxPost = float.MinValue;

        foreach(float f in hrVariationPostEvent)
        {
            maxPost = maxPost > f ? maxPost : f;
            mediaPost += f;
        }

        mediaPost /= (float)hrVariationPostEvent.Count;

        mediaVariacionHRPostEvento = mediaPost;
        maximaVariacionHRPostEvento = maxPost;

        float mediaEvent = 0;

        foreach(float f in hrVariationEvent)
        {
            mediaEvent += f;
        }

        mediaEvent /= (float)hrVariationEvent.Count;

        mediaVariacionHRDuranteEvento = mediaEvent;

        print("Incremento HR Post-Evento: " + mediaVariacionHRPostEvento + " /// Maximo Incremento HR Post-Evento: " + maximaVariacionHRPostEvento + " /// Variacion Media HR Durante Eventos: " + mediaVariacionHRDuranteEvento);
    }

    void CribaDatos(List<float> values)
    {
        for (int i = 0; i < values.Count; i++)
        {
            if (values[0] < 0) values.Remove(values[0]);
            else break;
        }
    }

    List<float> Simplify(List<float> listaBase, int scale)
    {
        if (scale <= 1) return listaBase;

        List<float> result = new List<float>();
        for (int i = 0; i < listaBase.Count; i+=scale)
        {
            result.Add(listaBase[i]);
        }
        return result;
    }

   

    void ClearList(List<GameObject> lista)
    {
        Debug.Log("LImpiamos");
        for (int i = lista.Count - 1; i >= 0; i--)
        {
            Destroy(lista[i]);
        }
        lista = new List<GameObject>();
    }

    public void Reset()
    {
        for (int i = graficas.Count - 1; i >= 0; i--)
        {
            for (int j = graficas[i].Count - 1; j >= 0; j--)
            {
                Destroy(graficas[i][j]);
            }
        }

        graficas = new List<List<GameObject>>();
        leyendas = new List<minMaxValues>();
        coloresGraficas = new List<Color>();
        ChangeLeyenda();
    }

    class HRDataEvent
    {
        public float hrPrev, hrEvent, hrPost;

        public HRDataEvent(float prev, float actual, float post)
        {
            hrPrev = prev;
            hrEvent = actual;
            hrPost = post;
        }
    }
}

public static class Auxiliares
{
    public static void PrintList(List<float> lista)
    {
        string s = "Valores (";
        for (int i = 0; i < lista.Count; i++)
        {
            s += lista[i];
            if (i < lista.Count - 1) s += ", ";
        }
        s += ")";
        Debug.Log(s);
    }
}


public enum coloresGraficas
{
    hearthRate,
    pupilDilatation,
    cognitiveLoad,
    events
}