using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimiteCamara : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {
            other.GetComponentInParent<RecentradoCamara>().Recentrar();
        }
    }
}
