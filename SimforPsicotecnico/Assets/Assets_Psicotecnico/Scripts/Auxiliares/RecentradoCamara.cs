using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecentradoCamara : MonoBehaviour
{
    [SerializeField] Transform target;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) Recentrar();
    }
    public void Recentrar()
    {
        Vector3 desviacion = target.position - Camera.main.transform.position;
        transform.position += desviacion;
    }
}
