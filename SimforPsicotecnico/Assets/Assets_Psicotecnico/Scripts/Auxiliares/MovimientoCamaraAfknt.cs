using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCamaraAfknt : MonoBehaviour
{
    Vector3 lastPosition;
    Quaternion lastRotation;

    private void Start()
    {
        InvokeRepeating("Comprobar", 0f, 1f);
    }

    void Comprobar()
    {
        if(lastPosition != transform.position || lastRotation != transform.rotation)
        {
            lastPosition = transform.position;
            lastRotation = transform.rotation;

            EventManager.TriggerEvent("afknt");
        }
    }
}
