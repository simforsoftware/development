using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Multisesion
{
    public static bool menuMultisesion;
    public static List<Scenes> ejercicios = new List<Scenes>();

    public static int multisesionAEditar;
    public static List<int>[] multisesiones = new List<int>[3];

    public static Scenes SiguienteEjercicio()
    {
        if (ejercicios.Count == 0) return Scenes.Resultado;

        Scenes siguienteEscena = ejercicios[0];
        ejercicios.Remove(siguienteEscena);
        return siguienteEscena;
    }    

    public static void DownLoadMultisesiones()
    {
        GruposDB grupos = new GruposDB();

        string s = grupos.GetMultisesionesFromGrupo(AuxiliarBuild.instance.IDGrupo);

        string[] multiSessionesT = s.Split(':');


        for(int i = 0; i < 3; ++i)
        {
            multisesiones[i] = new List<int>();
            if (multiSessionesT.Length <= i || multiSessionesT[i] == "VACIO")
            {
                Debug.Log("VACIO");
            }
            else
            {
                string[] sesiones = multiSessionesT[i].Split('.');
                Debug.Log(sesiones.Length);
                for (int j = 0; j < sesiones.Length; ++j)
                {
                    Debug.Log(sesiones[j]);
                    multisesiones[i].Add((int.Parse(sesiones[j])));
                }
            }
        }
        grupos.Close();
    }
    public static void BorrarUnaMultisesion(int i)
    {
        multisesiones[i].Clear();
        UpdatearMultisesiones();
    }
    public static void UpdatearMultisesiones()
    {
        GruposDB grupos = new GruposDB();
        string multisesionesS = "";
        for(int i = 0; i < multisesiones.Length; ++i)
        {
            if (multisesiones[i].Count == 0) multisesionesS += "VACIO";
            else
            {
                for(int j = 0; j < multisesiones[i].Count; ++j)
                {
                    multisesionesS += multisesiones[i][j].ToString();
                    if(j < multisesiones[i].Count - 1) multisesionesS += ".";
                }
            }
           if(i < multisesiones.Length -1) multisesionesS += ":";
        }

        grupos.UpdatearMultisesiones(multisesionesS, AuxiliarBuild.instance.IDGrupo);
        grupos.Close();
    }
}
