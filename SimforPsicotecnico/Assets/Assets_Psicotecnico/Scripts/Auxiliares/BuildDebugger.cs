using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildDebugger : MonoBehaviour
{

    public static BuildDebugger instance;

    [SerializeField] Text textDebug;

    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

       // InvokeRepeating("HR", 0, 5);
    }

    public void Print_Message(string s)
    {
        textDebug.text = s;
    }

    public void HR()
    {
        textDebug.text = HPManager.instance.heartRate.ToString();
    }
}
