using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resyzer : MonoBehaviour
{
    [SerializeField] float childSyze;
    [SerializeField] float offsetPorArriba;
    public void Resyze()
    {
        RectTransform thisTransform = GetComponent<RectTransform>();

        thisTransform.sizeDelta = new Vector2(thisTransform.rect.x, (transform.childCount * childSyze) + offsetPorArriba) ;
    }

    private void Update()
    {
        Resyze();
    }
}
