using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exiteador : MonoBehaviour
{
    [SerializeField] float timeAFK;
    [SerializeField] List<Scenes> escenasSinAFK;
    float timer = 0;
    Vector3 lastPosition = Vector3.zero;

    private void Start()
    {
        EventManager.StartListening("afknt", AFKnt);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Exit();


        if (escenasSinAFK.Contains(ScenesSelector.CurrentScene())) return;

        timer += Time.deltaTime;

        if (Input.anyKeyDown) timer = 0;
        if (Input.anyKey) timer = 0;
        if (Input.GetMouseButtonDown(0)) timer = 0;
        if (Input.GetMouseButtonDown(1)) timer = 0;
        if (Input.mousePosition != lastPosition)
        {
            timer = 0;
            lastPosition = Input.mousePosition;
        }

        if(timer > timeAFK)
        {
            timer = 0;
            ScenesSelector.GoToScene(Scenes.MainMenu);
        }
    }

    void Exit()
    {       
        if (ScenesSelector.IsCurrentScene(Scenes.Login))
        {
            Application.Quit();
            return;
        }

        Scenes anterior = Scenes.MainMenu;
        escenaAnterior.TryGetValue(ScenesSelector.CurrentScene(), out anterior);
        ScenesSelector.GoToScene(anterior);
        
        Multisesion.ejercicios.Clear();
        AuxiliarBuild.instance.tipo.Clear();
        AuxiliarBuild.instance.resultados.Clear();
    }

    public void AFKnt()
    {
        timer = 0;
    }


    Dictionary<Scenes, Scenes> escenaAnterior = new Dictionary<Scenes, Scenes>()
    {
        {Scenes.CambiarContrasena,Scenes.InfoUsuario },
        {Scenes.Daltonismo,Scenes.MainMenu },
        {Scenes.EdicionMultisesion,Scenes.InstructorMultisesion },
        {Scenes.Historial,Scenes.MainMenu },
        {Scenes.InfoUsuario,Scenes.MainMenu },
        {Scenes.InstructorMultisesion,Scenes.SeleccionMultisesion },
        {Scenes.Ishihara,Scenes.MainMenu },
        {Scenes.MainMenu,Scenes.Login },
        {Scenes.NuevaSimulacion1,Scenes.MainMenu },
        {Scenes.NuevaSimulacion2,Scenes.MainMenu },
        {Scenes.NuevaSimulacion3,Scenes.MainMenu },
        {Scenes.NuevaSimulacion4,Scenes.MainMenu },
        {Scenes.NuevaSimulacion5,Scenes.MainMenu },
        {Scenes.NuevaSimulacion5Alt,Scenes.MainMenu },
        {Scenes.Registro,Scenes.Login },
        {Scenes.Resultado,Scenes.Historial },
        {Scenes.SeleccionEjercicio,Scenes.SeleccionModulo },
        {Scenes.SeleccionIdioma,Scenes.Login },
        {Scenes.SeleccionModulo,Scenes.MainMenu },
        {Scenes.SeleccionModuloCompleto,Scenes.MainMenu },
        {Scenes.SeleccionMultisesion,Scenes.MainMenu },
        {Scenes.Test1,Scenes.MainMenu },
        {Scenes.Test2,Scenes.MainMenu },
        {Scenes.Test3,Scenes.MainMenu },
        {Scenes.Test4,Scenes.MainMenu },
        {Scenes.Test5,Scenes.MainMenu },
        {Scenes.Test6,Scenes.MainMenu },
        {Scenes.Test7,Scenes.MainMenu },
        {Scenes.Test8,Scenes.MainMenu },
        {Scenes.TestESenllen,Scenes.MainMenu },
        {Scenes.Creditos,Scenes.Login },
    };
}
