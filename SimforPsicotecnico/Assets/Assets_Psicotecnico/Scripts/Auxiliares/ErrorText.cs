using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorText : MonoBehaviour
{
    public void PrintError(int id)
    {
        Text text = GetComponent<Text>();
        switch (id)
        {
            case 1:
                text.text = Localizador.Texto("loc_0109");
                break;
            case 3:
                text.text = Localizador.Texto("loc_0083");
                break;
            case 4:
                text.text = Localizador.Texto("loc_0084");
                break;
            case 7:
                text.text = Localizador.Texto("loc_0085");
                break;
            case 8:
                text.text = Localizador.Texto("loc_0086");
                break;
            case 9:
                text.text = Localizador.Texto("loc_0087");
                break;
            default:
                text.text = Localizador.Texto("loc_0088");
                break;
        }
    }
}
