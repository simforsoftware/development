using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class DinamicText : MonoBehaviour
{
    [HideInInspector] public string code;


    private void Start()
    {
        CambioTexto();
        try { EventManager.StartListening("CambioIdioma", CambioTexto); }
        catch { }
    }

    void CambioTexto()
    {
        GetComponent<Text>().text = Localizador.Texto(code);
    }

    public void SetTextByID(string code)
    {
        if (code.Length > 4) return;

        this.code = code;
        CambioTexto();
    }    

    private void OnDestroy()
    {
        EventManager.StopListening("CambioIdioma", CambioTexto);
    }


    private void Update()
    {
        if (GetComponent<Text>().text.Contains("loc_"))
        {
            string newCode = GetComponent<Text>().text.Remove(0, 4);
            string newString = Localizador.Texto(GetComponent<Text>().text.Remove(0, 4));
            if(newString != newCode)
            {
                GetComponent<Text>().text = newString;
                code = newCode;
            }
        }
    }
}