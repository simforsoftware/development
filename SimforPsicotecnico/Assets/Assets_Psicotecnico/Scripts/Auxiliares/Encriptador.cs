using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;
using System.Runtime;
using System;

public static class Encriptador 
{
    static string[] KEY = { "hamburguesa", "cocacola", "Clantamaforo" };
    static char KEYID = 'F';
    static string hash = "hamburguesa";

    public static string Encode(string original)
    {


        byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(original);
        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        {
            byte[] keys = md5.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(hash));


            using(TripleDESCryptoServiceProvider triplsDes = new TripleDESCryptoServiceProvider() {Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 }) 
            {
                ICryptoTransform transform = triplsDes.CreateEncryptor();
                byte[] results = transform.TransformFinalBlock(data, 0, data.Length);

                return   Convert.ToBase64String(results, 0, results.Length);

            }
        }
        //return original;


        string codificada = "";
        int intKey = UnityEngine.Random.Range(0, KEY.Length);
        string key = KEY[intKey];

        codificada += (char)(intKey.ToString()[0] ^ KEYID);

        string codeOriginal = Code(original, key);

        for (int i = 0; i < codeOriginal.Length; i++)
        {
            codificada += codeOriginal[i];
        }
        return codificada;
    }

    public static string Decode(string original)
    {

        byte[] data = Convert.FromBase64String(original);
        using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        {
            byte[] keys = md5.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(hash));


            using (TripleDESCryptoServiceProvider triplsDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
            {
                ICryptoTransform transform = triplsDes.CreateDecryptor();
                byte[] results = transform.TransformFinalBlock(data, 0, data.Length);

                return System.Text.UTF8Encoding.UTF8.GetString(results);

            }
        }
        //return original;



        char intCrypt = original[0];
        string codeOriginal = "";



        for (int i = 1; i < original.Length; i++)
        {
            codeOriginal += original[i];
        }

        intCrypt = (char)(intCrypt ^ KEYID);

        int id = int.Parse(intCrypt.ToString());

        string key = KEY[id];

        return Code(codeOriginal, key);
    }

    static string Code(string original, string key)
    {
        string codificada = "";

        int keyCaracter = 0;

        for (int i = 0; i < original.Length; i++)
        {
            if (keyCaracter >= key.Length) keyCaracter = 0;
            char kcaracter = key[keyCaracter];

            char newChar = (char)(original[i] ^ kcaracter);

            codificada += newChar;

            ++keyCaracter;
        }

        return codificada;
    }
}
