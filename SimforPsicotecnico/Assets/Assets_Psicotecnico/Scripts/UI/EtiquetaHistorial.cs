 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Data;

public class EtiquetaHistorial : MonoBehaviour
{
    [SerializeField] Text dateEnd;
    [SerializeField] Text ejercicio;
    [SerializeField] Text aptoText;
    [SerializeField] Text nombre;
    [SerializeField] Text Id;
    [SerializeField] Text fecha;
    bool apto;

    string ejercicioBackEnd = "";
    public int id;
    
    public void SetDatos( string hora, string _ejercicio, string _apto, int _id, int _alumnoId, string _fecha, string _nombre)
    {
        dateEnd.text =  hora;
        ejercicioBackEnd = _ejercicio;
        Localizar();
        apto = bool.Parse(_apto);
        if (apto)
        {
            aptoText.text = Localizador.Texto("loc_0023");
        }
        else aptoText.text = Localizador.Texto("loc_0073");


        id = _id;
        Id.text = _alumnoId.ToString();
        nombre.text = _nombre;
        fecha.text = _fecha;
    }
    public void SetDatosVacio()
    {
        dateEnd.text = "";
        ejercicio.text = "";
        aptoText.text = "";
        nombre.text = "";
        fecha.text = "";
        Id.text = "";
    }

    public void Localizar()
    {
        switch (ejercicioBackEnd)
        {
            case "Test1":
                ejercicio.text = Localizador.Texto("loc_0043");
                break;
            case "Test2":
                ejercicio.text = Localizador.Texto("loc_0046");
                break;
            case "Test3":
                ejercicio.text = Localizador.Texto("loc_0049");
                break;
            case "Test4":
                ejercicio.text = Localizador.Texto("loc_0052");
                break;
            case "Test5":
                ejercicio.text = Localizador.Texto("loc_0055");
                break;
           
            case "Test7":
                ejercicio.text = Localizador.Texto("loc_0058");

                break;
            case "Test6":
                ejercicio.text = Localizador.Texto("loc_0175");
                break;
            case "Test8":
                ejercicio.text = Localizador.Texto("loc_0059");
                break;
            case "new simulacion 1":
                ejercicio.text = Localizador.Texto("loc_0044");
                break;
            case "new simulacion 2":
                ejercicio.text = Localizador.Texto("loc_0047");
                break;
            case "new simulacion 3":
                ejercicio.text = Localizador.Texto("loc_0050");
                break;
            case "new simulacion 3 alt":
                ejercicio.text = Localizador.Texto("loc_0050");
                break;
            case "new simulacion 4":
                ejercicio.text = Localizador.Texto("loc_0053");
                break;
            case "new simulacion 5":
                ejercicio.text = Localizador.Texto("loc_0056");
                break;
            case "new simulacion 5 alt":
                ejercicio.text = Localizador.Texto("loc_0056");
                break;
            case "TestIshihara":
                ejercicio.text = Localizador.Texto("loc_0111");
                break;
            case "TestDaltonismo":
                ejercicio.text = Localizador.Texto("loc_0117");
                break;
            case "Test3Alt":
                ejercicio.text = Localizador.Texto("loc_0049");
                break;
            default:
                Debug.Log("No esta implementado este test");
                break;
        }
    }
    public void Set_resultado()
    {

        switch (ejercicioBackEnd)
        {
            case "Test1":
                FSetResultado123(1);
                break;
            case "Test2":
                FSetResultado123(2);
                break;
            case "Test3":
                FSetResultado123(3);
                break;
            case "Test4":
                FSetResultado4();
                break;
            case "Test5":
                FSetResultado5();
                break;
            case "Simulacion1":
                StartCoroutine(Set_Resultado_Simulacion());
                break;
            case "Simulacion2":
                StartCoroutine(Set_Resultado_Simulacion());
                break;
            case "Simulacion3":
                StartCoroutine(Set_Resultado_Simulacion());
                break;
            case "Simulacion4":
                StartCoroutine(Set_Resultado_Simulacion());
                break;
            case "Simulacion5":
                StartCoroutine(Set_Resultado_Simulacion());
                break;
            case "Test7":
                FSetResultado7();
                break;
            case "Test6":
                Set_Resultado6();
                break;
            case "Test8":
                StartCoroutine(Set_Resultado8());
                break;
            case "new simulacion 1":
                FSetNewSimulacion1Resultado();
                break;
            case "new simulacion 2":
                FSetNewSimulacion1Resultado();
                break;
            case "new simulacion 3":
                FSetNewSimulacion1Resultado();
                break;
            case "new simulacion 4":
                FSetNewSimulacion1Resultado();
                break;
            case "new simulacion 5":
                FSetNewSimulacion1Resultado();
                break;
            case "new simulacion 5 alt":
                FSetNewSimulacion1Resultado(17);
                break;
            case "new simulacion 3 alt":
                FSetNewSimulacion1Resultado(16);
                break;
            case "TestIshihara":
                FSetIshihara();
                break;
            case "TestDaltonismo":
                FSetIshihara();
                break;
            case "Test3Alt":
                FSetResultado123(3);
                break;
            default:
                Debug.Log("No esta implementado este test");
                break;
        }
    }
    public void FSetIshihara()
    {
        IshiharaDB ishiDB = new IshiharaDB();

        IDataReader reader = ishiDB.GetDataUsingID(id);
        AuxiliarBuild.instance.tipo.Add(15);

        Result7 result7 = new Result7();


        result7.linea = (int)reader[2];

        result7.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(result7);


        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    public void FSetNewSimulacion1Resultado(int tipo = 0)
    {
        ResultNewSimulacionDB resultados = new ResultNewSimulacionDB();

        IDataReader reader = resultados.GetDataUsingID(id);

        if (tipo == 0) AuxiliarBuild.instance.tipo.Add(14);
        else AuxiliarBuild.instance.tipo.Add(tipo);
        ResultNuevaSimulacion1 rsimulacion = new ResultNuevaSimulacion1();
        rsimulacion.nombreEjrcicio = ejercicioBackEnd;
        rsimulacion.time = float.Parse(reader[2].ToString());
        rsimulacion.reactionTime = float.Parse(reader[3].ToString());
        rsimulacion.aciertos = (int)reader[4];
        rsimulacion.fallos = (int)reader[5];
        rsimulacion.minutos = (int)reader[8];
        Debug.Log("MINUTOS " + rsimulacion.minutos);
        if (reader[6].ToString() != "VACIO" && reader[7].ToString() != "VACIO")
        {
            string[] hg = reader[6].ToString().Split(':');
            string[] ht = reader[7].ToString().Split(':');
            List<float> HRV = new List<float>();
            List<float> HRT = new List<float>();

            for (int i = 0; i < hg.Length; ++i)
            {
                HRV.Add(float.Parse(hg[i]));
                HRT.Add(float.Parse(ht[i]));
            }

            rsimulacion.graficas.Add(new Grafics.graficasData(HRV, HRT, coloresGraficas.hearthRate));
        }
        if (reader[9].ToString() != "VACIO" && reader[10].ToString() != "VACIO")
        {
            string[] pv = reader[9].ToString().Split(':');
            string[] pt = reader[10].ToString().Split(':');
            List<float> PV = new List<float>();
            List<float> PT = new List<float>();

            for (int i = 0; i < pv.Length; ++i)
            {
                PV.Add(float.Parse(pv[i]));
                PT.Add(float.Parse(pt[i]));
            }

            rsimulacion.graficas.Add(new Grafics.graficasData(PV, PT, coloresGraficas.pupilDilatation));
        }
        if (reader[11].ToString() != "VACIO" && reader[12].ToString() != "VACIO")
        {
            string[] cv = reader[11].ToString().Split(':');
            string[] ct = reader[12].ToString().Split(':');
            List<float> CV = new List<float>();
            List<float> CT = new List<float>();

            for (int i = 0; i < cv.Length; ++i)
            {
                CV.Add(float.Parse(cv[i]));
                CT.Add(float.Parse(ct[i]));
            }

            rsimulacion.graficas.Add(new Grafics.graficasData(CV, CT, coloresGraficas.cognitiveLoad));
        }
        if (reader[13].ToString() != "VACIO")
        {
            string[] et = reader[13].ToString().Split(':');
            List<float> ET = new List<float>();

            for (int i = 0; i < et.Length; ++i)
            {
                ET.Add(float.Parse(et[i]));
                Debug.Log(float.Parse(et[i]));
            }

            rsimulacion.eventos.Add(new Grafics.eventData(ET, coloresGraficas.events));
        }

        rsimulacion.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(rsimulacion);
        ScenesSelector.GoToScene(Scenes.Resultado);
    }

    public void FTestIshihara()
    {
        Result7 resultado = new Result7();


        IshiharaDB ishDB = new IshiharaDB();

        IDataReader reader = ishDB.GetDataUsingID(id);

        AuxiliarBuild.instance.tipo.Add(15);

        resultado.apto = bool.Parse(apto.ToString());
        resultado.linea = (int)(reader[2]);

        AuxiliarBuild.instance.resultados.Add(resultado);


        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    public void FSetResultado123(int tipo)
    {
        Result123DB resultados = new Result123DB();

        IDataReader reader = resultados.GetDataUsingID(id);
        IDataReader auxReader = resultados.GetDataUsingID(id);

        while (auxReader.Read())
        {
            for(int i = 0; i < auxReader.FieldCount; ++i)
            {
                Debug.Log(i + " " + auxReader.GetName(i) + " " + auxReader[i]);
            }

        }

             AuxiliarBuild.instance.tipo.Add(tipo);

            Result123 resultados123 = new Result123();

            resultados123.time = float.Parse(reader[2].ToString());
            resultados123.reactionTime = float.Parse( reader[3].ToString());
            resultados123.aciertos = (int)reader[4] ;
            resultados123.fallos = (int)reader[5];

            resultados123.apto = bool.Parse(apto.ToString());
            AuxiliarBuild.instance.resultados.Add(resultados123);
        

        ScenesSelector.GoToScene(Scenes.Resultado);

    }

    //DEPRECATED
    /*
    IEnumerator Set_Resultado123(int tipo)
    {
        FSetResultado123(tipo);
        WWWForm form = new WWWForm();
        form.AddField("idSession", id);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionDownload123.php", form))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                AuxiliarBuild.instance.tipo.Add( tipo);
                string[] s = www.downloadHandler.text.Split('.');
                Result123 resultados123 = new Result123();
                resultados123.time = float.Parse( s[1]);
                resultados123.reactionTime = float.Parse(s[2]);
                resultados123.aciertos = int.Parse(s[3]);
                resultados123.fallos = int.Parse(s[4]);
                
                resultados123.apto = bool.Parse(apto.ToString());
                AuxiliarBuild.instance.resultados.Add(resultados123);
            }
        }
        ScenesSelector.GoToScene(Scenes.Resultado);
    }*/
    public void FSetResultado4()
    {
        Result4DB result4DB = new Result4DB();

        IDataReader reader = result4DB.GetDataUsingID(id);

        AuxiliarBuild.instance.tipo.Add(4);

        Result4 result4 = new Result4();

        result4.tiempoTotal = float.Parse(reader[2].ToString());
        result4.tiempoReaccion = float.Parse(reader[3].ToString());
        result4.aciertos = (int)reader[4];
        result4.fallos = (int)reader[5];

        result4.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(result4);


        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    //DEPRECATED
    /*
    IEnumerator Set_Resultado4()
    {
        WWWForm form = new WWWForm();
        form.AddField("idSession", id);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionDownload4.php", form))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                AuxiliarBuild.instance.tipo.Add( 4);
                string[] s = www.downloadHandler.text.Split('.');
                Result4 resultados4 = new Result4();
                resultados4.aciertos = int.Parse(s[1]);
                Debug.Log(apto + " me estoy volciendo loco");
                resultados4.apto = bool.Parse(apto.ToString());
                AuxiliarBuild.instance.resultados.Add(resultados4);

            }
        }
        ScenesSelector.GoToScene(Scenes.Resultado);
    }*/
    public void FSetResultado5()
    {
        Result5DB result5Db = new Result5DB();

        IDataReader reader = result5Db.GetDataUsingID(id);
        IDataReader AUXreader = result5Db.GetDataUsingID(id);

        while (AUXreader.Read())
        {
            for(int i = 0; i < AUXreader.FieldCount; ++i)
            {
                Debug.Log(i + " " + AUXreader.GetName(i) + " " + AUXreader[i]);
            }
        }
        AuxiliarBuild.instance.tipo.Add(5);

        Result5 result5 = new Result5();

        result5.time = float.Parse(reader[2].ToString());
        result5.reactionTime = float.Parse(reader[3].ToString());
        Debug.Log(reader[4]);
        result5.fallos = (int)reader[4];

        result5.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(result5);


        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    //DEPRECATED
    /*
    IEnumerator Set_Resultado5()
    {
        WWWForm form = new WWWForm();
        form.AddField("idSession", id);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/sessionDownload5.php", form))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                AuxiliarBuild.instance.tipo.Add( 5);
                string[] s = www.downloadHandler.text.Split('.');
                Debug.Log(s[1]);
                Debug.Log(float.Parse(s[1]));
                Result5 resultados5 = new Result5();
                resultados5.reactionTime = float.Parse(s[1]);
                resultados5.fallos = int.Parse(s[2]);
                resultados5.apto = bool.Parse(apto.ToString());
                AuxiliarBuild.instance.resultados.Add(resultados5);

            }
        }
        ScenesSelector.GoToScene(Scenes.Resultado);
    }*/

    IEnumerator Set_Resultado_Simulacion()
    {
        WWWForm form = new WWWForm();
        form.AddField("idSession", id);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/SimulacionDownload.php", form))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                AuxiliarBuild.instance.tipo.Add( 9);
                string[] s = www.downloadHandler.text.Split('.');
                ResultSimulacion3 rsimulacion = new ResultSimulacion3();
                rsimulacion.duracion = float.Parse(s[1]);
                rsimulacion.choqueFallos = float.Parse(s[2]);
                rsimulacion.veloidadInferiorFallos = float.Parse(s[3]);
                rsimulacion.velocidadSuperiorFallos = int.Parse(s[4]);
                rsimulacion.semaforoFallos = float.Parse(s[5]);
                rsimulacion.salidaFallos = float.Parse(s[6]);
                rsimulacion.minutos = int.Parse(s[9]);
                Debug.Log("MINUTOS " + rsimulacion.minutos);
                if(s[7] != "VACIO" && s[8] != "VACIO")
                {
                    string[] hg = s[7].Split(':');
                    string[] ht = s[8].Split(':');
                    List<float> HRV = new List<float>();
                    List<float> HRT = new List<float>();

                    for(int i = 0; i < hg.Length; ++i)
                    {
                        HRV.Add(float.Parse(hg[i]));
                        HRT.Add(float.Parse(ht[i]));
                    }
                    
                    rsimulacion.graficas.Add( new Grafics.graficasData(HRV, HRT, coloresGraficas.hearthRate));
                }
                if (s[10] != "VACIO" && s[11] != "VACIO")
                {
                    string[] pv = s[10].Split(':');
                    string[] pt = s[11].Split(':');
                    List<float> PV = new List<float>();
                    List<float> PT = new List<float>();

                    for (int i = 0; i < pv.Length; ++i)
                    {
                        PV.Add(float.Parse(pv[i]));
                        PT.Add(float.Parse(pt[i]));
                    }

                    rsimulacion.graficas.Add(new Grafics.graficasData(PV, PT, coloresGraficas.pupilDilatation));
                }
                if (s[12] != "VACIO" && s[13] != "VACIO")
                {
                    string[] cv = s[10].Split(':');
                    string[] ct = s[11].Split(':');
                    List<float> CV = new List<float>();
                    List<float> CT = new List<float>();

                    for (int i = 0; i < cv.Length; ++i)
                    {
                        CV.Add(float.Parse(cv[i]));
                        CT.Add(float.Parse(ct[i]));
                    }

                    rsimulacion.graficas.Add(new Grafics.graficasData(CV, CT, coloresGraficas.cognitiveLoad));
                }
                if(s[14] != "VACIO")
                {
                    string[] et = s[14].Split(':');
                    List<float> ET = new List<float>();

                    for(int i = 0; i < et.Length; ++i)
                    {
                        ET.Add(float.Parse(et[i]));
                        Debug.Log(float.Parse(et[i]));
                    }

                    rsimulacion.eventos.Add(new Grafics.eventData(ET, coloresGraficas.events));
                }

                rsimulacion.apto = bool.Parse(apto.ToString());
                AuxiliarBuild.instance.resultados.Add(rsimulacion);

            }
        }
        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    public void FSetResultado7()
    {
        Result7DB result7Db = new Result7DB();

        IDataReader reader = result7Db.GetDataUsingID(id);

        AuxiliarBuild.instance.tipo.Add(7);

        Result7 result7 = new Result7();


        result7.linea = (int)reader[2];

        result7.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(result7);


        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    //DEPRECATED
    /*
    IEnumerator Set_Resultado7()
    {
        WWWForm form = new WWWForm();
        form.AddField("idSession", id);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/DownloadResultado7.php", form))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                AuxiliarBuild.instance.tipo.Add( 7);
                string[] s = www.downloadHandler.text.Split('.');
                Result7 resultado7 = new Result7();
                resultado7.linea = float.Parse(s[1]);
                resultado7.apto = bool.Parse(apto.ToString());
                AuxiliarBuild.instance.resultados.Add(resultado7);

            }
        }
        ScenesSelector.GoToScene(Scenes.Resultado);
    }*/
    //Este abra que adaptarlo al test psicologico que quieran
    public void Set_Resultado6()
    {

        AuxiliarBuild.instance.tipo.Add(6) ;
        Result6 resultado6 = new Result6();

        Result6DB db = new Result6DB();

        IDataReader reader = db.GetDataUsingID(id);

        resultado6.neuroticismo = float.Parse(reader[2].ToString());
        resultado6.paranoia = float.Parse(reader[3].ToString());
        resultado6.agitacion = float.Parse(reader[4].ToString());
        resultado6.inestabilidad = float.Parse(reader[5].ToString());
        resultado6.psicastenia = float.Parse(reader[6].ToString());
        resultado6.hipocondria = float.Parse(reader[7].ToString());
        resultado6.suicidio = float.Parse(reader[8].ToString());

        resultado6.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(resultado6);

        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    public void SetResultado()
    {
        Result8DB result8DB = new Result8DB();
        IDataReader reader = result8DB.GetDataUsingID(id);

        AuxiliarBuild.instance.tipo.Add(8);


        Result8 resultado8 = new Result8();
        resultado8.iz250 = float.Parse(reader[2].ToString());
        resultado8.iz500 = float.Parse(reader[3].ToString());
        resultado8.iz1000 = float.Parse(reader[4].ToString());
        resultado8.iz2000 = float.Parse(reader[5].ToString());
        resultado8.iz4000 = float.Parse(reader[6].ToString());
        resultado8.iz8000 = float.Parse(reader[7].ToString());
        resultado8.de250 = float.Parse(reader[8].ToString());
        resultado8.de500 = float.Parse(reader[9].ToString());
        resultado8.de1000 = float.Parse(reader[10].ToString());
        resultado8.de2000 = float.Parse(reader[11].ToString());
        resultado8.de4000 = float.Parse(reader[12].ToString());
        resultado8.de8000 = float.Parse(reader[13].ToString());
        resultado8.apto = bool.Parse(apto.ToString());
        AuxiliarBuild.instance.resultados.Add(resultado8);
        ScenesSelector.GoToScene(Scenes.Resultado);
    }
    IEnumerator Set_Resultado8()
    {
        WWWForm form = new WWWForm();
        form.AddField("idSession", id);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/serverConnect/DownloadResultado8.php", form))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                AuxiliarBuild.instance.tipo.Add( 8);
                string[] s = www.downloadHandler.text.Split('.');

                Result8 resultado8 = new Result8();
                resultado8.iz250 = float.Parse(s[1]);
                resultado8.iz500 = float.Parse(s[2]);
                resultado8.iz1000 = float.Parse(s[3]);
                resultado8.iz2000 = float.Parse(s[4]);
                resultado8.iz4000 = float.Parse(s[5]);
                resultado8.iz8000 = float.Parse(s[6]);
                resultado8.de250 = float.Parse(s[7]);
                resultado8.de500 = float.Parse(s[8]);
                resultado8.de1000 = float.Parse(s[9]);
                resultado8.de2000 = float.Parse(s[10]);
                resultado8.de4000 = float.Parse(s[11]);
                resultado8.de8000 = float.Parse(s[12]);
                resultado8.apto = bool.Parse(apto.ToString());
                AuxiliarBuild.instance.resultados.Add(resultado8);

            }
        }
        ScenesSelector.GoToScene(Scenes.Resultado);
    }
}
