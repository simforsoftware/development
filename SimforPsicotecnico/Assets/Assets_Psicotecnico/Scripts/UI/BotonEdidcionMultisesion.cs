using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonEdidcionMultisesion : MonoBehaviour
{
    public Scenes escena;
    public bool seleccionada;
    public EdicionMultisesion manager;

    [SerializeField] Sprite imagenSelected;

    public Color colorNormal;
    public Color colorText;

    private Button m_button;
    private Image m_image;

    [SerializeField] private bool swichImageColor;

    private void Awake()
    {
        if (TryGetComponent(out Image image))
            m_image = image;

        if (TryGetComponent(out Button button))
            m_button = button;
    }

    public void Set(int escenaID, bool estado, EdicionMultisesion padre)
    {
        manager = padre;
        seleccionada = estado;
        if (estado)
        {
            if (swichImageColor)
            {
                m_image.color = colorNormal;

                SpriteState spriteState = new SpriteState();
                spriteState.highlightedSprite = imagenSelected;
                spriteState.pressedSprite = imagenSelected;
                spriteState.selectedSprite = imagenSelected;

                m_button.spriteState = spriteState;
            }
            else
            {
                ColorBlock cBlock = m_button.colors;
                cBlock.normalColor = colorNormal;
                m_button.colors = cBlock;
            }

            //m_image.sprite = imagenSelected;
            

            GetComponentInChildren<Text>().color = colorText;
        }
        escena = (Scenes)escenaID;
        string key;
        DiccionarioNombresEscenas.escenasIntName.TryGetValue(escenaID, out key);
        print("Escena ID: " + escenaID);
        print(key);
        print(Localizador.Texto(key));
        GetComponentInChildren<Text>().text = Localizador.Texto(key);
    }
    public void Activar()
    {
        manager.CambioBoton(this);
    }
}
