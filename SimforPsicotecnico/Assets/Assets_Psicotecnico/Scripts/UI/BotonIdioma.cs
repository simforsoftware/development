using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonIdioma : MonoBehaviour
{
    [SerializeField] Localizador.Idioma idioma;

    public void Seleccionar()
    {
        GetComponentInParent<MenuCambioIdioma>().Cambio(idioma);
    }
}
