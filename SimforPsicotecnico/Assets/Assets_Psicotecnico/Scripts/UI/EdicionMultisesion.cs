using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdicionMultisesion : MonoBehaviour
{
    [SerializeField] ListaModulos modulosdisponibles;

    [SerializeField] Resyzer seleccionados, disponibles;

    [SerializeField] GameObject botonprefab;

    List<int> multisesionActual = new List<int>();

    public void CambioBoton(BotonEdidcionMultisesion boton)
    {
        if (boton.seleccionada)
        {
            GameObject g = Instantiate(botonprefab, disponibles.transform);
            disponibles.Resyze();
            g.GetComponent<BotonEdidcionMultisesion>().Set((int)boton.escena, false, this);
            multisesionActual.Remove((int)boton.escena);
            Destroy(boton.gameObject);
        }
        else
        {
            GameObject g = Instantiate(botonprefab, seleccionados.transform);
            seleccionados.Resyze();
            g.GetComponent<BotonEdidcionMultisesion>().Set((int)boton.escena, true, this);
            multisesionActual.Add((int)boton.escena);
            Destroy(boton.gameObject);
        }
    }

    private void Start()
    {
        multisesionActual.Clear();
        foreach (int i in Multisesion.multisesiones[Multisesion.multisesionAEditar])
        {
            multisesionActual.Add(i);
            GameObject g = Instantiate(botonprefab, seleccionados.transform);
            seleccionados.Resyze();
            Debug.Log(i);
            g.GetComponent<BotonEdidcionMultisesion>().Set(i, true, this);
        }

        foreach(Modulo m in modulosdisponibles.tests)
        {
            foreach(InfoTest it in m.tests)
            {
                if (!Multisesion.multisesiones[Multisesion.multisesionAEditar].Contains((int)it.escena))
                {
                    GameObject g = Instantiate(botonprefab, disponibles.transform);
                    disponibles.Resyze();
                    g.GetComponent<BotonEdidcionMultisesion>().Set((int)it.escena, false, this);
                }                
            }
        }
    }

    public void AplicarCambios()
    {
        Multisesion.multisesiones[Multisesion.multisesionAEditar].Clear();
        foreach (int i in multisesionActual) Multisesion.multisesiones[Multisesion.multisesionAEditar].Add(i);
        Multisesion.UpdatearMultisesiones();
    }
}
