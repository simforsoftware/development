using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCambioIdioma : MonoBehaviour
{
    public void Cambio (Localizador.Idioma i)
    {
        Localizador.idioma = i;

        ScenesSelector.GoToScene(Scenes.Login);
    }
}
