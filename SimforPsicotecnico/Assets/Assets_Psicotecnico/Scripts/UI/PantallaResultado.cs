using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PantallaResultado : MonoBehaviour
{
    //pantalla test normales
    [SerializeField] GameObject pantalla;
    [SerializeField] GameObject titulo1;
    [SerializeField] Text tiempoTotal;
    [SerializeField] Text tiempoReaccion;
    [SerializeField] Text aciertos;
    [SerializeField] Text fallos;
    [SerializeField] Text apto;
    [SerializeField] DinamicText explicativoP1C1;
    [SerializeField] DinamicText explicativoP1C2;
    [SerializeField] DinamicText explicativoP1C3;
    [SerializeField] DinamicText explicativoP1C4;


    //pantalla para las antiguas simulaciones
    [SerializeField] GameObject pantalla2;
    [SerializeField] GameObject titulo2;
    [SerializeField] Text tiempoTotalS;
    [SerializeField] Text falloChoque;
    [SerializeField] Text falloVMin;
    [SerializeField] Text falloVMax;
    [SerializeField] Text falloSemaforo;
    [SerializeField] Text falloSalida;
    [SerializeField] Text minHearRate;
    [SerializeField] Text maxHeartRate;
    [SerializeField] Text averageHearRate;
    [SerializeField] Grafiquero grafica;

    //pantalla para los ejercicios visuales
    [SerializeField] GameObject pantalla3;
    [SerializeField] GameObject titulo3;
    [SerializeField] Text mensajePersonalidad;
    [SerializeField] GameObject mensajeLinea;

    //pantalla test auditivo
    [SerializeField] GameObject pantalla4;
    [SerializeField] GameObject titulo4;
    [SerializeField] Text aptoAuditivo;
    [SerializeField] Text textIz250;
    [SerializeField] Text textIz500;
    [SerializeField] Text textIz1000;
    [SerializeField] Text textIz2000;
    [SerializeField] Text textIz4000;
    [SerializeField] Text textIz8000;

    [SerializeField] Text textDe250;
    [SerializeField] Text textDe500;
    [SerializeField] Text textDe1000;
    [SerializeField] Text textDe2000;
    [SerializeField] Text textDe4000;
    [SerializeField] Text textDe8000;

    //pantalla nuevas simulaciones
    [SerializeField] GameObject pantalla5;
    [SerializeField] GameObject titulo5;
    [SerializeField] GameObject suBDivision1;
    [SerializeField] Text tiempoTotal5;
    [SerializeField] Text tiempoReaccion5;
    [SerializeField] Text aciertos5;
    [SerializeField] Text fallos5;
    [SerializeField] Text apto5;
    [SerializeField] Grafiquero grafica5;
    [SerializeField] DinamicText explicativoP2C1;
    [SerializeField] DinamicText explicativoP2C2;
    [SerializeField] DinamicText explicativoP2C3;
    [SerializeField] DinamicText explicativoP2C4;
    [SerializeField] Button botonLupa;

    [SerializeField] GameObject suBDivision2;
    [SerializeField] Text clMedio;
    [SerializeField] Text clEvento;
    [SerializeField] Text falloPorDesconcentracion;


    //pantalla test de personalidad
    [SerializeField] GameObject pantalla6;
    [SerializeField] Text titulo6;
    [SerializeField] Text neuroticismo;
    [SerializeField] Text paranoia;
    [SerializeField] Text agitacion;
    [SerializeField] Text inestabilidad;
    [SerializeField] Text psicastenia;
    [SerializeField] Text hipocondria;
    [SerializeField] Text suicidio;




    public int indice = 0;
    
    [SerializeField] GameObject botonMultiAtras;
    [SerializeField] GameObject botonMultiAdelante;
    void Start()
    {
        botonMultiAtras.SetActive(false);

        if (AuxiliarBuild.instance.tipo.Count == 1)
        {
            botonMultiAdelante.SetActive(false);
        }
        indice = 0;

        Presentar();
 
       
    }

    public void DesUsar()
    {

        AuxiliarBuild.instance.tipo.Clear();
        AuxiliarBuild.instance.resultados.Clear();
    }

    public void Presentar()
    {
        pantalla.SetActive(false);
        pantalla2.SetActive(false);
        pantalla3.SetActive(false);
        pantalla4.SetActive(false);
        pantalla5.SetActive(false);

        if (AuxiliarBuild.instance.tipo[indice] < 3)
        {
            Result123 res = (Result123)AuxiliarBuild.instance.resultados[indice];
            pantalla.SetActive(true);
            tiempoTotal.text = res.time.ToString("F2") + " s";
            tiempoReaccion.text = res.reactionTime.ToString("F2") + " s";
            aciertos.text = res.aciertos.ToString();
            fallos.text = res.fallos.ToString();

            explicativoP1C1.SetTextByID("0026");
            explicativoP1C2.SetTextByID("0027");
            explicativoP1C3.SetTextByID("0028");
            explicativoP1C4.SetTextByID("0029");




            if (res.apto)
            {
                apto.text = Localizador.Texto("loc_0023");
            }
            else
            {
                apto.text = Localizador.Texto("loc_0073");
            }

            switch (AuxiliarBuild.instance.tipo[indice])
            {
                case 1:
                    titulo1.GetComponent<DinamicText>().SetTextByID("0043");
                    break;
                case 2:
                    titulo1.GetComponent<DinamicText>().SetTextByID("0046");
                    break;
            }
        }
        else if(AuxiliarBuild.instance.tipo[indice] == 3)
        {
            pantalla.SetActive(true);
            titulo1.GetComponent<DinamicText>().SetTextByID("0049");
            
            Result123 res = (Result123)AuxiliarBuild.instance.resultados[indice];
            explicativoP1C1.SetTextByID("0028");
            tiempoTotal.text = res.aciertos.ToString();
            explicativoP1C2.SetTextByID("0192");
            tiempoReaccion.text = res.fallos.ToString();
            explicativoP1C3.SetTextByID("0193");
            aciertos.text = res.time.ToString("F2") + "%";
            explicativoP1C4.SetTextByID("0194");

            fallos.text = res.reactionTime.ToString("F2");



        }
        else if (AuxiliarBuild.instance.tipo[indice] == 5)
        {
            explicativoP1C1.SetTextByID("0026");
            explicativoP1C2.SetTextByID("0027");
            explicativoP1C3.SetTextByID("0028");
            explicativoP1C4.SetTextByID("0029");
            pantalla.SetActive(true);
            tiempoTotal.text = "";
            Result5 result5 = (Result5)AuxiliarBuild.instance.resultados[indice];
            tiempoReaccion.text = result5.reactionTime.ToString("F2") + " s";
            aciertos.transform.parent.gameObject.SetActive(false);
            explicativoP1C3.gameObject.SetActive(false);
            tiempoTotal.text = result5.time.ToString("F2") + " s";
            fallos.text = result5.fallos.ToString();
            if (result5.apto)
            {
                apto.text = Localizador.Texto("loc_0023");
            }
            else
            {
                apto.text = Localizador.Texto("loc_0073");
            }

            titulo1.GetComponent<DinamicText>().SetTextByID("0055");
        }
        else if (AuxiliarBuild.instance.tipo[indice] == 4)
        {
            pantalla.SetActive(true);
            Result4 result4 = (Result4)AuxiliarBuild.instance.resultados[indice];
            tiempoTotal.text = result4.tiempoTotal.ToString("F2") + " s";
            tiempoReaccion.text = result4.tiempoReaccion.ToString("F2") + " s";

            aciertos.text = result4.aciertos.ToString();
            fallos.text = result4.fallos.ToString();
            if (result4.apto)
            {
                apto.text = Localizador.Texto("loc_0023");
            }
            else
            {
                apto.text = Localizador.Texto("loc_0073");
            }
            titulo1.GetComponent<DinamicText>().SetTextByID("0052");
        }
        else if(AuxiliarBuild.instance.tipo[indice] == 15)
        {
            pantalla.SetActive(false);
            pantalla3.SetActive(true);
            Result7 resultado7 = (Result7)AuxiliarBuild.instance.resultados[indice];


            if (resultado7.linea > 0) mensajePersonalidad.text = Localizador.Texto("loc_0073") + ". " + Localizador.Texto("loc_0076");
            else mensajePersonalidad.text = Localizador.Texto("loc_0023");

            mensajeLinea.SetActive(false);

            titulo3.GetComponent<DinamicText>().SetTextByID("0117");
        }

        else if(AuxiliarBuild.instance.tipo[indice] >= 14)
        {
            pantalla.SetActive(false);
            pantalla5.SetActive(true);
            switch (AuxiliarBuild.instance.tipo[indice])
            {
                case 14:
                    grafica5.cognitiveLoadMedio = 0;
                    grafica5.cognitiveLoadMedioEventos = 0;
                    grafica5.fallosDesconcentrado = 0;

                    explicativoP2C1.SetTextByID("0026");
                    explicativoP2C2.SetTextByID("0027");
                    explicativoP2C4.SetTextByID("0029");
                    ResultNuevaSimulacion1 res = (ResultNuevaSimulacion1)AuxiliarBuild.instance.resultados[indice];

                    Debug.Log(res.nombreEjrcicio);
                    switch (res.nombreEjrcicio)
                    {
                        case "new simulacion 1":
                            titulo5.GetComponent<Text>().text = Localizador.Texto("loc_0044");
                            break;
                        case "new simulacion 2":
                            titulo5.GetComponent<Text>().text = Localizador.Texto("loc_0047");
                            break;
                        case "new simulacion 4":
                            titulo5.GetComponent<Text>().text = Localizador.Texto("loc_0053");
                            break;
                        default:
                            Debug.Log("error en el titulo 5");
                            break;
                    }


                    tiempoTotal5.text = res.time.ToString("F2") + "s";
                    
                    tiempoReaccion5.text = res.reactionTime.ToString("F2") + "s";
                    if (float.IsNaN (res.reactionTime)) tiempoReaccion5.text = Localizador.Texto("loc_0113");
                    
                    aciertos5.text = res.aciertos.ToString();
                    fallos5.text = res.fallos.ToString();
                    grafica5.minutosPrintear = res.minutos;
                    for (int i = 0; i < res.eventos.Count; i++)
                    {
                        grafica5.Draw(res.eventos[i].tiempos, res.eventos[i].color);
                    }
                    for (int i = 0; i < res.graficas.Count; i++)
                    {
                        if(res.graficas[i].color != coloresGraficas.pupilDilatation) grafica5.Draw(res.graficas[i].values, res.graficas[i].tiempos, res.graficas[i].color);
                    }

                    if (res.apto)
                    {
                        apto5.text = Localizador.Texto("loc_0023");
                    }
                    else
                    {
                        apto5.text = Localizador.Texto("loc_0073");
                    }

                    if (grafica5.cognitiveLoadMedio != 0)
                    {
                        clMedio.text = grafica5.cognitiveLoadMedio.ToString("F2");
                        clEvento.text = grafica5.cognitiveLoadMedioEventos.ToString("F2");
                        falloPorDesconcentracion.text = grafica5.fallosDesconcentrado.ToString("F2");

                    }
                    else
                    {
                        botonLupa.interactable = false;
                        /*
                        clMedio.text = "Sin datos";
                        clEvento.text = "Sin datos";
                        falloPorDesconcentracion.text = "Sin datos";*/
                    }

                    break;
                case 17:
                    Debug.Log(Localizador.Texto("loc_0056"));
                    titulo5.GetComponent<Text>().text = Localizador.Texto("loc_0056");
                    grafica5.cognitiveLoadMedio = 0;
                    grafica5.cognitiveLoadMedioEventos = 0;
                    grafica5.fallosDesconcentrado = 0;

                    explicativoP2C1.SetTextByID("0026");
                    explicativoP2C2.SetTextByID("0027");
                    explicativoP2C4.SetTextByID("0029");
                    ResultNuevaSimulacion1 res2 = (ResultNuevaSimulacion1)AuxiliarBuild.instance.resultados[indice];

                    tiempoTotal5.text = res2.time.ToString("F2") + "s";

                    tiempoReaccion5.text = res2.reactionTime.ToString("F2") + "s";
                    if (float.IsNaN(res2.reactionTime)) tiempoReaccion5.text = Localizador.Texto("loc_0113");

                    aciertos5.transform.parent.gameObject.SetActive(false);
                    explicativoP2C3.gameObject.SetActive(false);
                    fallos5.text = res2.fallos.ToString();
                    grafica5.minutosPrintear = res2.minutos;
                    for (int i = 0; i < res2.eventos.Count; i++)
                    {
                        grafica5.Draw(res2.eventos[i].tiempos, res2.eventos[i].color);
                    }
                    for (int i = 0; i < res2.graficas.Count; i++)
                    {
                        grafica5.Draw(res2.graficas[i].values, res2.graficas[i].tiempos, res2.graficas[i].color);
                    }

                    if (res2.apto)
                    {
                        apto5.text = Localizador.Texto("loc_0023");
                    }
                    else
                    {
                        apto5.text = Localizador.Texto("loc_0073");
                    }

                    if (grafica5.cognitiveLoadMedio != 0)
                    {
                        clMedio.text = grafica5.cognitiveLoadMedio.ToString("F2");
                        clEvento.text = grafica5.cognitiveLoadMedioEventos.ToString("F2");
                        falloPorDesconcentracion.text = grafica5.fallosDesconcentrado.ToString("F2");

                    }
                    else
                    {
                        botonLupa.interactable = false;
                        /*
                        clMedio.text = "Sin datos";
                        clEvento.text = "Sin datos";
                        falloPorDesconcentracion.text = "Sin datos";*/
                    }
                    break;

                case 16:
                    Debug.Log(Localizador.Texto("loc_0056"));
                    titulo5.GetComponent<Text>().text = Localizador.Texto("loc_0050");
                    grafica5.cognitiveLoadMedio = 0;
                    grafica5.cognitiveLoadMedioEventos = 0;
                    grafica5.fallosDesconcentrado = 0;
                    explicativoP2C1.SetTextByID("0193");
                    explicativoP2C2.SetTextByID("0194");
                    explicativoP2C4.SetTextByID("0192");
                    ResultNuevaSimulacion1 res1 = (ResultNuevaSimulacion1)AuxiliarBuild.instance.resultados[indice];
                    tiempoTotal5.text = res1.time.ToString("F2") + "%";
                    tiempoReaccion5.text = res1.reactionTime.ToString("F2");
                    aciertos5.text = res1.aciertos.ToString();
                    fallos5.text = res1.fallos.ToString();


                    grafica5.minutosPrintear = res1.minutos;
                    for (int i = 0; i < res1.eventos.Count; i++)
                    {
                        grafica5.Draw(res1.eventos[i].tiempos, res1.eventos[i].color);
                    }
                    for (int i = 0; i < res1.graficas.Count; i++)
                    {
                        grafica5.Draw(res1.graficas[i].values, res1.graficas[i].tiempos, res1.graficas[i].color);
                    }

                    if (res1.apto)
                    {
                        apto5.text = Localizador.Texto("loc_0023");
                    }
                    else
                    {
                        apto5.text = Localizador.Texto("loc_0073");
                    }

                    if (grafica5.cognitiveLoadMedio != 0)
                    {
                        clMedio.text = grafica5.cognitiveLoadMedio.ToString("F2");
                        clEvento.text = grafica5.cognitiveLoadMedioEventos.ToString("F2");
                        falloPorDesconcentracion.text = grafica5.fallosDesconcentrado.ToString("F2");

                    }
                    else
                    {
                        botonLupa.interactable = false;
                        /*
                        clMedio.text = "Sin datos";
                        clEvento.text = "Sin datos";
                        falloPorDesconcentracion.text = "Sin datos";*/
                    }
                    break;

            }

        }
        else if (AuxiliarBuild.instance.tipo[indice] >= 9)
        {
            pantalla.SetActive(false);
            pantalla2.SetActive(true);

            ResultSimulacion3 res = (ResultSimulacion3) AuxiliarBuild.instance.resultados[indice];

            tiempoTotalS.text = res.duracion.ToString("F2") + " s";
            falloChoque.text = res.choqueFallos.ToString();
            falloVMin.text = res.veloidadInferiorFallos.ToString();
            falloVMax.text = res.velocidadSuperiorFallos.ToString();
            falloSemaforo.text = res.semaforoFallos.ToString();
            falloSalida.text = res.salidaFallos.ToString();
            minHearRate.text = res.MinHeartRate.ToString();
            maxHeartRate.text = res.MaxHeartRate.ToString();
            //averageHearRate.text = (simulacion3.MaxHeartRate - simulacion3.MinHeartRate).ToString();
            grafica.Reset();
            grafica.minutosPrintear = res.minutos;
            
            for (int i = 0; i < res.eventos.Count; i++)
            {
                grafica.Draw(res.eventos[i].tiempos, res.eventos[i].color);
            }
            for (int i = 0; i < res.graficas.Count; i++)
            {
                grafica.Draw(res.graficas[i].values, res.graficas[i].tiempos, res.graficas[i].color);
            }

            if (res.apto)
            {
                apto.text = Localizador.Texto("loc_0023");
            }
            else
            {
                apto.text = Localizador.Texto("loc_0073");
            }

            switch (AuxiliarBuild.instance.tipo[indice])
            {
                case 9:
                    titulo2.GetComponent<DinamicText>().SetTextByID("0044");
                    break;
                case 10:
                    titulo2.GetComponent<DinamicText>().SetTextByID("0045");
                    break;
                case 11:
                    titulo2.GetComponent<DinamicText>().SetTextByID("0048");
                    break;
                case 12:
                    titulo2.GetComponent<DinamicText>().SetTextByID("0051");
                    break;
                case 13:
                    titulo2.GetComponent<DinamicText>().SetTextByID("0051");
                    break;
            }
        }
        else if (AuxiliarBuild.instance.tipo[indice] == 6)
        {
            pantalla.SetActive(false);
            pantalla6.SetActive(true);

            Result6 result6 = (Result6)AuxiliarBuild.instance.resultados[indice];

            titulo6.text = Localizador.Texto("loc_0175");
            neuroticismo.text = ((int)result6.neuroticismo).ToString();
            paranoia.text = ((int)result6.paranoia).ToString();
            agitacion.text = ((int)result6.agitacion).ToString();
            inestabilidad.text = ((int)result6.inestabilidad).ToString();
            psicastenia.text = ((int)result6.psicastenia).ToString();
            hipocondria.text = ((int)result6.hipocondria).ToString();
            suicidio.text = ((int)result6.suicidio).ToString();

        }

        else if (AuxiliarBuild.instance.tipo[indice] == 7)
        {
            pantalla.SetActive(false);
            pantalla3.SetActive(true);
            mensajeLinea.SetActive(true);
            Result7 resultado7 = (Result7)AuxiliarBuild.instance.resultados[indice];

            Text t = mensajeLinea.transform.GetChild(0).GetComponent<Text>();
            t.text = Localizador.Texto("loc_0033") + ": " + (resultado7.linea + 1);
            if (resultado7.linea < 7) mensajePersonalidad.text = Localizador.Texto("loc_0073") + ". " + Localizador.Texto("loc_0076");
            else mensajePersonalidad.text = Localizador.Texto("loc_0023");

            titulo3.GetComponent<DinamicText>().SetTextByID("0058");
        }

        else if (AuxiliarBuild.instance.tipo[indice] == 8)
        {
            pantalla.SetActive(false);
            pantalla4.SetActive(true);

            Result8 resultado8 = (Result8)AuxiliarBuild.instance.resultados[indice];

            /*if (resultado8.apto)
            {
                aptoAuditivo.text = Localizador.Texto((Localizador.IDTexto)23);

            }
            else
            {
                aptoAuditivo.text = Localizador.Texto((Localizador.IDTexto)73);
            }*/

            aptoAuditivo.text = Localizador.Texto("loc_0023");

            textDe250.text = (resultado8.de250 * 100).ToString() + " dB";
            textDe500.text = (resultado8.de500 * 100).ToString() + " dB";
            textDe1000.text = (resultado8.de1000 * 100).ToString() + " dB";
            textDe2000.text = (resultado8.de2000 * 100).ToString() + " dB";
            textDe4000.text = (resultado8.de4000 * 100).ToString() + " dB";
            textDe8000.text = (resultado8.de8000 * 100).ToString() + " dB";

            textIz250.text = (resultado8.iz250 * 100).ToString() + " dB";
            textIz500.text = (resultado8.iz500 * 100).ToString() + " dB";
            textIz1000.text = (resultado8.iz1000 * 100).ToString() + " dB";
            textIz2000.text = (resultado8.iz2000 * 100).ToString() + " dB";
            textIz4000.text = (resultado8.iz4000 * 100).ToString() + " dB";
            textIz8000.text = (resultado8.iz8000 * 100).ToString() + " dB";

            titulo4.GetComponent<DinamicText>().SetTextByID("0059");
        }
    }
    public void Avanzar()
    {
        ++indice;
        Presentar();
        botonMultiAtras.SetActive(true);
        if (indice == AuxiliarBuild.instance.tipo.Count - 1) botonMultiAdelante.SetActive(false);
    }

    public void Atras()
    {
        --indice;
        Presentar();
        botonMultiAdelante.SetActive(true);
        if (indice == 0) botonMultiAtras.SetActive(false);
    }

    public void CambiarDatosGrafica()
    {
        if (suBDivision1.activeSelf)
        {
            suBDivision1.SetActive(false);
            suBDivision2.SetActive(true);
        }
        else
        {
            suBDivision1.SetActive(true);
            suBDivision2.SetActive(false);
        }

        EventSystem.current.SetSelectedGameObject(null);
    }
}
