using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Test8Manager : MonoBehaviour
{
    enum pruebas
    {
        r250,
        r500,
        r1k,
        r2k,
        r4k,
        r8k,
        l250,
        l500,
        l1k,
        l2k,
        l4k,
        l8k
    }

    [SerializeField] AudioClip hz250, hz500, hz1k, hz2k, hz4k, hz8k;
     Result8 resultados;

    List<pruebas> pruebasFaltan = new List<pruebas>();

    float   senseL250, senseR250,
            senseL500, senseR500,
            senseL1k, senseR1k,
            senseL2k, senseR2k,
            senseL4k, senseR4k,
            senseL8k, senseR8k;

    [SerializeField] float timeSound, timeSilence;

    AudioSource source;

    bool active, right;
    int hz;
    float volume;

    public bool skip;

    int fallos;

    public void StartTest()
    {
        resultados = new Result8();
        for (int i = 0; i < 12; i++)
        {
            pruebasFaltan.Add((pruebas)i);
        }
        source = GetComponent<AudioSource>();
        StartCoroutine(Test());
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            if (active && right) SaveCorrect();
            else ++fallos;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            if (active && !right) SaveCorrect();
            else ++fallos;
        }
    }

    IEnumerator Test()
    {
        bool b = false;
        int j = 0;
        while(pruebasFaltan.Count > 0)
        {
            pruebas esta = pruebasFaltan[Random.Range(0, pruebasFaltan.Count)];
            switch (esta)
            {
                case pruebas.r250:
                    b = true;
                    j = 250;
                    break;
                case pruebas.r500:
                    b = true;
                    j = 500;
                    break;
                case pruebas.r1k:
                    b = true;
                    j = 1000;
                    break;
                case pruebas.r2k:
                    b = true;
                    j = 2000;
                    break;
                case pruebas.r4k:
                    b = true;
                    j = 4000;
                    break;
                case pruebas.r8k:
                    b = true;
                    j = 8000;
                    break;
                case pruebas.l250:
                    b = false;
                    j = 250;
                    break;
                case pruebas.l500:
                    b = false;
                    j = 500;
                    break;
                case pruebas.l1k:
                    b = false;
                    j = 1000;
                    break;
                case pruebas.l2k:
                    b = false;
                    j = 2000;
                    break;
                case pruebas.l4k:
                    b = false;
                    j = 4000;
                    break;
                case pruebas.l8k:
                    b = false;
                    j = 8000;
                    break;
            }

            pruebasFaltan.Remove(esta);

            for (float k = 0.1f; k < 0.7f; k+=0.1f)
            {
                On(j, b, k);
                yield return new WaitForSeconds(timeSound);
                Off();
                yield return new WaitForSeconds(timeSilence);
                if (skip) break;
            }
            
            skip = false;
        }

        End();
    }

    public void On(int hz, bool right, float volume)
    {
        source.time = 5f;
        source.clip = intToClip(hz);
        if (right) source.panStereo = 1;
        else source.panStereo = -1;
        source.volume = volume;
        active = true;
        this.right = right;
        this.hz = hz;
        this.volume = volume;
        source.Play();
    }

    void SaveCorrect()
    {
        switch (hz)
        {
            case 250:
                if (right) senseR250 = volume;
                else senseL250 = volume;
                break;
            case 500:
                if (right) senseR500 = volume;
                else senseL500 = volume;
                break;
            case 1000:
                if (right) senseR1k = volume;
                else senseL1k = volume;
                break;
            case 2000:
                if (right) senseR2k = volume;
                else senseL2k = volume;
                break;
            case 4000:
                if (right) senseR4k = volume;
                else senseL4k = volume;
                break;
            case 8000:
                if (right) senseR8k = volume;
                else senseL8k = volume;
                break;
        }
        skip = true;
    }

    void Off()
    {
        source.Stop();
        active = false;
    }

    AudioClip intToClip(int hz)
    {
        switch (hz)
        {
            case 250:
                return hz250;
            case 500:
                return hz500;
            case 1000:
                return hz1k;
            case 2000:
                return hz2k;
            case 4000:
                return hz4k;
            case 8000:
                return hz8k;
            default:
                return null;
        }
    }

    void End()
    {
        print(senseL250 +"/"+ senseR250 + "/" +
            senseL500 + "/" + senseR500 + "/" +
            senseL1k + "/" + senseR1k + "/" +
            senseL2k + "/" + senseR2k + "/" +
            senseL4k + "/" + senseR4k + "/" +
            senseL8k + "/" + senseR8k + "/" + fallos);
        FPrepararSubida();
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
    public void FPrepararSubida()
    {
        resultados.iz250 = senseL250;
        resultados.iz500 = senseL500;
        resultados.iz1000 = senseL1k;
        resultados.iz2000 = senseL2k;
        resultados.iz4000 = senseL4k;
        resultados.iz8000 = senseL8k;

        resultados.de250 = senseR250;
        resultados.de500 = senseR500;
        resultados.de1000 = senseR1k;
        resultados.de2000 = senseR2k;
        resultados.de4000 = senseR4k;
        resultados.de8000 = senseR8k;
        AuxiliarBuild.instance.tipo.Add(8);
        AuxiliarBuild.instance.resultados.Add(resultados);
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.UploadAlumno();

        resultados.FSubirSesion();
    }
    //DEPRECATED
    /*
    IEnumerator PrepararSubida()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                string s = "";
                bool cambio = false;
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio && www.downloadHandler.text[i] == ':')
                    {
                        cambio = true;
                    }
                    else if (cambio)
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateEnd = s;
                resultados.iz250 = senseL250;
                resultados.iz500 = senseL500;
                resultados.iz1000 = senseL1k;
                resultados.iz2000 = senseL2k;
                resultados.iz4000 = senseL4k;
                resultados.iz8000 = senseL8k;

                resultados.de250 = senseR250;
                resultados.de500 = senseR500;
                resultados.de1000 = senseR1k;
                resultados.de2000 = senseR2k;
                resultados.de4000 = senseR4k;
                resultados.de8000 = senseR8k;
                AuxiliarBuild.instance.tipo.Add( 8);
                AuxiliarBuild.instance.resultados.Add(resultados);
                resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
                resultados.UploadAlumno();
                yield return resultados.SubirSesion();
            }
        }
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }*/
    //DEPRECATED
    /*
    IEnumerator CStart()
    {

        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                bool cambio = false;
                string s = "";
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio)
                    {
                        if (www.downloadHandler.text[i] == ':')
                        {
                            cambio = true;
                            resultados.fecha = s;
                            s = "";
                        }
                        else
                        {
                            s += www.downloadHandler.text[i];
                        }
                    }
                    else
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateInit = s;
            }
        }
    }*/
}
