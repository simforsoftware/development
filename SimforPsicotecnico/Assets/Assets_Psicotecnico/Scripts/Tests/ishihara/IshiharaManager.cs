using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IshiharaManager : MonoBehaviour
{
    [SerializeField] GameObject content, button;
    [SerializeField] ListaIsihara lista;
    [SerializeField] Image imagen;
    [SerializeField] GameObject descripcion;

    List<IshiharaButton> botones = new List<IshiharaButton>();

    int aciertos, fallos;
    int question = 0;

    Result7 resultado7;
    public void Start()
    {
        resultado7 = new Result7();
        resultado7.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultado7.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");

        for (int i = 0; i < 4; i++)
        {
            botones.Add(Instantiate(button, content.transform).GetComponent<IshiharaButton>());
        }
        foreach (IshiharaButton boton in botones) boton.Set(this);
        botones[3].SetText(Localizador.Texto("loc_108"));
    }

    public void StartTest()
    {
        descripcion.SetActive(false);
        NextQuestion();
    }

    void NextQuestion()
    {
        if (question >= lista.grupos.Count) EndTest();
        else
        {
            IshiharaQuestion thisQuestion = lista.grupos[question].preguntas[Random.Range(0, lista.grupos[question].preguntas.Count)];
            UpdateQuestion(thisQuestion);
            ++question;
        }
    }

    void UpdateQuestion(IshiharaQuestion pregunta)
    {
        imagen.sprite = pregunta.Imagen();

        foreach (IshiharaButton boton in botones) boton.correcto = false;

        List<int> indices = new List<int>();

        while(indices.Count < 3)
        {
            int indice = -1;
            while(indice < 0 || indices.Contains(indice))
            {
                indice = Random.Range(0, 3);
            }
            indices.Add(indice);
        }

        for (int i = 0; i < 3; i++)
        {
            botones[i].SetText(pregunta.Respuestas()[indices[i]]);
        }
        if (pregunta.SinRespuesta()) botones[3].correcto = true;
        else botones[indices.IndexOf(0)].correcto = true;
    }

    public void Responder(bool acierto)
    {
        if (acierto) ++aciertos;
        else ++fallos;

        NextQuestion();
    }

    void EndTest()
    {
        resultado7.linea = fallos;
        resultado7.apto = fallos < 1;
        resultado7.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultado7.UploadAlumno();
        resultado7.IshiharaFSubir();
        AuxiliarBuild.instance.tipo.Add(15);
        AuxiliarBuild.instance.resultados.Add(resultado7);
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }

}
