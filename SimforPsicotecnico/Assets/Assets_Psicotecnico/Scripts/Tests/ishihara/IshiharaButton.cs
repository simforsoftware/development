using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IshiharaButton : MonoBehaviour
{
    public bool correcto;

    IshiharaManager padre;

    public void Set(IshiharaManager manager)
    {
        padre = manager;
    }

    public void SetText(string s)
    {
        GetComponentInChildren<Text>().text = s;
    }

    public void Responder()
    {
        padre.Responder(correcto);
    }
}
