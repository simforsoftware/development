using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class seleccionador : MonoBehaviour
{

    List<string> posibilidades = new List<string>();
    [SerializeField] List<Text> opciones;
    public bool cambiar = true;
    public Test7 padre;

    [SerializeField] List<string> E_Senllen = new List<string>();
    public void Rellenar(string a, string b, string c, string d)
    {
        if (!cambiar) return;
        posibilidades.Clear();
        List<string> l = new List<string>();
        l.Add(a);
        l.Add(b);
        l.Add(c);
        l.Add(d);
        int j;
        for (int i = 0; i < 4; ++i)
        {
            int s = 0;
            do
            {
                ++s;
                j = Random.Range(0, 3 - i);
                
            }while (posibilidades.Contains(l[j]) && s < 100);
            posibilidades.Add(l[j]);
            l.Remove(l[j]);
        }
        for(int i = 0; i < 4; ++i)
        {
            opciones[i].text = posibilidades[i];
        }
    }

    public void Respuesta(int i)
    {
        if (cambiar)
        {
            Debug.Log("Respuestas a " + posibilidades[i]);
            padre.Comprobar_Respuesta(posibilidades[i]);
        }
        else
        {
            Debug.Log(E_Senllen[i]);
            padre.Comprobar_Respuesta(E_Senllen[i]);
        }

    }

}
