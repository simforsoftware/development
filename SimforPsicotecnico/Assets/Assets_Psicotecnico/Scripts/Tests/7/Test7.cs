using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Test7 : MonoBehaviour
{

    public List<List<char>> referencias = new List<List<char>>();

    public List<string[]> lineas = new List<string[]>();

    int linea_Actual = 0;
    int letra_Actual = 0;

    int aciertos = 0;
    int fallos = 0;

    bool con_fallo = false;

    float posicion_Originalx_IndicadorL;

    [SerializeField] GameObject indicador_Linea;
    [SerializeField] GameObject indicador_Letras;
    [SerializeField] GameObject menuRespuestas;
    [SerializeField] GameObject posiciones;
     Result7 resultados;

    public string lineasTest;

    bool terminado = false;
    public List<char> L = new List<char>();
    public List<char> E = new List<char>();
    public List<char> T = new List<char>();
    public List<char> D = new List<char>();
    public List<char> O = new List<char>();
    public List<char> F = new List<char>();
    public List<char> P = new List<char>();
    public List<char> C = new List<char>();
    public List<char> Z = new List<char>();
    public List<Transform> tranforms = new List<Transform>();
    public Transform[] arrayPosiciones;
    int posicionActual = 0;
    private void Start()
    {
        resultados = new Result7();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        posicion_Originalx_IndicadorL = indicador_Letras.transform.position.x;
        menuRespuestas.GetComponent<seleccionador>().padre = this;

        referencias.Add(L);
        referencias.Add(E);
        referencias.Add(T);
        referencias.Add(D);
        referencias.Add(O);
        referencias.Add(F);
        referencias.Add(P);
        referencias.Add(C);
        referencias.Add(Z);

        string[] a = lineasTest.Split(',');
        for(int i = 0; i < a.Length; ++i)
        {
            string[] b = a[i].Split('.');
            lineas.Add(b);
        }      

       // arrayPosiciones = posiciones.GetComponentsInChildren<Transform>();


        indicador_Letras.GetComponent<RectTransform>().position = new Vector3(tranforms[posicionActual].position.x, tranforms[posicionActual].position.y, indicador_Letras.GetComponent<RectTransform>().position.z);
        Start_Test();
    }

    public void Start_Test()
    {

        linea_Actual = lineas.Count - 1;
        letra_Actual = lineas[linea_Actual].Length - 1;
        Debug.Log(lineas[linea_Actual][letra_Actual]);
        Preguntar_Letra();
    }

    public void Preguntar_Letra()
    {
        int j = 0;
        for(int i = 0; i < referencias.Count; ++i)
        {
            if( lineas[linea_Actual][letra_Actual] == referencias[i][0].ToString())
            {
                 j = i;
            }
        }
        Debug.Log("SIGUIENTE LETRA: " + lineas[linea_Actual][letra_Actual]);
        menuRespuestas.GetComponent<seleccionador>().Rellenar(referencias[j][0].ToString(), referencias[j][1].ToString(), referencias[j][2].ToString(), referencias[j][3].ToString());
    }

    public void Comprobar_Respuesta(string respuesta)
    {
        if (terminado) return;
        if (lineas[linea_Actual][letra_Actual] == respuesta)
        {
            ++aciertos;
        }
        else
        {
            ++fallos;
            con_fallo = true;
        }
        --letra_Actual;
        /*indicador_Letras.GetComponent<RectTransform>().position =
            new Vector3(indicador_Letras.GetComponent<RectTransform>().position.x + (indicador_Letras.GetComponent<RectTransform>().rect.width / 1.4f) * (((lineas.Count - linea_Actual))*(indicador_Letras.GetComponent<RectTransform>().localScale.x)),
            indicador_Letras.GetComponent<RectTransform>().position.y,
            indicador_Letras.GetComponent<RectTransform>().position.z);*/
        ++posicionActual;

        if (linea_Actual == 0 && letra_Actual < 0)
        {
            Debug.Log("vicotria");
            resultados.linea = linea_Actual;
            resultados.apto = linea_Actual > 6;
            AuxiliarBuild.instance.tipo.Add(7);
            AuxiliarBuild.instance.resultados.Add(resultados);
            FPrepararSubida();
            terminado = true;
            return;
        }
        indicador_Letras.GetComponent<RectTransform>().position = new Vector3(tranforms[posicionActual].position.x, tranforms[posicionActual].position.y, indicador_Letras.GetComponent<RectTransform>().position.z);



        if (letra_Actual < 0)
        {
            if (!con_fallo)
            {
                
                Debug.Log("vicotria");
                resultados.linea = linea_Actual;
                resultados.apto = linea_Actual > 6;
                AuxiliarBuild.instance.tipo.Add(7);
                AuxiliarBuild.instance.resultados.Add(resultados);
                FPrepararSubida();
                terminado = true;
                return;
            }
            con_fallo = false;
            --linea_Actual;
            
            Debug.Log(tranforms[posicionActual].position.y);
            indicador_Linea.GetComponent<RectTransform>().position = new Vector3(indicador_Linea.GetComponent<RectTransform>().position.x,
                tranforms[posicionActual].position.y,
                indicador_Linea.GetComponent<RectTransform>().position.z);

            /*indicador_Letras.GetComponent<RectTransform>().position =
            new Vector3(posicion_Originalx_IndicadorL - (indicador_Letras.GetComponent<RectTransform>().rect.width / 1.4f) * (lineas.Count - linea_Actual - 1),
            indicador_Letras.GetComponent<RectTransform>().position.y + indicador_Letras.GetComponent<RectTransform>().rect.height*2,
            indicador_Letras.GetComponent<RectTransform>().position.z);*/

            indicador_Letras.GetComponent<RectTransform>().localScale += new Vector3(0.2f,0.2f,0.2f);

            letra_Actual = lineas[linea_Actual].Length - 1;

        }
        Preguntar_Letra();
    }
    public void FPrepararSubida()
    {
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.UploadAlumno();
        resultados.FSubirSession();
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }

    //DEPRECATED
    /*
    IEnumerator PrepararSubida()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                string s = "";
                bool cambio = false;
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio && www.downloadHandler.text[i] == ':')
                    {
                        cambio = true;
                    }
                    else if (cambio)
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateEnd = s;
                resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
                resultados.UploadAlumno(); 
                yield return resultados.SubirSesion();
            }
        }
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }*/
    //DEPRECATED
    /*
    IEnumerator CStart()
    {

        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                bool cambio = false;
                string s = "";
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio)
                    {
                        if (www.downloadHandler.text[i] == ':')
                        {
                            cambio = true;
                            resultados.fecha = s;
                            s = "";
                        }
                        else
                        {
                            s += www.downloadHandler.text[i];
                        }
                    }
                    else
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateInit = s;
            }
        }
    }*/

}
