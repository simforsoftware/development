using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test7Respuesta : MonoBehaviour
{
    [SerializeField] seleccionador padre;
    [SerializeField] int numero;
    public void Seleccionado()
    {
        padre.Respuesta(numero);
    }
}
