using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test7CameraFixIntro : MonoBehaviour
{
    [SerializeField] GameObject camera;
    [SerializeField] GameObject posicion;
    public void FixIntro()
    {
        camera.transform.position = posicion.transform.position;
        camera.GetComponent<RecentradoCamara>().Recentrar();
    }
}
