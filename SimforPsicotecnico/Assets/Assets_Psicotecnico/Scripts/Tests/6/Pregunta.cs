public struct Pregunta
{
    public string enunciado;
    public caracteristicasPersonalidad caracteristica;
    public bool negacion;

    public Pregunta(string[] pregunta)
    {
        enunciado = pregunta[(int)Localizador.idioma + 2];
        switch (pregunta[1])
        {
            case "Ne":
                caracteristica = caracteristicasPersonalidad.Neuroticismo;
                break;
            case "Pa":
                caracteristica = caracteristicasPersonalidad.Paranoia;
                break;
            case "Ag":
                caracteristica = caracteristicasPersonalidad.Agitacion;
                break;
            case "In":
                caracteristica = caracteristicasPersonalidad.Inestabilidad;
                break;
            case "Ps":
                caracteristica = caracteristicasPersonalidad.Psicastenia;
                break;
            case "Hi":
                caracteristica = caracteristicasPersonalidad.Hipocondria;
                break;
            case "Su":
                caracteristica = caracteristicasPersonalidad.Suicidio;
                break;
            default:
                caracteristica = caracteristicasPersonalidad.none;
                break;
        }
        negacion = pregunta[2] == "1";
    }
}
