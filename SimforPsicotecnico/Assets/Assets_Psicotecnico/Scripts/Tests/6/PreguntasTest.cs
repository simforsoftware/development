using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PreguntasTest : MonoBehaviour
{
    string textoBruto;
    List<string[]> textoNeto = new List<string[]>();

    private void Start()
    {
        StartCoroutine(CargarTextos());
    }

    IEnumerator CargarTextos()
    {
        WWW csv = new WWW("https://docs.google.com/spreadsheets/d/e/2PACX-1vQ9ZuokvM8vyyzzKNyw5KOZIFQaq4816J95tFy7qrp8adnMh2_wF_p3C5-prMi7SiXPyOklsJjcVRUm/pub?gid=0&single=true&output=csv");


        yield return csv;

        if (csv.error != null)
        {
            Debug.LogError("Sin conexion, usando archivo local");
            string pathName = Path.Combine(Application.streamingAssetsPath, "preguntasTest");
            FileStream loc = File.Open(pathName, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(loc);
            textoBruto = sr.ReadToEnd();
            sr.Close();
            loc.Close();
        }
        else
        {
            Debug.Log("CSV Descargado:");
            Debug.Log(csv.text);
            string pathName = Path.Combine(Application.streamingAssetsPath, "preguntasTest");
            textoBruto = csv.text;
            FileStream loc = File.Open(pathName, FileMode.Create, FileAccess.ReadWrite);
            StreamWriter sw = new StreamWriter(loc);
            sw.Write(textoBruto);
            sw.Close();
            loc.Close();
        }

        GenerarInfo();
    }

    void GenerarInfo()
    {
        string[] preguntasBrutas = textoBruto.Split('[');
        foreach(string s in preguntasBrutas)
        {
            string[] preguntaSeparada = s.Split(',');
            for (int i = 0; i < preguntaSeparada.Length; i++) preguntaSeparada[i] = preguntaSeparada[i].Replace('*', ',');
            textoNeto.Add(preguntaSeparada);
        }

        EventManager.TriggerEvent("testReady");
    }

    public List<Pregunta> GetPreguntas()
    {
        List<Pregunta> preguntas = new List<Pregunta>();
        foreach(string[] s in textoNeto)
        {
            if (s.Length >= textoNeto[0].Length)
                preguntas.Add(new Pregunta(s));
        }
        return preguntas;
    }
}