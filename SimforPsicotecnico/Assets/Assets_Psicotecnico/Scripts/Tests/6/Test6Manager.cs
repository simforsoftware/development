using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Test6Manager : MonoBehaviour
{
    List<GameObject> respuestas = new List<GameObject>();
    [SerializeField] Text pregunta;
    Result6 resultados;
    bool primeraVez = true;
    [SerializeField] GameObject etiquetaRespuesta;
    [SerializeField] GameObject content;

    [SerializeField] Button botonStart;
    [SerializeField] DinamicText botonInicio;
    [SerializeField] 

    int numero_pregunta = 0;

    int aciertos = 0;
    int fallos = 0;

    Pregunta preguntaActiva;

    TextAsset listaPreguntas;

    float[] resultData = new float[8];
    

    List<Pregunta> preguntas_respuestas;

    private void Start()
    {
        EventManager.StartListening("testReady", Ready);
        botonInicio.SetTextByID("0069");
    }

    public void Ready()
    {
        botonInicio.SetTextByID("0061");
        botonStart.interactable = true;
        preguntas_respuestas = GetComponent<PreguntasTest>().GetPreguntas();
    }

    public void StartTest()
    { 
        resultados = new Result6();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        Preguntar();
    }

    public void Preguntar()
    {
        respuestas.Clear();
        if (numero_pregunta >= preguntas_respuestas.Count && primeraVez)
        {
            primeraVez = false;
            bool apto = false;

            resultados.neuroticismo = resultData[(int)caracteristicasPersonalidad.Neuroticismo];
            resultados.paranoia = resultData[(int)caracteristicasPersonalidad.Paranoia];
            resultados.agitacion = resultData[(int)caracteristicasPersonalidad.Agitacion];
            resultados.inestabilidad = resultData[(int)caracteristicasPersonalidad.Inestabilidad];
            resultados.psicastenia = resultData[(int)caracteristicasPersonalidad.Psicastenia];
            resultados.hipocondria = resultData[(int)caracteristicasPersonalidad.Hipocondria];
            resultados.suicidio = resultData[(int)caracteristicasPersonalidad.Suicidio];



            if (aciertos > 5) apto = true;
            AuxiliarBuild.instance.tipo.Add( 6);
            AuxiliarBuild.instance.resultados.Add(resultados);
            resultados.apto = apto;
            FPrepararSubida();
        }
        preguntaActiva = preguntas_respuestas[numero_pregunta];

        pregunta.text = preguntaActiva.enunciado;
    }

    public void Respondido(int value)
    {
        float realValue = value / (float)2;
        if (preguntaActiva.negacion)
        {
            if (realValue == 1) realValue = 0;
            else if (realValue == 0) realValue = 1;
        }
        ++numero_pregunta;
        resultData[(int)preguntaActiva.caracteristica] += realValue;
        print(preguntaActiva.caracteristica);
        EventSystem.current.SetSelectedGameObject(null);
        Preguntar();
    }

    public void FPrepararSubida()
    {
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.UploadAlumno();
        resultados.FSubirSesion();
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}

public enum caracteristicasPersonalidad
{
    Neuroticismo,
    Paranoia,
    Agitacion,
    Inestabilidad,
    Psicastenia,
    Hipocondria,
    Suicidio,
    none,
}
