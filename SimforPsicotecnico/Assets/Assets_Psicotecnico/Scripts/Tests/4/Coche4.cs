using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche4 : MonoBehaviour
{
    public float speed;
    public bool move;

    private void Update()
    {
        if(move) transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
    }
}
