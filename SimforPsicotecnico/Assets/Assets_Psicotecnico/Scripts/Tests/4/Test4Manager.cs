using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Test4Manager : MonoBehaviour
{
    [SerializeField] Coche4 coche;
    [SerializeField] Transform referencia;
    [SerializeField] GameObject telon;
    [SerializeField] InfoTest4 info;
    [SerializeField] float tiempoEsperaUltimaIteracion;
     Result4 resultados;
    [SerializeField] TypeImput correctInput;

    Vector3 posicionInicial;

    int repeticiones, aciertosNecesarios;
    float tiempo1, tiempo2, tiempoAUsar, margenTemporal;

    int aciertos, fallos;
    float timerIteracion, timerTotal, tiemposReaccionMedia;
    List<float> tiemposReaccion = new List<float>();
    float fallo;
    bool apto;

    bool ready;

    /*private void Start()
    {
        posicionInicial = coche.transform.position;
        coche.move = false;
        repeticiones = info.repeticiones;
        aciertosNecesarios = info.aciertosNecesarios;
        tiempo = info.tiempo;
        margenTemporal = info.margenTemporal;
        StartCoroutine(CStart());
        SetIteracion();
        timerTotal = 0f;
    }*/
    

    void SetIteracion()
    {
        if (Random.Range(0, 2) == 0) tiempoAUsar = tiempo1;
        else tiempoAUsar = tiempo2;

        timerIteracion = 0;
        telon.SetActive(true);
        coche.transform.position = posicionInicial;
        coche.speed = Mathf.Abs((posicionInicial - referencia.position).magnitude) / tiempoAUsar;
        coche.move = true;
        ready = true;
        --repeticiones;
    }

    private void Update()
    {
        timerTotal += Time.deltaTime;
        timerIteracion += Time.deltaTime;

        if (ready && GodOfInputs.instance.GetInput(correctInput)) Pausar();
        if (ready && coche.transform.position.x - referencia.transform.position.x > coche.speed * margenTemporal * 1.2f) Pausar();
    }

    public void Pausar()
    {
        coche.move = false;
        telon.SetActive(false);
        fallo = Mathf.Abs((coche.transform.position - referencia.position).magnitude) / coche.speed;
        if (fallo <= margenTemporal) ++aciertos;
        else ++fallos;
        ready = false;

        tiemposReaccion.Add(Mathf.Abs(tiempoAUsar - timerIteracion));

        if (repeticiones > 0) Invoke("SetIteracion", 3f);
        else StartCoroutine(EndWithWait());
    }
    IEnumerator EndWithWait()
    {
        yield return new WaitForSeconds(tiempoEsperaUltimaIteracion);
        EndGame();
    }
     
    void EndGame()
    {
        if (aciertos >= aciertosNecesarios) apto = true;
        if (tiemposReaccion.Count > 0)
        {
            float suma = 0;
            foreach (float f in tiemposReaccion) suma += f;
            tiemposReaccionMedia = suma / tiemposReaccion.Count;
        }

        ///Variables con info:
        ///aciertos
        ///fallos
        ///timerTotal
        ///timeposReaccionMedia
        ///apto
        resultados.aciertos = aciertos;
        resultados.apto = apto;
        AuxiliarBuild.instance.tipo.Add( 4);
        resultados.fallos = fallos;
        resultados.tiempoReaccion = tiemposReaccionMedia;
        resultados.tiempoTotal = timerTotal;

        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.UploadAlumno();
        AuxiliarBuild.instance.resultados.Add(resultados);
        FPrepararSubida();
    }
    public void FPrepararSubida()
    {
        resultados.FSubirSesion();
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
    //DEPRECATED
    /*
    IEnumerator PrepararSubida()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                string s = "";
                bool cambio = false;
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio && www.downloadHandler.text[i] == ':')
                    {
                        cambio = true;
                    }
                    else if (cambio)
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateEnd = s;
                yield return resultados.SubirSesion();
            }
        }
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }*/
    public void BStart()
    {
        resultados = new Result4();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        posicionInicial = coche.transform.position;
        coche.move = false;
        repeticiones = info.repeticiones;
        aciertosNecesarios = info.aciertosNecesarios;
        tiempo1 = info.tiempo1;
        tiempo2 = info.tiempo2;
        margenTemporal = info.margenTemporal;
        SetIteracion();
        timerTotal = 0f;
    }
    //DEPRECATED
    /*
    IEnumerator CStart()
    {

        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                bool cambio = false;
                string s = "";
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio)
                    {
                        if (www.downloadHandler.text[i] == ':')
                        {
                            cambio = true;
                            resultados.fecha = s;
                            s = "";
                        }
                        else
                        {
                            s += www.downloadHandler.text[i];
                        }
                    }
                    else
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateInit = s;
            }
        }
    }*/
}
