using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test4VRAdapter : MonoBehaviour
{
   [SerializeField] GameObject posicionVR;

    public void Mover()
    {
        this.transform.position = posicionVR.transform.position;
    }
}
