using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoTests : MonoBehaviour
{
    List<TestSignal> signalsDemo = new List<TestSignal>();
    [SerializeField] Image imagen;
    AudioSource audioComponent;
    [SerializeField] Text texto;
    TestSignal activeSignal;
    TestSignalsManager manager;

    

    public void StartDemo(TestSignalsManager padre)
    {
        audioComponent = GetComponent<AudioSource>();
        manager = padre;
        foreach (TestSignal ts in manager.signals) signalsDemo.Add(ts);
        texto.text = Localizador.Texto("loc_0097");
        Invoke("NextSignal",3f);
    }

    void NextSignal()
    {
        print(signalsDemo.Count);
        if (signalsDemo.Count == 0)
        {
            texto.text = Localizador.Texto("loc_0104");
            Invoke("EndDemo", 3f);
            return;
        }

        activeSignal = signalsDemo[0];
        signalsDemo.Remove(activeSignal);

        if (activeSignal.GetComponent<Image>()) imagen.sprite = activeSignal.GetComponent<Image>().sprite;
        else audioComponent.Play();
        texto.text = DescripcionFromTypeImput(activeSignal.correctInput);
    }

    private void Update()
    {
        if (!activeSignal) return;
        if(activeSignal.correctInput == TypeImput.none)
        {
            activeSignal = null;
            Invoke("Cambio", 3f);
            return;
        }
        if (GodOfInputs.instance.GetInput(activeSignal.correctInput))
        {
            activeSignal = null;
            audioComponent.Stop();
            imagen.sprite = null;
            texto.text = Localizador.Texto("loc_0103");
            Invoke("NextSignal", 1f);
            return;
        }
    }
    void Cambio()
    {
        activeSignal = null;
        audioComponent.Stop();
        imagen.sprite = null;
        texto.text = Localizador.Texto("loc_0103");
        Invoke("NextSignal", 1f);
    }

    void EndDemo()
    {
        manager.StartTestReal();
        Destroy(this.gameObject);
    }

    string DescripcionFromTypeImput(TypeImput input)
    {
        switch (input)
        {
            case TypeImput.none:
                return Localizador.Texto("loc_0102");
            case TypeImput.ManoDerecha:
                return Localizador.Texto("loc_0100");
            case TypeImput.ManoIzquierda:
                return Localizador.Texto("loc_0101");
            case TypeImput.PieDerecho:
                return Localizador.Texto("loc_0099");
            case TypeImput.PieIzquierdo:
                return Localizador.Texto("loc_0098");
            default:
                return "";
        }
    }
}
