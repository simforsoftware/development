using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ControladorPruebaTest3Alt : MonoBehaviour
{
    public Action Answered;
    bool inDemo;
    [HideInInspector]
     public int failure, success;

     List<float> riskDistances = new List<float>();
    [SerializeField] GameObject signal;
    CircleTest3 currentSignal;
    float signalradius;

    [SerializeField] float maximumDistanceX, maximumDistanceY;

    float maxCooldown = 1.5f;
    float timer = 0;

    private void Start()
    {
        signalradius = signal.GetComponent<CircleCollider2D>().radius * 2;
    }
    public void InputDown(int type) //Esto ha sido un a�adido de ultima hora, por favor no me juzgues Fawer :(
    {
        if (timer < maxCooldown) return;
        timer = 0;
        if(type == 2)
        {
            NoRespuesta();
            if (currentSignal) Destroy(currentSignal.gameObject);
            return;
        }
        else
        {
            currentSignal.InputDown(type == 0);
        }
    }
    public void InstantiateCircle(bool demo, int tipo, int lado = 3)
    {
        if(currentSignal) Destroy(currentSignal.gameObject);
        Debug.Log("Next Circle");
        Vector3 pos = NewPosition(tipo, lado);
        GameObject g = Instantiate(signal, pos, Quaternion.identity);

         currentSignal =  g.GetComponent<CircleTest3>();
        currentSignal.Answer += Respuesta;
    
        inDemo = demo;
    }

    public void Respuesta(float distance, bool acierto)
    {
        if (!inDemo )
        {
            riskDistances.Add(distance);
            if (acierto)
            {
                ++success;
                Debug.Log("Acierto " + success);
            }
            else
            {
                ++failure;
                Debug.Log("Fallo " + failure);
            }
        }

        Answered?.Invoke();

    }
    public void NoRespuesta()
    {
        Answered?.Invoke();
    }

    private void Update()
    {
        if(timer <= maxCooldown)timer += Time.deltaTime;
    }

    public Vector3 NewPosition(int type, int lado = 3)
    {
        Vector3 v = new Vector3(0, 0, 0);

        float rndY = UnityEngine.Random.Range(-maximumDistanceY, maximumDistanceY);
        float rndX = 0;

            int rnd = 3;
        if (lado >= 3) rnd = UnityEngine.Random.Range(0, 2);
        else rnd = lado;

            switch (type)
            {
                //Close to the line 
                case 0:
                    Debug.Log("TOCA INSIDE");
                    if (rnd == 0)
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(-maximumDistanceX, rndY), Vector2.right, 20f);

                        rndX = UnityEngine.Random.Range(-signalradius / 2, signalradius);
                        rndX = -maximumDistanceX + info.distance + rndX;


                    }
                    else
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(maximumDistanceX, rndY), Vector2.left, 20f);

                        rndX = UnityEngine.Random.Range(-signalradius / 2, signalradius);
                        rndX = maximumDistanceX - info.distance - rndX;

                    }

                    break;
                case 1:
                    Debug.Log("TOCA CLOSE");
                    if (rnd == 0)
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(-maximumDistanceX, rndY), Vector2.right, 20f);

                        rndX = UnityEngine.Random.Range(signalradius + (signalradius / 2), signalradius + (signalradius * 1.5f));



                        rndX = -maximumDistanceX + info.distance - rndX;
                    }
                    else
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(maximumDistanceX, rndY), Vector2.left, 20f);


                        rndX = UnityEngine.Random.Range(signalradius + (signalradius / 2), signalradius + (signalradius * 1.5f));
                        rndX = maximumDistanceX - info.distance + rndX;
                    }
                    break;
                case 2:
                    Debug.Log("TOCA LIMITE");
                    if (rnd == 0)
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(-maximumDistanceX, rndY), Vector2.right, 20f);

                        rndX = info.distance - (UnityEngine.Random.Range(-signalradius , signalradius + (signalradius * 1.5f)));
                        rndX = -maximumDistanceX + info.distance - rndX;

                    }
                    else
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(maximumDistanceX, rndY), Vector2.left, 20f);

                        rndX = info.distance - (UnityEngine.Random.Range(-signalradius , signalradius + (signalradius * 1.5f)));
                        rndX = maximumDistanceX - info.distance + rndX;
                    }
                    break;
                case 3:
                    Debug.Log("TOCA NORMAL");
                    if (rnd == 0)
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(-maximumDistanceX, rndY), Vector2.right, 20f);

                        rndX = info.distance - (UnityEngine.Random.Range(signalradius * 1.5f, info.distance - (signalradius * 4)));
                        rndX = -maximumDistanceX + info.distance - rndX;
                    }
                    else
                    {
                        RaycastHit2D info;
                        info = Physics2D.Raycast(new Vector2(maximumDistanceX, rndY), Vector2.left, 20f);

                        rndX = info.distance - (UnityEngine.Random.Range(signalradius * 1.5f, info.distance - (signalradius * 4)));
                        rndX = maximumDistanceX - info.distance + rndX;
                    }

                    break;
            }
        v.x = rndX;
        v.y = rndY;
        v.z = -2.2f;
        return v;
    }

    public float RiskFactor()
    {
        float maxDistance = float.MinValue;
        float riskFactor = 0;
        float aux = 0;
        foreach(float f in riskDistances)
        {
            if (f > maxDistance) maxDistance = f;
            aux += f;
        }
        Debug.Log("la distancia maxima es " + maxDistance);

         riskFactor = maxDistance - (aux / riskDistances.Count);
        Debug.Log("la resta es : " + aux + " " + riskDistances.Count);
        return (riskFactor * 113) / 10.5f;
    }
}
