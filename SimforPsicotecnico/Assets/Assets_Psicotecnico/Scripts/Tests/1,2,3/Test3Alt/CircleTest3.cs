using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CircleTest3 : MonoBehaviour
{
    public Action<float, bool> Answer;
  

    bool firstAnswer = false;
    bool onTheLine = false;
    public void InputDown(bool left)
    {
        firstAnswer = true;
        RaycastHit2D info;
        info = Physics2D.Raycast(transform.position, Vector2.left, 20f);

        if (info.collider == null)
        {
            info = Physics2D.Raycast(transform.position, Vector2.right, 20f);

            if (info.collider) Answer?.Invoke(info.distance, ((left == false) && !onTheLine));
            else Debug.Log("NO COLLIDER WAS HIT");
        }
        else
        {
            Answer?.Invoke(info.distance, ((left == true) && !onTheLine));
        }


        Destroy(this.gameObject);
    }




    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("ON the line");
        onTheLine = true;
    }
}
