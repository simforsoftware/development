using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerTest3Alt : MonoBehaviour
{

    [SerializeField] Text descripcion;

    [SerializeField] int demoNumber, realNumber;
    int actualNumber;
    [SerializeField] SpriteRenderer lineImage;
    [SerializeField] ControladorPruebaTest3Alt controller;

    [SerializeField] int onLinea, onCloseLine, onLateral, onNormal;
    Result123 result;
    public void StartTest()
    {
        actualNumber = 0;
        controller.Answered += NextSignal;
        NextSignal();
        result = new Result123();
        result.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        result.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
    }

    public void NextSignal()
    {
        StartCoroutine(CNextSignal());
    }
    IEnumerator  CNextSignal()
    {
        yield return new WaitForSeconds(2);
        int type = DecideType();
        if(actualNumber < realNumber)
        {
            if(actualNumber < demoNumber)
            {
                lineImage.enabled = false;
                if (actualNumber == 0) controller.InstantiateCircle(true, type, 0);
                else if (actualNumber == 1) controller.InstantiateCircle(true, type, 1);
                else controller.InstantiateCircle(true, type);
            }
            else
            {
                lineImage.enabled = true;
                controller.InstantiateCircle(false, type);
            }
        }
        else
        {
            EndTest();
        }
        ++actualNumber;
    }

    public void EndTest()
    {
        Debug.Log("FINISH");
        Debug.Log(controller.failure + " " + controller.success + " " + controller.RiskFactor());

        result.aciertos = controller.success;
        result.fallos = controller.success + controller.failure;
        result.time  = ((float)controller.success / (float) result.fallos) * 100;
        result.reactionTime = controller.RiskFactor();

        result.apto = true;

        result.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        result.UploadAlumno();

        AuxiliarBuild.instance.tipo.Add(3);
        AuxiliarBuild.instance.resultados.Add(result);

        result.FSubirSession();
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());




    }

    public int DecideType()
    {
        switch (actualNumber)
        {
            case 0:
                descripcion.text = Localizador.Texto("loc_0195");
                --onNormal;
                return 3;
               
            case 1:
                descripcion.text = Localizador.Texto("loc_0196");
                --onNormal;
                return 3;
            case 2:
                descripcion.text = Localizador.Texto("loc_0197") + " " + Localizador.Texto("loc_0198");
                --onLinea;
                return 0;
            case 3:
                descripcion.text = Localizador.Texto("loc_0104");
                break;
            default:
                descripcion.gameObject.SetActive(false);
                break;


        }
 
        float rnd =Random.Range(0, onLinea + onCloseLine + onLateral + onNormal);

        int type = 0;

        if (rnd < onLinea)
        {
            type = 0;
            --onLinea;
        }
        else if (rnd < onLinea + onCloseLine)
        {
            type = 1;
            --onCloseLine;
        }

        else if (rnd < onLinea + onCloseLine + onLateral)
        {
            type = 2;
            --onLateral;
        }
        else
        {
            type = 3;
            --onNormal;
        }
        return type;
    }
}
