using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonNavigator : MonoBehaviour
{
    int currentButton = 1;

    [SerializeField] List<Button> buttons = new List<Button>();

    public void RStart()
    {
        buttons[currentButton].Select();
    }


    public void MoverBoton(bool back)
    {
        if(back)
        {
            if (currentButton == 0) return;

            --currentButton;
            buttons[currentButton].Select();
        }
        else {
            if (currentButton == buttons.Count - 1) return;

            ++currentButton;
            buttons[currentButton].Select();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) MoverBoton(true);
        else if (Input.GetKeyDown(KeyCode.RightArrow)) MoverBoton(false);
        else if (Input.GetKeyDown(KeyCode.Return))
        {

            buttons[currentButton].onClick.Invoke();
            currentButton = 1;
            buttons[currentButton].Select();
        }
    }


}
