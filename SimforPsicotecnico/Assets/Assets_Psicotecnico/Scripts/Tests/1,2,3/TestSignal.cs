using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Logica de las se�ales
public class TestSignal : MonoBehaviour
{
    [SerializeField] public TypeImput correctInput;
    public float time;
    protected float timer = 0;
    [HideInInspector] public TestSignalsManager manager;
    public bool failIfIgnore;
    [HideInInspector] public TypeImput lastInput;
    bool fallada;

   

    private void Update()
    {
        timer += Time.deltaTime;
        TypeImput currentInput = GodOfInputs.instance.GetAnyInput();
        if (currentInput != TypeImput.none)
        {
            if(lastInput != currentInput)
            {
                print(lastInput + " / " + currentInput);
                lastInput = currentInput;
                InputRecived();
            }
        }

        if (time > 0 && timer > time) TimeEnd();
    }

    void InputRecived()
    {
        if (GodOfInputs.instance.GetInput(correctInput))
        {            
            manager.aciertos++;
            manager.tiempos.Add(timer);
            manager.SignalComplete();
            Destroy(this.gameObject);
        }
        else
        {
            if (fallada) return;
            print("Fallo");
            manager.fallos++;
            fallada = true;
        }

    }

    void TimeEnd()
    {
        if (failIfIgnore) manager.fallos++;
        else ++manager.aciertos;
        manager.SignalComplete();
        Destroy(this.gameObject);
    }
}
