using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


//Gestor de logicas generales de los test 1, 2 y 3
public class TestSignalsManager : MonoBehaviour
{
    [SerializeField] public TestSignal[] signals;
    public int fallos, aciertos;
    [HideInInspector] public List<float> tiempos = new List<float>();
    protected float timeScreen;

    protected int repetitions;

    protected float reaccionMedia;
    protected int signal = 1;

    protected int maxFallos;
    protected float minReaccionMedia;
    protected bool apto =  false;
    float startTime;
    int lastSignal;
    Result123 resultados;

    [SerializeField] int tipo;

    [SerializeField] bool demo;
    [SerializeField] DemoTests demoManager;

    RandomizadorSignals randomizador;


    public void StartTest()
    {
        print(demo);
        if (!demo) StartTestReal();
        else demoManager.StartDemo(this);

        randomizador = new RandomizadorSignals(signals.Length);
    }

    public void StartTestReal()
    {
        resultados = new Result123();
        startTime = Time.time;
        NextSignal();
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
    }
   
    public virtual void SignalComplete()
    {
        Invoke("NextSignal", 0.1f);
    }
    public virtual void NextSignal()
    {
        if (repetitions <= 0)
        {
            EndTest();
            return;
        }

        signal = randomizador.Next();

        GameObject currentSignalObj = Instantiate(signals[signal].gameObject, transform);
        TestSignal currentSignal = currentSignalObj.GetComponent<TestSignal>();
        currentSignal.lastInput = signals[lastSignal].correctInput;
        currentSignal.manager = this;
        currentSignal.time = timeScreen;
        --repetitions;

        lastSignal = signal;
    }

    public void EndTest()
    {
        float reaccionesSumadas = 0;
        foreach (float f in tiempos) reaccionesSumadas += f;
        reaccionMedia = reaccionesSumadas / tiempos.Count;
        if (fallos <= maxFallos && reaccionMedia <= minReaccionMedia) apto = true;
        print("Aciertos: " + aciertos + "  /  Fallos: " + fallos + "  /  ReaccionMedia: " + reaccionMedia + "  /  Apto: " + apto);
        resultados.aciertos = aciertos;
        resultados.apto = apto;
        resultados.fallos = fallos;
        resultados.reactionTime = reaccionMedia;
        resultados.time = Time.time - startTime;
        AuxiliarBuild.instance.tipo.Add( tipo);
        AuxiliarBuild.instance.resultados.Add(resultados);
        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.UploadAlumno();
        FPrepararSubida();
    }
    public void FPrepararSubida()
    {
        resultados.FSubirSession();
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
    //DEPRECATED
    /*
    IEnumerator PrepararSubida()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());    
            }
            else
            {
                string s = "";
                bool cambio = false;
                for(int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if(!cambio && www.downloadHandler.text[i] == ':')
                    {
                        cambio = true;
                    }
                    else if (cambio)
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateEnd = s;
                yield return  resultados.SubirSesion();
            }
        }
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }*/
    //DEPRECATED
    /*IEnumerator Start()
    {
        
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                bool cambio = false;
                string s = "";
                for(int i = 0;  i < www.downloadHandler.text.Length; ++i)
                {
                    if(!cambio)
                    {
                        if (www.downloadHandler.text[i] == ':')
                        {
                            cambio = true;
                            resultados.fecha = s;
                            s = "";
                        }
                        else
                        {
                            s += www.downloadHandler.text[i];
                        }                   
                    }
                    else
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateInit = s;
                
            }
        }
    }*/
}

public class RandomizadorSignals
{
    List<int> disponibles = new List<int>();
    List<int> usadas = new List<int>();

    int lastValue = -1;

    public RandomizadorSignals(int size)
    {
        for (int i = 0; i < size; i++)
        {
            disponibles.Add(i);
        }
    }

    public int Next()
    {
        int value = -1;
        do
        {
            value = disponibles[Random.Range(0, disponibles.Count)];
        } while (value == lastValue);

        lastValue = value;

        disponibles.Remove(value);
        usadas.Add(value);

        if (disponibles.Count == 0) Reset();

        return value;
    }

    void Reset()
    {
        disponibles = usadas;
        usadas = new List<int>();
    }
}
