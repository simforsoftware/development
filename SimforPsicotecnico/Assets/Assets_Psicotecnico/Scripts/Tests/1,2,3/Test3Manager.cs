using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Gestor de logicas del test 3
public class Test3Manager : TestSignalsManager
{
    float timeGreen, timeRed;
    [SerializeField] InfoTest3 info;
    [SerializeField] Text demoText;

    int demoFase = 0;

    private void Start()
    {
        repetitions = info.repeticiones;
        ++repetitions; //demo;
        timeGreen = info.timePerSignal1;
        timeRed = info.timePerSignal2;
        maxFallos = info.maxFallos;
        minReaccionMedia = info.minTimepoReaccion;
        repetitions *= 2;
        demoText.text = Localizador.Texto("loc_105");
    }



    public override void NextSignal()
    {
        if (repetitions <= 0)
        {
            EndTest();
            return;
        }

        if (signal == 0)
        {
            signal = 1;
            timeScreen = timeRed;
        }
        else
        {
            signal = 0;
            timeScreen = timeGreen;
        }

        switch (demoFase)
        {
            case 0:
                ++demoFase;
                break;
            case 1:
                timeScreen = 300;
                demoText.text = Localizador.Texto("loc_98");
                ++demoFase;
                break;
            case 2:
                fallos = 0;
                demoText.text = Localizador.Texto("loc_104");
                ++demoFase;
                Invoke("CleanText", 3f);
                break;            
            default:
                break;
        }

        GameObject currentSignalObj = Instantiate(signals[signal].gameObject, transform);
        TestSignal currentSignal = currentSignalObj.GetComponent<TestSignal>();
        currentSignal.manager = this;
        currentSignal.time = timeScreen;
        --repetitions;       
    }

    void CleanText()
    {
        demoText.text = "";
    }
}
