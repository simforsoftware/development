using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Logica de la se�al de semaforo verde
public class TestSignalGreen : TestSignal
{
    bool active = false;
    Image sprite;
    Sprite original;
    [SerializeField] Sprite arranca;

    private void Start()
    {
        sprite = GetComponent<Image>();
        original = sprite.sprite;
        sprite.enabled = true;
        active = true;
    }


    private void Update()
    {
        if (GodOfInputs.instance.GetInput(correctInput) && !active)
        {
            active = true;
            timer = 0;
            sprite.sprite = original;
        }
        else if (!GodOfInputs.instance.GetInput(correctInput) && active)
        {
            active = false;
            sprite.sprite = arranca;
        }
        if (active)
        {
            timer += Time.deltaTime;
            if(timer >= time)
            {
                manager.SignalComplete();
                Destroy(this.gameObject);
            }
        }
    }
}
