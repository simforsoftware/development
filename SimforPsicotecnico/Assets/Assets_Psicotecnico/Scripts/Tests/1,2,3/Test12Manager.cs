using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Gestor de logicas de los test 1 y 2
public class Test12Manager : TestSignalsManager
{
    [SerializeField] InfoTest12 info;
    private void Awake()
    {
        UnityEngine.XR.XRSettings.enabled = false;
    }
    private void Start()
    {
        UnityEngine.XR.XRSettings.enabled = false;
        repetitions = info.repeticiones;
        maxFallos = info.maxFallos;
        minReaccionMedia = info.minTimepoReaccion;
        timeScreen = info.timePerSignal;
    }
}
