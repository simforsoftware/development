using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreadorCaminos : MonoBehaviour
{
    [SerializeField] GameObject coche;
    [SerializeField] float speedRoad;
    [SerializeField] GameObject tile;
    [SerializeField] float turnSpeed;
    [SerializeField] int maxDeviation;
    [SerializeField] float aceleration;

    [SerializeField] bool image;

    [SerializeField] Sprite tile1, tile2;

    List<GameObject> tiles = new List<GameObject>();

    float deviation, deviationTarget;

    Transform lastTile;

    bool start;

    public bool ready;


    void CreateTile()
    {
        GameObject g;
        if (tiles.Count > 0)
        {
            g = tiles[0];
            tiles.Remove(g);
            if (!ready) ready = true;
        }
        else
        {
            g = Instantiate(tile);
        }
        if (!start) deviation = 0;
        if(lastTile == null) g.transform.position = transform.position + new Vector3(deviation, 0, 0);
        else g.transform.position = new Vector3(transform.position.x + deviation, lastTile.position.y + tile.transform.localScale.y, transform.position.z);
        g.GetComponent<Tile>().speed = speedRoad;
        lastTile = g.transform;
    }

    private void Start()
    {
        print(tile.transform.localScale.y);
        print(speedRoad);
        print((1 / speedRoad) * tile.transform.localScale.y);
        InvokeRepeating("CreateTile", 0, (1/speedRoad) * tile.transform.localScale.y);
    }

    public void StartTest()
    {
        start = true;
        InvokeRepeating("SetDeviation", 0, 3f);
    }

    float speed = 0;

    private void Update()
    {
        float ace = aceleration;

        if (deviation > deviationTarget) ace = -aceleration;
        if (Mathf.Abs(deviation - deviationTarget) < 0.5) ace *= -20;
        speed += ace * Time.deltaTime;
        speed = Mathf.Clamp(speed, -turnSpeed, turnSpeed);

        deviation += speed * Time.deltaTime;
        deviation = Mathf.Clamp(deviation, -maxDeviation, maxDeviation);
        

    }

    void SetDeviation()
    {
        float newDeviation = 1;
        int counter = 0;
        do
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    newDeviation = -maxDeviation;
                    break;
                case 1:
                    newDeviation = 0;
                    break;
                case 2:
                    newDeviation = maxDeviation;
                    break;
            }
            ++counter;
        } while (newDeviation == deviationTarget && counter < 1000);

        deviationTarget = newDeviation;
    }

    public void SaveTile(GameObject g)
    {
        tiles.Add(g);
    }
}