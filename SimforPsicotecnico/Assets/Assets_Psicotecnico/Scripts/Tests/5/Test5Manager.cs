using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Test5Manager : MonoBehaviour
{
    [SerializeField] CreadorCaminos caminoDerecha, caminoIzquierda;
    [SerializeField] Coche5 cocheDerecha, cocheIzquierda;
    [SerializeField] GameObject botonInicio;
    Result5 resultados;

    bool start, falloDerecha, falloIzquierda;
    float timerDerecha, timerIzquierda;

    int fallosMaximos;
    float tiempoReaccionMaximo, tiempoTest;

    [SerializeField] InfoTest5 info;
    [SerializeField] GameObject pantalla;
    int fallos;
    List<float> tiemposFallo = new List<float>();
    bool apto;

    bool terminado;
    private void Start()
    {
        botonInicio.transform.GetChild(0).GetComponent<DinamicText>().SetTextByID("0069");
    }
    void EnableBoton()
    {
        if (botonInicio.GetComponent<Button>().enabled || start) return;
        botonInicio.GetComponent<Button>().enabled = true;
        botonInicio.transform.GetChild(0).GetComponent<DinamicText>().SetTextByID("0061");
    }

    public void StartTest()
    {
        fallosMaximos = info.fallos;
        tiempoReaccionMaximo = info.tiempoReaccion;
        resultados = new Result5();
        terminado = false;
        resultados.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultados.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        pantalla.SetActive(false);
        caminoDerecha.StartTest();
        caminoIzquierda.StartTest();
        cocheDerecha.start = true;
        cocheIzquierda.start = true;
        tiempoTest = info.tiempo;
        start = true;
    }

    private void Update()
    {
        if (caminoDerecha.ready && caminoIzquierda.ready) EnableBoton();

        if (!start) return;

        tiempoTest -= Time.deltaTime;

        if(!falloDerecha && !cocheDerecha.dentro)
        {
            ++fallos;
            falloDerecha = true;
            timerDerecha = 0;
        }

        if (falloDerecha)
        {
            if (!cocheDerecha.dentro)
            {
                timerDerecha += Time.deltaTime;
            }
            if (cocheDerecha.dentro)
            {
                tiemposFallo.Add(timerDerecha);
                falloDerecha = false;
            }
        }

        if (!falloIzquierda && !cocheIzquierda.dentro)
        {
            ++fallos;
            falloIzquierda = true;
            timerIzquierda = 0;
        }

        if (falloIzquierda)
        {
            if (!cocheIzquierda.dentro)
            {
                timerIzquierda += Time.deltaTime;
            }
            if (cocheIzquierda.dentro)
            {
                tiemposFallo.Add(timerIzquierda);
                falloIzquierda = false;
            }
        }
        
        if (tiempoTest <= 0 && !terminado)
        {
            terminado = true;
            EndTest();
        }
    }
    //DEPRECATED
    /*
    IEnumerator cStart()
    {
        fallosMaximos = info.fallos;
        tiempoReaccionMaximo = info.tiempoReaccion;
        resultados = new Result5();

        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                bool cambio = false;
                string s = "";
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio)
                    {
                        if (www.downloadHandler.text[i] == ':')
                        {
                            cambio = true;
                            resultados.fecha = s;
                            s = "";
                        }
                        else
                        {
                            s += www.downloadHandler.text[i];
                        }
                    }
                    else
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateInit = s;
            }
        }
    }*/
    public void FPrepararSubida()
    {
        resultados.FSubirSesion();
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }

    //DEPRECATED
    /*
    IEnumerator PrepararSubida()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/serverConnect/getDate.php"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.result.ToString());
            }
            else
            {
                string s = "";
                bool cambio = false;
                for (int i = 0; i < www.downloadHandler.text.Length; ++i)
                {
                    if (!cambio && www.downloadHandler.text[i] == ':')
                    {
                        cambio = true;
                    }
                    else if (cambio)
                    {
                        s += www.downloadHandler.text[i];
                    }
                }
                resultados.dateEnd = s;
                yield return resultados.SubirSesion();
            }
        }
        Debug.Log("salgo");
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }*/

    void EndTest()
    {
        float tiempoMedioFallos = 0;
        foreach (float f in tiemposFallo) tiempoMedioFallos += f;
        if(tiemposFallo.Count != 0 )tiempoMedioFallos /= tiemposFallo.Count;

        if (fallos <= fallosMaximos && tiempoMedioFallos <= tiempoReaccionMaximo) apto = true;

        resultados.apto = apto;
        resultados.fallos = fallos;
        resultados.reactionTime = tiempoMedioFallos;
        resultados.time = info.tiempo;
        Debug.Log("MAMA MIA " + tiempoMedioFallos);
        AuxiliarBuild.instance.tipo.Add( 5);
        AuxiliarBuild.instance.resultados.Add(resultados);

        print("Fallos: " + fallos + " / Tiempo de Reaccion:" + tiempoMedioFallos + " / Apto: " + apto);

        resultados.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultados.UploadAlumno();
        FPrepararSubida();
    }
}
