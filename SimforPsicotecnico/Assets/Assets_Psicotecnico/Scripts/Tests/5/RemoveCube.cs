using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveCube : MonoBehaviour
{
    [SerializeField] CreadorCaminos manager;
    
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("tile")) manager.SaveTile(other.gameObject);
    }
}
