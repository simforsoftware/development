using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComprobacionCocheCamino : MonoBehaviour
{    public bool Comprobacion()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (!Physics.Raycast(child.position, Vector3.forward, 100f)) return false;
        }
        return true;
    }   
}
