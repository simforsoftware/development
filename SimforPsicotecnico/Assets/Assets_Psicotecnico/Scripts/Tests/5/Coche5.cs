using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche5 : MonoBehaviour
{
    public enum Carril
    {
        derecho,
        izquierdo
    }

    [SerializeField] float speed;
    [SerializeField] float bounds;
    public Carril carril;

    public bool dentro, start;


    [HideInInspector] public bool active = true;

    public bool sonando;

    float referencia;
    string eje;
    bool derecha;
    KeyCode positiva, negativa;
    private void Start()
    {
        switch (carril)
        {
            case Carril.derecho:
                positiva = KeyCode.RightArrow;
                negativa = KeyCode.LeftArrow;
                break;
            case Carril.izquierdo:
                positiva = KeyCode.D;
                negativa = KeyCode.A;
                break;
        }

        referencia = transform.position.x;
    }

    private void Update()
    {

        if (Input.GetKey(positiva) && transform.position.x > referencia + bounds) return;
        if (Input.GetKey(negativa) && transform.position.x < referencia - bounds) return;

        dentro = GetComponentInChildren<ComprobacionCocheCamino>().Comprobacion();
    
        if (!start) return;

        Vector3 desplazamiento = Vector3.zero;
        if (Input.GetKey(positiva) || Input.GetKey(negativa)) desplazamiento = new Vector3(speed * Time.deltaTime, 0, 0);
        if (Input.GetKey(negativa)) desplazamiento *= -1;
        transform.position += desplazamiento;

        if (!sonando && !dentro) { GetComponent<AudioSource>().Play(); sonando = true; }
        else if (sonando && dentro) { GetComponent<AudioSource>().Stop(); sonando = false; }
    }
}


