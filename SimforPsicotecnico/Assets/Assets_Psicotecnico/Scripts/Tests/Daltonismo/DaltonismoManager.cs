using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaltonismoManager : MonoBehaviour
{
    [SerializeField] SpriteRenderer circulo;
    [SerializeField] int iteraciones;

    int aciertos, fallos;

    Result7 resultado7;

    public void StartTest()
    {
        resultado7 = new Result7();
        resultado7.fecha = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yyyy");
        resultado7.dateInit = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        CambiarColor(Color.white);
    }

    void CambiarColor(Color lastColor)
    {
        --iteraciones;
        if (iteraciones < 0)
        {
            EndTest();
            return;
        }
        Color thisColor = Color.white;
        do
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    thisColor = Color.red;
                    break;
                case 1:
                    thisColor = Color.green;
                    break;
                case 2:
                    thisColor = Color.blue;
                    break;
            }

        } while (thisColor == lastColor);
        circulo.color = thisColor;
    }

    public void Responder(Color color)
    {
        if (color == circulo.color) ++aciertos;
        else ++fallos;

        CambiarColor(circulo.color);
    }

    public void EndTest()
    {
        resultado7.linea = fallos;
        resultado7.apto = fallos < 1;
        resultado7.nombreEjrcicio = ScenesSelector.GetCurrentScene();
        resultado7.UploadAlumno();
        resultado7.IshiharaFSubir();
        AuxiliarBuild.instance.tipo.Add(15);
        AuxiliarBuild.instance.resultados.Add(resultado7);
        ScenesSelector.GoToScene(Multisesion.SiguienteEjercicio());
    }
}
