using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaltonismoButton : MonoBehaviour
{
    [SerializeField] Color color;
    [SerializeField] DaltonismoManager manager;

    public void Responder()
    {
        manager.Responder(color);
    }
}
