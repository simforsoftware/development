using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controlador de la interfaz descriptiva del test
public class DescripcionTest123 : MonoBehaviour
{
    [SerializeField] TestSignalsManager manager;

    public bool sistemaComplejoActivo = false;

    List<TypeImput> inputs = new List<TypeImput>();

    private void Start()
    {
        if (!sistemaComplejoActivo) return;
        foreach(TestSignal signal in manager.signals)
        {
            if (signal.correctInput != TypeImput.none && !inputs.Contains(signal.correctInput)) inputs.Add(signal.correctInput);
        }
    }

    private void Update()
    {
        if (!sistemaComplejoActivo) return;
        foreach (TypeImput input in inputs)
        {
            if (GodOfInputs.instance.GetInput(input))
            {
                inputs.Remove(input);
                break;
            }
        }
        if (inputs.Count <= 0) StartTest();
    }

    public void StartTest()
    {
        if(manager) manager.StartTest();
        gameObject.SetActive(false);
    }
}
