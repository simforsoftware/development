<?php

$servername = "localhost:3307";
$username = "root";
$password = "root";
$BDname = "simforpsi";


$conn = new mysqli($servername, $username, $password, $BDname);


if(mysqli_connect_errno())
{
    echo "1: Error en la conexion con el servidor";
    exit();
}

$nuevaContraseña = $_POST["contraseña"];
$loginID = $_POST["id"];

$salt = "\$5\$rounds=5000\$" . "hamburguer" . $nuevaContraseña . "\$";
$hash = crypt($nuevaContraseña, $salt);

$cambioContraseñaQuery = "UPDATE users SET hash=? WHERE id=" . $loginID .";";
$stmt = mysqli_prepare($conn, $cambioContraseñaQuery);
mysqli_stmt_bind_param($stmt, 's', $hash);
mysqli_stmt_execute($stmt) or die("25: cambio hash Query failed");
mysqli_stmt_close($stmt);

$cambioSaltQuery = "UPDATE users SET salt=? WHERE id=" . $loginID .";";
$stmt1 = mysqli_prepare($conn, $cambioSaltQuery);
mysqli_stmt_bind_param($stmt1, 's', $salt);
mysqli_stmt_execute($stmt1) or die("26: cambio salt Query failed");
mysqli_stmt_close($stmt1);

echo "0: ALL GOOD";
exit();

?>