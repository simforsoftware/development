-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Feb 24, 2022 at 08:46 AM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simforpsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE `grupos` (
  `grupo` int(10) NOT NULL,
  `idInstructor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grupos`
--

INSERT INTO `grupos` (`grupo`, `idInstructor`) VALUES
(6, 15),
(7, 19),
(8, 21),
(9, 23);

-- --------------------------------------------------------

--
-- Table structure for table `resultados4`
--

CREATE TABLE `resultados4` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `aciertos` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resultados4`
--

INSERT INTO `resultados4` (`id`, `idSession`, `aciertos`) VALUES
(1, 18, 2),
(2, 19, 4),
(3, 47, 4),
(4, 53, 4),
(5, 57, 4),
(6, 71, 4),
(7, 82, 4),
(8, 151, 4),
(9, 156, 4),
(10, 161, 4),
(11, 163, 4),
(12, 165, 4),
(13, 170, 4),
(14, 177, 4),
(15, 185, 3),
(16, 193, 4);

-- --------------------------------------------------------

--
-- Table structure for table `resultados5`
--

CREATE TABLE `resultados5` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `tiempoReaccion` varchar(20) NOT NULL,
  `fallos` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resultados5`
--

INSERT INTO `resultados5` (`id`, `idSession`, `tiempoReaccion`, `fallos`) VALUES
(4, 23, '0.04', 1),
(5, 44, '0,00', 0),
(6, 45, '0,00', 0),
(7, 46, '0,00', 0),
(8, 48, '0,68', 1),
(9, 58, '0,00', 0),
(10, 72, '0,00', 0),
(11, 83, '2,67', 1),
(12, 113, '0,24', 2),
(13, 114, '1,22', 2),
(14, 152, '0,00', 0),
(15, 157, '0,00', 0),
(16, 162, '0,00', 0),
(17, 171, '0,00', 0),
(18, 172, '0,00', 0),
(19, 173, '0,00', 0),
(20, 178, '5,48', 9),
(21, 186, '0,07', 1),
(22, 194, '2,67', 1),
(23, 196, '11,68', 1);

-- --------------------------------------------------------

--
-- Table structure for table `resultados6`
--

CREATE TABLE `resultados6` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `aciertos` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resultados7`
--

CREATE TABLE `resultados7` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `linea` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resultados7`
--

INSERT INTO `resultados7` (`id`, `idSession`, `linea`) VALUES
(3, 105, 7),
(4, 106, 8),
(5, 180, 2),
(6, 188, 7);

-- --------------------------------------------------------

--
-- Table structure for table `resultados8`
--

CREATE TABLE `resultados8` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `iz250` varchar(10) NOT NULL,
  `iz500` varchar(10) NOT NULL,
  `iz1000` varchar(10) NOT NULL,
  `iz2000` varchar(10) NOT NULL,
  `iz4000` varchar(10) NOT NULL,
  `iz8000` varchar(10) NOT NULL,
  `de250` varchar(10) NOT NULL,
  `de500` varchar(10) NOT NULL,
  `de1000` varchar(10) NOT NULL,
  `de2000` varchar(10) NOT NULL,
  `de4000` varchar(10) NOT NULL,
  `de8000` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resultados8`
--

INSERT INTO `resultados8` (`id`, `idSession`, `iz250`, `iz500`, `iz1000`, `iz2000`, `iz4000`, `iz8000`, `de250`, `de500`, `de1000`, `de2000`, `de4000`, `de8000`) VALUES
(1, 122, '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1'),
(2, 181, '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1', '0,1'),
(3, 189, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `resultados123`
--

CREATE TABLE `resultados123` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `tiempoTotal` varchar(20) NOT NULL,
  `tiempoReaccion` varchar(20) NOT NULL,
  `aciertos` int(10) NOT NULL,
  `fallos` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resultados123`
--

INSERT INTO `resultados123` (`id`, `idSession`, `tiempoTotal`, `tiempoReaccion`, `aciertos`, `fallos`) VALUES
(7, 12, '41,02', '0,59', 60, 12),
(8, 13, '43,57', '0,64', 60, 13),
(9, 14, '38,19', '0,54', 60, 3),
(10, 15, '37,71', '0,54', 60, 1),
(11, 16, '46,14', '0,59', 58, 4),
(12, 17, '37,80', '0,25', 10, 0),
(13, 50, '67,85', '1,00', 59, 4),
(14, 51, '71,00', '0,75', 51, 1),
(15, 52, '43,18', '0,52', 10, 0),
(16, 55, '39,72', '0,57', 60, 0),
(17, 56, '37,58', '0,31', 10, 0),
(18, 62, '27,56', '0,18', 56, 73),
(19, 63, '63,15', '0,96', 60, 1),
(20, 64, '34,57', '0,48', 60, 5),
(21, 65, '34,84', '0,48', 60, 0),
(22, 66, '32,05', '0,44', 60, 3),
(23, 68, '52,49', '0,78', 60, 1),
(24, 69, '80,09', '0,80', 48, 3),
(25, 70, '42,04', '0,50', 10, 0),
(26, 79, '57,83', '0,87', 60, 0),
(27, 80, '86,34', '0,79', 45, 2),
(28, 81, '52,88', '0,48', 10, 0),
(29, 141, '35,68', '0,50', 60, 3),
(30, 142, '63,83', '0,56', 50, 2),
(31, 143, '37,47', '0,53', 60, 1),
(32, 144, '72,75', '0,54', 46, 1),
(33, 145, '40,63', '0,37', 10, 0),
(34, 146, '37,59', '0,53', 60, 2),
(35, 147, '73,69', '0,56', 46, 1),
(36, 148, '36,11', '0,50', 60, 1),
(37, 149, '77,40', '0,53', 44, 0),
(38, 150, '37,39', '0,36', 10, 0),
(39, 153, '39,42', '0,57', 60, 6),
(40, 154, '63,58', '0,51', 49, 1),
(41, 155, '38,05', '0,34', 10, 0),
(42, 158, '35,75', '0,50', 60, 0),
(43, 159, '54,53', '0,52', 53, 0),
(44, 160, '38,57', '0,37', 10, 0),
(45, 164, '34,59', '0,49', 60, 2),
(46, 167, '33,30', '0,46', 60, 2),
(47, 168, '67,54', '0,49', 47, 0),
(48, 169, '35,86', '0,24', 10, 0),
(49, 174, '40,14', '0,58', 60, 13),
(50, 175, '40,92', '0,16', 51, 271),
(51, 176, '42,78', '0,47', 10, 0),
(52, 182, '46,53', '0,60', 58, 5),
(53, 183, '69,56', '0,63', 49, 6),
(54, 184, '53,50', '0,64', 7, 4),
(55, 190, '31,84', '0,44', 60, 2),
(56, 191, '76,48', '0,46', 43, 1),
(57, 192, '37,69', '0,39', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) NOT NULL,
  `idAlumno` int(10) NOT NULL,
  `grupo` int(10) NOT NULL,
  `fecha` varchar(40) NOT NULL,
  `fechaInit` varchar(25) NOT NULL,
  `fechaEnd` varchar(25) NOT NULL,
  `ejercicio` varchar(40) NOT NULL,
  `estado` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `idAlumno`, `grupo`, `fecha`, `fechaInit`, `fechaEnd`, `ejercicio`, `estado`) VALUES
(12, 22, 6, '2021-12-02 ', ' 11:48:14', ' 11:48:55', 'Test1', 'True'),
(13, 22, 6, '2021-12-03 ', ' 08:49:43', ' 08:50:27', 'Test1', 'True'),
(14, 22, 6, '2021-12-07 ', ' 08:49:28', ' 08:50:06', 'Test1', 'True'),
(15, 22, 6, '2021-12-07 ', ' 08:57:34', ' 08:58:12', 'Test1', 'True'),
(16, 23, 9, '2021-12-15 ', ' 10:28:35', ' 10:29:21', 'Test1', 'True'),
(17, 23, 9, '2021-12-15 ', ' 11:40:49', ' 11:41:27', 'Test3', 'True'),
(18, 24, 7, '2021-12-24 ', ' 10:52:21', ' 10:52:43', 'Test4', 'False'),
(19, 24, 7, '2021-12-24 ', ' 10:54:04', ' 10:54:25', 'Test4', 'True'),
(23, 24, 7, '2021-12-24 ', ' 11:27:42', ' 11:29:13', 'Test5', 'True'),
(44, 25, 6, '2022-01-27 ', ' 10:20:57', ' 10:22:06', 'Test5', 'True'),
(45, 25, 6, '2022-01-27 ', ' 10:25:41', ' 10:26:51', 'Test5', 'True'),
(46, 25, 6, '2022-01-27 ', ' 10:28:45', ' 10:29:56', 'Test5', 'True'),
(47, 25, 6, '2022-01-27 ', ' 10:30:04', ' 10:30:26', 'Test4', 'True'),
(48, 25, 6, '2022-01-27 ', ' 10:30:37', ' 10:31:47', 'Test5', 'True'),
(50, 25, 6, '2022-01-27 ', ' 11:48:18', ' 11:49:26', 'Test1', 'True'),
(51, 25, 6, '2022-01-27 ', ' 11:50:01', ' 11:51:12', 'Test2', 'True'),
(52, 25, 6, '2022-01-27 ', ' 11:51:58', ' 11:52:42', 'Test3', 'True'),
(53, 25, 6, '2022-01-27 ', ' 11:53:23', ' 11:53:44', 'Test4', 'True'),
(55, 27, 6, '2022-01-27 ', ' 12:16:56', ' 12:17:36', 'Test1', 'True'),
(56, 27, 6, '2022-01-27 ', ' 12:17:52', ' 12:18:30', 'Test3', 'True'),
(57, 27, 6, '2022-01-27 ', ' 12:18:57', ' 12:19:17', 'Test4', 'True'),
(58, 27, 6, '2022-01-27 ', ' 12:19:25', ' 12:20:36', 'Test5', 'True'),
(62, 27, 6, '2022-01-27 ', ' 12:40:34', ' 12:41:01', 'Test1', 'False'),
(63, 27, 6, '2022-01-27 ', ' 12:47:15', ' 12:48:18', 'Test1', 'True'),
(64, 27, 6, '2022-01-28 ', ' 08:05:27', ' 08:06:02', 'Test1', 'True'),
(65, 28, 6, '2022-01-28 ', ' 08:34:47', ' 08:35:22', 'Test1', 'True'),
(66, 28, 6, '2022-01-28 ', ' 08:36:33', ' 08:37:05', 'Test1', 'True'),
(68, 28, 6, '2022-01-28 ', ' 09:12:51', ' 09:13:44', 'Test1', 'True'),
(69, 28, 6, '2022-01-28 ', ' 09:14:03', ' 09:15:23', 'Test2', 'True'),
(70, 28, 6, '2022-01-28 ', ' 09:15:43', ' 09:16:25', 'Test3', 'True'),
(71, 28, 6, '2022-01-28 ', ' 09:16:52', ' 09:17:13', 'Test4', 'True'),
(72, 28, 6, '2022-01-28 ', ' 09:17:44', ' 09:18:58', 'Test5', 'True'),
(79, 29, 6, '2022-01-28 ', ' 10:51:27', ' 10:52:24', 'Test1', 'True'),
(80, 29, 6, '2022-01-28 ', ' 10:55:55', ' 10:57:21', 'Test2', 'True'),
(81, 29, 6, '2022-01-28 ', ' 10:59:22', ' 11:00:15', 'Test3', 'True'),
(82, 29, 6, '2022-01-28 ', ' 11:02:19', ' 11:02:40', 'Test4', 'True'),
(83, 29, 6, '2022-01-28 ', ' 11:03:32', ' 11:05:21', 'Test5', 'False'),
(96, 28, 6, '2022-02-01 ', ' 10:08:32', ' 10:09:24', 'Simulacion4', 'False'),
(106, 28, 6, '2022-02-02 ', ' 08:30:03', ' 08:30:17', 'Test7', 'False'),
(107, 28, 6, '2022-02-02 ', ' 09:05:20', ' 09:05:27', 'Test6', 'True'),
(108, 28, 6, '2022-02-02 ', ' 09:35:52', ' 09:39:14', 'Simulacion1', 'False'),
(109, 28, 6, '2022-02-02 ', ' 09:40:22', ' 09:41:42', 'Simulacion1', 'False'),
(110, 28, 6, '2022-02-02 ', ' 09:55:36', ' 09:57:00', 'Simulacion1', 'False'),
(111, 28, 6, '2022-02-03 ', ' 09:45:51', ' 09:47:29', 'Simulacion1', 'False'),
(112, 28, 6, '2022-02-03 ', ' 09:48:22', ' 09:49:58', 'Simulacion1', 'False'),
(113, 28, 6, '2022-02-04 ', ' 08:25:00', ' 08:26:12', 'Test5', 'True'),
(114, 28, 6, '2022-02-04 ', ' 08:27:36', ' 08:29:07', 'Test5', 'True'),
(115, 28, 6, '2022-02-07 ', ' 10:00:32', ' 10:01:04', 'Simulacion4', 'False'),
(116, 28, 6, '2022-02-07 ', ' 11:28:06', ' 11:28:42', 'Test8', 'False'),
(117, 28, 6, '2022-02-07 ', ' 11:39:00', ' 11:39:36', 'Test8', 'False'),
(118, 28, 6, '2022-02-07 ', ' 11:46:58', ' 11:47:34', 'Test8', 'False'),
(119, 28, 6, '2022-02-07 ', ' 12:10:06', ' 12:10:42', 'Test8', 'False'),
(120, 28, 6, '2022-02-07 ', ' 12:12:09', ' 12:12:45', 'Test8', 'False'),
(121, 28, 6, '2022-02-07 ', ' 12:17:44', ' 12:18:20', 'Test8', 'False'),
(122, 28, 6, '2022-02-07 ', ' 12:18:58', ' 12:19:35', 'Test8', 'False'),
(123, 28, 6, '2022-02-08 ', ' 08:51:11', ' 08:54:13', 'Simulacion2', 'False'),
(124, 28, 6, '2022-02-08 ', ' 08:54:22', ' 08:56:49', 'Simulacion3', 'False'),
(125, 28, 6, '2022-02-08 ', ' 08:57:06', ' 09:00:27', 'Simulacion3', 'False'),
(126, 28, 6, '2022-02-08 ', ' 09:03:10', ' 09:03:43', 'Simulacion4', 'False'),
(127, 28, 6, '2022-02-08 ', ' 09:03:55', ' 09:04:36', 'Simulacion4', 'False'),
(128, 28, 6, '2022-02-16 ', ' 09:35:10', ' 09:39:52', 'Simulacion3', 'False'),
(129, 28, 6, '2022-02-16 ', ' 10:04:39', ' 10:06:02', 'Simulacion3', 'False'),
(130, 28, 6, '2022-02-16 ', ' 10:06:02', ' 10:06:31', 'Simulacion4', 'False'),
(131, 28, 6, '2022-02-16 ', ' 10:06:31', ' 10:06:37', 'Test6', 'True'),
(132, 28, 6, '2022-02-16 ', ' 10:17:14', ' 10:18:38', 'Simulacion3', 'False'),
(133, 28, 6, '2022-02-16 ', ' 10:18:38', ' 10:19:08', 'Simulacion4', 'False'),
(134, 28, 6, '2022-02-16 ', ' 10:19:08', ' 10:19:16', 'Test6', 'True'),
(135, 28, 6, '2022-02-16 ', ' 10:21:18', ' 10:23:09', 'Simulacion3', 'False'),
(136, 28, 6, '2022-02-16 ', ' 10:23:09', ' 10:24:54', 'Simulacion4', 'False'),
(137, 28, 6, '2022-02-16 ', ' 10:24:54', ' 10:24:59', 'Test6', 'True'),
(138, 28, 6, '2022-02-16 ', ' 10:59:28', ' 11:03:26', 'Simulacion3', 'False'),
(139, 28, 6, '2022-02-16 ', ' 11:03:26', ' 11:03:53', 'Simulacion4', 'False'),
(140, 28, 6, '2022-02-16 ', ' 11:03:53', ' 11:03:58', 'Test6', 'True'),
(141, 28, 6, '2022-02-16 ', ' 12:04:35', ' 12:05:11', 'Test1', 'True'),
(142, 28, 6, '2022-02-16 ', ' 12:07:18', ' 12:08:22', 'Test2', 'True'),
(143, 28, 6, '2022-02-16 ', ' 12:10:07', ' 12:10:45', 'Test1', 'True'),
(144, 28, 6, '2022-02-17 ', ' 08:11:57', ' 08:13:10', 'Test2', 'True'),
(145, 28, 6, '2022-02-17 ', ' 08:15:37', ' 08:16:18', 'Test3', 'True'),
(146, 28, 6, '2022-02-17 ', ' 08:16:42', ' 08:17:19', 'Test1', 'True'),
(147, 28, 6, '2022-02-17 ', ' 08:17:22', ' 08:18:35', 'Test2', 'True'),
(148, 28, 6, '2022-02-17 ', ' 08:43:55', ' 08:44:32', 'Test1', 'True'),
(149, 28, 6, '2022-02-17 ', ' 08:44:36', ' 08:45:53', 'Test2', 'True'),
(150, 28, 6, '2022-02-17 ', ' 08:45:56', ' 08:46:33', 'Test3', 'True'),
(151, 28, 6, '2022-02-17 ', ' 08:46:35', ' 08:46:56', 'Test4', 'True'),
(152, 28, 6, '2022-02-17 ', ' 08:46:57', ' 08:48:06', 'Test5', 'True'),
(153, 28, 6, '2022-02-17 ', ' 08:51:47', ' 08:52:26', 'Test1', 'True'),
(154, 28, 6, '2022-02-17 ', ' 08:52:53', ' 08:53:57', 'Test2', 'True'),
(155, 28, 6, '2022-02-17 ', ' 08:54:10', ' 08:54:48', 'Test3', 'True'),
(156, 28, 6, '2022-02-17 ', ' 08:54:54', ' 08:55:15', 'Test4', 'True'),
(157, 28, 6, '2022-02-17 ', ' 08:55:15', ' 08:56:26', 'Test5', 'True'),
(158, 28, 6, '2022-02-17 ', ' 09:05:23', ' 09:05:59', 'Test1', 'True'),
(159, 28, 6, '2022-02-17 ', ' 09:06:02', ' 09:06:56', 'Test2', 'True'),
(160, 28, 6, '2022-02-17 ', ' 09:07:03', ' 09:07:42', 'Test3', 'True'),
(161, 28, 6, '2022-02-17 ', ' 09:07:44', ' 09:08:05', 'Test4', 'True'),
(162, 28, 6, '2022-02-17 ', ' 09:08:05', ' 09:09:15', 'Test5', 'True'),
(163, 28, 6, '2022-02-17 ', ' 09:10:57', ' 09:11:18', 'Test4', 'True'),
(164, 28, 6, '2022-02-17 ', ' 10:07:12', ' 10:07:47', 'Test1', 'True'),
(165, 0, 0, '2022-02-17 ', ' 10:25:38', ' 10:25:59', 'Test4', 'True'),
(166, 28, 6, '2022-02-17 ', ' 11:15:37', ' 11:16:56', 'Simulacion3', 'False'),
(167, 28, 6, '2022-02-18 ', ' 10:47:56', ' 10:48:29', 'Test1', 'True'),
(168, 28, 6, '2022-02-18 ', ' 10:48:32', ' 10:49:39', 'Test2', 'True'),
(169, 28, 6, '2022-02-18 ', ' 10:49:42', ' 10:50:18', 'Test3', 'True'),
(170, 28, 6, '2022-02-18 ', ' 10:50:24', ' 10:50:44', 'Test4', 'True'),
(171, 28, 6, '2022-02-18 ', ' 10:50:54', ' 10:51:54', 'Test5', 'True'),
(172, 28, 6, '2022-02-18 ', ' 10:50:54', ' 10:51:54', 'Test5', 'True'),
(173, 28, 6, '2022-02-18 ', ' 10:50:54', ' 10:51:54', 'Test5', 'True'),
(174, 28, 6, '2022-02-21 ', ' 11:06:02', ' 11:06:42', 'Test1', 'True'),
(175, 28, 6, '2022-02-21 ', ' 11:06:57', ' 11:07:38', 'Test2', 'False'),
(176, 28, 6, '2022-02-21 ', ' 11:07:42', ' 11:08:24', 'Test3', 'True'),
(177, 28, 6, '2022-02-21 ', ' 11:08:27', ' 11:08:48', 'Test4', 'True'),
(178, 28, 6, '2022-02-21 ', ' 11:09:08', ' 11:10:17', 'Test5', 'False'),
(179, 28, 6, '2022-02-21 ', ' 11:10:17', ' 11:15:00', 'Test6', 'True'),
(180, 28, 6, '2022-02-21 ', ' 11:15:00', ' 11:15:21', 'Test7', 'False'),
(181, 28, 6, '2022-02-21 ', ' 11:15:23', ' 11:15:59', 'Test8', 'False'),
(182, 30, 6, '2022-02-21 ', ' 12:07:07', ' 12:07:54', 'Test1', 'True'),
(183, 30, 6, '2022-02-21 ', ' 12:08:00', ' 12:09:10', 'Test2', 'True'),
(184, 30, 6, '2022-02-21 ', ' 12:09:12', ' 12:10:05', 'Test3', 'True'),
(185, 30, 6, '2022-02-21 ', ' 12:10:14', ' 12:10:31', 'Test4', 'False'),
(186, 30, 6, '2022-02-21 ', ' 12:12:14', ' 12:13:14', 'Test5', 'True'),
(187, 30, 6, '2022-02-21 ', ' 12:13:14', ' 12:13:21', 'Test6', 'True'),
(188, 30, 6, '2022-02-21 ', ' 12:13:21', ' 12:13:47', 'Test7', 'True'),
(189, 30, 6, '2022-02-21 ', ' 12:13:50', ' 12:17:26', 'Test8', 'False'),
(190, 28, 6, '2022-02-22 ', ' 10:06:52', ' 10:07:24', 'Test1', 'True'),
(191, 28, 6, '2022-02-22 ', ' 10:07:27', ' 10:08:44', 'Test2', 'True'),
(192, 28, 6, '2022-02-22 ', ' 10:08:46', ' 10:09:24', 'Test3', 'True'),
(193, 28, 6, '2022-02-22 ', ' 10:09:27', ' 10:09:47', 'Test4', 'True'),
(194, 28, 6, '2022-02-22 ', ' 10:09:59', ' 10:10:59', 'Test5', 'False'),
(195, 28, 6, '2022-02-22 ', ' 10:10:59', ' 10:11:05', 'Test6', 'True'),
(196, 28, 6, '2022-02-22 ', ' 10:11:28', ' 10:12:28', 'Test5', 'False'),
(197, 28, 6, '2022-02-22 ', ' 11:12:12', ' 11:12:46', 'Simulacion4', 'False'),
(198, 28, 6, '2022-02-22 ', ' 11:17:04', ' 11:17:33', 'Simulacion4', 'False'),
(199, 28, 6, '2022-02-22 ', ' 11:22:52', ' 11:23:22', 'Simulacion4', 'False'),
(200, 28, 6, '2022-02-22 ', ' 11:24:21', ' 11:24:48', 'Simulacion4', 'False'),
(201, 28, 6, '2022-02-22 ', ' 11:27:02', ' 11:27:52', 'Simulacion4', 'False'),
(202, 28, 6, '2022-02-22 ', ' 11:28:58', ' 11:29:25', 'Simulacion4', 'False'),
(203, 28, 6, '2022-02-22 ', ' 11:31:00', ' 11:31:44', 'Simulacion4', 'False'),
(204, 28, 6, '2022-02-22 ', ' 11:36:29', ' 11:36:55', 'Simulacion4', 'False'),
(205, 28, 6, '2022-02-22 ', ' 11:39:41', ' 11:40:16', 'Simulacion4', 'False'),
(206, 28, 6, '2022-02-22 ', ' 11:41:41', ' 11:42:11', 'Simulacion4', 'False'),
(207, 28, 6, '2022-02-22 ', ' 11:45:11', ' 11:45:46', 'Simulacion4', 'False'),
(208, 28, 6, '2022-02-22 ', ' 11:54:56', ' 11:55:25', 'Simulacion4', 'False'),
(209, 28, 6, '2022-02-22 ', ' 11:57:08', ' 11:57:36', 'Simulacion4', 'False'),
(210, 28, 6, '2022-02-23 ', ' 08:35:49', ' 08:38:05', 'Simulacion3', 'False'),
(211, 28, 6, '2022-02-23 ', ' 08:42:22', ' 08:44:17', 'Simulacion3', 'False'),
(212, 28, 6, '2022-02-23 ', ' 09:13:48', ' 09:22:05', 'Simulacion4', 'False'),
(213, 28, 6, '2022-02-23 ', ' 09:29:40', ' 09:30:09', 'Simulacion4', 'False'),
(214, 28, 6, '2022-02-23 ', ' 09:35:15', ' 09:35:46', 'Simulacion4', 'False'),
(215, 28, 6, '2022-02-23 ', ' 09:40:55', ' 09:41:25', 'Simulacion4', 'False'),
(216, 28, 6, '2022-02-23 ', ' 09:42:08', ' 09:42:37', 'Simulacion4', 'False'),
(217, 28, 6, '2022-02-23 ', ' 09:46:19', ' 09:46:55', 'Simulacion4', 'False'),
(218, 28, 6, '2022-02-23 ', ' 10:06:30', ' 10:07:15', 'Simulacion4', 'False'),
(219, 28, 6, '2022-02-23 ', ' 10:09:44', ' 10:10:14', 'Simulacion4', 'False'),
(220, 28, 6, '2022-02-23 ', ' 10:11:24', ' 10:12:20', 'Simulacion4', 'False'),
(221, 28, 6, '2022-02-23 ', ' 10:15:57', ' 10:16:24', 'Simulacion4', 'False'),
(222, 28, 6, '2022-02-23 ', ' 10:22:02', ' 10:22:36', 'Simulacion4', 'False'),
(223, 28, 6, '2022-02-23 ', ' 10:31:17', ' 10:31:45', 'Simulacion4', 'False'),
(224, 28, 6, '2022-02-23 ', ' 10:33:35', ' 10:34:09', 'Simulacion4', 'False'),
(225, 28, 6, '2022-02-23 ', ' 11:03:59', ' 11:04:28', 'Simulacion4', 'False'),
(226, 28, 6, '2022-02-23 ', ' 11:16:26', ' 11:18:19', 'Simulacion3', 'False'),
(227, 28, 6, '2022-02-23 ', ' 11:32:02', ' 11:32:32', 'Simulacion4', 'False'),
(228, 28, 6, '2022-02-23 ', ' 11:44:21', ' 11:44:47', 'Simulacion4', 'False'),
(229, 28, 6, '2022-02-23 ', ' 11:47:20', ' 11:47:50', 'Simulacion4', 'False'),
(230, 28, 6, '2022-02-23 ', ' 11:50:55', ' 11:51:57', 'Simulacion4', 'False'),
(231, 28, 6, '2022-02-23 ', ' 11:53:38', ' 11:54:06', 'Simulacion4', 'False'),
(232, 28, 6, '2022-02-23 ', ' 11:55:06', ' 11:55:36', 'Simulacion4', 'False'),
(233, 28, 6, '2022-02-23 ', ' 12:04:04', ' 12:04:31', 'Simulacion4', 'False');

-- --------------------------------------------------------

--
-- Table structure for table `simulaciones`
--

CREATE TABLE `simulaciones` (
  `id` int(10) NOT NULL,
  `idSession` int(10) NOT NULL,
  `duracion` varchar(10) NOT NULL,
  `choqueFallos` int(10) NOT NULL,
  `VelocidadInferior` int(10) NOT NULL,
  `VelocidadSuperior` int(10) NOT NULL,
  `SemaforoFallos` int(10) NOT NULL,
  `fueraDePista` int(10) NOT NULL,
  `HRV` varchar(1500) NOT NULL,
  `HRT` varchar(1500) NOT NULL,
  `HRM` int(10) NOT NULL,
  `PV` varchar(1500) NOT NULL,
  `PT` varchar(1500) NOT NULL,
  `CV` varchar(1500) NOT NULL,
  `CT` varchar(1500) NOT NULL,
  `ET` varchar(1500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `simulaciones`
--

INSERT INTO `simulaciones` (`id`, `idSession`, `duracion`, `choqueFallos`, `VelocidadInferior`, `VelocidadSuperior`, `SemaforoFallos`, `fueraDePista`, `HRV`, `HRT`, `HRM`, `PV`, `PT`, `CV`, `CT`, `ET`) VALUES
(4, 96, '52,24', 1, 4, 2, 0, 0, '', '', 0, '', '', '', '', ''),
(5, 108, '195,32', 0, 45, 28, 0, 5, '', '', 0, '', '', '', '', ''),
(6, 109, '77,04', 0, 15, 19, 0, 0, '', '', 0, '', '', '', '', ''),
(7, 110, '80,31', 0, 14, 17, 0, 1, '', '', 0, '', '', '', '', ''),
(8, 111, '91,44', 0, 18, 18, 0, 1, '', '', 0, '', '', '', '', ''),
(9, 112, '92,30', 0, 16, 15, 0, 1, '', '', 0, '', '', '', '', ''),
(10, 115, '32,75', 0, 4, 2, 0, 0, '', '', 0, '', '', '', '', ''),
(11, 210, '133,16', 0, 0, 0, 0, 1, '', '', 0, '', '', '', '', ''),
(12, 214, '30,55', 0, 5, 1, 0, 0, 'VACIO', '', 0, '', '', '', '', ''),
(13, 215, '30,00', 0, 5, 2, 0, 0, 'VACIO', 'VACIO', 0, '', '', '', '', ''),
(14, 216, '28,62', 0, 4, 1, 0, 0, 'VACIO', 'VACIO', 0, '', '', '', '', ''),
(15, 217, '35,34', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', '', '', ''),
(16, 220, '55,81', 1, 9, 3, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', ''),
(17, 221, '26,98', 0, 4, 1, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', ''),
(18, 222, '33,51', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', ''),
(19, 224, '33,65', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', 'VACIO'),
(20, 225, '29,28', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', 'VACIO'),
(21, 226, '111,56', 0, 0, 0, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', 'VACIO'),
(22, 227, '30,11', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', 'VACIO'),
(23, 228, '27,36', 0, 4, 1, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', 'VACIO'),
(24, 229, '30,42', 0, 4, 1, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', 'VACIO'),
(25, 230, '62,65', 0, 5, 1, 0, 1, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', '16:49:57'),
(26, 231, '28,81', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', '14:22'),
(27, 232, '31,08', 0, 4, 2, 0, 0, 'VACIO', 'VACIO', 0, 'VACIO', 'VACIO', 'VACIO', 'VACIO', '16:27'),
(28, 233, '27,73', 0, 5, 2, 0, 0, 'VACIO', 'VACIO', 1, 'VACIO', 'VACIO', 'VACIO', 'VACIO', '13:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `email` varchar(120) NOT NULL,
  `instructor` varchar(3) NOT NULL,
  `grupo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nombre`, `apellido`, `hash`, `salt`, `email`, `instructor`, `grupo`) VALUES
(15, 'i', 'i', '$5$rounds=5000$hamburgueri$snzy6Orr0hPoIaGSVVIrZV9i4RNxNZ6QYPgYFBtSCt3', '$5$rounds=5000$hamburgueri$', 'i', 'yes', 6),
(18, 'a', 'a', '$5$rounds=5000$hamburguera$2Ph868OZoBGxBiuehVTlkEMbdUnHt0P1ASzvVugX7n2', '$5$rounds=5000$hamburguera$', 'a', 'no', 6),
(19, 't', 't', '$5$rounds=5000$hamburguervvvvvv$5owYytizdPvLNTbIaoQ8a6XjQsxIGUexnHfXgivhrg6', '$5$rounds=5000$hamburguervvvvvv$', 't', 'yes', 7),
(20, 'f', 'f', '$5$rounds=5000$hamburguerf$rUcWXCqIQ.rVOceXx7czFUG4bTPQlp7OyWNF5K3YMU3', '$5$rounds=5000$hamburguerf$', 'f', 'no', 7),
(21, 'carlos', 'lantaron', '$5$rounds=5000$hamburguercarlos$VbWJDVFbb70w/mByy1m4LgwskvnFtONXfwyGm9./RN0', '$5$rounds=5000$hamburguercarlos$', 'carlos', 'yes', 8),
(22, 'w', 'w', '$5$rounds=5000$hamburguervvvvvv$5owYytizdPvLNTbIaoQ8a6XjQsxIGUexnHfXgivhrg6', '$5$rounds=5000$hamburguervvvvvv$', 'w', 'no', 6),
(23, 'l', 'l', '$5$rounds=5000$hamburguerl$1Ui2PzR/.WIajVVaJhibS6QwrS9ZOdvbrk5XeNc63q2', '$5$rounds=5000$hamburguerl$', 'l', 'yes', 9),
(24, 'c', 'c', '$5$rounds=5000$hamburguerc$gYB3YycetOicjLG6hJcDjTWEx4r5H3zEc0Kuf2Dy/GA', '$5$rounds=5000$hamburguerc$', 'c', 'no', 7),
(25, 'n', 'nn', '$5$rounds=5000$hamburguern$1uHuELsIvgcwP9bAPK3cGDDUXZe/ghcKEv4fMY1bDv8', '$5$rounds=5000$hamburguern$', 'n', 'no', 6),
(26, 'Pepito', 'Perez García', '$5$rounds=5000$hamburguerpepito$1814gjnNwHKFM3GFlcPNAcs3jZa1LAnL/bU9szcHcPD', '$5$rounds=5000$hamburguerpepito@simfor.es$', 'pepito@simfor.es', 'no', 8),
(27, 'juanito', 'perez', '$5$rounds=5000$hamburguerjuanit$qdQd3Mwc3uNRddCeIyU46FzG2k3PcsGrdxRpY6VV2FD', '$5$rounds=5000$hamburguerjuanito@simfor.es$', 'juanito@simfor.es', 'no', 6),
(28, 'ira', 'ira', '$5$rounds=5000$hamburguerira$npEOaWv19c/prpwKrY6VGaLul4Bc52lnb4o6DLgumW/', '$5$rounds=5000$hamburguerira$', 'ira', 'no', 6),
(29, 'Nacho', 'Huete', '$5$rounds=5000$hamburguernacho@$5H0fF2HJLw4lVpfFTuUb52Y81tcqUx.FXMrbT5XXe59', '$5$rounds=5000$hamburguernacho@simfor.es$', 'nacho@simfor.es', 'no', 6),
(30, 'R', 'G', '$5$rounds=5000$hamburguerrgarci$aWcoqrHkYtteM4NFJ15XVfqDlQ6mVG3Lt.1Bn42K/Y2', '$5$rounds=5000$hamburguerrgarcia@simfor.com$', 'rgarcia@simfor.com', 'no', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`grupo`),
  ADD UNIQUE KEY `idInstructor` (`idInstructor`);

--
-- Indexes for table `resultados4`
--
ALTER TABLE `resultados4`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `resultados5`
--
ALTER TABLE `resultados5`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `resultados6`
--
ALTER TABLE `resultados6`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `resultados7`
--
ALTER TABLE `resultados7`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `resultados8`
--
ALTER TABLE `resultados8`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `resultados123`
--
ALTER TABLE `resultados123`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simulaciones`
--
ALTER TABLE `simulaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSession` (`idSession`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `grupo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `resultados4`
--
ALTER TABLE `resultados4`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `resultados5`
--
ALTER TABLE `resultados5`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `resultados6`
--
ALTER TABLE `resultados6`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resultados7`
--
ALTER TABLE `resultados7`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `resultados8`
--
ALTER TABLE `resultados8`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `resultados123`
--
ALTER TABLE `resultados123`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `simulaciones`
--
ALTER TABLE `simulaciones`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
