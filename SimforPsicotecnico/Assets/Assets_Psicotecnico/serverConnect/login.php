<?php

$servername = "localhost:3307";
$username = "root";
$password = "root";
$BDname = "simforpsi";


$conn = new mysqli($servername, $username, $password, $BDname);


if(mysqli_connect_errno())
{
    echo "1: Error en la conexion con el servidor";
    exit();
}


$loginEmail = $_POST["email"];
$loginPassword = $_POST["password"];


//$emailCheckQuery = "SELECT email FROM users WHERE email='" . $loginEmail . "';";
$emailCheckQuery = "SELECT email, id, grupo FROM users WHERE email=?;";
$stmtEmail = mysqli_prepare($conn, $emailCheckQuery);

mysqli_stmt_bind_param($stmtEmail, 's', $loginEmail);
mysqli_stmt_execute($stmtEmail) or die("2: Email Query failed");
$emailCheck = $stmtEmail->get_result();
mysqli_stmt_close($stmtEmail);

//$emailCheck = mysqli_query($conn, $emailCheckQuery) or die ("2: Email Query failed");

if(mysqli_num_rows($emailCheck) != 1)
{
    echo "4: Either there is no email, or more than one";
    exit();
}

//$getpasswordQuery = "SELECT hash  FROM users WHERE email='" . $loginEmail . "';";
//$getsaltQuery = "SELECT salt  FROM users WHERE email='" . $loginEmail . "';";
$getpasswordQuery = "SELECT hash  FROM users WHERE email=?;";
$getsaltQuery     = "SELECT salt  FROM users WHERE email=?;";

$stmtPassword = mysqli_prepare($conn, $getpasswordQuery);
$stmtSalt     = mysqli_prepare($conn, $getsaltQuery);

mysqli_stmt_bind_param($stmtPassword, 's', $loginEmail);
mysqli_stmt_bind_param($stmtSalt, 's', $loginEmail);

mysqli_stmt_execute($stmtPassword) or die("5: password query failed");
$resultPassword = mysqli_stmt_get_result($stmtPassword);
mysqli_stmt_close($stmtPassword);

mysqli_stmt_execute($stmtSalt)     or die("6: salt query failed");
$resulSalt      = mysqli_stmt_get_result($stmtSalt);
mysqli_stmt_close($stmtSalt);

//$resultPassword = mysqli_query($conn, $getpasswordQuery) or die("5: password query failed");
//$resulSalt = mysqli_query($conn, $getsaltQuery) or die("6: salt query failed");

$infopassword = mysqli_fetch_assoc($resultPassword);
$infoSalt     = mysqli_fetch_assoc($resulSalt);

$salt = $infoSalt["salt"];
$hash = $infopassword["hash"];

$loginHash = crypt($loginPassword, $salt);

if($hash != $loginHash)
{
    echo "7: Incorrect password";
    exit();
}


$a = mysqli_fetch_array($emailCheck);

echo "0: ALL GOOD," ;
echo $a['id'];
echo",";
echo $a['grupo'];
exit();

?>