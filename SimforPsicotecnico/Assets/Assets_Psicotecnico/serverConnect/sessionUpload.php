<?php

$servername = "localhost:3307";
$username = "root";
$password = "root";
$BDname = "simforpsi";


$conn = new mysqli($servername, $username, $password, $BDname);


if(mysqli_connect_errno())
{
    echo "1: Error en la conexion con el servidor";
    exit();
}

$loginTipo = $_POST["tipo"];
$loginIdAlumno = $_POST["idAlumno"];
$loginIdGrupo = $_POST["grupo"];
$loginDateInit = $_POST["fechaInit"];
$loginDateEnd = $_POST["fechaEnd"];
$loginFecha = $_POST["fecha"];
$loginEjercicio = $_POST["ejercicio"];
$loginEstado = $_POST["estado"];
$insertSesionQuery = "INSERT INTO sessions (idAlumno, grupo, fecha, fechaInit, fechaEnd, ejercicio, estado) VALUES 
    (" . $loginIdAlumno. ", " . $loginIdGrupo . 
    ", '" . $loginFecha . "', '" . $loginDateInit . "', '" . $loginDateEnd . "', '" . $loginEjercicio . "', '" . $loginEstado . "');";

mysqli_query($conn, $insertSesionQuery) or die("8: Insert session query failed");

if($loginTipo == 123)
{
    $loginTiempoTotal = $_POST["tiempo"];
    $loginTiempoReaccion = $_POST["tiempoReaccion"];
    $loginAciertos = $_POST["aciertos"];
    $loginFallos = $_POST["fallos"];

    $getLastSession = "SELECT id FROM sessions ORDER BY id DESC LIMIT 1;";
    $getLastSessionResult = mysqli_query($conn, $getLastSession) or die("15: Get last session query failed");

    if(mysqli_num_rows($getLastSessionResult) == 0)
    {
        echo "16: no se han encontrado sessiones con el id";
        exit();
    }
    $row = mysqli_fetch_array($getLastSessionResult);
    $loginSessionId = $row['id'];

    $insertResultados123Query = "INSERT INTO resultados123 (idSession, tiempoTotal, tiempoReaccion, aciertos, fallos) VALUES 
    (" . $loginSessionId . ", '" . $loginTiempoTotal . "', '" . $loginTiempoReaccion . "', ". $loginAciertos . ", " . $loginFallos .")";

    mysqli_query($conn, $insertResultados123Query) or die("16: Insert result123 query failed");
}
else if($loginTipo == 5)
{
    $loginTiempoReaccion = $_POST["tiempoReaccion"];
    $loginFallos = $_POST["fallos"];

    $getLastSession = "SELECT id FROM sessions ORDER BY id DESC LIMIT 1;";
    $getLastSessionResult = mysqli_query($conn, $getLastSession) or die("15: Get last session query failed");

    if(mysqli_num_rows($getLastSessionResult) == 0)
    {
        echo "16: no se han encontrado sessiones con el id";
        exit();
    }
    $row = mysqli_fetch_array($getLastSessionResult);
    $loginSessionId = $row['id'];

    $insertResultados5Query = "INSERT INTO resultados5 (idSession, tiempoReaccion, fallos) VALUES 
    (" . $loginSessionId . ", '" . $loginTiempoReaccion . "', " . $loginFallos .")";

    mysqli_query($conn, $insertResultados5Query) or die("16: Insert resultados5 query failed");
}

else if($loginTipo == 4 )
{
    $loginAciertos = $_POST["aciertos"];

    $getLastSession = "SELECT id FROM sessions ORDER BY id DESC LIMIT 1;";
    $getLastSessionResult = mysqli_query($conn, $getLastSession) or die("15: Get last session query failed");

    if(mysqli_num_rows($getLastSessionResult) == 0)
    {
        echo "16: no se han encontrado sessiones con el id";
        exit();
    }
    $row = mysqli_fetch_array($getLastSessionResult);
    $loginSessionId = $row['id'];
    //PREGUNTAR A MIKEL LOS DATOS QUE SE RECOPILAN EN EL EJERCICIO DE TIPO 4
    $insertResultados4Query = "INSERT INTO resultados4 (idSession, aciertos) VALUES 
    (" . $loginSessionId . ", ". $loginAciertos . ")";

    mysqli_query($conn, $insertResultados4Query) or die("16: Insert result4 query failed");
}
else if($loginTipo == 7)
{
    $loginDuracion  = $_POST['duracion'];
    $loginChoques = $_POST['choqueFallos'];
    $loginVelocidadInfe = $_POST['velocidadInferior'];
    $loginVelocidadSup = $_POST['velocidadSuperior'];
    $loginSemaforoFallos = $_POST['semaforoFallos'];
    $loginfueraDePista = $_POST['fueraDePista'];

    $loginHRV = $_POST['HRV'];
    $loginHRT = $_POST['HRT'];
    $loginHRM = $_POST['HRM'];

    $loginPV = $_POST['PV'];
    $loginPT = $_POST['PT'];

    $loginCV = $_POST['CV'];
    $loginCT = $_POST['CT'];

    $loginET = $_POST['ET'];

    echo $loginHRV;
    echo "\n";
    echo $loginHRT;
    echo "\n";
    echo $loginHRM;
    echo "\n";
    echo $loginPV;
    echo "\n";
    echo $loginPT;
    echo "\n";
    echo $loginCV;
    echo "\n";
    echo $loginCT;
    echo "\n";
    echo $loginET;
    echo "\n";


    $getLastSession = "SELECT id FROM sessions ORDER BY id DESC LIMIT 1;";
    $getLastSessionResult = mysqli_query($conn, $getLastSession) or die("15: Get last session query failed");

    if(mysqli_num_rows($getLastSessionResult) == 0)
    {
        echo "16: no se han encontrado sessiones con el id";
        exit();
    }

    $row = mysqli_fetch_array($getLastSessionResult);
    $loginSessionId = $row['id'];

    /*$insertSimulacionResultados = "INSERT INTO simulaciones (idSession, duracion, choqueFallos, VelocidadInferior, VelocidadSuperior, SemaforoFallos, fueraDePista)
    VALUES (". $loginSessionId .", '". $loginDuracion ."', ". $loginChoques .", ". $loginVelocidadInfe .", ". $loginVelocidadSup .", ". $loginSemaforoFallos .", ". $loginfueraDePista .")";

    mysqli_query($conn, $insertSimulacionResultados) or die("17: Insert simulaciones query failed, f in the chat");*/

    
    $insertSimulacionResultados = "INSERT INTO simulaciones (idSession, duracion, choqueFallos, VelocidadInferior, VelocidadSuperior, SemaforoFallos, fueraDePista, HRV, HRT, HRM, PV, PT, CV, CT, ET)
    VALUES (". $loginSessionId .", '". $loginDuracion ."', ". $loginChoques .", ". $loginVelocidadInfe .", ". $loginVelocidadSup .", ". $loginSemaforoFallos .", ". $loginfueraDePista 
    .", '". $loginHRV ."', '" . $loginHRT . "', " . $loginHRM . ", '". $loginPV . "', '" . $loginPT . "', '" . $loginCV . "', '" . $loginCT . "', '". $loginET . "')";
    mysqli_query($conn, $insertSimulacionResultados) or die("17: Insert simulaciones query failed, f in the chat");
    
    /*$insertSimulacionResultados = "INSERT INTO simulaciones (idSession, duracion, choqueFallos, VelocidadInferior, VelocidadSuperior, SemaforoFallos, fueraDePista, HRV, HRT, HRM, PV, PT, PM, CV, CT, CM)
    VALUES (". $loginSessionId .", '". $loginDuracion ."', ". $loginChoques .", ". $loginVelocidadInfe .", ". $loginVelocidadSup .", ". $loginSemaforoFallos .", ". $loginfueraDePista 
    .", '". $loginHRV . "', '". $loginHRT . "', ". $loginHRM . ", '" . $loginPV . "', '" . $loginPT . "', " .$loginPM . ", '" . $loginCV . "', '" . $loginCT . "', " . $loginCM  .")";
    mysqli_query($conn, $insertSimulacionResultados) or die("17: Insert simulaciones query failed, f in the chat");*/
}

else if($loginTipo == 8)
{

    $loginDe250 = $_POST['de250'];
    $loginDe500 = $_POST['de500'];
    $loginDe1000 = $_POST['de1000'];
    $loginDe2000 = $_POST['de2000'];
    $loginDe4000 = $_POST['de4000'];
    $loginDe8000 = $_POST['de8000'];

    $loginIz250 = $_POST['iz250'];
    $loginIz500 = $_POST['iz500'];
    $loginIz1000 = $_POST['iz1000'];
    $loginIz2000 = $_POST['iz2000'];
    $loginIz4000 = $_POST['iz4000'];
    $loginIz8000 = $_POST['iz8000'];

    $getLastSession = "SELECT id FROM sessions ORDER BY id DESC LIMIT 1;";
    $getLastSessionResult = mysqli_query($conn, $getLastSession) or die("15: Get last session query failed");

    if(mysqli_num_rows($getLastSessionResult) == 0)
    {
        echo "16: no se han encontrado sessiones con el id";
        exit();
    }

    $row = mysqli_fetch_array($getLastSessionResult);
    $loginSessionId = $row['id'];


    /*echo $loginSessionId;
    echo ".";
    echo $loginIz250;
    echo ".";
    echo $loginIz500;
    echo ".";
    echo $loginIz1000;
    echo ".";
    echo $loginIz2000;
    echo ".";
    echo $loginIz4000;
    echo ".";
    echo $loginIz8000;

    echo $loginDe250;
    echo ".";
    echo $loginDe500;
    echo ".";
    echo $loginDe1000;
    echo ".";
    echo $loginDe2000;
    echo ".";
    echo $loginDe4000;
    echo ".";
    echo $loginDe8000;

    exit();*/


    $insertIntoResultados8 = "INSERT INTO resultados8 (idSession, iz250, iz500, iz1000, iz2000, iz4000, iz8000, de250, de500, de1000, de2000, de4000, de8000) 
    VALUES (". $loginSessionId .", '" . $loginIz250 . "', '" . $loginIz500 . "', '" . $loginIz1000 . "', '" . $loginIz2000 . "', '" . $loginIz4000 . "', '" . $loginIz8000 . "', '" 
    . $loginDe250 . "', '" . $loginDe500 . "', '" . $loginDe1000 . "', '" . $loginDe2000 . "', '" . $loginDe4000 . "', '" . $loginDe8000 . "')";
   
    mysqli_query($conn, $insertIntoResultados8) or die("19: Insert resultados8 query failed, f in the chat");
}

else if($loginTipo == 6)
{

    $loginlinea = $_POST['linea'];

    echo $loginlinea;

    $getLastSession = "SELECT id FROM sessions ORDER BY id DESC LIMIT 1;";
    $getLastSessionResult = mysqli_query($conn, $getLastSession) or die("15: Get last session query failed");

    if(mysqli_num_rows($getLastSessionResult) == 0)
    {
        echo "16: no se han encontrado sessiones con el id";
        exit();
    }

    $row = mysqli_fetch_array($getLastSessionResult);
    $loginSessionId = $row['id'];
    echo ",";
    echo $loginSessionId;
    $insertResultados7 = "INSERT INTO resultados7 (idSession, linea)
    VALUES (" . $loginSessionId . ", ". $loginlinea . ")";
    mysqli_query($conn, $insertResultados7) or die("18: Insert resultados7 query failed, f in the chat");

    $insertResultados4Query = "INSERT INTO resultados4 (idSession, aciertos) VALUES 
    (" . $loginSessionId . ", ". $loginAciertos . ")";
}

echo "0: ALL GOOD";
exit();

?>