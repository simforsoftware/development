using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class CryptoTester : MonoBehaviour
{
    [SerializeField] string testString;

    private void Start()
    {
        string original = testString;
        string encriptada = Encriptador.Encode(original);
        string desencriptada = Encriptador.Decode(encriptada);

        print(original + " -> " + desencriptada + " -> " + encriptada);
    }
}
