using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonAtrasResultado : MonoBehaviour
{
    [SerializeField] GameObject botonAtras, botonOkey;

    private void Start()
    {
        switch (ScenesSelector.sceneBefore)
        {
            case Scenes.Historial:
                botonOkey.SetActive(false);
                botonAtras.SetActive(true);
                break;
            default:
                botonAtras.SetActive(false);
                botonOkey.SetActive(true);
                break;
        }
    }
}
