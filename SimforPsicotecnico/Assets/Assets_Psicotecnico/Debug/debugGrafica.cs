using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugGrafica : MonoBehaviour
{
    List<float> valuesHr = new List<float>();
    List<float> timesHr = new List<float>();
    List<float> timesEvents = new List<float>();
    List<float> valuesCL = new List<float>();
    List<float> timesCL = new List<float>();

    [SerializeField] Grafiquero graf;
    float timeStart;

    private void Start()
    {
        timeStart = Time.time;

        /*
        */
        valuesHr.Add(10);
        valuesHr.Add(70);
        valuesHr.Add(80);
        valuesHr.Add(60);
        valuesHr.Add(75);

        timesHr.Add(0);
        timesHr.Add(15);
        timesHr.Add(20);
        timesHr.Add(30);
        timesHr.Add(40);
        
        valuesCL.Add(30);
        valuesCL.Add(40);
        valuesCL.Add(30);
        valuesCL.Add(50);
        valuesCL.Add(20);

        timesCL.Add(0);
        timesCL.Add(15);
        timesCL.Add(20);
        timesCL.Add(30);
        timesCL.Add(40);

        timesEvents.Add(30);

        /*
        InvokeRepeating("TakeValueHR", 0, 5f);
        InvokeRepeating("TakeValue", 0, 0.2f);
        */
    }

    void TakeValueHR()
    {
        float hr = (float)HPManager.instance.heartRate;
        print(hr);
        if (hr > 40 && hr < 350)
        {
            valuesHr.Add(hr);
            timesHr.Add(Time.time - timeStart);
        }
    }    

    public void ShowGrafic()
    {
        graf.Draw(timesEvents, coloresGraficas.events);
        graf.Draw(valuesHr, timesHr, coloresGraficas.hearthRate);
        graf.Draw(valuesCL, timesCL, coloresGraficas.cognitiveLoad);
    }
}