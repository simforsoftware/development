using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AnalizadorColor : MonoBehaviour
{
    Image[] imagenes;
    List<Color> colores = new List<Color>();
    private void Start()
    {
        imagenes = GetComponentsInChildren<Image>();

        foreach (Image i in imagenes) colores.Add(i.color);

        string mesage = "";
        foreach(Color c in colores)
        {
            mesage += c.r.ToString("F2");
            mesage += '\t';
            mesage += c.g.ToString("F2");
            mesage += '\t';
            mesage += c.b.ToString("F2");
            mesage += '\t';
            mesage += (c.r + c.g + c.b).ToString("F2");
            mesage += '\n';
        }
        print(mesage);
    }
}
