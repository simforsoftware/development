using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonAtras : MonoBehaviour
{
    public static bool inMultisesion = false;
    private void Start()
    {
        if (inMultisesion)
        {
            gameObject.SetActive(false);
            return;
        }

        BotonEscenas escenas = GetComponent<BotonEscenas>();
        if (!escenas)
        {
            Debug.LogError("Falta el componente BotonEscenas en el objeto BotonAtras");
        }

        escenas.escenaObjetivo = ScenesSelector.sceneBefore;
    }
}
