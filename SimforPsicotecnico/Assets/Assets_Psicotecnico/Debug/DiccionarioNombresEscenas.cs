using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DiccionarioNombresEscenas
{
    public static Dictionary<int, string> escenasIntName = new Dictionary<int, string>
    {
        {(int)Scenes.Test1, "loc_0043" },
        {(int)Scenes.Test2, "loc_0046" },
        {(int)Scenes.Test3Alt, "loc_0049" },
        {(int)Scenes.Test4,"loc_0052" },
        {(int)Scenes.Test5, "loc_0055" },
        {(int)Scenes.Test6, "loc_0057" },
        {(int)Scenes.Test7, "loc_0058" },
        {(int)Scenes.Test8, "loc_0059" },
        {(int)Scenes.NuevaSimulacion1, "loc_0044" },
        {(int)Scenes.NuevaSimulacion2, "loc_0047" },
        {(int)Scenes.NuevaSimulacion3Alt, "loc_0050" },
        {(int)Scenes.NuevaSimulacion4, "loc_0053" },
        {(int)Scenes.NuevaSimulacion5Alt, "loc_0056" },
        {(int)Scenes.Daltonismo, "loc_0117" },
        {(int)Scenes.TestESenllen, "loc_0185" },
    };
    
}
