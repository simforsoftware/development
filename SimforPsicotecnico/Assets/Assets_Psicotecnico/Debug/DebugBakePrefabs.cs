using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugBakePrefabs : MonoBehaviour
{
    [SerializeField] GameObject prefabBaked;

    bool instantiate = false;

    private void Update()
    {
        if (!instantiate && Input.GetKeyDown(KeyCode.Space))
        {
            GameObject g  = Instantiate(prefabBaked);
            instantiate = true;
        }
    }
}
