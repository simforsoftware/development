using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Search : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);        
    }
    private void Update()
    {
        SearchF();
    }
    private void SearchF()
    {
        AudioSource[] audios = FindObjectsOfType<AudioSource>();
        foreach (AudioSource audio in audios) print(audio.gameObject.name + ": " + audio.clip.name);
        print(audios.Length);
    }
}
