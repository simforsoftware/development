using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class ScreenShoter : MonoBehaviour
{
    public void ScreenShot()
    {
        if (!Directory.Exists(Application.dataPath + "/Screenshots")) Directory.CreateDirectory(Application.dataPath + "/Screenshots");

        string filename = string.Format(Application.dataPath + "/Screenshots/" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");

        ScreenCapture.CaptureScreenshot(filename);
    }
}
