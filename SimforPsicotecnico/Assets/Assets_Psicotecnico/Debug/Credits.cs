using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    [SerializeField] GameObject text, titleText;


    private void Start()
    {
        string creditosBrutos = "";
        string pathName = Path.Combine(Application.streamingAssetsPath, "creditos");
        FileStream loc = File.Open(pathName, FileMode.Open, FileAccess.Read);
        StreamReader sr = new StreamReader(loc);
        creditosBrutos = sr.ReadToEnd();
        sr.Close();
        loc.Close();


        string[] creditosSemi = creditosBrutos.Split('\n');

        foreach(string s in creditosSemi)
        {
            string[] creditosSemi2 = s.Split(':');

            GameObject g = Instantiate(titleText, transform);
            g.GetComponent<Text>().text = creditosSemi2[0];

            foreach(string ss in creditosSemi2[1].Split(','))
            {
                GameObject gg = Instantiate(text, transform);
                gg.GetComponent<Text>().text = ss;
            }
        }
    }
}
